<?php

namespace Rizzen\MainBundle\Controller;

use Rizzen\MainBundle\Entity\BugReport;
use Rizzen\MainBundle\Form\Type\BugReportType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class BugController extends Controller
{
    /**
     * Report a bug
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function reportAction(Request $request)
    {
        $bug = new BugReport();
        $form = $this->createForm(BugReportType::class, $bug);

        $form->handleRequest($request);
        if ($form->isValid()) {

            $bug->setCreator($this->getUser());
            $em = $this->getDoctrine()->getManager();

            $em->persist($bug);
            $em->flush();
            $this->addFlash('success', 'Bug successfully reported! Thank you for helping us smash all the bugs left in Grost!');

            return $this->redirectToRoute('rizzen_main_homepage');
        }

        return $this->render('RizzenMainBundle:Bug:report.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * List the bugs reported
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listAction()
    {
        $bugs = $this->getDoctrine()->getRepository('RizzenMainBundle:BugReport')->getBugs();

        return $this->render('RizzenMainBundle:Bug:list.html.twig', [
            'bugs' => $bugs
        ]);
    }
}
