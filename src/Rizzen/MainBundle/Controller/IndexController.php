<?php

namespace Rizzen\MainBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class IndexController extends Controller
{
    /**
     * The main page of grost
     * Displays the landing page if the user isn't logged in
     * Displays the user dashboard if the user is logged in
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        // If the user isn't logged in, display the landing page
        if (!$this->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            return $this->render('RizzenMainBundle:Home:index.html.twig');
        }

        $registrations = $this->getDoctrine()->getRepository('RizzenCalendarBundle:Registration')->getUpcomingRegisteredEvents($this->getUser());
        $recentlyVisited = $this->get('dashboard.recently.visited')->getGuildsThreadsRecentlyVisited($this->getUser());

        return $this->render('RizzenMainBundle::dashboard.html.twig', [
                'registrations' => $registrations,
                'recentlyVisited' => $recentlyVisited
            ]);
    }

    /**
     * List the patch notes
     *
     * @param int $page
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function updatesAction($page)
    {
        $updates = $this->getDoctrine()->getRepository('RizzenMainBundle:Update')->getUpdates();

        return $this->render('RizzenMainBundle:Footer:updates.html.twig', [
            'updates' => $this->get('knp_paginator')->paginate($updates, $page, 10),
        ]);
    }

    public function contactAction()
    {
        return $this->render('RizzenMainBundle:Footer:contact.html.twig');
    }

    public function termsAction()
    {
        return $this->render('RizzenMainBundle:Footer:terms.html.twig');
    }

    public function privacyAction()
    {
        return $this->render('RizzenMainBundle:Footer:privacy.html.twig');
    }

    public function faqAction()
    {
        return $this->render('RizzenMainBundle:Footer:faq.html.twig');
    }

    public function aboutAction()
    {
        return $this->render('RizzenMainBundle:Footer:about.html.twig');
    }

    public function sitemapAction()
    {
        return $this->render('RizzenMainBundle::sitemap.xml.twig');
    }
}
