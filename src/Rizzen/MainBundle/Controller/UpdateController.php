<?php

namespace Rizzen\MainBundle\Controller;

use Rizzen\MainBundle\Entity\Update;
use Rizzen\MainBundle\Form\Type\UpdateType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class UpdateController extends Controller
{
    /**
     * Write a new patch note
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function writeAction(Request $request)
    {
        $update = new Update();
        $form = $this->createForm(UpdateType::class, $update);

        $form->handleRequest($request);
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $em->persist($update);
            $em->flush();
            $this->addFlash('success', 'Patch notes created!');

            return $this->redirectToRoute('rizzen_admin_index');
        }

        return $this->render('RizzenMainBundle:Update:write.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * Edit an already written patch notes
     *
     * @param $updateId
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editAction($updateId, Request $request)
    {
        $update = $this->getDoctrine()->getRepository('RizzenMainBundle:Update')->find($updateId);
        $form = $this->createForm(UpdateType::class, $update);

        $form->handleRequest($request);
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $em->flush();
            $this->addFlash('success', 'Patch notes edited!');

            return $this->redirectToRoute('rizzen_admin_index');
        }

        return $this->render('RizzenMainBundle:Update:edit.html.twig', [
            'form' => $form->createView(),
            'update' => $update
        ]);
    }

}
