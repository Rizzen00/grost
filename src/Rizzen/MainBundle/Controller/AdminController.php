<?php

namespace Rizzen\MainBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class AdminController extends Controller
{
    /**
     * The admin dashboard page
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        $doctrine = $this->getDoctrine();

        $userCount = $doctrine->getRepository('RizzenUserBundle:User')->getUsersCount();
        $latestUser = $doctrine->getRepository('RizzenUserBundle:User')->getLatestUser();

        $guildCount = $doctrine->getRepository('RizzenGuildBundle:Guild')->getGuildsCount();
        $latestGuild = $doctrine->getRepository('RizzenGuildBundle:Guild')->getLatestGuild();

        return $this->render('RizzenMainBundle:Admin:index.html.twig', [
            'userCount' => $userCount,
            'latestUser' => $latestUser,
            'guildCount' => $guildCount,
            'latestGuild' => $latestGuild
        ]);
    }

    public function serverAction()
    {
        return $this->render('RizzenMainBundle:Admin:server.html.twig');
    }


}
