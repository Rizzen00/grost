<?php


namespace Rizzen\MainBundle;


/**
 * List the constants used to describe a bug report
 *
 * Class BugReport
 * @package Rizzen\MainBundle
 */
class BugReport
{
    /**
     * The bug has just been reported
     */
    const STATUS_NEW = 'new';

    /**
     * The bug has been confirmed and is currently being fixed
     */
    const STATUS_IN_PROGRESS = 'in_progress';

    /**
     * The bug has been fixed and is ready to be pushed to the production branch
     */
    const STATUS_FIXED = 'fixed';

    /**
     * The bug won't be fixed
     */
    const STATUS_WONT_FIX = 'wont_fix';

    /**
     * The bug is impossible to reproduce or incomplete
     */
    const STATUS_INCOMPLETE = 'incomplete';


    /**
     *  The severity of the bug is unknown, the bug has probably just been reported
     */
    const SEVERITY_UNKNOWN = 'unknown';

    /**
     *  The bug is harmless and in the low priority list
     */
    const SEVERITY_LOW = 'low';

    /**
     * The bug is mostly harmless
     */
    const SEVERITY_MEDIUM = 'medium';

    /**
     *  The bug is preventing the use of certain functionality
     */
    const SEVERITY_HIGH = 'high';

    /**
     *  The bug is to be fixed ASAP
     */
    const SEVERITY_CRITICAL = 'critical';


    /**
     * The bug type is unknown
     */
    const TYPE_UNKNOWN = 'unknown';

    /**
     * This is a visual bug
     */
    const TYPE_VISUAL = 'visual';

    /**
     * This is a functionality bug
     */
    const TYPE_FUNCTIONALITY = 'functionality';

    /**
     * There is a typo somewhere in Grost
     */
    const TYPE_TYPO = 'type';

    /**
     * Unexpected behavior in the site
     */
    const TYPE_UNEXPECTED = 'unexpected';

    /**
     *  Security bug
     */
    const TYPE_SECURITY = 'security';

}
