<?php

namespace Rizzen\MainBundle\Services\Dashboard;


use Predis\Client;
use Rizzen\UserBundle\Entity\User;

class RecentlyVisited
{

    private $redis;

    function __construct(Client $redis)
    {
        $this->redis = $redis;
    }

    /**
     * Fetch the 10 latest thread and guilds the user has seen
     *
     * @param User $user        Current user
     * @return array            Contains the 10 latest guilds and threads the user has seen
     */
    public function getGuildsThreadsRecentlyVisited(User $user)
    {
        $threads = $guilds = [];

        $key = 'visitedGuilds-u' . $user->getId();
        $lastVisitedGuilds = $this->redis->lrange($key, 0, -1);
        foreach ($lastVisitedGuilds as $guild) {
            $guilds[] = json_decode($guild);
        }

        $key = 'visitedThreads-u' . $user->getId();
        $lastVisitedThreads = $this->redis->lrange($key, 0, -1);
        foreach ($lastVisitedThreads as $thread) {
            $threads[] = json_decode($thread);
        }

        return [
            'guilds' => $guilds,
            'threads' => $threads
        ];
    }

} 
