<?php

namespace Rizzen\MainBundle\Services;


interface PurifierInterface {

    /**
     * Purify a raw HTML string to make it safe to put in a view
     *
     * @param String $type
     * @param String $input
     * @return String
     */
    public function purify($type, $input);

} 
