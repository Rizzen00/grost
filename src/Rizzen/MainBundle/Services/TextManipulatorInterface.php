<?php

namespace Rizzen\MainBundle\Services;


interface TextManipulatorInterface {

    /**
     * Will add rel="nofollow" to every link that isn't pointing to this site
     *
     * @param string $input HTML to process
     * @return string
     */
    public function addNoFollow($input);

} 
