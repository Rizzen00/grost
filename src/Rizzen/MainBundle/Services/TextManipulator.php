<?php

namespace Rizzen\MainBundle\Services;


/**
 * Service used to manipulate text
 *
 * Class TextManipulator
 * @package Rizzen\MainBundle\Services
 */
class TextManipulator implements TextManipulatorInterface {

    private $domain;

    /**
     * @param string $domain Domain name of the site
     */
    function __construct($domain)
    {
        $this->domain = $domain;
    }

    /**
     * {@inheritdoc}
     */
    public function addNoFollow($input)
    {
        $dom = new \DOMDocument;

        // This is a hack that fix the DOMDocument UTF-8 encoding
        $dom->loadHTML('<?xml encoding="UTF-8">' . $input, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);

        // Remove the hack, we just needed it in the loadHTML method and don't want it in the output
        foreach ($dom->childNodes as $item) {
            if ($item->nodeType == XML_PI_NODE) {
                $dom->removeChild($item);
            }
        }

        $links = $dom->getElementsByTagName('a');

        foreach ($links as $link) {
            $rel = [];

            if ($link->hasAttribute('rel') && ($relAtt = $link->getAttribute('rel')) !== '') {
                $rel = preg_split('/\s+/', trim($relAtt));
            }

            // Get the content of the href, parse the URL, split it, keep the domain name and TLD and merge it
            $linkHref = parse_url($link->getAttribute('href'));
            $linkHref = explode('.', $linkHref['host']);
            $linkHref = implode('.', array_slice($linkHref, -2));

            if (in_array('nofollow', $rel) || $linkHref == $this->domain) {
                continue;
            }

            $rel[] = 'nofollow';
            $link->setAttribute('rel', implode(' ', $rel));
        }

        return $dom->saveHTML();

    }

} 
