<?php


namespace Rizzen\MainBundle\Services\CommentPoster;

use Doctrine\ORM\EntityManager;
use Rizzen\MainBundle\Services\Purifier;
use Rizzen\SocketBundle\Services\SocketProcessor;
use Rizzen\UserBundle\Entity\User;
use Symfony\Bridge\Twig\TwigEngine;
use Rizzen\BlogBundle\Entity\PostComment;
use Rizzen\CalendarBundle\Entity\EventComment;

/**
 * Class Poster
 * @author Bryan Haag <misios11@hotmail.fr>
 * @package Rizzen\MainBundle\Services\CommentPoster
 */
class Poster implements PosterInterface {

    private $target;
    private $replyEntity;

    /** @var PostComment|EventComment $comment */
    private $comment;
    private $creator;
    private $websocketTopic;
    private $template;

    private $em;
    private $purifier;
    private $socketProcessor;

    /**
     * @param EntityManager $em
     * @param Purifier $purifier
     * @param SocketProcessor $socketProcessor
     * @param TwigEngine $templating
     */
    public function __construct(EntityManager $em, Purifier $purifier, SocketProcessor $socketProcessor, TwigEngine $templating)
    {
        $this->em = $em;
        $this->purifier = $purifier;
        $this->socketProcessor = $socketProcessor;
        $this->templating = $templating;
    }

    /**
     * {@inheritdoc}
     */
    public function post()
    {
        $this->comment->setSender($this->creator);

        $this->comment->setResponse(false);
        $subtopic = 'newMessage';
        $templateParam['isResponse'] = false;

        // Check if the comment is a reply to another comment
        if ($this->comment->getResponseTarget() !== null) {

            $responseTarget = $this->em->getRepository($this->replyEntity)->find(intval($this->comment->getResponseTarget()));

            if ($this->target === null) {
                throw new \Exception("Comment not found");
            }

            $this->comment->setResponse(true);
            $subtopic = 'newMessageResponse';
            $templateParam['isResponse'] = true;
            $responseTarget->addResponse($this->comment);
            $this->comment->setParent($responseTarget);
            $this->em->persist($responseTarget);
        }

        $content = $this->purifier->purify('comment', $this->comment->getContent());

        // Remove the <br> at the end of the message
        $content = preg_replace('/\s*(?:<br\s*\/?>\s*)*$/i', '', $content);
        $content = str_ireplace('<p>','',$content);
        $content = str_ireplace('</p>','',$content);


        $this->comment->setTarget($this->target);
        $this->comment->setContent($content);

        $this->em->persist($this->comment);
        $this->em->flush();

        $additionalInfo = null;
        if ($this->comment->isResponse() === true) {
            $additionalInfo = $this->comment->getParent()->getId();
        }

        $templateParam['HTML'] = $this->templating->render($this->template, [
                'comment' => $this->comment,
        ]);

        $this->socketProcessor
            ->addTopic($this->websocketTopic)
            ->setSubtopic($subtopic)
            ->useTemplate('raw', $templateParam)
            ->setAdditionalInfo($additionalInfo)
            ->broadcast();
    }

    /**
     * {@inheritdoc}
     */
    public function setTarget($target)
    {
        $this->target = $target;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setReplyEntity($entity)
    {
        $this->replyEntity = $entity;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setCreator(User $creator)
    {
        $this->creator = $creator;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setWebsocketTopic($topic)
    {
        $this->websocketTopic = $topic;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setTemplate($template)
    {
        $this->template = $template;

        return $this;
    }
}
