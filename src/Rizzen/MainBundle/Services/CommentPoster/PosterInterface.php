<?php

namespace Rizzen\MainBundle\Services\CommentPoster;

use Rizzen\UserBundle\Entity\User;

interface PosterInterface {

    /**
     * Post the comment once all the parameters have been set, will broadcast the comment in the corresponding websocket topic once the comment have been saved
     */
    public function post();

    /**
     * Entity to which we will attach the comment to
     *
     * @param mixed $target
     */
    public function setTarget($target);

    /**
     * String of the entity of the comment we are going to reply to if it is a response
     *
     * @param mixed $entity
     */
    public function setReplyEntity($entity);

    /**
     * Direct output of the form we're getting from the client
     *
     * @param $comment
     */
    public function setComment($comment);

    /**
     * The user that posted the comment
     *
     * @param User $creator
     */
    public function setCreator(User $creator);

    /**
     * Websocket topic in which we will broadcast the comment once it's saved
     *
     * @param String $topic
     */
    public function setWebsocketTopic($topic);

    /**
     * Template to generate that will be sent to the clients via websocket
     *
     * @param String $template
     */
    public function setTemplate($template);

} 
