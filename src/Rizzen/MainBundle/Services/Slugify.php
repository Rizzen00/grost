<?php

namespace Rizzen\MainBundle\Services;

class Slugify implements SlugifyInterface
{
    /**
     * {@inheritdoc}
     */
    public function getSluged($input)
    {
        return preg_replace("/[\/_|+ -]+/",'-',strtolower(trim(preg_replace("/[^a-zA-Z0-9\/_|+ -]/",'',trim(iconv('UTF-8','ASCII//TRANSLIT',$input))),'-')));
    }

    /**
     * {@inheritdoc}
     */
    public function getCanonicalized($input)
    {
        return null === $input ? null : mb_convert_case($input, MB_CASE_LOWER, mb_detect_encoding($input));
    }
}
