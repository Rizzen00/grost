<?php

namespace Rizzen\MainBundle\Services;


interface SlugifyInterface {

    /**
     * Transform a string into a url friendly slug
     *
     * @param String $input
     * @return String
     */
    public function getSluged($input);

    /**
     * @param String $input
     * @return String
     */
    public function getCanonicalized($input);

} 
