<?php

namespace Rizzen\MainBundle\Services;


class Purifier implements PurifierInterface
{

    private $config;

    public function __construct() {
        $this->config = \HTMLPurifier_Config::createDefault();
        $this->config->autoFinalize = false;
    }

    /**
     * {@inheritdoc}
     */
    public function purify($type, $input)
    {
        $this->applyPurificationOptions($type);
        $purifier = new \HTMLPurifier($this->config);
        return $purifier->purify($input);
    }

    private function applyPurificationOptions($type)
    {
        if ($type === 'bio' || $type === 'forumPost' || $type === 'default') {
            $this->config->set('HTML.Allowed', 'p,u,br,b,a[href],img[src],img[style],img[alt],i,h1,h2,h3,h4,h5,h6,ul,ol,li,strong,em,blockquote');
        } elseif ($type === 'comment') {
            $this->config->set('HTML.Allowed', 'p,u,b,a[href],i,strong,em,br,');
        }
    }

    /**
     * Purify the custom CSS for the guilds
     *
     * @param string $input The CSS
     * @return string
     */
    public function purifyCSS($input)
    {

        $this->config->set('Filter.ExtractStyleBlocks', true);
        $this->config->set('CSS.AllowTricky', true);
        $this->config->set('CSS.Proprietary', true);
        $this->config->set('CSS.AllowImportant', true);
        $this->config->set('CSS.Trusted', true);

        $this->addAllowedCSS();

        $purifier = new \HTMLPurifier($this->config);

        // wrap the CSS in style tags so HTMLPurifier can process it
        $purifier->purify('<style>' . $input . '</style>');

        $output = $purifier->context->get('StyleBlocks');

        $css = $output[0];

        return $css;
    }

    private function addAllowedCSS()
    {
        $cssDefinition = $this->config->getDefinition('CSS');

        $borderRadius =
        $attributes['border-top-left-radius'] =
        $attributes['border-top-right-radius'] =
        $attributes['border-bottom-left-radius'] =
        $attributes['border-bottom-right-radius'] =
            new \HTMLPurifier_AttrDef_CSS_Composite([
                new \HTMLPurifier_AttrDef_CSS_Length('0'),
                new \HTMLPurifier_AttrDef_CSS_Percentage(true)
                ]);

        $attributes['border-radius'] = new \HTMLPurifier_AttrDef_CSS_Multiple($borderRadius);

        $allowImportant = $this->config->get('CSS.AllowImportant');
        foreach ($attributes as $key => $attribute) {
            $cssDefinition->info[$key] = new \HTMLPurifier_AttrDef_CSS_ImportantDecorator($attribute, $allowImportant);
        }
    }

}
