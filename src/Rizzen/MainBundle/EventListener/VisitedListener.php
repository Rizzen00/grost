<?php

namespace Rizzen\MainBundle\EventListener;


use Doctrine\ORM\EntityManager;
use Predis\Client;
use Symfony\Component\HttpKernel\Event\PostResponseEvent;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;

class VisitedListener
{
    private $em;
    private $storage;
    private $checker;
    private $redis;

    function __construct(EntityManager $em, TokenStorage $storage, AuthorizationChecker $checker, Client $redis)
    {
        $this->em = $em;
        $this->storage = $storage;
        $this->checker = $checker;
        $this->redis = $redis;
    }

    public function updateVisitedList(PostResponseEvent $event)
    {
        /** @var Request $request */
        $request = $event->getRequest();
        if ($request->isXmlHttpRequest() ||
            $request->isMethod('POST') ||
            !($this->storage->getToken() && $this->checker->isGranted('IS_AUTHENTICATED_REMEMBERED')) ||
            $event->getResponse()->getStatusCode() !== 200) {
            return;
        }

        $route = $request->get('_route');

        // The 6 latest guilds the user has seen
        if ($route === 'rizzen_guild_see' ||
            $route === 'rizzen_forum_see' ||
            $route === 'rizzen_forum_subcat' ||
            $route === 'rizzen_event_list' ||
            $route === 'rizzen_event_see' ||
            $route === 'rizzen_calendar_see') {

            $routeParam = $request->attributes->get('_route_params');

            $guild = $this->em->getRepository('RizzenGuildBundle:Guild')->findOneBySlug($routeParam['subdomain']);

            if ($guild === null) {
                return;
            }

            $key = 'visitedGuilds-u' . $this->storage->getToken()->getUser()->getId();
            $data = [
                'slug' => $guild->getSlug(),
                'title' => $guild->getTitle(),
            ];

            $oldData = $this->redis->lrange($key, 0, -1);

            /*
             * Check if the guild is already in the list
             * */
            $guildAlreadyInList = false;
            $guildToDelete = null;
            foreach ($oldData as $guildData) {
                $decodedData = json_decode($guildData, true);

                if ($decodedData['slug'] === $data['slug']) {

                    $guildAlreadyInList = true;
                    $guildToDelete = $guildData;

                    break;
                }
            }

            $data = json_encode($data);

            $pipe = $this->redis->pipeline();

            /*
             * If the current guild is in the list, remove it
             * */
            if ($guildAlreadyInList) {
                $pipe->lrem($key, 0, $guildToDelete);
            }

            $pipe->lpush($key, $data);
            $pipe->ltrim($key, 0, 8);

            $pipe->execute();
        }

        // The 6 latest threads the user has seen
        if ($route === 'rizzen_forum_thread') {

            $routeParam = $request->attributes->get('_route_params');

            $guild = $this->em->getRepository('RizzenGuildBundle:Guild')->findOneBySlug($routeParam['subdomain']);
            $thread = $this->em->getRepository('RizzenForumBundle:Thread')->findOneBySlug($routeParam['threadSlug']);

            if ($guild === null || $thread === null) {
                return;
            }

            $key = 'visitedThreads-u' . $this->storage->getToken()->getUser()->getId();
            $data = [
                'guildSlug' => $guild->getSlug(),
                'threadTitle' => $thread->getTitle(),
                'threadSlug' => $thread->getSlug(),
            ];


            $oldData = $this->redis->lrange($key, 0, -1);

            /*
             * Check if the thread is already in the list
             * */
            $threadAlreadyInList = false;
            $threadToDelete = null;
            foreach ($oldData as $threadData) {
                $decodedData = json_decode($threadData, true);

                if ($decodedData['threadSlug'] === $data['threadSlug']) {

                    $threadAlreadyInList = true;
                    $threadToDelete = $threadData;

                    break;
                }
            }

            $data = json_encode($data);
            $pipe = $this->redis->pipeline();

            /*
             * If the current thread is in the list, remove it
             * */
            if ($threadAlreadyInList) {
                $pipe->lrem($key, 0, $threadToDelete);
            }

            $pipe->lpush($key, $data);
            $pipe->ltrim($key, 0, 8);

            $pipe->execute();

        }
    }
}
