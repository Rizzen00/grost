<?php

namespace Rizzen\MainBundle\Form\Type;

use Rizzen\MainBundle\BugReport;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class BugReportType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class)
            ->add('content', TextareaType::class)
            ->add('type', ChoiceType::class, [
                'choices'   => [
                    BugReport::TYPE_UNKNOWN         => 'Unknown',
                    BugReport::TYPE_FUNCTIONALITY   => 'Functionality',
                    BugReport::TYPE_VISUAL          => 'Visual',
                    BugReport::TYPE_SECURITY        => 'Security',
                    BugReport::TYPE_TYPO            => 'Typo',
                    BugReport::TYPE_UNEXPECTED      => 'Unexpected'
                ],
                    'multiple' => false,
                    'expanded' =>false
                ]
            )
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'Rizzen\MainBundle\Entity\BugReport'
        ]);
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'grost_bug_report';
    }
}
