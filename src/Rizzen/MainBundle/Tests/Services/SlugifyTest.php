<?php

namespace Rizzen\MainBundle\Tests\Services;

use Rizzen\MainBundle\Services\Slugify;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class SlugifyTest extends WebTestCase
{

    public function setUp()
    {
        static::$kernel = static::createKernel();
        static::$kernel->boot();
    }

    public function testGetSluged()
    {
        $slugify = new Slugify();

        $input = 'This is a string, 1 2 3 ! / \ / @, testing Rïzzen';
        $slugifyOutput = $slugify->getSluged($input);
        $this->assertEquals("this-is-a-string-1-2-3-testing-rizzen", $slugifyOutput);
    }

    public function testGetCanonicalized()
    {
        $slugify = new Slugify();

        $input = 'Rïzzen';
        $slugifyOutput = $slugify->getCanonicalized($input);
        $this->assertEquals("rïzzen", $slugifyOutput);
    }
}
