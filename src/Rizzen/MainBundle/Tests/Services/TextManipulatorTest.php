<?php

namespace Rizzen\MainBundle\Tests\Services;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Rizzen\MainBundle\Services\TextManipulator;

class TextManipulatorTest extends WebTestCase
{
    /** @var TextManipulator $textManipulator */
    private $textManipulator;

    public function setUp()
    {
        static::$kernel = static::createKernel();
        static::$kernel->boot();
        $this->textManipulator = static::$kernel->getContainer()->get('text.manipulator');
    }

    public function testAddUnread()
    {
        $input = '<p><a href="http://www.google.com"></a></p>';
        $result = $this->textManipulator->addNoFollow($input);

        $this->assertSame(trim($result), '<p><a href="http://www.google.com" rel="nofollow"></a></p>');
    }
}
