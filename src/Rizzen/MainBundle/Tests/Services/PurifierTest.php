<?php

namespace Rizzen\MainBundle\Tests\Services;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Rizzen\MainBundle\Services\Purifier;

class PurifierTest extends WebTestCase
{
    /** @var Purifier $purifier */
    private $purifier;

    public function setUp()
    {
        static::$kernel = static::createKernel();
        static::$kernel->boot();
        $this->purifier = static::$kernel->getContainer()->get('html.purifier');
    }

    public function testForumInputPurifier()
    {
        $input = '<p><button></button><script></script><img src="/" alt="image" /><i></i><strong></strong><br /></p><h1></h1><h2></h2><h3></h3><h4></h4><a href="/"></a>';
        $result = $this->purifier->purify('forumPost', $input);

        $this->assertSame($result, '<p><img src="/" alt="image" /><i></i><strong></strong><br /></p><h1></h1><h2></h2><h3></h3><h4></h4><a href="/"></a>');
    }

    public function testCommentInputPurifier()
    {
        $input = '<p><button></button><script></script><img src="/" alt="image"/></p>';
        $result = $this->purifier->purify('comment', $input);

        $this->assertSame($result, '<p></p>');
    }
}
