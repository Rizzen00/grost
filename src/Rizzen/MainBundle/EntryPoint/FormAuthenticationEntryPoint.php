<?php

namespace Rizzen\MainBundle\EntryPoint;


use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Http\EntryPoint\AuthenticationEntryPointInterface;


class FormAuthenticationEntryPoint implements AuthenticationEntryPointInterface {

    private $router;

    /**
     * @param Router $router
     */
    function __construct(Router $router)
    {
        $this->router = $router;
    }

    function start(Request $request, AuthenticationException $authException = null) {

        if ($request->get('_route') === 'rizzen_forum_join') {
            $routeParams = $request->attributes->get('_route_params');
            $request->getSession()->set('forumToJoin', $routeParams['subdomain']);
        }

        return new RedirectResponse($this->router->generate('fos_user_security_login'));

    }
}
