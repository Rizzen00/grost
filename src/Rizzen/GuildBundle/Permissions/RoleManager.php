<?php

namespace Rizzen\GuildBundle\Permissions;

use Doctrine\ORM\EntityManager;
use Rizzen\GuildBundle\Entity\Guild;
use Rizzen\GuildBundle\Entity\RoleAssignment;
use Rizzen\GuildBundle\Entity\Role;

/**
 * Class RoleManager
 * @author Bryan Haag <misios11@hotmail.fr>
 * @package Rizzen\GuildBundle\Services
 */
class RoleManager implements RoleManagerInterface
{
    private $em;
    private $permissionsManager;

    /**
     * @param EntityManager $em
     * @param PermissionsManager $permissionsManager
     */
    public function __construct(EntityManager $em, PermissionsManager $permissionsManager)
    {
        $this->em = $em;
        $this->permissionsManager = $permissionsManager;
    }

    /**
     * {@inheritdoc}
     */
    public function assignRole($userId, $roleId, Guild $guild)
    {
        $user = $this->em->getRepository('RizzenUserBundle:User')->find($userId);

        if ($user === null) {
            return [
                'hasBeenAssigned' => false,
                'message' => 'Error, the user was not found',
                'alert' => 'alert-danger'
            ];
        }

        $this->permissionsManager->clearLocalPermissions();
        $this->permissionsManager->setUser($user)->setGuild($guild);
        $this->permissionsManager->isForumMember();

        $roleAssignment = new RoleAssignment();

        if (!$this->permissionsManager->isGuildMember(false)) {
            $roleAssignment->setOnlyForumMember(true);
        }

        $role = $this->em->getRepository('RizzenGuildBundle:Role')->getRoleToAssign($roleId);
        if ($role === null || $role->getGuild() !== $guild) {
            return [
                'hasBeenAssigned' => false,
                'message' => 'Error, the role was not found',
                'alert' => 'alert-danger'
            ];
        }

        /** @var RoleAssignment[] $roleAssignments */
        $roleAssignments = $user->getRolesAssigned();
        foreach ($roleAssignments as $assignment) {
            if ($assignment->getRole() === $role) {
                return [
                    'hasBeenAssigned' => false,
                    'message' => 'Error, this role is already assigned to this user',
                    'alert' => 'alert-danger'
                ];
            }
        }

        $roleAssignment->setUser($user);
        $roleAssignment->setRole($role);
        $roleAssignment->setGuild($guild);
        $role->addAssignment($roleAssignment);
        $user->addRolesAssigned($roleAssignment);

        $this->em->persist($roleAssignment);
        $this->em->persist($role);
        $this->em->persist($user);
        $this->em->flush();

        $this->permissionsManager->removePermissionsFromCache();
        $this->permissionsManager->clearLocalPermissions();

        $permissionsJson = $this->permissionsManager->getJSONPermissions();

        return [
            'hasBeenAssigned' => true,
            'message' => 'The role has been successfully assigned to the user!',
            'alert' => 'alert-success',
            'permissions' => $permissionsJson
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function removeRole($userId, $roleId, Guild $guild)
    {
        $user = $this->em->getRepository('RizzenUserBundle:User')->find($userId);
        if ($user === null) {
            return [
                'hasBeenRemoved' => false,
                'message' => 'Error, the user was not found',
                'alert' => 'alert-danger'
            ];
        }

        $this->permissionsManager->clearLocalPermissions();
        $this->permissionsManager->setUser($user)->setGuild($guild);
        $this->permissionsManager->isForumMember();

        $role = $this->em->getRepository('RizzenGuildBundle:Role')->find($roleId);
        if ($role === null || $role->getGuild() !== $guild) {
            return ['hasBeenRemoved' => false,
                    'message' => 'Error, the role was not found',
                    'alert' => 'alert-danger'
            ];
        }

        foreach ($user->getRolesAssigned() as $assignment) {
            if ($assignment->getGuild() === $guild && $assignment->getRole() === $role) {

                if ($assignment->isRemovable() === true) {
                    $assignmentToRemove = $assignment;
                    break;
                } else {
                    return [
                        'hasBeenRemoved' => false,
                        'message' => 'Error, this role is not removable from this user.',
                        'alert' => 'alert-danger'
                    ];
                }
            }
        }

        if (!isset($assignmentToRemove)) {
            return [
                'hasBeenRemoved' => false,
                'message' => 'Error, the user does not have have this role, it might have been removed by someone else already.',
                'alert' => 'alert-danger'
            ];
        }

        $this->em->remove($assignmentToRemove);
        $this->em->flush();

        $this->permissionsManager->removePermissionsFromCache();
        $this->permissionsManager->clearLocalPermissions();

        $permissionsJson = $this->permissionsManager->getJSONPermissions();

        return [
            'hasBeenRemoved' => true,
            'message' => 'The role has been successfully removed from the user!',
            'alert' => 'alert-success',
            'permissions' => $permissionsJson
        ];
    }
}
