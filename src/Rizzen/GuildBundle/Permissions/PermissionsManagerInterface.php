<?php


namespace Rizzen\GuildBundle\Permissions;

use Rizzen\GuildBundle\Entity\Guild;
use Rizzen\UserBundle\Entity\User;
use Rizzen\GuildBundle\Entity\Role;
use Rizzen\GuildBundle\Exception\InsufficientGuildPermissionsException;

interface PermissionsManagerInterface
{
    /**
     * Set the guild where we want to check the permissions
     *
     * @param Guild $guild      The guild object
     * @return $this
     */
    public function setGuild(Guild $guild);

    /**
     * The bearer of the permissions
     *
     * @param User $user        The user object
     * @return $this
     */
    public function setUser(User $user = null);

    /**
     * Return the unserialized permissions
     *
     * @return Role
     */
    public function getPermissions();

    /**
     * Return the permissions in JSON
     *
     * @return string           The permissions in JSON
     */
    public function getJSONPermissions();

    /**
     * Remove the permissions from the cache
     *
     * @return void
     */
    public function removePermissionsFromCache();

    /**
     * Clear the local permissions, needed to fetch the permissions of another user
     *
     * @return void
     */
    public function clearLocalPermissions();

    /**
     * Set the permissions manually from a pre-existing role object
     *
     * @param Role $permissions     The permissions
     * @return void
     */
    public function setPermissions(Role $permissions);

    /**
     * Check if the user has at least one of the permissions, throw an exception if not
     *
     * @param array $permissionsToCheck     Permission(s) to check in PascalCase
     * @throws InsufficientGuildPermissionsException
     * @return void
     */
    public function must(array $permissionsToCheck);

    /**
     * Return if the user has at least one of the permissions
     *
     * @param array $permissionsToCheck     Permission(s) to check in PascalCase
     * @return bool
     */
    public function can(array $permissionsToCheck);

    /**
     * Check if the user in a member of the guild
     *
     * @param bool $throw       Throw an exception if not a guild member
     * @throws InsufficientGuildPermissionsException
     * @return void|bool
     */
    public function isGuildMember($throw = true);

    /**
     * Check if the user is a member of the forum of the guild
     *
     * @param bool $throw       Throw an exception if not a forum member
     * @throws InsufficientGuildPermissionsException
     * @return void|bool
     */
    public function isForumMember($throw = true);
}
