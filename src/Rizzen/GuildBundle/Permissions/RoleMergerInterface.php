<?php

namespace Rizzen\GuildBundle\Permissions;

use Rizzen\GuildBundle\Entity\Role;

interface RoleMergerInterface {

    /**
     * Get all the permission related methods inside the Permission entity.
     * It removes all the set method too.
     *
     * @param mixed $cleanRole
     * @return object
     */
    public function getRoleMethods(Role $cleanRole = null);

    /**
     * Merge all the roles the user have in a guild in a single Role object.
     *
     * @param array $permissions
     * @return Role
     */
    public function getMergedPermissions(array $permissions);

} 
