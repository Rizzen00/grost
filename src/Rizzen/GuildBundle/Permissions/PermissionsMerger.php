<?php

namespace Rizzen\GuildBundle\Permissions;

use Rizzen\GuildBundle\Entity\Role;
use Rizzen\GuildBundle\Entity\RoleAssignment;

/**
 * Class PermissionsMerger
 * @author Bryan Haag <mail@bryanhaag.fr>
 * @package Rizzen\GuildBundle\Services
 */
class PermissionsMerger implements RoleMergerInterface
{
    /**
     * {@inheritdoc}
     */
    public function getRoleMethods(Role $cleanRole = null)
    {
        if ($cleanRole === null) {
            $cleanRole = new Role();
        }

        // Get all the methods of the role object, and set the key and value to the same value
        $methods = get_class_methods($cleanRole);
        $methods = array_combine($methods, $methods);

        // Remove the non wanted methods from the array, so we don't loop on it later
        unset(
            $methods['getId'],
            $methods['setTitle'],
            $methods['getTitle'],
            $methods['setSlug'],
            $methods['getSlug'],
            $methods['setGuild'],
            $methods['getGuild'],
            $methods['addRole'],
            $methods['getRolesAttributed'],
            $methods['setRoleEditable'],
            $methods['getRoleEditable'],
            $methods['addAssignment'],
            $methods['removeAssignment'],
            $methods['getAssignments'],
            $methods['__construct']
        );

        // Remove ALL the set* methods, just keep the get* ones
        $i = 1;
        foreach ($methods as $key => $method) {
            if ($i % 2 !== 0) {
                unset($methods[$key]);
            }
            $i++;
        }

        return $methods;
    }

    /**
     * {@inheritdoc}
     */
    public function getMergedPermissions(array $permissions)
    {
        // We'll apply all the permissions to a new clean role
        $mergedRole = new Role();
        $methods = $this->getRoleMethods($mergedRole);

        /** @var RoleAssignment $permission */
        foreach ($permissions as $permission) {

            /** @var Role $role */
            $role = $permission->getRole();
            $mergedRole->addRole($role->getId());

            // check all the permissions in the role
            foreach ($methods as $key => $method) {

                if ($role->$key() === true) {
                    $setMethod = str_replace('get', 'set', $key);
                    $mergedRole->$setMethod(true);
                }
            }
        }

        return $mergedRole;
    }
}
