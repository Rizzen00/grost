<?php

namespace Rizzen\GuildBundle\Permissions;

use Rizzen\GuildBundle\Entity\Guild;
use Rizzen\GuildBundle\Entity\Role;
use Rizzen\UserBundle\Entity\User;

interface RoleCreatorInterface {

    /**
     * Create a new role
     *
     * @param Guild $guild
     * @param Role $role
     * @param boolean $editMode
     * @return boolean
     */
    public function createRole(Guild $guild, Role $role, $editMode);

    /**
     * Generate the base roles needed when creating a new guild
     *
     * @param Guild $guild
     * @param User $user
     */
    public function generateBaseRoles(Guild $guild, User $user);

}
