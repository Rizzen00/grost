<?php

namespace Rizzen\GuildBundle\Permissions;

use Doctrine\ORM\EntityManager;
use Rizzen\GuildBundle\Entity\Guild;
use Rizzen\GuildBundle\Entity\RoleAssignment;
use Rizzen\GuildBundle\Entity\Role;
use Rizzen\MainBundle\Services\Slugify;
use Rizzen\UserBundle\Entity\User;

/**
 * Class RoleCreator
 * @author Bryan Haag <misios11@hotmail.fr>
 * @package Rizzen\GuildBundle\Services
 */
class RoleCreator implements RoleCreatorInterface
{
    private $em;
    private $slugify;

    /**
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
        $this->slugify = new Slugify();
    }


    /**
     * {@inheritdoc}
     */
    public function createRole(Guild $guild, Role $role, $editMode = false)
    {

        // If one of the permission can only be accessed by being able to see the admin panel, set CanSeeAdminPanel to true
        if ($role->getCanEditForum() ||
            $role->getCanAddToGuild() ||
            $role->getCanBan() ||
            $role->getCanModifyRank() ||
            $role->getCanCreateRole() ||
            $role->getCanCreateRole() ||
            $role->getCanWriteBlogPost() ||
            $role->getCanEditGuildStyle() ||
            $role->getCanEditGuildLogo()) {

            $role->setCanSeeAdminPanel(true);
        }

        $reservedTitles = ['administrator', 'admin', 'super-admin'];

        if (in_array(strtolower($role->getTitle()), $reservedTitles)) {
            return false;
        }

        if (!$editMode) {
            $role->setGuild($guild);
        }

        $this->em->persist($role);
        $this->em->flush();

        if (!$editMode) {
            $role->setSlug($this->slugify->getSluged($role->getTitle().'-'.$role->getId()));
            $guild->addRole($role);

            $this->em->persist($role);
            $this->em->persist($guild);
            $this->em->flush();
        }

        return true;

    }

    /**
     * {@inheritdoc}
     */
    public function generateBaseRoles(Guild $guild, User $user)
    {
        $roleAdmin = new Role();
        $roleModerator = new Role();
        $roleWriter = new Role();

        $merger = new PermissionsMerger();

        foreach ($merger->getRoleMethods($roleAdmin) as $key => $method) {
            $setMethod = str_replace('get', 'set', $key);
            $roleAdmin->$setMethod(true);
        }

        $roleAdmin->setTitle('Administrator');
        $roleAdmin->setRoleEditable(false);
        $roleAdmin->setGuild($guild);

        $roleModerator->setTitle('Moderator');
        $roleModerator->setCanEditForumPost(true);
        $roleModerator->setCanDeleteForumPost(true);
        $roleModerator->setCanEditForumThread(true);
        $roleModerator->setCanDeleteForumThread(true);
        $roleModerator->setCanPinThread(true);
        $roleModerator->setRoleEditable(false);
        $roleModerator->setGuild($guild);

        $roleWriter->setTitle('Writer');
        $roleWriter->setCanWriteBlogPost(true);
        $roleWriter->setCanEditBlogPost(true);
        $roleWriter->setRoleEditable(false);
        $roleWriter->setGuild($guild);

        $this->em->persist($roleAdmin);
        $this->em->persist($roleModerator);
        $this->em->persist($roleWriter);
        $this->em->flush();

        $roleAdmin->setSlug($this->slugify->getSluged($roleAdmin->getTitle().'-'.$roleAdmin->getId()));
        $roleModerator->setSlug($this->slugify->getSluged($roleModerator->getTitle().'-'.$roleModerator->getId()));
        $roleWriter->setSlug($this->slugify->getSluged($roleWriter->getTitle().'-'.$roleWriter->getId()));

        $roleAssignment = new RoleAssignment();
        $roleAssignment->setUser($user);
        $roleAssignment->setRole($roleAdmin);
        $roleAssignment->setRemovable(false);
        $roleAssignment->setGuild($guild);
        $roleAdmin->addAssignment($roleAssignment);
        $user->addRolesAssigned($roleAssignment);

        $this->em->persist($roleAssignment);
        $this->em->persist($user);

        $this->em->flush();
    }
}
