<?php

namespace Rizzen\GuildBundle\Permissions;

use Doctrine\ORM\EntityManager;
use JMS\Serializer\Serializer;
use Predis\Client;
use Rizzen\GuildBundle\Entity\Guild;
use Rizzen\GuildBundle\Exception\InsufficientGuildPermissionsException;
use Rizzen\UserBundle\Entity\User;
use Rizzen\GuildBundle\Entity\Role;

/**
 * Class PermissionsManager
 * @package Rizzen\GuildBundle\Permissions
 */
class PermissionsManager implements PermissionsManagerInterface
{
    /**
     * @var Guild $guild
     */
    private $guild;

    /**
     * @var User $user
     */
    private $user;

    /**
     * @var Role $permissions
     */
    private $permissions;
    private $serializedPermissions;

    private $em;
    private $redis;
    private $serializer;

    /**
     * @param EntityManager $em
     * @param Client $redis
     * @param Serializer $serializer
     */
    public function __construct(EntityManager $em, Client $redis, Serializer $serializer)
    {
        $this->em = $em;
        $this->redis = $redis;
        $this->serializer = $serializer;
    }

    /**
     * {@inheritdoc}
     */
    public function setGuild(Guild $guild)
    {
        $this->guild = $guild;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setUser(User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getPermissions()
    {
        if (empty($this->permissions)) {
            $this->fetchCachedPermissions();
        }

        return $this->permissions;
    }

    /**
     * {@inheritdoc}
     */
    public function getJSONPermissions()
    {
        if (empty($this->serializedPermissions)) {
            $this->fetchCachedPermissions();
        }

        return $this->serializedPermissions;
    }

    /**
     * {@inheritdoc}
     */
    public function removePermissionsFromCache()
    {
        $this->redis->del($this->getPermissionsKey());
    }

    /**
     * {@inheritdoc}
     */
    public function clearLocalPermissions()
    {
        $this->serializedPermissions = null;
        $this->permissions = null;
    }

    /**
     * {@inheritdoc}
     */
    public function setPermissions(Role $permissions)
    {
        $this->permissions = $permissions;
        $this->serializedPermissions = $this->serializer->serialize($this->permissions, 'json');

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function must(array $permissionsToCheck)
    {

        foreach ($permissionsToCheck as $permission) {

            if ($this->isPermissionTrue($permission)) {
                return;
            }

        }

        throw new InsufficientGuildPermissionsException();

    }

    /**
     * {@inheritdoc}
     */
    public function can(array $permissionsToCheck)
    {
        foreach ($permissionsToCheck as $permission) {

            if ($this->isPermissionTrue($permission)) {
                return true;
            }

        }

        return false;
    }

    /**
     * {@inheritdoc}
     */
    public function isGuildMember($throw = true)
    {
        $isGuildMember = $this->getPermissions()->isGuildMember();

        if ($throw === false) {
            return $isGuildMember;
        }

        if (!$isGuildMember) {
            throw new InsufficientGuildPermissionsException('You need to be a guild member to perform this action.');
        }
    }

    /**
     * {@inheritdoc}
     */
    public function isForumMember($throw = true)
    {
        $isForumMember = $this->getPermissions()->isForumMember();

        if ($throw === false) {
            return $isForumMember;
        }

        if (!$isForumMember) {
            throw new InsufficientGuildPermissionsException('You need to be a forum member to perform this action.');
        }
    }

    /**
     * Check if the permissions is true or false
     *
     * @param string $permission    Permissions to check
     * @return bool
     */
    private function isPermissionTrue($permission)
    {
        $permissionMethod = 'get'. $permission;

        if ($this->getPermissions()->$permissionMethod() === true) {
            return true;
        }

        return false;
    }

    /**
     * Fetch the cached permissions, if not it cache, generate them and put them in the cache.
     *
     * @return void
     */
    private function fetchCachedPermissions()
    {
        $this->serializedPermissions = $this->redis->get($this->getPermissionsKey());

        if ($this->serializedPermissions === null) {
            $this->mergePermissionsOfUser();
            $this->savePermissionsToCache();
            return;
        }

        $this->permissions = $this->serializer->deserialize($this->serializedPermissions, 'Rizzen\\GuildBundle\\Entity\\Role', 'json');
    }

    /**
     * Merge all the roles of the user in a single one
     *
     * @return void
     */
    private function mergePermissionsOfUser()
    {
        $permissions =
            !empty($this->user)
            ? $this->em->getRepository('RizzenGuildBundle:RoleAssignment')->getRolesOfUser($this->user, $this->guild)
            : [];

        $roleMerger = new PermissionsMerger();
        $permissions = $roleMerger->getMergedPermissions($permissions);


        if (empty($this->user)) {
            $this->permissions = $permissions;
            $this->serializedPermissions = $this->serializer->serialize($this->permissions, 'json');
            return;
        }

        $guildsOfUser = $this->user->getGuilds();
        $forumsOfUser = $this->user->getForums();

        if (!empty($guildsOfUser) && $guildsOfUser->contains($this->guild)) {
            $permissions
                ->setGuildMember(true)
                ->setForumMember(true);
        } else if (!empty($forumsOfUser) && $forumsOfUser->contains($this->guild->getForum())) {
            $permissions->setForumMember(true);
        }

        $userBans = $this->em->getRepository('RizzenGuildBundle:BannedUser')->findUserInBannedListOrNull($this->user, $this->guild);
        if (!empty($userBans)) {
            $permissions->setBannedFromForum($userBans->getBannedFromForum());
            $permissions->setBannedFromGuild($userBans->getBannedFromGuild());
        }

        $this->permissions = $permissions;
        $this->serializedPermissions = $this->serializer->serialize($this->permissions, 'json');
    }

    /**
     * Save the permissions to the cache
     *
     * @return void
     */
    private function savePermissionsToCache()
    {
        $this->redis->set($this->getPermissionsKey(), $this->serializedPermissions);
        $this->redis->expire($this->getPermissionsKey(), 86400);
    }

    /**
     * Get the key for redis
     *
     * @return string The permission key
     */
    private function getPermissionsKey()
    {
        if (empty($this->user)) {
            return 'perm-visitor';
        }

        return 'perm-u' . $this->user->getId() . '-g' . $this->guild->getId();
    }
}
