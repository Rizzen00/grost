<?php

namespace Rizzen\GuildBundle\Permissions;

use Rizzen\GuildBundle\Entity\Guild;

interface RoleManagerInterface {

    /**
     * Assign a role to someone
     *
     * @param int $userId
     * @param int $roleId
     * @param Guild $guild
     * @return array
     */
    public function assignRole($userId, $roleId, Guild $guild);

    /**
     * Remove a role from someone
     *
     * @param int $userId
     * @param int $roleId
     * @param Guild $guild
     * @return array
     */
    public function removeRole($userId, $roleId, Guild $guild);
} 
