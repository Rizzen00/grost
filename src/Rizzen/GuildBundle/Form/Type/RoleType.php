<?php

namespace Rizzen\GuildBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

class RoleType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, ['required'  => true, 'attr' => ['autocomplete' => 'off']])
            ->add('canEditForumPost', CheckboxType::class, ['required' => false])
            ->add('canDeleteForumPost', CheckboxType::class, ['required' => false])
            ->add('canEditForumThread', CheckboxType::class, ['required' => false])
            ->add('canDeleteForumThread', CheckboxType::class, ['required' => false])
            ->add('canPinThread', CheckboxType::class, ['required' => false])
            ->add('canWriteBlogPost', CheckboxType::class, ['required' => false])
            ->add('canEditBlogPost', CheckboxType::class, ['required' => false])
            ->add('canAddToGuild', CheckboxType::class, ['required' => false])
            ->add('canBan', CheckboxType::class, ['required' => false])
            ->add('canModifyRank', CheckboxType::class, ['required' => false])
            ->add('canCreateEvent', CheckboxType::class, ['required' => false])
            ->add('canManageEvent', CheckboxType::class, ['required' => false])
            ->add('canSeeAdminPanel', CheckboxType::class, ['required' => false])
            ->add('canCreateRole', CheckboxType::class, ['required' => false])
            ->add('canEditForum', CheckboxType::class, ['required' => false])
            ->add('canEditGuildStyle', CheckboxType::class, ['required' => false])
            ->add('canEditGuildLogo', CheckboxType::class, ['required' => false])
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'Rizzen\GuildBundle\Entity\Role'
        ]);
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'rizzen_guildbundle_role';
    }
}
