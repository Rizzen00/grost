<?php

namespace Rizzen\GuildBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * BannedUser
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Rizzen\GuildBundle\Entity\BannedUserRepository")
 */
class BannedUser
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var boolean
     *
     * @ORM\Column(name="bannedFromForum", type="boolean")
     */
    protected $bannedFromForum;

    /**
     * @var boolean
     *
     * @ORM\Column(name="bannedFromGuild", type="boolean")
     */
    protected $bannedFromGuild;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetimetz")
     */
    protected $date;

    /**
     * @ORM\ManyToOne(targetEntity="Rizzen\UserBundle\Entity\User")
     * @ORM\JoinColumn(nullable=false)
     */
    protected $user;

    /**
     * @ORM\ManyToOne(targetEntity="Rizzen\UserBundle\Entity\User")
     * @ORM\JoinColumn(nullable=true)
     */
    protected $forumBanIssuedBy;

    /**
     * @ORM\ManyToOne(targetEntity="Rizzen\UserBundle\Entity\User")
     * @ORM\JoinColumn(nullable=true)
     */
    protected $guildBanIssuedBy;

    /**
     * @ORM\ManyToOne(targetEntity="Guild", inversedBy="bannedUsers")
     */
    protected $guild;

    function __construct()
    {
        $this->date = new \DateTime('now');
        $this->bannedFromGuild = false;
        $this->bannedFromForum = false;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set bannedFromForum
     *
     * @param boolean $bannedFromForum
     * @return bannedUser
     */
    public function setBannedFromForum($bannedFromForum)
    {
        $this->bannedFromForum = $bannedFromForum;

        return $this;
    }

    /**
     * Get bannedFromForum
     *
     * @return boolean 
     */
    public function getBannedFromForum()
    {
        return $this->bannedFromForum;
    }

    /**
     * Set bannedFromGuild
     *
     * @param boolean $bannedFromGuild
     * @return BannedUser
     */
    public function setBannedFromGuild($bannedFromGuild)
    {
        $this->bannedFromGuild = $bannedFromGuild;

        return $this;
    }

    /**
     * Get bannedFromGuild
     *
     * @return boolean 
     */
    public function getBannedFromGuild()
    {
        return $this->bannedFromGuild;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return BannedUser
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set user
     *
     * @param \Rizzen\UserBundle\Entity\User $user
     * @return BannedUser
     */
    public function setUser(\Rizzen\UserBundle\Entity\User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Rizzen\UserBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set forumBanIssuedBy
     *
     * @param \Rizzen\UserBundle\Entity\User $forumBanIssuedBy
     * @return BannedUser
     */
    public function setForumBanIssuedBy(\Rizzen\UserBundle\Entity\User $forumBanIssuedBy = null)
    {
        $this->forumBanIssuedBy = $forumBanIssuedBy;

        return $this;
    }

    /**
     * Get forumBanIssuedBy
     *
     * @return \Rizzen\UserBundle\Entity\User 
     */
    public function getForumBanIssuedBy()
    {
        return $this->forumBanIssuedBy;
    }

    /**
     * Set guildBanIssuedBy
     *
     * @param \Rizzen\UserBundle\Entity\User $guildBanIssuedBy
     * @return BannedUser
     */
    public function setGuildBanIssuedBy(\Rizzen\UserBundle\Entity\User $guildBanIssuedBy = null)
    {
        $this->guildBanIssuedBy = $guildBanIssuedBy;

        return $this;
    }

    /**
     * Get guildBanIssuedBy
     *
     * @return \Rizzen\UserBundle\Entity\User 
     */
    public function getGuildBanIssuedBy()
    {
        return $this->guildBanIssuedBy;
    }

    /**
     * Set guild
     *
     * @param \Rizzen\GuildBundle\Entity\Guild $guild
     * @return BannedUser
     */
    public function setGuild(\Rizzen\GuildBundle\Entity\Guild $guild = null)
    {
        $this->guild = $guild;

        return $this;
    }

    /**
     * Get guild
     *
     * @return \Rizzen\GuildBundle\Entity\Guild 
     */
    public function getGuild()
    {
        return $this->guild;
    }
}
