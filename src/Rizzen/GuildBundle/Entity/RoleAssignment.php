<?php

namespace Rizzen\GuildBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * RoleAssignment
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Rizzen\GuildBundle\Entity\RoleAssignmentRepository")
 */
class RoleAssignment
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetimetz")
     */
    protected $date;

    /**
     * @var boolean
     *
     * @ORM\Column(name="isOnlyForummember", type="boolean")
     */
    protected $isOnlyForumMember;

    /**
     * @var boolean
     *
     * @ORM\Column(name="isRemovable", type="boolean")
     */
    protected $isRemovable;

    /**
     * @ORM\ManyToOne(targetEntity="Role", inversedBy="assignments")
     */
    protected $role;

    /**
     * @ORM\ManyToOne(targetEntity="Guild")
     */
    protected $guild;

    /**
     * @ORM\ManyToOne(targetEntity="Rizzen\UserBundle\Entity\User", inversedBy="rolesAssigned")
     */
    protected $user;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return RoleAssignment
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set role
     *
     * @param \Rizzen\GuildBundle\Entity\Role $role
     * @return RoleAssignment
     */
    public function setRole(\Rizzen\GuildBundle\Entity\Role $role = null)
    {
        $this->role = $role;

        return $this;
    }

    /**
     * Get role
     *
     * @return \Rizzen\GuildBundle\Entity\Role
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * Set user
     *
     * @param \Rizzen\UserBundle\Entity\User $user
     * @return RoleAssignment
     */
    public function setUser(\Rizzen\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Rizzen\UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->date = new \DateTime('now');
        $this->isOnlyForumMember = false;
        $this->isRemovable = true;
    }

    /**
     * Set isOnlyForumMember
     *
     * @param boolean $isOnlyForumMember
     * @return RoleAssignment
     */
    public function setOnlyForumMember($isOnlyForumMember)
    {
        $this->isOnlyForumMember = $isOnlyForumMember;

        return $this;
    }

    /**
     * Get isOnlyForumMember
     *
     * @return boolean
     */
    public function isOnlyForumMember()
    {
        return $this->isOnlyForumMember;
    }

    /**
     * Set guild
     *
     * @param \Rizzen\GuildBundle\Entity\Guild $guild
     * @return RoleAssignment
     */
    public function setGuild(\Rizzen\GuildBundle\Entity\Guild $guild = null)
    {
        $this->guild = $guild;

        return $this;
    }

    /**
     * Get guild
     *
     * @return \Rizzen\GuildBundle\Entity\Guild
     */
    public function getGuild()
    {
        return $this->guild;
    }

    /**
     * Set isRemovable
     *
     * @param boolean $isRemovable
     * @return RoleAssignment
     */
    public function setRemovable($isRemovable)
    {
        $this->isRemovable = $isRemovable;

        return $this;
    }

    /**
     * Get isRemovable
     *
     * @return boolean 
     */
    public function isRemovable()
    {
        return $this->isRemovable;
    }
}
