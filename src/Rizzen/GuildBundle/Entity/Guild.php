<?php

namespace Rizzen\GuildBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Guild
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Rizzen\GuildBundle\Entity\GuildRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Guild
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=100)
     *
     * @Assert\NotBlank()
     * @Assert\Length(
     *      min = "3",
     *      max = "40"
     * )
     *
     */
    protected $title;

    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=255, unique=true)
     */
    protected $slug;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creationDate", type="datetimetz")
     */
    protected $creationDate;

    /**
     * @ORM\ManyToOne(targetEntity="Rizzen\UserBundle\Entity\User")
     * @ORM\JoinColumn(nullable=false)
     */
    protected $creator;

    /**
     * @var boolean
     *
     * @ORM\Column(name="open", type="boolean")
     */
    protected $open;

    /**
     * @var boolean
     *
     * @ORM\Column(name="customCSSEnabled", type="boolean")
     */
    protected $customCSSEnabled;

    /**
     * @var boolean
     *
     * @ORM\Column(name="customLogoEnabled", type="boolean")
     */
    protected $customLogoEnabled;

    /**
     * @var string
     *
     */
    private $webPath;

    /**
     * @var string
     *
     * @ORM\Column(name="guildLogo", type="string", length=255, nullable=true)
     */
    private $guildLogo;

    /**
     * @Assert\Image(maxSize="512k")
     */
    public $file;

    protected $oldGuildLogo;

    /**
     * @var string
     *
     * @ORM\Column(name="customCSSFile", type="string", length=255, nullable=true)
     */
    protected $customCSSFile;

    /**
     * @ORM\OneToOne(targetEntity="Rizzen\CalendarBundle\Entity\Calendar", cascade={"persist"}, mappedBy="guild")
     */
    protected $calendar;

    /**
     * @ORM\OneToOne(targetEntity="Rizzen\ForumBundle\Entity\Forum", mappedBy="guild")
     */
    protected $forum;

    /**
     * @ORM\OneToOne(targetEntity="Rizzen\BlogBundle\Entity\Blog", mappedBy="guild")
     */
    protected $blog;

    /**
     * @ORM\ManyToMany(targetEntity="Rizzen\UserBundle\Entity\User", inversedBy="guilds")
     */
    protected $members;

    /**
     * @ORM\OneToMany(targetEntity="Role", mappedBy="guild", cascade={"remove"})
     */
    protected $roles;

    /**
     * @ORM\OneToMany(targetEntity="BannedUser", mappedBy="guild")
     */
    protected $bannedUsers;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set webPath
     *
     * @param string $webPath
     * @return Guild
     */
    public function setWebPath($webPath)
    {
        $this->webPath = $webPath;

        return $this;
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpload()
    {
        if (null !== $this->file) {

            list($this->width, $this->height) = getimagesize($this->file);
            $this->setGuildLogo(sha1(uniqid(mt_rand(), true)) . '_' . mt_rand(1000, 9999) . '.' . $this->file->guessExtension());

            $this->webPath = $this->getWebPath();
        }
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload()
    {
        if (null === $this->file) {
            return;
        }

        $this->file->move($this->getUploadRootDir(), $this->getGuildLogo());

        unset($this->file);

        // Delete any logo in the folder that isn't the one the user uploaded
        $folder = $this->getUploadRootDir();
        foreach (scandir($folder) as $fileName) {
            if ($fileName !== $this->getGuildLogo() && $fileName !== '.' && $fileName !== '..') {
                unlink($folder . '/' . $fileName);
            }

        }
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeUpload()
    {
        if ($file = $this->getAbsolutePath()) {
            unlink($file);
        }
    }

    public function getAbsolutePath()
    {
        return null === $this->getGuildLogo() ? null : $this->getUploadRootDir() . '/' . $this->getGuildLogo();
    }

    public function getWebPath()
    {
        return null === $this->getGuildLogo() ? null : $this->getUploadDir() . '/' . $this->getGuildLogo();
    }

    protected function getUploadRootDir()
    {
        return __DIR__.'/../../../../web/' . $this->getUploadDir();
    }

    protected function getUploadDir()
    {
        return 'media/upload/guild/' . hash('adler32', $this->getId()) . '_' .$this->getId() . '/logo';
    }

    /**
     * Set guildLogo
     *
     * @param string $guildLogo
     * @return Guild
     */
    public function setGuildLogo($guildLogo)
    {
        if ($this->getGuildLogo() === null) {
            $this->setCustomLogoEnabled(true);
        }

        $this->guildLogo = $guildLogo;

        return $this;
    }

    /**
     * Get guildLogo
     *
     * @return string
     */
    public function getGuildLogo()
    {
        return $this->guildLogo;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Guild
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return Guild
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set creationDate
     *
     * @param \DateTime $creationDate
     * @return Guild
     */
    public function setCreationDate($creationDate)
    {
        $this->creationDate = $creationDate;

        return $this;
    }

    /**
     * Get creationDate
     *
     * @return \DateTime
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }

    /**
     * Set open
     *
     * @param boolean $open
     * @return Guild
     */
    public function setOpen($open)
    {
        $this->open = $open;

        return $this;
    }

    /**
     * Is open
     *
     * @return boolean
     */
    public function isOpen()
    {
        return $this->open;
    }

    /**
     * Set forum
     *
     * @param \Rizzen\ForumBundle\Entity\Forum $forum
     * @return Guild
     */
    public function setForum(\Rizzen\ForumBundle\Entity\Forum $forum = null)
    {
        $this->forum = $forum;

        return $this;
    }

    /**
     * Get forum
     *
     * @return \Rizzen\ForumBundle\Entity\Forum
     */
    public function getForum()
    {
        return $this->forum;
    }

    /**
     * Set creator
     *
     * @param \Rizzen\UserBundle\Entity\User $creator
     * @return Guild
     */
    public function setCreator(\Rizzen\UserBundle\Entity\User $creator)
    {
        $this->creator = $creator;

        return $this;
    }

    /**
     * Get creator
     *
     * @return \Rizzen\UserBundle\Entity\User
     */
    public function getCreator()
    {
        return $this->creator;
    }

    /**
     * Set calendar
     *
     * @param \Rizzen\CalendarBundle\Entity\Calendar $calendar
     * @return Guild
     */
    public function setCalendar(\Rizzen\CalendarBundle\Entity\Calendar $calendar = null)
    {
        $this->calendar = $calendar;

        return $this;
    }

    /**
     * Get calendar
     *
     * @return \Rizzen\CalendarBundle\Entity\Calendar
     */
    public function getCalendar()
    {
        return $this->calendar;
    }


    /**
     * Add members
     *
     * @param \Rizzen\UserBundle\Entity\User $members
     * @return Guild
     */
    public function addMember(\Rizzen\UserBundle\Entity\User $members)
    {
        $this->members[] = $members;

        return $this;
    }

    /**
     * Remove members
     *
     * @param \Rizzen\UserBundle\Entity\User $members
     */
    public function removeMember(\Rizzen\UserBundle\Entity\User $members)
    {
        $this->members->removeElement($members);
    }

    /**
     * Get members
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMembers()
    {
        return $this->members;
    }

    /**
     * Set blog
     *
     * @param \Rizzen\BlogBundle\Entity\Blog $blog
     * @return Guild
     */
    public function setBlog(\Rizzen\BlogBundle\Entity\Blog $blog = null)
    {
        $this->blog = $blog;

        return $this;
    }

    /**
     * Get blog
     *
     * @return \Rizzen\BlogBundle\Entity\Blog
     */
    public function getBlog()
    {
        return $this->blog;
    }

    /**
     * Add roles
     *
     * @param \Rizzen\GuildBundle\Entity\Role $roles
     * @return Guild
     */
    public function addRole(\Rizzen\GuildBundle\Entity\Role $roles)
    {
        $this->roles[] = $roles;

        return $this;
    }

    /**
     * Remove roles
     *
     * @param \Rizzen\GuildBundle\Entity\Role $roles
     */
    public function removeRole(\Rizzen\GuildBundle\Entity\Role $roles)
    {
        $this->roles->removeElement($roles);
    }

    /**
     * Get roles
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRoles()
    {
        return $this->roles;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->open = true;
        $this->customCSSEnabled = false;
        $this->setCustomLogoEnabled(false);
        $this->creationDate = new \DateTime('now');
        $this->members = new \Doctrine\Common\Collections\ArrayCollection();
        $this->roles = new \Doctrine\Common\Collections\ArrayCollection();
        $this->bannedUsers = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add bannedUsers
     *
     * @param \Rizzen\GuildBundle\Entity\BannedUser $bannedUsers
     * @return Guild
     */
    public function addBannedUser(\Rizzen\GuildBundle\Entity\BannedUser $bannedUsers)
    {
        $this->bannedUsers[] = $bannedUsers;

        return $this;
    }

    /**
     * Remove bannedUsers
     *
     * @param \Rizzen\GuildBundle\Entity\BannedUser $bannedUsers
     */
    public function removeBannedUser(\Rizzen\GuildBundle\Entity\BannedUser $bannedUsers)
    {
        $this->bannedUsers->removeElement($bannedUsers);
    }

    /**
     * Get bannedUsers
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getBannedUsers()
    {
        return $this->bannedUsers;
    }

    /**
     * Get open
     *
     * @return boolean
     */
    public function getOpen()
    {
        return $this->open;
    }

    /**
     * Set customCSSEnabled
     *
     * @param boolean $customCSSEnabled
     *
     * @return Guild
     */
    public function setCustomCSSEnabled($customCSSEnabled)
    {
        $this->customCSSEnabled = $customCSSEnabled;

        return $this;
    }

    /**
     * Get customCSSEnabled
     *
     * @return boolean
     */
    public function getCustomCSSEnabled()
    {
        return $this->customCSSEnabled;
    }

    /**
     * Set customCSSFile
     *
     * @param string $customCSSFile
     *
     * @return Guild
     */
    public function setCustomCSSFile($customCSSFile)
    {
        $this->customCSSFile = $customCSSFile;

        return $this;
    }

    /**
     * Get customCSSFile
     *
     * @return string
     */
    public function getCustomCSSFile()
    {
        return $this->customCSSFile;
    }

    /**
     * Set customLogoEnabled
     *
     * @param boolean $customLogoEnabled
     *
     * @return Guild
     */
    public function setCustomLogoEnabled($customLogoEnabled)
    {
        $this->customLogoEnabled = $customLogoEnabled;

        return $this;
    }

    /**
     * Get customLogoEnabled
     *
     * @return boolean
     */
    public function getCustomLogoEnabled()
    {
        return $this->customLogoEnabled;
    }
}
