<?php

namespace Rizzen\GuildBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Exclude;
use JMS\Serializer\Annotation\Type;

/**
 * Role
 *
 * @ORM\Table(indexes={@ORM\Index(name="guild_role_idx", columns={"slug"})})
 * @ORM\Entity(repositoryClass="Rizzen\GuildBundle\Entity\RoleRepository")
 */
class Role
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Guild", inversedBy="roles")
     */
    protected $guild;

    /**
     * @ORM\OneToMany(targetEntity="RoleAssignment", mappedBy="role")
     * @Exclude
     */
    protected $assignments;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    protected $title;

    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=255, nullable=true)
     */
    protected $slug;

    /**
     * Used for the permissions.
     * Contain the ID's of the roles the user has granted to him
     *
     * @Type("array")
     * @var array
     */
    protected $rolesAttributed;

    /**
     * @Type("boolean")
     * @var boolean
     */
    protected $isForumMember;

    /**
     * @Type("boolean")
     * @var boolean
     */
    protected $isGuildMember;

    /**
     * @Type("boolean")
     * @var boolean
     */
    protected $isBannedFromForum;

    /**
     * @Type("boolean")
     * @var boolean
     */
    protected $isBannedFromGuild;

    /**
     * @var boolean
     *
     * @ORM\Column(name="canEditForumPost", type="boolean")
     */
    protected $canEditForumPost;

    /**
     * @var boolean
     *
     * @ORM\Column(name="canEditForum", type="boolean")
     */
    protected $canEditForum;

    /**
     * @var boolean
     *
     * @ORM\Column(name="canDeleteForumPost", type="boolean")
     */
    protected $canDeleteForumPost;

    /**
     * @var boolean
     *
     * @ORM\Column(name="canEditForumThread", type="boolean")
     */
    protected $canEditForumThread;

    /**
     * @var boolean
     *
     * @ORM\Column(name="canDeleteForumThread", type="boolean")
     */
    protected $canDeleteForumThread;

    /**
     * @var boolean
     *
     * @ORM\Column(name="canPinThread", type="boolean")
     */
    protected $canPinThread;

    /**
     * @var boolean
     *
     * @ORM\Column(name="canWriteBlogPost", type="boolean")
     */
    protected $canWriteBlogPost;

    /**
     * @var boolean
     *
     * @ORM\Column(name="canEditBlogPost", type="boolean")
     */
    protected $canEditBlogPost;

    /**
     * @var boolean
     *
     * @ORM\Column(name="canAddToGuild", type="boolean")
     */
    protected $canAddToGuild;

    /**
     * @var boolean
     *
     * @ORM\Column(name="canBan", type="boolean")
     */
    protected $canBan;

    /**
     * @var boolean
     *
     * @ORM\Column(name="canModifyRank", type="boolean")
     */
    protected $canModifyRank;

    /**
     * @var boolean
     *
     * @ORM\Column(name="canCreateEvent", type="boolean")
     */
    protected $canCreateEvent;

    /**
     * @var boolean
     *
     * @ORM\Column(name="canManageEvent", type="boolean")
     */
    protected $canManageEvent;

    /**
     * @var boolean
     *
     * @ORM\Column(name="canSeeAdminPanel", type="boolean")
     */
    protected $canSeeAdminPanel;

    /**
     * @var boolean
     *
     * @ORM\Column(name="canCreateRole", type="boolean")
     */
    protected $canCreateRole;

    /**
     * @var boolean
     *
     * @ORM\Column(name="canEditGuildStyle", type="boolean")
     */
    protected $canEditGuildStyle;

    /**
     * @var boolean
     *
     * @ORM\Column(name="canEditGuildLogo", type="boolean")
     */
    protected $canEditGuildLogo;

    /**
     * @var boolean
     *
     * @ORM\Column(name="roleEditable", type="boolean")
     * @Exclude
     */
    protected $roleEditable;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Role
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return Role
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Add a role the user has, used for the permissions
     *
     * @param int $role
     * @return $this
     */
    public function addRole($role)
    {
        $this->rolesAttributed[] = $role;

        return $this;
    }

    /**
     * Get the attributed roles
     *
     * @return $this
     */
    public function getRolesAttributed()
    {
        return $this->rolesAttributed;
    }

    /**
     * Set isForumMember
     *
     * @param boolean $isForumMember
     * @return $this
     */
    public function setForumMember($isForumMember)
    {
        $this->isForumMember = $isForumMember;

        return $this;
    }

    /**
     * Get isForumMember
     *
     * @return boolean
     */
    public function isForumMember()
    {
        return $this->isForumMember;
    }

    /**
     * Set isGuildMember
     *
     * @param boolean $isGuildMember
     * @return $this
     */
    public function setGuildMember($isGuildMember)
    {
        $this->isGuildMember = $isGuildMember;

        return $this;
    }

    /**
     * Get isGuildMember
     *
     * @return boolean
     */
    public function isGuildMember()
    {
        return $this->isGuildMember;
    }

    /**
     * Set isBannedFromForum
     *
     * @param boolean $isBannedFromForum
     * @return $this
     */
    public function setBannedFromForum($isBannedFromForum)
    {
        $this->isBannedFromForum = $isBannedFromForum;

        return $this;
    }

    /**
     * Get isBannedFromForum
     *
     * @return boolean
     */
    public function isBannedFromForum()
    {
        return $this->isBannedFromForum;
    }

    /**
     * Set isBannedFromGuild
     *
     * @param boolean $isBannedFromGuild
     * @return $this
     */
    public function setBannedFromGuild($isBannedFromGuild)
    {
        $this->isBannedFromGuild = $isBannedFromGuild;

        return $this;
    }

    /**
     * Get isBannedFromGuild
     *
     * @return boolean
     */
    public function isBannedFromGuild()
    {
        return $this->isBannedFromGuild;
    }


    /**
     * Set guild
     *
     * @param \Rizzen\GuildBundle\Entity\Guild $guild
     * @return Role
     */
    public function setGuild(\Rizzen\GuildBundle\Entity\Guild $guild = null)
    {
        $this->guild = $guild;

        return $this;
    }

    /**
     * Get guild
     *
     * @return \Rizzen\GuildBundle\Entity\Guild
     */
    public function getGuild()
    {
        return $this->guild;
    }

    /**
     * Add assignments
     *
     * @param \Rizzen\GuildBundle\Entity\RoleAssignment $assignments
     * @return Role
     */
    public function addAssignment(\Rizzen\GuildBundle\Entity\RoleAssignment $assignments)
    {
        $this->assignments[] = $assignments;

        return $this;
    }

    /**
     * Remove assignments
     *
     * @param \Rizzen\GuildBundle\Entity\RoleAssignment $assignments
     */
    public function removeAssignment(\Rizzen\GuildBundle\Entity\RoleAssignment $assignments)
    {
        $this->assignments->removeElement($assignments);
    }

    /**
     * Get assignments
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAssignments()
    {
        return $this->assignments;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->canEditForumPost     = false;
        $this->canDeleteForumPost   = false;
        $this->canEditForumThread   = false;
        $this->canDeleteForumThread = false;
        $this->canPinThread         = false;
        $this->canWriteBlogPost     = false;
        $this->canEditBlogPost      = false;
        $this->canAddToGuild        = false;
        $this->canBan               = false;
        $this->canModifyRank        = false;
        $this->canCreateEvent       = false;
        $this->canManageEvent       = false;
        $this->canSeeAdminPanel     = false;
        $this->canCreateRole        = false;
        $this->canEditForum         = false;
        $this->canEditGuildLogo     = false;
        $this->canEditGuildStyle    = false;

        $this->isForumMember        = false;
        $this->isGuildMember        = false;
        $this->isBannedFromForum    = false;
        $this->isBannedFromGuild    = false;
        $this->roleEditable         = true;
        $this->rolesAttributed      = [];

        $this->assignments = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Set roleEditable
     *
     * @param boolean $roleEditable
     * @return Role
     */
    public function setRoleEditable($roleEditable)
    {
        $this->roleEditable = $roleEditable;

        return $this;
    }

    /**
     * Get roleEditable
     *
     * @return boolean
     */
    public function getRoleEditable()
    {
        return $this->roleEditable;
    }

    /**
     * Set canEditForumPost
     *
     * @param boolean $canEditForumPost
     * @return Role
     */
    public function setCanEditForumPost($canEditForumPost)
    {
        $this->canEditForumPost = $canEditForumPost;

        return $this;
    }

    /**
     * Get canEditForumPost
     *
     * @return boolean
     */
    public function getCanEditForumPost()
    {
        return $this->canEditForumPost;
    }

    /**
     * Set canDeleteForumPost
     *
     * @param boolean $canDeleteForumPost
     * @return Role
     */
    public function setCanDeleteForumPost($canDeleteForumPost)
    {
        $this->canDeleteForumPost = $canDeleteForumPost;

        return $this;
    }

    /**
     * Get canDeleteForumPost
     *
     * @return boolean
     */
    public function getCanDeleteForumPost()
    {
        return $this->canDeleteForumPost;
    }

    /**
     * Set canEditForumThread
     *
     * @param boolean $canEditForumThread
     * @return Role
     */
    public function setCanEditForumThread($canEditForumThread)
    {
        $this->canEditForumThread = $canEditForumThread;

        return $this;
    }

    /**
     * Get canEditForumThread
     *
     * @return boolean
     */
    public function getCanEditForumThread()
    {
        return $this->canEditForumThread;
    }

    /**
     * Set canDeleteForumThread
     *
     * @param boolean $canDeleteForumThread
     * @return Role
     */
    public function setCanDeleteForumThread($canDeleteForumThread)
    {
        $this->canDeleteForumThread = $canDeleteForumThread;

        return $this;
    }

    /**
     * Get canDeleteForumThread
     *
     * @return boolean
     */
    public function getCanDeleteForumThread()
    {
        return $this->canDeleteForumThread;
    }

    /**
     * Set canPinThread
     *
     * @param boolean $canPinThread
     * @return Role
     */
    public function setCanPinThread($canPinThread)
    {
        $this->canPinThread = $canPinThread;

        return $this;
    }

    /**
     * Get canPinThread
     *
     * @return boolean
     */
    public function getCanPinThread()
    {
        return $this->canPinThread;
    }

    /**
     * Set canWriteBlogPost
     *
     * @param boolean $canWriteBlogPost
     * @return Role
     */
    public function setCanWriteBlogPost($canWriteBlogPost)
    {
        $this->canWriteBlogPost = $canWriteBlogPost;

        return $this;
    }

    /**
     * Get canWriteBlogPost
     *
     * @return boolean
     */
    public function getCanWriteBlogPost()
    {
        return $this->canWriteBlogPost;
    }

    /**
     * Set canEditBlogPost
     *
     * @param boolean $canEditBlogPost
     * @return Role
     */
    public function setCanEditBlogPost($canEditBlogPost)
    {
        $this->canEditBlogPost = $canEditBlogPost;

        return $this;
    }

    /**
     * Get canEditBlogPost
     *
     * @return boolean
     */
    public function getCanEditBlogPost()
    {
        return $this->canEditBlogPost;
    }

    /**
     * Set canAddToGuild
     *
     * @param boolean $canAddToGuild
     * @return Role
     */
    public function setCanAddToGuild($canAddToGuild)
    {
        $this->canAddToGuild = $canAddToGuild;

        return $this;
    }

    /**
     * Get canAddToGuild
     *
     * @return boolean
     */
    public function getCanAddToGuild()
    {
        return $this->canAddToGuild;
    }

    /**
     * Set canBan
     *
     * @param boolean $canBan
     * @return Role
     */
    public function setCanBan($canBan)
    {
        $this->canBan = $canBan;

        return $this;
    }

    /**
     * Get canBan
     *
     * @return boolean
     */
    public function getCanBan()
    {
        return $this->canBan;
    }

    /**
     * Set canModifyRank
     *
     * @param boolean $canModifyRank
     * @return Role
     */
    public function setCanModifyRank($canModifyRank)
    {
        $this->canModifyRank = $canModifyRank;

        return $this;
    }

    /**
     * Get canModifyRank
     *
     * @return boolean
     */
    public function getCanModifyRank()
    {
        return $this->canModifyRank;
    }

    /**
     * Set canCreateEvent
     *
     * @param boolean $canCreateEvent
     * @return Role
     */
    public function setCanCreateEvent($canCreateEvent)
    {
        $this->canCreateEvent = $canCreateEvent;

        return $this;
    }

    /**
     * Get canCreateEvent
     *
     * @return boolean
     */
    public function getCanCreateEvent()
    {
        return $this->canCreateEvent;
    }

    /**
     * Set canManageEvent
     *
     * @param boolean $canManageEvent
     * @return Role
     */
    public function setCanManageEvent($canManageEvent)
    {
        $this->canManageEvent = $canManageEvent;

        return $this;
    }

    /**
     * Get canManageEvent
     *
     * @return boolean
     */
    public function getCanManageEvent()
    {
        return $this->canManageEvent;
    }

    /**
     * Set canSeeAdminPanel
     *
     * @param boolean $canSeeAdminPanel
     * @return Role
     */
    public function setCanSeeAdminPanel($canSeeAdminPanel)
    {
        $this->canSeeAdminPanel = $canSeeAdminPanel;

        return $this;
    }

    /**
     * Get canSeeAdminPanel
     *
     * @return boolean
     */
    public function getCanSeeAdminPanel()
    {
        return $this->canSeeAdminPanel;
    }

    /**
     * Set canCreateRole
     *
     * @param boolean $canCreateRole
     * @return Role
     */
    public function setCanCreateRole($canCreateRole)
    {
        $this->canCreateRole = $canCreateRole;

        return $this;
    }

    /**
     * Get canCreateRole
     *
     * @return boolean
     */
    public function getCanCreateRole()
    {
        return $this->canCreateRole;
    }

    /**
     * Set canEditForum
     *
     * @param boolean $canEditForum
     * @return Role
     */
    public function setCanEditForum($canEditForum)
    {
        $this->canEditForum = $canEditForum;

        return $this;
    }

    /**
     * Get canEditForum
     *
     * @return boolean
     */
    public function getCanEditForum()
    {
        return $this->canEditForum;
    }

    /**
     * Set canEditGuildStyle
     *
     * @param boolean $canEditGuildStyle
     *
     * @return Role
     */
    public function setCanEditGuildStyle($canEditGuildStyle)
    {
        $this->canEditGuildStyle = $canEditGuildStyle;

        return $this;
    }

    /**
     * Get canEditGuildStyle
     *
     * @return boolean
     */
    public function getCanEditGuildStyle()
    {
        return $this->canEditGuildStyle;
    }

    /**
     * Set canEditGuildLogo
     *
     * @param boolean $canEditGuildLogo
     *
     * @return Role
     */
    public function setCanEditGuildLogo($canEditGuildLogo)
    {
        $this->canEditGuildLogo = $canEditGuildLogo;

        return $this;
    }

    /**
     * Get canEditGuildLogo
     *
     * @return boolean
     */
    public function getCanEditGuildLogo()
    {
        return $this->canEditGuildLogo;
    }
}
