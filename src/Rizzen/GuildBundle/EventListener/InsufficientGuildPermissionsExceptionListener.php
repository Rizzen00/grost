<?php

namespace Rizzen\GuildBundle\EventListener;

use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Rizzen\GuildBundle\Exception\InsufficientGuildPermissionsException;
use Symfony\Component\Routing\Router;

class InsufficientGuildPermissionsExceptionListener
{
    private $router;
    private $session;

    /**
     * @param Router $router
     * @param Session $session
     */
    function __construct(Router $router, Session $session)
    {
        $this->router = $router;
        $this->session = $session;
    }

    /**
     * Redirect the user to a page he can see without any particular permission
     *
     * @param GetResponseForExceptionEvent $event
     */
    public function onException(GetResponseForExceptionEvent $event)
    {
        $exception = $event->getException();
        if (!($exception instanceof InsufficientGuildPermissionsException) || !$event->isMasterRequest()) {
            return;
        }

        $this->session->getFlashBag()->add(
            'error',
            'Sorry, your roles does not let you access this page or perform this action.'
        );

        $routeParams = $event->getRequest()->attributes->get('_route_params');
        $redirectTo = $event->getRequest()->headers->get('referer');

        if (empty($redirectTo)) {
            $redirectTo = $this->router->generate('rizzen_guild_see', ['subdomain' => $routeParams['subdomain']]);
        }

        $event->setResponse(new RedirectResponse($redirectTo));

    }

}
