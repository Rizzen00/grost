<?php

namespace Rizzen\GuildBundle\Tests\Services;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class GetGuildTest extends WebTestCase {

    private $getGuild;

    public function setUp()
    {
        static::$kernel = static::createKernel();
        static::$kernel->boot();
        $this->getGuild = static::$kernel->getContainer()->get('get.guild');
    }

    public function testGetGuildValid()
    {
        $subdomain = 'testguild';

        $guild1 = $this->getGuild->getGuild($subdomain);
        $guild2 = $this->getGuild->getGuild('09234857', false);

        if ($guild1 === false) {
            $result1 = false;
        } else {
            $result1 = true;
        }

        if ($guild2 === false) {
            $result2 = false;
        } else {
            $result2 = true;
        }

        $this->assertTrue($result1);
        $this->assertFalse($result2);

        $this->setExpectedException('Symfony\Component\HttpKernel\Exception\NotFoundHttpException');
        $this->getGuild->getGuild('234085723804957', true);
    }

    public function testGetGuildInvalid()
    {
        $guild = $this->getGuild->getGuild('09234857', false);

        if ($guild === false) {
            $result = false;
        } else {
            $result = true;
        }

        $this->assertFalse($result);

    }

    public function testGetGuildInvalidException()
    {
        $this->setExpectedException('Symfony\Component\HttpKernel\Exception\NotFoundHttpException');
        $this->getGuild->getGuild('234085723804957', true);
    }
}
