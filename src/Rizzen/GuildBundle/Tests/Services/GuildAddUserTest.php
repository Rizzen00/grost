<?php

namespace Rizzen\GuildBundle\Tests\Services;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Rizzen\GuildBundle\Services\GuildAddUser;

class GuildAddUserTest extends WebTestCase
{
    /** @var GuildAddUser $permissionsOfUser */
    private $guildAddUser;
    private $em;
    private $userManager;

    public function setUp()
    {
        static::$kernel = static::createKernel();
        static::$kernel->boot();
        $this->guildAddUser = static::$kernel->getContainer()->get('guild.add.user');
        $this->userManager = static::$kernel->getContainer()->get('fos_user.user_manager');
        $this->em = static::$kernel->getContainer()->get('doctrine.orm.entity_manager');
    }

    public function testAddUser()
    {
        $guild = $this->em->getRepository('RizzenGuildBundle:Guild')->findOneByTitle('TestGuild');
        $user = $this->userManager->findUserByUsername('testUser1');

        $result = $this->guildAddUser->addUser($guild, $user->getId());
        $this->assertTrue($result['hasBeenAdded']);

        $result = $this->guildAddUser->addUser($guild, $user->getId());
        $this->assertFalse($result['hasBeenAdded']);
    }
}
