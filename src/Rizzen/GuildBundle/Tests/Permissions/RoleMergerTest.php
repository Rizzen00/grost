<?php

namespace Rizzen\GuildBundle\Tests\Permissions;

use Rizzen\GuildBundle\Entity\Role;
use Rizzen\GuildBundle\Entity\RoleAssignment;
use Rizzen\GuildBundle\Permissions\PermissionsMerger;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class RoleMergerTest extends WebTestCase
{

    public function setUp()
    {
        static::$kernel = static::createKernel();
        static::$kernel->boot();
    }

    public function testGetMergedPermissions()
    {
        $role1 = new Role();
        $role1->setCanCreateEvent(true);

        $assignment1 = new RoleAssignment();
        $assignment1->setRole($role1);

        $role2 = new Role();
        $role2->setCanEditForumThread(true);

        $assignment2 = new RoleAssignment();
        $assignment2->setRole($role2);

        $roleMerger = new PermissionsMerger();

        $mergedPermissions = $roleMerger->getMergedPermissions([$assignment1, $assignment2]);

        $this->assertTrue($mergedPermissions->getCanCreateEvent());
        $this->assertTrue($mergedPermissions->getCanEditForumThread());
        $this->assertFalse($mergedPermissions->getCanBan());
    }

}
