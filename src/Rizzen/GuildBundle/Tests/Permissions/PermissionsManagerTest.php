<?php

namespace Rizzen\GuildBundle\Tests\Permissions;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Rizzen\GuildBundle\Permissions\PermissionsManager;

class PermissionsManagerTest extends WebTestCase
{
    /** @var PermissionsManager $permissionsManager */
    private $permissionsManager;
    private $userManager;
    private $em;
    private $guild;

    public function setUp()
    {
        static::$kernel = static::createKernel();
        static::$kernel->boot();
        $this->permissionsManager = static::$kernel->getContainer()->get('permissions.manager');
        $this->userManager = static::$kernel->getContainer()->get('fos_user.user_manager');
        $this->em = static::$kernel->getContainer()->get('doctrine.orm.entity_manager');
        $this->guild = $this->em->getRepository('RizzenGuildBundle:Guild')->findOneByTitle('TestGuild');
    }

    public function testGetPermissions()
    {
        $user = $this->userManager->findUserBy(['username' => 'testUser0']);
        $this->permissionsManager->setUser($user)->setGuild($this->guild);

        $permissions = $this->permissionsManager->getPermissions();

        $this->assertNotNull($permissions);

        $permissionsJSON = $this->permissionsManager->getJSONPermissions();
        json_decode($permissionsJSON);

        $isJson = (json_last_error() === JSON_ERROR_NONE);

        $this->assertTrue($isJson);

    }

    public function testCan()
    {
        $this->permissionsManager->clearLocalPermissions();

        $user = $this->userManager->findUserBy(['username' => 'testUser4']);
        $this->permissionsManager->setUser($user)->setGuild($this->guild);

        $this->assertFalse($this->permissionsManager->can(['CanSeeAdminPanel', 'CanEditForum']));

        $this->permissionsManager->clearLocalPermissions();

        $user = $this->userManager->findUserBy(['username' => 'testUser0']);
        $this->permissionsManager->setUser($user)->setGuild($this->guild);

        $this->assertTrue($this->permissionsManager->can(['CanSeeAdminPanel', 'CanEditForum']));
    }

    public function testMust()
    {
        $this->permissionsManager->clearLocalPermissions();

        $user = $this->userManager->findUserBy(['username' => 'testUser0']);
        $this->permissionsManager->setUser($user)->setGuild($this->guild);

        $this->permissionsManager->must(['CanSeeAdminPanel', 'CanEditForum']);

        $this->permissionsManager->clearLocalPermissions();

        $user = $this->userManager->findUserBy(['username' => 'testUser4']);
        $this->permissionsManager->setUser($user)->setGuild($this->guild);


        $this->setExpectedException('Rizzen\GuildBundle\Exception\InsufficientGuildPermissionsException');
        $this->permissionsManager->must(['CanSeeAdminPanel', 'CanEditForum']);
    }

    public function testIsGuildMember()
    {
        $this->permissionsManager->clearLocalPermissions();

        $user = $this->userManager->findUserBy(['username' => 'testUser0']);
        $this->permissionsManager->setUser($user)->setGuild($this->guild);
        $this->assertTrue($this->permissionsManager->isGuildMember(false));

        $this->permissionsManager->clearLocalPermissions();

        $user = $this->userManager->findUserBy(['username' => 'testUser4']);
        $this->permissionsManager->setUser($user)->setGuild($this->guild);
        $this->assertFalse($this->permissionsManager->isGuildMember(false));

        $this->setExpectedException('Rizzen\GuildBundle\Exception\InsufficientGuildPermissionsException');
        $this->permissionsManager->isGuildMember();
    }

    public function testIsForumMember()
    {
        $this->permissionsManager->clearLocalPermissions();

        $user = $this->userManager->findUserBy(['username' => 'testUser0']);
        $this->permissionsManager->setUser($user)->setGuild($this->guild);
        $this->assertTrue($this->permissionsManager->isForumMember(false));

        $this->permissionsManager->clearLocalPermissions();

        $user = $this->userManager->findUserBy(['username' => 'testUser4']);
        $this->permissionsManager->setUser($user)->setGuild($this->guild);
        $this->assertFalse($this->permissionsManager->isForumMember(false));

        $this->setExpectedException('Rizzen\GuildBundle\Exception\InsufficientGuildPermissionsException');
        $this->permissionsManager->isForumMember();
    }

    public function testRolesAttributed()
    {
        $this->permissionsManager->clearLocalPermissions();

        $user = $this->userManager->findUserBy(['username' => 'testUser0']);
        $this->permissionsManager->setUser($user)->setGuild($this->guild);

        $this->assertNotNull($this->permissionsManager->getPermissions()->getRolesAttributed());
    }
}
