<?php

namespace Rizzen\GuildBundle\Tests\Permissions;

use Rizzen\GuildBundle\Entity\Role;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Rizzen\GuildBundle\Permissions\RoleCreator;

class RoleCreatorTest extends WebTestCase
{
    private $em;
    private $guild;

    /** @var RoleCreator $roleCreator */
    private $roleCreator;

    public function setUp()
    {
        static::$kernel = static::createKernel();
        static::$kernel->boot();
        $this->em = static::$kernel->getContainer()->get('doctrine.orm.entity_manager');
        $this->guild = $this->em->getRepository('RizzenGuildBundle:Guild')->findOneByTitle('TestGuild');
        $this->roleCreator = static::$kernel->getContainer()->get('role.creator');
    }

    public function testCreateRole()
    {
        $role = new Role();
        $role->setTitle('TestRole');
        $role->setCanEditForumThread(true);

        $this->roleCreator->createRole($this->guild, $role);
    }

}
