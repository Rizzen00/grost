<?php

namespace Rizzen\GuildBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Rizzen\GuildBundle\Form\Type\GuildType;
use Rizzen\GuildBundle\Entity\Guild;


class GuildCreationController extends Controller
{
    /**
     * Register then redirect to the guild creation page
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function registerThenCreateAction(Request $request)
    {
        $request->getSession()->set('registerThenCreate', true);
        return $this->redirectToRoute('fos_user_registration_register');
    }

    /**
     * Create a new guild
     *
     * @param Request $request
     * @return JsonResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function createAction(Request $request)
    {
        $guild = new Guild();
        $form = $this->createForm(GuildType::class, $guild);

        $guildsOfUserCount = count($this->getDoctrine()->getRepository('RizzenGuildBundle:Guild')->getGuildsCreatedByUser($this->getUser()));

        $form->handleRequest($request);
        if ($form->isValid() && $guildsOfUserCount < 5) {

            $data = $this->get('guild.creator')->create($guild, $this->getUser());
            return new JsonResponse($data);
        }

        return $this->render('RizzenGuildBundle:Guild:create.html.twig', [
            'form' => $form->createView(),
            'guildsOfUserCount' => $guildsOfUserCount
        ]);
    }
}
