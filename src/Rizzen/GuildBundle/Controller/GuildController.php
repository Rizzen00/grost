<?php

namespace Rizzen\GuildBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class GuildController extends Controller
{
    /**
     * The public guild dashboard
     *
     * @param string $subdomain
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function seeAction($subdomain)
    {
        $guild = $this->get('get.guild')->getGuildForDashboard($subdomain);
        $permissionsManager = $this->get('permissions.manager')->setGuild($guild)->setUser($this->getUser());
        $upcomingEvents = $this->getDoctrine()->getRepository('RizzenCalendarBundle:Event')->getUpcomingEvents($guild);
        $upcomingRegisteredEvents = $this->getDoctrine()->getRepository('RizzenCalendarBundle:Registration')->getUpcomingRegisteredEventsForGuild($this->getUser());
        $latestBlogPost = $this->getDoctrine()->getRepository('RizzenBlogBundle:Post')->getLatestVisiblePost($guild);

        $key = 'activeThreads-g' . $guild->getId();
        $activeThreads = $this->get('snc_redis.default')->lrange($key, 0, -1);
        $decodedThreads = $this->decodeThreads($activeThreads);
        $threads = $this->get('forum.viewable')->removeThreadsNotVisibleForDashboard($decodedThreads, $guild, $permissionsManager->getPermissions());

        return $this->render('RizzenGuildBundle::dashboard.html.twig', [
            'upcomingRegisteredEvents' => $upcomingRegisteredEvents,
            'subdomain' => $subdomain,
            'guild' => $guild,
            'chatMessages' => $this->get('guild.chat.manager')->getChatMessages($guild),
            'chatForm' => $this->getGuildChatFormView(),
            'upcomingEvents' => $upcomingEvents,
            'latestBlogPost' => $latestBlogPost,
            'activeThreads' => $threads,
            'permissions' => $permissionsManager->getPermissions(),
            'permissionsJSON' => $permissionsManager->getJSONPermissions()
        ]);
    }

    /**
     * Decode the json encoded threads
     *
     * @param array $activeThreads
     * @return array
     */
    private function decodeThreads($activeThreads)
    {
        $threads = [];
        foreach ($activeThreads as $activeThread) {
            $threads[] = json_decode($activeThread, true);
        }

        return $threads;
    }

    /**
     * Get the guild chat view
     *
     * @return \Symfony\Component\Form\FormView
     */
    private function getGuildChatFormView()
    {
        return $this->createFormBuilder([])
            ->add('message', TextType::class)
            ->getForm()
            ->createView();
    }
}
