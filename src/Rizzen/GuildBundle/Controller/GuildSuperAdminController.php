<?php
namespace Rizzen\GuildBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;


class GuildSuperAdminController extends Controller
{
    /**
     * List all the guild that have been created
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listAction()
    {
        $doctrine = $this->getDoctrine();

        $guildCount = $doctrine->getRepository('RizzenGuildBundle:Guild')->getGuildsCount();
        $guilds = $doctrine->getRepository('RizzenGuildBundle:Guild')->getGuilds();

        return $this->render('RizzenGuildBundle:SuperAdmin:guildsList.html.twig', [
            'guildCount' => $guildCount,
            'guilds' => $guilds
        ]);
    }

    /**
     * View the details of a guild
     *
     * @param int $guildID
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function guildAction($guildID)
    {
        $doctrine = $this->getDoctrine();

        $guild = $doctrine->getRepository('RizzenGuildBundle:Guild')->getGuildForSuperAdmin($guildID);

        return $this->render('RizzenGuildBundle:SuperAdmin:guild.html.twig', [
            'guildAdmin' => $guild,
            'chatMessages' => $this->get('guild.chat.manager')->getChatMessages($guild)
        ]);
    }
}
