<?php

namespace Rizzen\GuildBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Rizzen\GuildBundle\Form\Type\RoleType;
use Rizzen\GuildBundle\Entity\Role;


class GuildAdminController extends Controller
{
    /**
     * The dashboard of the guild administration
     *
     * @param string $subdomain
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function dashboardAction($subdomain)
    {
        $guild = $this->get('get.guild')->getGuild($subdomain);

        $permissionsManager = $this->get('permissions.manager')->setGuild($guild)->setUser($this->getUser());
        $permissionsManager->must(['CanSeeAdminPanel']);

        return $this->render('RizzenGuildBundle:Admin:dashboard.html.twig', [
                'guild' => $guild,
                'permissions' => $permissionsManager->getPermissions(),
                'subdomain' => $subdomain
        ]);
    }

    /**
     * Page that list all the roles the guild has created
     *
     * @param string $subdomain
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function seeRolesAction($subdomain)
    {
        $guild = $this->get('get.guild')->getGuild($subdomain);

        $permissionsManager = $this->get('permissions.manager')->setGuild($guild)->setUser($this->getUser());
        $permissionsManager->must(['CanCreateRole', 'CanModifyRank']);

        return $this->render('RizzenGuildBundle:Admin:roles.html.twig', [
                'guild' => $guild,
                'subdomain' => $subdomain,
                'permissions' => $permissionsManager->getPermissions()
        ]);
    }

    /**
     * Page used to create a new guild/forum role
     *
     * @param string $subdomain
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function createRoleAction($subdomain, Request $request)
    {
        $guild = $this->get('get.guild')->getGuild($subdomain);

        $permissionsManager = $this->get('permissions.manager')->setGuild($guild)->setUser($this->getUser());
        $permissionsManager->must(['CanCreateRole']);

        $role = new Role();
        $form = $this->createForm(RoleType::class, $role);

        $form->handleRequest($request);
        if ($form->isValid()) {

            if ($this->get('role.creator')->createRole($guild, $role)) {
                $this->addFlash('success', 'Role created successfully!');
                return $this->redirect($this->generateUrl('rizzen_guild_admin_roles', ['subdomain' => $subdomain]));
            }

            $this->addFlash('error', "Sorry, you cannot use this title");
        }

        return $this->render('RizzenGuildBundle:Admin:createRole.html.twig', [
                'guild' => $guild,
                'subdomain' => $subdomain,
                'form' => $form->createView(),
                'permissions' => $permissionsManager->getPermissions()
        ]);
    }

    /**
     * Page used to edit a new guild/forum role
     *
     * @param string $subdomain
     * @param string $slug
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editRoleAction($subdomain, $slug, Request $request)
    {
        $guild = $this->get('get.guild')->getGuild($subdomain);
        $role = $this->getDoctrine()->getRepository('RizzenGuildBundle:Role')->findOneBySlug($slug);

        $permissionsManager = $this->get('permissions.manager')->setGuild($guild)->setUser($this->getUser());
        $permissionsManager->must(['CanCreateRole']);

        if ($role === null || $role->getGuild() !== $guild || $role->getRoleEditable() === false) {
            throw $this->createNotFoundException('Role not found');
        }

        $form = $this->createForm(RoleType::class, $role);

        $form->handleRequest($request);
        if ($form->isValid()) {
            if ($this->get('role.creator')->createRole($guild, $role, true)) {
                $this->addFlash('success', 'Role edited successfully!');
                return $this->redirect($this->generateUrl('rizzen_guild_admin_roles', ['subdomain' => $subdomain]));
            }

            $this->addFlash('error', 'Sorry, you cannot use this title');
        }

        return $this->render('RizzenGuildBundle:Admin:editRole.html.twig', [
                'guild' => $guild,
                'role' => $role,
                'subdomain' => $subdomain,
                'form' => $form->createView(),
                'permissions' => $permissionsManager->getPermissions()
        ]);
    }
}
