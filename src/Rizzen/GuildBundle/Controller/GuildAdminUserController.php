<?php

namespace Rizzen\GuildBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;

class GuildAdminUserController extends Controller
{
    /**
     * Remove a role from a user
     *
     * @param string $subdomain
     * @param int $userId
     * @param int $roleId
     * @return JsonResponse
     */
    public function userRemoveRoleAction($subdomain, $userId, $roleId)
    {
        $guild = $this->get('get.guild')->getGuild($subdomain);

        $permissionsManager = $this->get('permissions.manager')->setGuild($guild)->setUser($this->getUser());
        $permissionsManager->must(['CanModifyRank']);

        $responseArray = $this->get('role.manager')->removeRole($userId, $roleId, $guild);
        return new JsonResponse($responseArray);
    }

    /**
     * Page that list all the users in a guild/forum
     *
     * @param string $subdomain
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function usersAction($subdomain)
    {
        $guild = $this->get('get.guild')->getGuild($subdomain);

        $permissionsManager = $this->get('permissions.manager')->setGuild($guild)->setUser($this->getUser());
        $permissionsManager->must(['CanBan', 'CanAddToGuild', 'CanModifyRank']);

        return $this->render('RizzenGuildBundle:Admin:users.html.twig', [
                'guild' => $guild,
                'subdomain' => $subdomain,
                'permissions' => $permissionsManager->getPermissions()
        ]);
    }

    /**
     * List of the banned users
     *
     * @param string $subdomain
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function usersBannedAction($subdomain)
    {
        $guild = $this->get('get.guild')->getGuild($subdomain);

        $permissionsManager = $this->get('permissions.manager')->setGuild($guild)->setUser($this->getUser());
        $permissionsManager->must(['CanBan']);

        return $this->render('RizzenGuildBundle:Admin:usersBanned.html.twig', [
                'guild' => $guild,
                'subdomain' => $subdomain,
                'permissions' => $permissionsManager->getPermissions()
        ]);
    }

    /**
     * Page used to modify the parameters of the user in a specific guild/forum
     *
     * @param string $subdomain
     * @param int $userId
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function userAction($subdomain, $userId)
    {
        $guild = $this->get('get.guild')->getGuild($subdomain);
        $user = $this->getDoctrine()->getRepository('RizzenUserBundle:User')->find($userId);

        $permissionsManager = $this->get('permissions.manager')->setGuild($guild)->setUser($this->getUser());
        $permissionsManager->must(['CanBan', 'CanAddToGuild', 'CanModifyRank']);

        $permissions = $permissionsManager->getPermissions();

        if ($user === null) { throw $this->createNotFoundException('User not found'); }

        $permissionsManager->setUser($user)->clearLocalPermissions();
        $bannedUser = $this->get('guild.ban.manager')->isInBanList($user, $guild);

        if (!$permissionsManager->isForumMember(false) && $bannedUser['hasActiveBan'] === false) {
            $this->addFlash('error', 'This user is not a member of the forum/guild and does not have an active ban from it.');
            return $this->redirect($this->generateUrl('rizzen_guild_admin_users', ['subdomain' => $subdomain]));
        }

        return $this->render('RizzenGuildBundle:Admin:user.html.twig', [
                'guild' => $guild,
                'user' => $user,
                'ban' => $bannedUser,
                'userPermissions' => $permissionsManager->getPermissions(),
                'subdomain' => $subdomain,
                'permissions' => $permissions
        ]);
    }

    /**
     * Assign a role to a user
     *
     * @param string $subdomain
     * @param int $userId
     * @param int $roleId
     * @return JsonResponse
     */
    public function userAssignRoleAction($subdomain, $userId, $roleId)
    {
        $guild = $this->get('get.guild')->getGuild($subdomain);

        $permissionsManager = $this->get('permissions.manager')->setGuild($guild)->setUser($this->getUser());
        $permissionsManager->must(['CanModifyRank']);

        $responseArray = $this->get('role.manager')->assignRole($userId, $roleId, $guild);
        return new JsonResponse($responseArray);
    }

    /**
     * Add a user to a guild
     *
     * @param string $subdomain
     * @param int $userId
     * @return JsonResponse
     */
    public function addToGuildAction($subdomain, $userId)
    {
        $guild = $this->get('get.guild')->getGuild($subdomain);

        $permissionsManager = $this->get('permissions.manager')->setGuild($guild)->setUser($this->getUser());

        if (!$permissionsManager->can(['CanAddToGuild'])) {
            return new JsonResponse(['hasBeenAdded' => false]);
        }

        $response = $this->get('guild.add.user')->addUser($guild, $userId);

        return new JsonResponse($response);
    }

    /**
     * Used to ban from the guild
     *
     * @param string $subdomain
     * @param int $userId
     * @return JsonResponse
     */
    public function banGuildAction($subdomain, $userId)
    {
        $guild = $this->get('get.guild')->getGuild($subdomain);

        $permissionsManager = $this->get('permissions.manager')->setGuild($guild)->setUser($this->getUser());

        if (!$permissionsManager->can(['CanBan'])) {
            return new JsonResponse(['hasBeenBanned' => false]);
        }

        return new JsonResponse($this->get('guild.ban.manager')->banFromGuild($userId, $guild, $this->getUser()));
    }

    /**
     * Used to ban somebody from the forum
     *
     * @param string $subdomain
     * @param int $userId
     * @return JsonResponse
     */
    public function banForumAction($subdomain, $userId)
    {
        $guild = $this->get('get.guild')->getGuild($subdomain);

        $permissionsManager = $this->get('permissions.manager')->setGuild($guild)->setUser($this->getUser());

        if (!$permissionsManager->can(['CanBan'])) {
            return new JsonResponse(['hasBeenBanned' => false]);
        }

        return new JsonResponse($this->get('guild.ban.manager')->banFromForum($userId, $guild, $this->getUser()));
    }

    /**
     * Unban a user from a guild
     *
     * @param string $subdomain
     * @param int $userId
     * @param string $banToUpdate Can be "forum", "guild" or "both"
     * @return JsonResponse
     */
    public function unbanUserAction($subdomain, $userId, $banToUpdate)
    {
        $guild = $this->get('get.guild')->getGuild($subdomain);

        $permissionsManager = $this->get('permissions.manager')->setGuild($guild)->setUser($this->getUser());

        if (!$permissionsManager->can(['CanBan'])) {
            return new JsonResponse(['hasBeenUnbanned' => false]);
        }

        return new JsonResponse($this->get('guild.ban.manager')->removeBan($userId, $guild, $banToUpdate));

    }
}
