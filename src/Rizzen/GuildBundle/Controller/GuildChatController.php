<?php

namespace Rizzen\GuildBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Form\Extension\Core\Type\TextType;


class GuildChatController extends Controller
{
    /**
     * Post a message in the chat
     *
     * @param string $subdomain
     * @param Request $request
     * @return JsonResponse
     */
    public function postAction($subdomain, Request $request)
    {
        $guild = $this->get('get.guild')->getGuild($subdomain);

        $permissionsManager = $this->get('permissions.manager')->setGuild($guild)->setUser($this->getUser());
        $permissionsManager->isGuildMember();

        $form = $this->getChatForm();

        $form->handleRequest($request);
        if ($form->isValid()) {
            $data = $form->getData();
            $this->get('guild.chat.manager')->postMessage($this->getUser(), $guild, $data);
        }

        return new JsonResponse(['hasBeenSent' => true]);

    }

    /**
     * Get the chat form
     *
     * @return \Symfony\Component\Form\Form
     */
    private function getChatForm()
    {
        return $this->createFormBuilder()
            ->add('message', TextType::class, [
                'constraints' => [
                    new NotBlank(),
                    new Length([
                        'min' => 2,
                        'max' => 250
                    ]),
                ]
            ])
            ->getForm();
    }
}
