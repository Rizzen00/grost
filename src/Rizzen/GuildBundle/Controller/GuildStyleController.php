<?php

namespace Rizzen\GuildBundle\Controller;

use Rizzen\GuildBundle\Entity\Guild;
use Rizzen\GuildBundle\Form\Type\GuildLogoType;
use Rizzen\GuildBundle\Form\Type\GuildNameType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

class GuildStyleController extends Controller
{
    /**
     * Rename the guild
     *
     * @param string $subdomain
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response|\Symfony\Component\Security\Core\Exception\AccessDeniedException
     */
    public function renameGuildAction($subdomain, Request $request)
    {
        $guild = $this->get('get.guild')->getGuild($subdomain);
        $permissionsManager = $this->get('permissions.manager')->setGuild($guild)->setUser($this->getUser());

        if ($this->getUser() !== $guild->getCreator()) {
            return $this->createAccessDeniedException('Only the guild creator can rename the guild');
        }

        $form = $this->createForm(GuildNameType::class, $guild);

        $form->handleRequest($request);
        if ($form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('success', 'The name of your guild has been successfully modified!');
        }

        return $this->render('RizzenGuildBundle:Admin:rename.html.twig', [
            'guild' => $guild,
            'subdomain' => $subdomain,
            'permissions' => $permissionsManager->getPermissions(),
            'form' => $form->createView()
        ]);
    }

    /**
     * Page used to modify the logo
     *
     * @param string $subdomain
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function logoAction($subdomain)
    {
        $guild = $this->get('get.guild')->getGuild($subdomain);

        $permissionsManager = $this->get('permissions.manager')->setGuild($guild)->setUser($this->getUser());
        $permissionsManager->must(['CanEditGuildLogo']);

        $form = $this->createForm(GuildLogoType::class, $guild);

        return $this->render('RizzenGuildBundle:Admin:customLogo.html.twig', [
            'guild' => $guild,
            'subdomain' => $subdomain,
            'permissions' => $permissionsManager->getPermissions(),
            'form' => $form->createView()
        ]);
    }

    /**
     * Enable or disable the custom guild logo
     *
     * @param string $subdomain
     * @param string $action
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function toggleLogoAction($subdomain, $action)
    {
        $guild = $this->get('get.guild')->getGuild($subdomain);

        $permissionsManager = $this->get('permissions.manager')->setGuild($guild)->setUser($this->getUser());
        $permissionsManager->must(['CanEditGuildLogo']);

        if ($action === 'enable' && $guild->getGuildLogo() !== null) {
            $guild->setCustomLogoEnabled(true);
            $this->addFlash('success', 'The logo of your guild has been successfully enabled!');
        } else {
            $guild->setCustomLogoEnabled(false);
            $this->addFlash('success', 'The logo of your guild has been successfully disabled!');
        }

        $this->getDoctrine()->getManager()->flush();
        return $this->redirectToRoute('rizzen_guild_admin_logo', ['subdomain' => $subdomain]);

    }

    /**
     * Upload a new custom logo
     *
     * @param string $subdomain
     * @param Request $request
     * @return JsonResponse
     */
    public function logoUploadAction($subdomain, Request $request)
    {
        $guild = $this->get('get.guild')->getGuild($subdomain);

        $permissionsManager = $this->get('permissions.manager')->setGuild($guild)->setUser($this->getUser());
        $permissionsManager->must(['CanEditGuildLogo']);

        $form = $this->createForm(GuildLogoType::class, $guild);

        $logoUploaded = false;

        $form->handleRequest($request);
        if ($form->isValid()) {

            $guild->preUpload();
            $this->getDoctrine()->getManager()->flush();

            $logoUploaded = true;
        }

        return new JsonResponse([
            'logoUploaded' => $logoUploaded,
            'logoEnabled' => $guild->getCustomCSSEnabled()
        ]);
    }

    /**
     * Use a custom CSS
     *
     * @param string $subdomain
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function updateStyleAction($subdomain, Request $request)
    {
        $guild = $this->get('get.guild')->getGuild($subdomain);

        $permissionsManager = $this->get('permissions.manager')->setGuild($guild)->setUser($this->getUser());
        $permissionsManager->must(['CanEditGuildStyle']);

        $form = $this->getCSSForm($guild);
        $css = null;

        $form->handleRequest($request);
        if ($form->isValid()) {
            $css = $this->get('guild.css')->updateGuildCSS($guild, $form->getData());

            $this->addFlash('success', 'Your custom CSS has been successfully saved! Please, verify that what we saved on our server match with what you sent us, if it does not, it means that you used some CSS properties that aren\'t allowed.');
        }

        return $this->render('RizzenGuildBundle:Admin:customCSS.html.twig', [
            'guild' => $guild,
            'subdomain' => $subdomain,
            'permissions' => $permissionsManager->getPermissions(),
            'css' => $css,
            'form' => $form->createView()
        ]);
    }

    /**
     * Get the custom CSS form
     *
     * @param Guild $guild
     * @return \Symfony\Component\Form\Form
     */
    private function getCSSForm(Guild $guild)
    {
        $currentData = $this->get('guild.css')->getCurrentCSS($guild);

        return $this->createFormBuilder($currentData)
            ->add('css', TextareaType::class, [
                'constraints' => [
                    new NotBlank(),
                    new Length([
                        'max' => 10000
                    ]),
                ]
            ])
            ->add('enabled', CheckboxType::class, [
                'required' => false
            ])
            ->getForm();
    }
}
