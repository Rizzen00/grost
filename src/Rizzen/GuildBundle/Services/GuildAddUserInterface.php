<?php

namespace Rizzen\GuildBundle\Services;


use Rizzen\GuildBundle\Entity\Guild;

interface GuildAddUserInterface
{
    /**
     * @param Guild $guild      Guild where the user will be added
     * @param int $userId       ID of the user to add
     * @return mixed
     */
    public function addUser(Guild $guild, $userId);
} 
