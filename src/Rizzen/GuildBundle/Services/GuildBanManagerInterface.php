<?php

namespace Rizzen\GuildBundle\Services;


use Rizzen\UserBundle\Entity\User;
use Rizzen\GuildBundle\Entity\Guild;

interface GuildBanManagerInterface
{
    /**
     * Ban an user from a guild
     *
     * @param int $userId           ID of the user to ban
     * @param Guild $guild          Guild where the user will be banned from
     * @param User $currentUser     User that issued the ban
     * @return array
     */
    public function banFromGuild($userId, Guild $guild, User $currentUser);

    /**
     * @param int $userId           ID of the user to ban
     * @param Guild $guild          Guild where the user will be banned from, will only be banned from the forum
     * @param $currentUser          User that issued the ban
     * @return array
     */
    public function banFromForum($userId, Guild $guild, $currentUser);

    /**
     * @param User $user            User that might be in the ban list of a guild
     * @param Guild $guild          Guild where the search will be made
     * @return array                The array will contain the BannedUser object if the user is banned, and a clean one if he isn't
     */
    public function isInBanList(User $user, Guild $guild);

    /**
     * @param int $userId           ID of the user to unban
     * @param Guild $guild          Guild where the user will be unbanned from
     * @param string $banToUpdate   Can be both, guild of forum
     * @return array
     */
    public function removeBan($userId, Guild $guild, $banToUpdate);
} 
