<?php

namespace Rizzen\GuildBundle\Services;

use Rizzen\GuildBundle\Entity\Guild;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;


interface GetGuildInterface
{

    /**
     * Get the guild linked to a subdomain
     *
     * @param string $subdomain                 The slug of the guild
     * @param bool $throw                       if true it will throw an exception if the guild is not found
     * @return Guild|bool
     * @throws NotFoundHttpException
     */
    public function getGuild($subdomain, $throw);

    /**
     * Get the guild for the dashboard, the difference from the normal getGuild
     * is that the query is getting the forum, forum categories, calendar and the blog with it
     *
     * @param string $subdomain                 The slug of the guild
     * @param bool $throw                       if true it will throw an exception if the guild is not found
     * @return Guild
     * @throws NotFoundHttpException
     */
    public function getGuildForDashboard($subdomain, $throw);

} 
