<?php

namespace Rizzen\GuildBundle\Services;

use Doctrine\ORM\EntityManager;


use Rizzen\BlogBundle\Entity\Blog;

use Rizzen\BlogBundle\Entity\Category;
use Rizzen\CalendarBundle\Entity\Calendar;
use Rizzen\ForumBundle\Entity\Forum;
use Rizzen\GuildBundle\Entity\Guild;
use Rizzen\GuildBundle\Permissions\RoleCreator;
use Rizzen\MainBundle\Services\Slugify;
use Rizzen\UserBundle\Entity\User;

/**
 * Class GuildCreator
 * @author Bryan Haag <misios11@hormail.fr>
 * @package Rizzen\GuildBundle\Services
 */
class GuildCreator implements GuildCreatorInterface
{
    private $em;
    private $slugify;
    private $roleCreator;
    private $getGuild;

    /**
     * @param EntityManager $em
     * @param RoleCreator $roleCreator
     * @param GetGuild $getGuild
     */
    public function __construct(EntityManager $em, RoleCreator $roleCreator, GetGuild $getGuild)
    {
        $this->em = $em;
        $this->slugify = new Slugify();
        $this->roleCreator = $roleCreator;
        $this->getGuild = $getGuild;
    }

    /**
     * {@inheritdoc}
     */
    public function create(Guild $guild, User $user)
    {
        $calendar = new Calendar();
        $blog = new Blog();
        $forum = new Forum();
        $blogCategory = new Category();


        $guild->setSlug($this->slugify->getSluged($guild->getTitle()));

        if ($this->getGuild->getGuild($guild->getSlug(), false) || $this->isNameRestricted($guild->getTitle())) {
            return [
                'created' => false,
                'alreadyTaken' => true
            ];
        }

        $guild->setCreator($user);
        $guild->addMember($user);

        $forum->setTitle($guild->getTitle());
        $blog->setTitle($guild->getTitle());

        $guild->setForum($forum);
        $guild->setBlog($blog);

        $blogCategory->setTitle('General');
        $blogCategory->setBlog($blog);
        $blogCategory->setEditable(false);

        $blog->addCategory($blogCategory);

        $guild->setCalendar($calendar);
        $calendar->setGuild($guild);

        $forum->setGuild($guild);
        $blog->setGuild($guild);

        $forum->addMember($user);

        $user->addGuild($guild);
        $user->addForum($forum);

        $em = $this->em;
        $em->persist($guild);
        $em->persist($forum);
        $em->persist($blog);
        $em->persist($calendar);
        $em->persist($blogCategory);
        $em->flush();

        $blogCategory->setSlug($this->slugify->getSluged('General-' . $blogCategory->getId()));

        $this->roleCreator->generateBaseRoles($guild, $user);

        return [
            'created' => true,
            'slug' => $guild->getSlug()
        ];
    }

    /**
     * Check if the guild name isn't restricted
     *
     * @param string $name      The name of the guild the user is trying to create
     * @return bool
     */
    private function isNameRestricted($name)
    {
        // List containing popular swears and names that we wouldn't want used as subdomains
        $restrictedNames = [
            'admin',
            'admins',
            'administration',
            'administrator',
            'administrators',
            'manager',
            'managers',
            'management',
            'guild',
            'guilds',
            'group',
            'groups',
            'demo',
            'demos',
            'secure',
            'security',
            'mod',
            'moderator',
            'moderators',
            'test',
            'demonstration',
            'shit',
            'fuck',
            'dick',
            'fucker',
            'stat',
            'stats',
            'moderateur',
            'ssl',
            'health',
            'server',
            'servers',
            'support',
            'report',
            'reports',
            'forum',
            'forums',
            'blog',
            'blogs',
            'calendar',
            'calendars',
            'event',
            'events',
            'messenger',
            'messengers',
            'reserved',
            'site',
            'privacy',
            'ticket',
            'tickets',
            'help',
            'faq',
            'updates',
            'update',
            'twitter',
            'facebook',
            'contact',
            'features',
            'testing',
            'alpha',
            'beta',
            'prod',
            'production',
            'live',
            'cdn',
            'content',
            'cache',
            'grost-demo',
            'grost-admin',
            'grost-administration',
            'demo-grost',
            'admin-grost',
            'administration-grost',
            'password',
            '2-step',
            'authentication',
            'auth',
            'redis',
            'database',
            'data',
            'image',
            'images',
            'media',
            'notification',
            'notifications',
            'verify',
            'verification',
            'ass',
            'xxx',
            'asshole',
            'mofo',
            'motherfucker',
            'bitch',
            'cunt',
            'dickhead',
            'nazi',
            'bug',
            'bugs',
            'issues',
            'issue',
            'static',
            'private',
            'public',
            'gro_st',
            'gro-st',
            'www',
            'wwww'
        ];

        if (in_array($name, $restrictedNames) || in_array($this->slugify->getSluged($name), $restrictedNames)) {
            return true;
        }

        return false;
    }

}
