<?php

namespace Rizzen\GuildBundle\Services;

use Predis\Client;
use Rizzen\GuildBundle\Entity\Guild;
use Rizzen\SocketBundle\Services\SocketProcessor;
use Rizzen\UserBundle\Entity\User;

/**
 * Class used to post and get messages from the chat of a guild
 *
 * Class ChatManager
 * @package Rizzen\GuildBundle\Services
 */
class ChatManager
{

    private $redis;
    private $socketProcessor;

    /**
     * @param Client $redis
     * @param SocketProcessor $socketProcessor
     */
    function __construct(Client $redis, SocketProcessor $socketProcessor)
    {
        $this->redis = $redis;
        $this->socketProcessor = $socketProcessor;
    }

    /**
     * Post a message in the guild chat
     *
     * @param User $user        The user that is posting the message
     * @param Guild $guild      Guild where the chat message will be posted
     * @param Array $data       The form data
     * @return void
     */
    public function postMessage(User $user, Guild $guild, $data)
    {

        $currentDate = new \DateTime('now');
        $currentDate->setTimezone(new \DateTimeZone('UTC'));

        $data = [
            'message' => htmlspecialchars($data['message']),
            'sentBy' => $user->getUsername(),
            'timestamp' => $currentDate->getTimestamp()
        ];

        $key = $this->getKey($guild);

        $pipe = $this->redis->pipeline();

        $pipe->lpush($key, json_encode($data));
        $pipe->ltrim($key, 0, 32);

        $pipe->execute();

        $this
            ->socketProcessor
            ->addTopic($this->getKey($guild))
            ->setSubtopic('guildChatMessage')
            ->setAdditionalInfo($data)
            ->useTemplate('raw', null)
            ->broadcast();
    }

    /**
     * Get the chat messages of a guild
     *
     * @param Guild $guild
     * @return array
     */
    public function getChatMessages(Guild $guild)
    {
        $chatMessages = $this->redis->lrange($this->getKey($guild), 0, -1);

        $messages = [];
        foreach ($chatMessages as $message) {
            $messages[] = json_decode($message, true);
        }

        return array_reverse($messages);

    }

    /**
     * Get the redis key of the guild chat
     *
     * @param Guild $guild
     * @return string
     */
    private function getKey(Guild $guild)
    {
        return 'presence-guild-chat-' . $guild->getSlug();
    }
}
