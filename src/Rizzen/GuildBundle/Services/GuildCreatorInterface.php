<?php

namespace Rizzen\GuildBundle\Services;

use Rizzen\GuildBundle\Entity\Guild;
use Rizzen\UserBundle\Entity\User;

interface GuildCreatorInterface {

    /**
     * Create a new guild
     *
     * @param Guild $guild          Guild object from the form
     * @param User $user            User that is creating the guild
     * @return
     */
    public function create(Guild $guild, User $user);

} 
