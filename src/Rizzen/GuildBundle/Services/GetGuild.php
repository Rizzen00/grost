<?php

namespace Rizzen\GuildBundle\Services;

use Doctrine\ORM\EntityManager;
use Rizzen\GuildBundle\Entity\Guild;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class GetGuild
 * @author Bryan Haag <misios11@hotmail.fr>
 * @package Rizzen\GuildBundle\Services
 */
class GetGuild implements GetGuildInterface
{
    private $em;

    /**
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }


    /**
     * {@inheritdoc}
     */
    public function getGuild($subdomain, $throw = true)
    {
        $guild = $this->em->getRepository('RizzenGuildBundle:Guild')->findOneBySlug($subdomain);
        return $this->guildResult($guild, $throw);
    }

    /**
     * {@inheritdoc}
     */
    public function getGuildForDashboard($subdomain, $throw = true)
    {
        $guild = $this->em->getRepository('RizzenGuildBundle:Guild')->getGuildForDashboard($subdomain);
        return $this->guildResult($guild, $throw);
    }

    /**
     * @param Guild $guild      The guild from the database
     * @param bool $throw       Throw an exception if the guild was not found
     * @return bool|Guild
     */
    private function guildResult(Guild $guild = null, $throw = true)
    {
        if ($guild === null && $throw === true) {
            throw new NotFoundHttpException('The guild was not found');
        } else if ($guild === null && $throw === false) {
            return false;
        }

        if ($throw === false) {
            return true;
        }

        return $guild;
    }

}
