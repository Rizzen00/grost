<?php

namespace Rizzen\GuildBundle\Services;

use Doctrine\ORM\EntityManager;
use Rizzen\GuildBundle\Entity\Guild;
use Rizzen\MainBundle\Services\Purifier;

class GuildCSS
{

    private $em;
    private $purifier;

    /**
     * @param EntityManager $em
     * @param Purifier $purifier
     */
    function __construct(EntityManager $em, Purifier $purifier)
    {
        $this->em = $em;
        $this->purifier = $purifier;
    }

    /**
     * Save the new CSS on the server, keep an original copy and minify another.
     *
     * @param Guild $guild          Guild where the CSS will be applied
     * @param array $style          An array containing the CSS and if the custom style is enabled
     * @return string
     */
    public function updateGuildCSS(Guild $guild, $style)
    {
        $css = $style['css'];
        $purifiedCSS = $this->purifier->purifyCSS($css);

        $guild->setCustomCSSEnabled($style['enabled']);

        $fileNames = $this->getFileNames($guild);

        /* Write the original purified user CSS in a file */
        file_put_contents($this->getCSSFolder($guild) . $fileNames['full'], $this->getCSSBodyRule($css) . $purifiedCSS);

        $purifiedCSS = $this->wrapThenCompile($purifiedCSS);
        $purifiedCSS = $this->minifyCSS($this->getCSSBodyRule($css) . $purifiedCSS);

        /* Write the minified user CSS in a file */
        file_put_contents($this->getCSSFolder($guild) . $fileNames['minified'], $purifiedCSS);

        $this->deleteOldStyles($guild, $fileNames);

        $guild->setCustomCSSFile($this->getWebPath($guild, $fileNames['minified']));

        $this->em->flush();

        return $purifiedCSS;
    }

    /**
     * Do a quick CSS minification
     *
     * @param $css string       The CSS to minify
     * @return string           The minified CSS
     */
    private function minifyCSS($css)
    {
        // Very basic minification, it should be cached and compressed by cloudflare anyway
        $css = preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*/!', '', $css);
        $css = str_replace(': ', ':', $css);
        $css = str_replace(["\r\n", "\r", "\n", "\t", '  ', '    ', '    '], '', $css);

        return $css;
    }

    /**
     * Wrap the CSS with a .stylable class, then compile it with SASS
     *
     * @param $css string       The CSS to wrap
     * @return string           The compiled CSS
     */
    private function wrapThenCompile($css)
    {
        $css = '.stylable{' . $css . '}';

        $sass = new \Sass();
        return $sass->compile($css);
    }

    /**
     * Get the folder where the CSS file will be saved, create it if id does not exist
     *
     * @param Guild $guild
     * @return string
     */
    private function getCSSFolder(Guild $guild)
    {
        $cssFolder = __DIR__.'/../../../../web/media/upload/guild/' . hash('adler32', $guild->getId()) . '_' . $guild->getId() . '/css/';

        if (!file_exists($cssFolder)) {
            mkdir($cssFolder, 0777, true);
        }

        return $cssFolder;
    }

    /**
     * Remove the old CSS files that might have been created before
     *
     * @param Guild $guild
     * @param $filesCreated string     Name of the new css files created
     */
    private function deleteOldStyles(Guild $guild, $filesCreated)
    {
        $folder = $this->getCSSFolder($guild);
        foreach (scandir($folder) as $fileName) {
            if ($fileName !== $filesCreated['full'] && $fileName !== $filesCreated['minified'] && $fileName !== '.' && $fileName !== '..') {
                unlink($folder . $fileName);
            }

        }
    }

    /**
     * Generate the name of the files
     *
     * @param Guild $guild
     * @return mixed
     */
    private function getFileNames(Guild $guild)
    {
        $baseName = hash('adler32', $guild->getId()) . '_' . hash('adler32', uniqid());

        $name['full'] = $baseName . '.css';
        $name['minified'] = $baseName . '.min.css';

        return $name;
    }

    /**
     * Get the path to the CSS usable in the views
     *
     * @param Guild $guild
     * @param $fileName
     * @return string
     */
    private function getWebPath(Guild $guild, $fileName)
    {
        return 'media/upload/guild/' . hash('adler32', $guild->getId()) . '_' . $guild->getId() . '/css/' . $fileName;
    }

    /**
     * Get the current CSS of the guild, and if the CSS is enabled or not
     *
     * @param Guild $guild
     * @return array
     */
    public function getCurrentCSS(Guild $guild)
    {
        if ($guild->getCustomCSSFile() !== null) {
            $fileName = __DIR__.'/../../../../web/' . $guild->getCustomCSSFile();
            $fileName = str_replace('.min.css', '.css', $fileName);

            if (file_exists($fileName)) {
                $style['css'] = file_get_contents($fileName);
            } else {
                $style['css'] = null;
            }
        } else {
            $style['css'] = null;
        }

        $style['enabled'] = $guild->getCustomCSSEnabled();

        return $style;
    }

    /**
     * Check if there is a body definition in the CSS, if there is keep all the background* properties that are in it and return the definition.
     *
     * @param $input string             The CSS
     * @return null|string
     */
    private function getCSSBodyRule($input)
    {
        $bodyRules = null;
        // The purification will remove the body tag, so to let the user modify the site background, we try to find it and keep it here.
        preg_match_all( '/(?ims)([a-z0-9\s\,\.\:#_\-@]+)\{([^\}]*)\}/', $input, $matches);
        foreach ($matches[0] as $key => $match)
        {
            $selector = trim($matches[1][$key]);

            if ($selector !== 'body') {
                continue;
            }

            $rules = explode(';', trim($matches[2][$key]));

            foreach ($rules as $cssRule)
            {
                if (!empty($cssRule))
                {
                    $rule = explode(":", $cssRule);

                    if ($rule[0] === 'background' ||
                        $rule[0] === 'background-color' ||
                        $rule[0] === 'background-image' ||
                        $rule[0] === 'background-position' ||
                        $rule[0] === 'background-size' ||
                        $rule[0] === 'background-repeat' ||
                        $rule[0] === 'background-origin' ||
                        $rule[0] === 'background-clip' ||
                        $rule[0] === 'background-attachment') {

                        $bodyRules[] = trim($rule[0]) . ':' . trim($rule[1]) . ';';

                    }

                }
            }

            if ($selector === 'body') {
                // return the CSS, the PHP_EOL is just to keep the CSS readable when displayed to the user, we'll minify it anyway
                return 'body {' . PHP_EOL . implode('', $bodyRules) . PHP_EOL . '}' . PHP_EOL . PHP_EOL;
            }
        }
        return null;
    }

}
