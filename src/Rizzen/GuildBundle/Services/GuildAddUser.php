<?php

namespace Rizzen\GuildBundle\Services;


use Doctrine\ORM\EntityManager;
use Rizzen\GuildBundle\Entity\Guild;
use Rizzen\GuildBundle\Permissions\PermissionsManager;

class GuildAddUser implements GuildAddUserInterface
{

    private $em;
    private $permissionsManager;

    /**
     * @param EntityManager $em
     * @param PermissionsManager $permissionsManager
     */
    public function __construct(EntityManager $em, PermissionsManager $permissionsManager)
    {
        $this->em = $em;
        $this->permissionsManager = $permissionsManager;
    }

    /**
     * {@inheritdoc}
     */
    public function addUser(Guild $guild, $userId)
    {
        $user = $this->em->getRepository('RizzenUserBundle:User')->find($userId);
        if ($user === null) {
            return ['hasBeenAdded' => false];
        }

        $this->permissionsManager->setUser($user)->setGuild($guild);
        $this->permissionsManager->clearLocalPermissions();

        if ($this->permissionsManager->getPermissions()->isBannedFromGuild() ||
            $this->permissionsManager->isGuildMember(false) ||
            $this->permissionsManager->isForumMember(false) === false) {
            return ['hasBeenAdded' => false];
        }

        $guild->addMember($user);
        $user->addGuild($guild);

        $this->em->persist($guild);
        $this->em->persist($user);
        $this->em->flush();

        $this->permissionsManager->removePermissionsFromCache();
        $this->permissionsManager->clearLocalPermissions();

        return ['hasBeenAdded' => true];
    }

} 
