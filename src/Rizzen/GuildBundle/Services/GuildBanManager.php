<?php

namespace Rizzen\GuildBundle\Services;


use Doctrine\ORM\EntityManager;
use Rizzen\GuildBundle\Entity\Guild;
use Rizzen\GuildBundle\Entity\BannedUser;
use Rizzen\GuildBundle\Permissions\PermissionsManager;
use Rizzen\UserBundle\Entity\User;

class GuildBanManager implements GuildBanManagerInterface{

    private $em;
    private $permissionsManager;

    /**
     * @param EntityManager $em
     * @param PermissionsManager $permissionsManager
     */
    public function __construct(EntityManager $em, PermissionsManager $permissionsManager)
    {
        $this->em = $em;
        $this->permissionsManager = $permissionsManager;
    }

    /**
     * {@inheritdoc}
     */
    public function banFromGuild($userId, Guild $guild, User $currentUser)
    {
        $user = $this->em->getRepository('RizzenUserBundle:User')->find($userId);

        $this->permissionsManager->clearLocalPermissions();
        $this->permissionsManager->setUser($user)->setGuild($guild);

        if ($user === null || !$this->permissionsManager->isGuildMember(false)) {
            return ['hasBeenBanned' => false];
        }

        $isInBanList = $this->isInBanList($user, $guild);
        $bannedUser = $isInBanList['bannedUser'];

        $bannedUser->setBannedFromGuild(true);
        $bannedUser->setGuildBanIssuedBy($currentUser);

        if (!$isInBanList['hasActiveBan']) {
            $bannedUser->setGuild($guild);
            $bannedUser->setUser($user);

            $guild->addBannedUser($bannedUser);
        }

        $guild->removeMember($user);
        $user->removeGuild($guild);

        $this->em->persist($bannedUser);
        $this->em->persist($guild);
        $this->em->persist($user);

        $this->em->flush();

        $this->permissionsManager->removePermissionsFromCache();

        return ['hasBeenBanned' => true];
    }

    /**
     * {@inheritdoc}
     */
    public function banFromForum($userId, Guild $guild, $currentUser)
    {
        $user = $this->em->getRepository('RizzenUserBundle:User')->find($userId);

        $this->permissionsManager->setUser($user)->setGuild($guild);

        if ($user === null || !$this->permissionsManager->isForumMember(false)) {
            return ['hasBeenBanned' => false];
        }

        $isInBanList = $this->isInBanList($user, $guild);
        $bannedUser = $isInBanList['bannedUser'];

        $bannedUser->setBannedFromGuild(true);
        $bannedUser->setBannedFromForum(true);
        $bannedUser->setGuildBanIssuedBy($currentUser);
        $bannedUser->setForumBanIssuedBy($currentUser);

        if (!$isInBanList['hasActiveBan']) {
            $bannedUser->setGuild($guild);
            $bannedUser->setUser($user);

            $guild->addBannedUser($bannedUser);
        }

        $guild->removeMember($user);
        $user->removeGuild($guild);

        $forum = $guild->getForum();
        $forum->removeMember($user);
        $user->removeForum($forum);

        $roleAssignments = $this->em->getRepository('RizzenGuildBundle:RoleAssignment')->getRolesOfUser($user, $guild);

        foreach ($roleAssignments as $assignment) {
            $this->em->remove($assignment);
        }

        $this->em->persist($bannedUser);
        $this->em->persist($guild);
        $this->em->persist($forum);
        $this->em->persist($user);

        $this->em->flush();

        $this->permissionsManager->removePermissionsFromCache();

        return ['hasBeenBanned' => true];
    }

    /**
     * {@inheritdoc}
     */
    public function isInBanList(User $user, Guild $guild)
    {

        $result = $this->em->getRepository('RizzenGuildBundle:BannedUser')->findUserInBannedListOrNull($user, $guild);

        if ($result === null) {
            return ['hasActiveBan' => false,
                    'bannedUser' => new BannedUser()];
        } else {
            return ['hasActiveBan' => true,
                    'bannedUser' => $result];
        }
    }

    /**
     * {@inheritdoc}
     */
    public function removeBan($userId, Guild $guild, $banToUpdate)
    {
        $user = $this->em->getRepository('RizzenUserBundle:User')->find($userId);
        if ($user === null) {
            return ['hasBeenUnbanned' => false];
        }

        $ban = $this->isInBanList($user, $guild);
        if ($ban['hasActiveBan'] === false) {
            return ['hasBeenUnbanned' => false];
        }

        /** @var BannedUser $ban */
        $ban = $ban['bannedUser'];

        if ($banToUpdate === 'both') {

            $this->removeGuildAndForumBan($ban, $guild);

        } elseif ($banToUpdate === 'guild') {

            if ($ban->getBannedFromForum() === false) {
                $this->removeGuildAndForumBan($ban, $guild);
            } else {
                $ban->setBannedFromGuild(false);
                $ban->setGuildBanIssuedBy(null);
            }

        } elseif ($banToUpdate === 'forum') {

            if ($ban->getBannedFromGuild() === false) {
                $this->removeGuildAndForumBan($ban, $guild);
            } else {
                $ban->setBannedFromForum(false);
                $ban->setForumBanIssuedBy(null);
            }

        }

        $this->em->flush();

        return ['hasBeenUnbanned' => true];
    }

    /**
     * Remove both bans
     *
     * @param BannedUser $ban
     * @param Guild $guild
     */
    private function removeGuildAndForumBan(BannedUser $ban, Guild $guild)
    {
        $guild->removeBannedUser($ban);
        $this->em->remove($ban);
    }

} 
