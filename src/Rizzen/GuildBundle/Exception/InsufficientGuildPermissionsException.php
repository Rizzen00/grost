<?php

namespace Rizzen\GuildBundle\Exception;


use Symfony\Component\HttpKernel\Exception\HttpException;

class InsufficientGuildPermissionsException extends HttpException implements RizzenGuildBundleExceptionInterface
{
    /**
     * Constructor.
     *
     * @param string     $message  The internal exception message
     * @param \Exception $previous The previous exception
     * @param int        $code     The internal exception code
     */
    public function __construct($message = 'Your roles does not let you access this page or perform this action', \Exception $previous = null, $code = 0)
    {
        parent::__construct(403, $message, $previous, [], $code);
    }
}
