<?php
/**
 * Created by PhpStorm.
 * User: bryan
 * Date: 18/07/15
 * Time: 17:55
 */

namespace Rizzen\GuildBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Rizzen\GuildBundle\Entity\Guild;
use Rizzen\UserBundle\Entity\User;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;


class LoadGuildData extends AbstractFixture implements OrderedFixtureInterface, FixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;
    private $userManager;

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
        $this->userManager = $this->container->get('fos_user.user_manager');

        $guildCreator = $this->container->get('guild.creator');

        /** @var User $user */
        $user = $this->userManager->findUserBy(['username' => 'testUser0']);

        $guild = new Guild();
        $guild->setTitle('TestGuild');

        $guildCreator->create($guild, $user);
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 2;
    }
}
