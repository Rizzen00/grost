<?php

namespace Rizzen\ProfileBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Cover
 *
 * @ORM\Table()
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class Cover
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetimetz")
     */
    private $date;

    /**
     * @var string
     *
     * @ORM\Column(name="webPath", type="string", length=255, nullable=true)
     */
    private $webPath;

    /**
     * @var string
     *
     * @ORM\Column(name="path", type="string", length=255, nullable=true)
     */
    private $path;

    /**
     * @Assert\Image(maxSize="1M")
     */
    public $file;

    /**
     * @ORM\OneToOne(targetEntity="Rizzen\UserBundle\Entity\User", inversedBy="cover")
     */
    protected $user;

    /**
     * @var boolean
     *
     * @ORM\Column(name="defaultCover", type="boolean")
     */
    protected $defaultCover;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param string $date
     * @return Cover
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return string
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set webPath
     *
     * @param string $webPath
     * @return Cover
     */
    public function setWebPath($webPath)
    {
        $this->webPath = $webPath;

        return $this;
    }

    public function __construct()
    {
        $this->defaultCover = true;
        $this->path = '/img/user/default/cover/default.jpg';
        $this->date = new \DateTime('now');
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpload()
    {
        if (null !== $this->file) {
            list($this->width, $this->height) = getimagesize($this->file);
            $this->path = sha1(uniqid(mt_rand(), true)) . '_' . mt_rand(1000000, 9999999) . '.' . $this->file->guessExtension();

            $this->webPath = $this->getWebPath();
        }
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload()
    {
        if (null === $this->file) {
            return;
        }

        $this->file->move($this->getUploadRootDir(), $this->path);

        unset($this->file);
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeUpload()
    {
        if ($file = $this->getAbsolutePath() || !$this->isDefaultCover()) {
            unlink($file);
        }
    }

    public function getAbsolutePath()
    {
        return null === $this->path ? null : $this->getUploadRootDir() . '/' . $this->path;
    }

    public function getWebPath()
    {
        return null === $this->path ? null : $this->getUploadDir() . '/' . $this->path;
    }

    protected function getUploadRootDir()
    {
        return __DIR__.'/../../../../web/' . $this->getUploadDir();
    }

    protected function getUploadDir()
    {
        if (!$this->isDefaultCover()) {
            return 'media/upload/user/' . hash('adler32', $this->getUser()->getUsername()) . '_' . $this->getUser()->getId() . '/cover';
        } else {
            return null;
        }
    }

    /**
     * Set path
     *
     * @param string $path
     * @return Cover
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * Get path
     *
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Set user
     *
     * @param \Rizzen\UserBundle\Entity\User $user
     * @return Cover
     */
    public function setUser(\Rizzen\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Rizzen\UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set defaultCover
     *
     * @param boolean $defaultCover
     * @return Cover
     */
    public function setDefaultCover($defaultCover)
    {
        $this->defaultCover = $defaultCover;

        return $this;
    }

    /**
     * Get defaultCover
     *
     * @return boolean 
     */
    public function isDefaultCover()
    {
        return $this->defaultCover;
    }
}
