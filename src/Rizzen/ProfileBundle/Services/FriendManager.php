<?php


namespace Rizzen\ProfileBundle\Services;


use Doctrine\ORM\EntityManager;
use Rizzen\ProfileBundle\FriendRequestStates;
use Rizzen\SocketBundle\Services\Notify;
use Rizzen\UserBundle\Entity\FriendRequest;
use Rizzen\UserBundle\Entity\User;

class FriendManager implements FriendManagerInterface
{

    private $em;
    private $notify;

    function __construct(EntityManager $em, Notify $notify)
    {
        $this->em = $em;
        $this->notify = $notify;
    }


    /**
     * {@inheritdoc}
     */
    public function sendFriendRequest(User $currentUser, $targetUsername)
    {
        $targetUsername = mb_convert_case($targetUsername, MB_CASE_LOWER, mb_detect_encoding($targetUsername));

        $target = $this->em->getRepository('RizzenUserBundle:User')->findOneByUsernameCanonical($targetUsername);

        if ($target === null || $target === $currentUser || $this->areFriends($currentUser, $target)) {
            throw new \Exception("We weren't able to send a friend request to this user.");
        }

        $friendRequest = new FriendRequest();

        $friendRequest->setFrom($currentUser);
        $friendRequest->setTo($target);

        $this->em->persist($friendRequest);
        $this->em->flush();

        $this->notify->addUser($target);
        $this->notify->setContent('New Friend Request', 'You received a new friend request, click the "Request" link in the menu to see it.');
        $this->notify->setStyle('info', 'fa fa-user');
        $this->notify->notify();

    }

    /**
     * {@inheritdoc}
     */
    public function answerRequest($requestId, User $currentUser, $newState)
    {
        $newState = intval($newState);

        $friendRequest = $this->em->getRepository('RizzenUserBundle:FriendRequest')->find($requestId);

        if ($friendRequest === null ||
            $friendRequest->getTo() !== $currentUser ||
            $friendRequest->getState() !== FriendRequestStates::REQUEST_PENDING ||
            $newState > 3) {
            throw new \Exception("Unable to update the friend request.");
        }

        if ($newState === FriendRequestStates::REQUEST_ACCEPTED) {

            $this->notify->setContent('Request accepted!', $friendRequest->getFrom()->getUsername() . ' accepted your friend request!');

            $friendRequest->setState(FriendRequestStates::REQUEST_ACCEPTED);
            $currentUser->addFriend($friendRequest->getFrom());
            $friendRequest->getFrom()->addFriend($currentUser);

        } elseif ($newState === FriendRequestStates::REQUEST_REFUSED) {

            $this->notify->setContent('New Friend Request', $friendRequest->getFrom()->getUsername() . ' refused your friend request!');

            $friendRequest->setState(FriendRequestStates::REQUEST_REFUSED);
        }

        $this->em->flush();

        $this->notify->addUser($friendRequest->getFrom());
        $this->notify->setStyle('info', 'fa fa-user');
        $this->notify->notify();
    }

    /**
     * {@inheritdoc}
     */
    public function areFriends(User $firstUser, User $secondUser)
    {
        if ($firstUser->getFriends()->contains($secondUser)) {
            return true;
        }

        return false;
    }
}
