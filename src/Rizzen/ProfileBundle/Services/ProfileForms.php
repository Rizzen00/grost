<?php


namespace Rizzen\ProfileBundle\Services;


use Rizzen\ProfileBundle\Entity\Avatar;
use Rizzen\ProfileBundle\Entity\Cover;
use Rizzen\ProfileBundle\Entity\Screenshot;
use Rizzen\ProfileBundle\Form\Type\AvatarType;
use Rizzen\ProfileBundle\Form\Type\CoverType;
use Rizzen\ProfileBundle\Form\Type\ScreenshotType;
use Rizzen\UserBundle\Entity\User;
use Rizzen\UserBundle\Form\Type\BioType;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\Form\FormView;

class ProfileForms
{
    private $factory;

    /**
     * @param FormFactory $factory
     */
    public function __construct(FormFactory $factory)
    {
        $this->factory = $factory;
    }

    /**
     * Generate the needed forms when the current user is visiting his own profile
     *
     * @param User $currentUser
     * @return FormView[]
     */
    public function getProfileForms(User $currentUser)
    {
        $screenshotForm = $this->factory->create(ScreenshotType::class, new Screenshot())->createView();
        $coverForm      = $this->factory->create(CoverType::class, new Cover())->createView();
        $avatarForm     = $this->factory->create(AvatarType::class, new Avatar())->createView();
        $bioForm        = $this->factory->create(BioType::class, $currentUser)->createView();

        return [
            'screenshotForm' => $screenshotForm,
            'coverForm' => $coverForm,
            'avatarForm' => $avatarForm,
            'bioForm' => $bioForm
        ];
    }
} 
