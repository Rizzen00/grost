<?php

namespace Rizzen\ProfileBundle\Services;

use Doctrine\ORM\EntityManager;
use Rizzen\ProfileBundle\Entity\Avatar;
use Rizzen\ProfileBundle\Entity\Cover;
use Rizzen\ProfileBundle\Entity\Screenshot;
use Rizzen\UserBundle\Entity\User;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class UserUploadManager
 * @package Rizzen\ProfileBundle\Services
 */
class UserUploadManager implements UserUploadManagerInterface
{
    private $em;

    /**
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * {@inheritdoc}
     */
    public function uploadScreenshot(User $user, Screenshot $screenshot, $ipAddress)
    {
        if ($this->em->getRepository('RizzenProfileBundle:Screenshot')->getVisibleScreenshotsCount($user) >= 50) {
            throw new \Exception('You have reached the upload limit of 50');
        }

        $screenshot->setCreator($user);
        $screenshot->setIpAddress($ipAddress);

        $this->em->persist($screenshot);
        $this->em->flush();

        return $screenshot;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteScreenshot(User $user, $screenshotId)
    {
        /** @var Screenshot $screenshot */
        $screenshot = $this->em->getRepository('RizzenProfileBundle:Screenshot')->find($screenshotId);

        if ($screenshot === null || $user !== $screenshot->getCreator() || !$screenshot->isVisible()) {
            throw new NotFoundHttpException('The screenshot was not found');
        }

        $this->em->remove($screenshot);
        $this->em->flush();
    }

    /**
     * {@inheritdoc}
     */
    public function updateAvatar(Avatar $avatar)
    {
        $oldAvatar = $avatar->isDefaultAvatar() ? null : $avatar->getAbsolutePath();

        $avatar->setDate(new \DateTime());
        $avatar->setDefaultAvatar(false);
        $avatar->preUpload();

        $this->em->flush();

        if (file_exists($oldAvatar) && $oldAvatar !== null) {
            unlink($oldAvatar);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function updateCover(Cover $cover)
    {
        $oldCover = $cover->isDefaultCover() ? null : $cover->getAbsolutePath();

        $cover->setDate(new \DateTime());
        $cover->setDefaultCover(false);
        $cover->preUpload();

        $this->em->flush();

        if (file_exists($oldCover) && $oldCover !== null) {
            unlink($oldCover);
        }
    }


}
