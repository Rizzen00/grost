<?php


namespace Rizzen\ProfileBundle\Services;

use Rizzen\UserBundle\Entity\User;

interface FriendManagerInterface
{
    /**
     * @param User $currentUser
     * @param string $targetUsername
     * @return mixed
     */
    public function sendFriendRequest(User $currentUser, $targetUsername);

    /**
     * @param int $requestId
     * @param User $currentUser
     * @param int $newState
     * @return mixed
     */
    public function answerRequest($requestId, User $currentUser, $newState);

    /**
     * @param User $firstUser
     * @param User $secondUser
     * @return bool
     */
    public function areFriends(User $firstUser, User $secondUser);
}
