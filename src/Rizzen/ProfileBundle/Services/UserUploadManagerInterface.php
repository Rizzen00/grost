<?php


namespace Rizzen\ProfileBundle\Services;

use Rizzen\ProfileBundle\Entity\Avatar;
use Rizzen\ProfileBundle\Entity\Cover;
use Rizzen\ProfileBundle\Entity\Screenshot;
use Rizzen\UserBundle\Entity\User;

interface UserUploadManagerInterface
{
    /**
     * User screenshot uploading
     *
     * @param User $user                Current user
     * @param Screenshot $screenshot    Screenshot to upload
     * @param string $ipAddress         IP of the current user
     * @throws \Exception
     * @return Screenshot               The uploaded screenshot
     */
    public function uploadScreenshot(User $user, Screenshot $screenshot, $ipAddress);

    /**
     * User screenshot deletion
     *
     * @param User $user                User trying to delete the screenshot
     * @param int $screenshotId         ID of the screenshot to delete
     */
    public function deleteScreenshot(User $user, $screenshotId);

    /**
     * Update the avatar
     *
     * @param Avatar $avatar            Avatar to update
     * @return void
     */
    public function updateAvatar(Avatar $avatar);

    /**
     * Update the cover
     *
     * @param Cover $cover              Cover to update
     * @return void
     */
    public function updateCover(Cover $cover);
} 
