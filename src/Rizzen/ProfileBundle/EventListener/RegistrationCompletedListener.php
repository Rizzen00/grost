<?php


namespace Rizzen\ProfileBundle\EventListener;

use Doctrine\ORM\EntityManager;
use FOS\UserBundle\Event\FilterUserResponseEvent;
use Rizzen\ProfileBundle\Entity\Avatar;
use Rizzen\ProfileBundle\Entity\Cover;
use Rizzen\UserBundle\Entity\User;

class RegistrationCompletedListener
{
    private $em;

    /**
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * Generate the necessary entities to link to the user that has registered
     *
     * @param FilterUserResponseEvent $event
     */
    public function setBaseCoverAndAvatar(FilterUserResponseEvent $event)
    {
        /** @var User $user */
        $user = $event->getUser();

        $avatar = new Avatar();
        $avatar->setUser($user);

        $cover = new Cover();
        $cover->setUser($user);

        $user->setAvatar($avatar);
        $user->setCover($cover);

        $this->em->persist($cover);
        $this->em->persist($avatar);

        $this->em->flush();
    }

} 
