<?php

namespace Rizzen\ProfileBundle\Controller;

use Rizzen\ProfileBundle\Entity\Screenshot;
use Rizzen\ProfileBundle\Form\Type\ScreenshotType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ScreenshotController extends Controller
{
    /**
     * Upload a new screenshot
     *
     * @param string $username
     * @param Request $request
     * @return JsonResponse|Response
     * @throws \Exception
     */
    public function uploadScreenshotAction($username, Request $request)
    {
        $screenshot = new Screenshot();
        $form = $this->createForm(ScreenshotType::class, $screenshot);

        $form->handleRequest($request);
        if ($form->isValid()) {

            $screenshot = $this->get('user.upload.manager')->uploadScreenshot(
                $this->getUser(),
                $screenshot,
                $request->getClientIp()
            );

            return $this->render('RizzenProfileBundle:Upload:thumbnail.html.twig', ['screenshot' => $screenshot]);
        }

        return new JsonResponse([
                'error' => 'Unable to upload the screenshot, please refresh the page and try again.'
            ], 500);

    }

    /**
     * Load more screenshot to display on the profile page
     *
     * @param string $username      Username of the profile the user is currently visiting
     * @param int $currentCount     Current number of screenshots displayed to the user
     * @return Response
     */
    public function loadMoreScreenshotsAction($username, $currentCount)
    {
        if ($currentCount < 16 || $currentCount > 49) {
            throw $this->createNotFoundException('No screenshots found');
        }

        $user = $this->getDoctrine()->getRepository('RizzenUserBundle:User')->findOneByUsername($username);
        $screenshots = $this->getDoctrine()->getRepository('RizzenProfileBundle:Screenshot')->getMoreVisibleScreenshots($user, $currentCount);
        return $this->render('RizzenProfileBundle:Upload:thumbnails.html.twig', ['screenshots' => $screenshots]);
    }

    /**
     * Delete a screenshot
     *
     * @param string $username
     * @param int $screenshotId
     * @return Response
     */
    public function deleteScreenshotAction($username, $screenshotId)
    {
        $this->get('user.upload.manager')->deleteScreenshot($this->getUser(), $screenshotId);

        return new Response();
    }
}
