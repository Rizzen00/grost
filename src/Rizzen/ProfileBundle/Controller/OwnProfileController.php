<?php

namespace Rizzen\ProfileBundle\Controller;

use Rizzen\ProfileBundle\Form\Type\AvatarType;
use Rizzen\ProfileBundle\Form\Type\CoverType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Rizzen\UserBundle\Form\Type\BioType;

class OwnProfileController extends Controller
{
    /**
     * Return the pending friends requests
     *
     * @return Response
     */
    public function requestsAction()
    {
        $friendRequests = $this->getDoctrine()->getManager()->getRepository('RizzenUserBundle:FriendRequest')->getFriendRequests($this->getUser());

        return $this->render('RizzenProfileBundle:Own:requests.html.twig', [
            'friendRequests' => $friendRequests
        ]);
    }

    /**
     * @param int $requestId
     * @param $choice
     * @return Response
     * @throws \Exception
     */
    public function processRequestAction($requestId, $choice)
    {
        $this->get('user.friend.manager')->answerRequest($requestId, $this->getUser(), $choice);

        return new Response();
    }

    /**
     * Update the about me text of the profile page
     *
     * @param Request $request
     * @return Response
     * @throws \Exception
     */
    public function updateAboutMeAction(Request $request)
    {
        $user = $this->getUser();
        $form = $this->createForm(BioType::class, $user);

        $form->handleRequest($request);
        if ($form->isValid()) {

            $purifier = $this->get('html.purifier');
            $manipulator = $this->get('text.manipulator');

            $user->setBio($purifier->purify('bio', $user->getBio()));
            $user->setBio($manipulator->addNoFollow($user->getBio()));

            $em = $this->getDoctrine()->getManager();
            $em->flush();

            return new Response();
        }

        throw new \Exception("Unable to update the about me content");
    }

    /**
     * Upload a new avatar
     *
     * @param string $username          Username of the current profile
     * @param Request $request
     * @return JsonResponse|Response
     */
    public function uploadAvatarAction($username, Request $request)
    {
        $avatar = $this->getUser()->getAvatar();
        $form = $this->createForm(AvatarType::class, $avatar);

        $form->handleRequest($request);
        if ($form->isValid()) {

            $this->get('user.upload.manager')->updateAvatar($avatar);

            return $this->render('RizzenProfileBundle:Upload:avatar.html.twig');
        }

        return new JsonResponse([
                'error' => 'Unable to upload the avatar, please refresh the page and try again.'
            ], 500);
    }

    /**
     * Upload a new cover
     *
     * @param string $username          Username of the current profile
     * @param Request $request
     * @return JsonResponse|Response
     */
    public function uploadCoverAction($username, Request $request)
    {
        $cover = $this->getUser()->getCover();
        $form = $this->createForm(CoverType::class, $cover);

        $form->handleRequest($request);
        if ($form->isValid()) {

            $this->get('user.upload.manager')->updateCover($cover);

            return $this->render('RizzenProfileBundle:Upload:cover.html.twig');
        }

        return new JsonResponse([
                'error' => 'Unable to upload the cover, please refresh the page and try again.'
            ], 500);
    }

}
