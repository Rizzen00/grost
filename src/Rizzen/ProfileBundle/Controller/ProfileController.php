<?php

namespace Rizzen\ProfileBundle\Controller;

use Rizzen\ProfileBundle\Entity\Screenshot;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Rizzen\UserBundle\Entity\User;

class ProfileController extends Controller
{
    /**
     * See the profile of an user
     *
     * @param string $username      Username of the profile to see
     * @return Response
     */
    public function seeAction($username)
    {
        $profileForms = null;
        $screenshotCount = null;

        /** @var User $profile */
        $profile = $this->getDoctrine()->getRepository('RizzenUserBundle:User')->getUserForProfile($username);
        if ($profile === null) {
            throw $this->createNotFoundException('The user was not found.');
        }

        if ($this->isGranted('IS_AUTHENTICATED_REMEMBERED') && $username === $this->getUser()->getUsername()) {
            $profileForms = $this->get('user.profile.get.forms')->getProfileForms($profile);
        }

        /** @var Screenshot[] $screenshots */
        $screenshots = $this->getDoctrine()->getRepository('RizzenProfileBundle:Screenshot')->getVisibleScreenshotsOfUserForProfile($profile);
        $screenshotCount = $this->getDoctrine()->getRepository('RizzenProfileBundle:Screenshot')->getVisibleScreenshotsCount($profile);

        return $this->render('RizzenProfileBundle:Profile:see.html.twig', [
            'profile' => $profile,
            'screenshots' => $screenshots,
            'screenshotCount' => $screenshotCount,
            'profileForms' => $profileForms
            ]);
    }

    /**
     * Send a friend request
     *
     * @param $username
     * @return Response
     * @throws \Exception
     */
    public function sendFriendRequestAction($username)
    {
        $this->get('user.friend.manager')->sendFriendRequest($this->getUser(), $username);

        return new Response();
    }
}
