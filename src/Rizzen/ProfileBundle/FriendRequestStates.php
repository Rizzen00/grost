<?php

namespace Rizzen\ProfileBundle;

class FriendRequestStates
{
    /**
     * The request has not been answered yet
     */
    const REQUEST_PENDING = 1;

    /**
     * The request has been accepted
     */
    const REQUEST_ACCEPTED = 2;

    /**
     * The request has been refused
     */
    const REQUEST_REFUSED = 3;
}
