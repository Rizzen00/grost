<?php


namespace Rizzen\BlogBundle\Services;


use Doctrine\ORM\EntityManager;
use Rizzen\BlogBundle\Entity\Category;
use Rizzen\BlogBundle\Entity\Post;
use Rizzen\GuildBundle\Entity\Guild;
use Rizzen\MainBundle\Services\Purifier;
use Rizzen\MainBundle\Services\Slugify;
use Rizzen\MainBundle\Services\TextManipulator;
use Rizzen\UserBundle\Entity\User;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Service used to process the creation and edition of all the things blog related
 *
 * Class BlogPoster
 */
class BlogPoster implements BlogPosterInterface
{
    private $em;
    private $slugify;
    private $textManipulator;
    private $purifier;

    /**
     * @param EntityManager $em
     * @param TextManipulator $textManipulator
     * @param Purifier $purifier
     */
    public function __construct(EntityManager $em, TextManipulator $textManipulator, Purifier $purifier)
    {

        $this->em = $em;
        $this->slugify = new Slugify();
        $this->textManipulator = $textManipulator;
        $this->purifier = $purifier;
    }

    /**
     * {@inheritdoc}
     */
    public function newCategory(Guild $guild, Category $category)
    {
        $blog = $guild->getBlog();

        $category->setBlog($blog);

        $blog->addCategory($category);

        $this->em->persist($category);
        $this->em->flush();

        $slug = $this->slugify->getSluged($category->getTitle() . '-' . $category->getId());
        $category->setSlug($slug);
        $this->em->flush();
    }

    /**
     * {@inheritdoc}
     */
    public function editCategory(Guild $guild, Category $category)
    {
        $blog = $guild->getBlog();

        if ($category->getBlog() !== $blog) {
            throw new AccessDeniedHttpException('This category is not present in this blog');
        }

        $slug = $this->slugify->getSluged($category->getTitle() . '-' . $category->getId());
        $category->setSlug($slug);

        $this->em->flush();
    }

    /**
     * {@inheritdoc}
     */
    public function newPost(Guild $guild, Post $post, User $user)
    {
        $blog = $guild->getBlog();

        $post->setCreator($user);

        $content = $post->getContent();
        $content = $this->purifier->purify('default', $content);
        $content = $this->textManipulator->addNoFollow($content);

        $post->setContent($content);

        $post->setBlog($blog);
        $blog->addPost($post);

        $this->em->persist($post);
        $this->em->flush();

        $slug = $this->slugify->getSluged($post->getTitle() . '-' . $post->getId());

        $post->setSlug($slug);
        $this->em->flush();
    }

    /**
     * {@inheritdoc}
     */
    public function editPost(Guild $guild, Post $post)
    {
        if ($guild->getBlog() !== $post->getBlog()) {
            throw new NotFoundHttpException('The blog post was not created in the current guild.');
        }

        $content = $post->getContent();
        $content = $this->purifier->purify('default', $content);
        $content = $this->textManipulator->addNoFollow($content);

        $post->setContent($content);

        $this->em->flush();
    }
}
