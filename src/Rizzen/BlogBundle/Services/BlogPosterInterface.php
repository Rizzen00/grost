<?php

namespace Rizzen\BlogBundle\Services;


use Rizzen\BlogBundle\Entity\Category;
use Rizzen\BlogBundle\Entity\Post;
use Rizzen\GuildBundle\Entity\Guild;
use Rizzen\UserBundle\Entity\User;

interface BlogPosterInterface
{

    /**
     * Create a new category
     *
     * @param Guild $guild              Guild where the new category will be created
     * @param Category $category        Category object to process
     * @return void
     */
    public function newCategory(Guild $guild, Category $category);

    /**
     * Edit an existing category
     *
     * @param Guild $guild              Guild where the category is located
     * @param Category $category        Category to edit
     * @return void
     */
    public function editCategory(Guild $guild, Category $category);

    /**
     * Create a new post
     *
     * @param Guild $guild              Guild where the post will be posted
     * @param Post $post                Post object to process
     * @param User $user                The user that is writing the post
     * @return void
     */
    public function newPost(Guild $guild, Post $post, User $user);

    /**
     * Edit an already existing post
     *
     * @param Guild $guild              Guild where the post is located
     * @param Post $post                Post to edit
     * @return void
     */
    public function editPost(Guild $guild, Post $post);

} 
