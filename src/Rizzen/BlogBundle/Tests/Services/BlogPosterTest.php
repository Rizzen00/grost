<?php

namespace Rizzen\BlogBundle\Tests\Services;

use Rizzen\BlogBundle\Entity\Category;
use Rizzen\BlogBundle\Entity\Post;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Rizzen\BlogBundle\Services\BlogPoster;
use Doctrine\ORM\EntityManager;

class BlogPosterTest extends WebTestCase
{
    /**
     * @var BlogPoster $blogPoster
     */
    private $blogPoster;

    /**
     * @var EntityManager $em
     */
    private $em;

    public function setUp()
    {
        static::$kernel = static::createKernel();
        static::$kernel->boot();
        $this->blogPoster = static::$kernel->getContainer()->get('blog.poster');
        $this->em = static::$kernel->getContainer()->get('doctrine.orm.entity_manager');
    }

    public function testNewCategory()
    {
        $category = new Category();
        $category->setTitle('DevCategory');

        $guild = $this->em->getRepository('RizzenGuildBundle:Guild')->findOneByTitle('TestGuild');

        $this->blogPoster->newCategory($guild, $category);

        $result = $this->em->getRepository('RizzenBlogBundle:Category')->findOneByTitle('DevCategory');

        $this->assertNotNull($result);
    }

    public function testEditCategory()
    {
        /** @var Category $category */
        $category = $this->em->getRepository('RizzenBlogBundle:Category')->find(2);
        $category->setTitle('CategoryEdited');

        $guild = $this->em->getRepository('RizzenGuildBundle:Guild')->findOneByTitle('TestGuild');

        $this->blogPoster->editCategory($guild, $category);

        $result = $this->em->getRepository('RizzenBlogBundle:Category')->findOneByTitle('CategoryEdited');

        $this->assertNotNull($result);
    }

    public function testNewPost()
    {
        $guild = $this->em->getRepository('RizzenGuildBundle:Guild')->findOneByTitle('TestGuild');
        $user = $this->em->getRepository('RizzenUserBundle:User')->findOneByUsername('testUser0');
        $category = $this->em->getRepository('RizzenBlogBundle:Category')->find(2);

        $post = new Post();

        $post->setTitle('DevPost');
        $post->setContent('<p>This is just for the tests!</p>');
        $post->setCategory($category);

        $this->blogPoster->newPost($guild, $post, $user);

        /** @var Post $result */
        $result = $this->em->getRepository('RizzenBlogBundle:Post')->findOneByTitle('DevPost');

        $this->assertNotNull($result);
        $this->assertNotNull($result->getSlug());
    }

    public function testEditPost()
    {
        /** @var Post $post */
        $post = $this->em->getRepository('RizzenBlogBundle:Post')->findOneByTitle('DevPost');
        $guild = $this->em->getRepository('RizzenGuildBundle:Guild')->findOneByTitle('TestGuild');

        $post->setTitle('DevPostEdited');

        $this->blogPoster->editPost($guild, $post);

        /** @var Post $result */
        $result = $this->em->getRepository('RizzenBlogBundle:Post')->findOneByTitle('DevPostEdited');

        $this->assertNotNull($result);
        $this->assertNotNull($result->getSlug());
    }
}
