<?php

namespace Rizzen\BlogBundle\DataFixtures\ORM;


use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Rizzen\UserBundle\Entity\User;
use Rizzen\BlogBundle\Entity\Post;
use Rizzen\GuildBundle\Entity\Guild;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadPostData extends AbstractFixture implements OrderedFixtureInterface, FixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {

        $userManager = $this->container->get('fos_user.user_manager');

        /** @var User $user */
        $user = $userManager->findUserBy(['username' => 'testUser0']);

        /** @var Guild $guild */
        $guild = $manager->getRepository('RizzenGuildBundle:Guild')->findOneByTitle('TestGuild');
        $category = $manager->getRepository('RizzenBlogBundle:Category')->findOneByTitle('TestCategory');

        $post = new Post();
        $post->setTitle('DevPost');
        $post->setContent('<p>This is a test post</p>');
        $post->setCategory($category);

        $this->container->get('blog.poster')->newPost($guild, $post, $user);

    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 7;
    }
}
