<?php

namespace Rizzen\BlogBundle\Controller;

use Rizzen\BlogBundle\Entity\Category;
use Rizzen\BlogBundle\Entity\Post;
use Rizzen\BlogBundle\Form\Type\CategoryType;
use Rizzen\BlogBundle\Form\Type\PostType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class BlogAdminController extends Controller
{

    /**
     * The blog dashboard
     *
     * @param string $subdomain
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function dashboardAction($subdomain)
    {
        $guild = $this->get('get.guild')->getGuild($subdomain);

        $permissionsManager = $this->get('permissions.manager')->setUser($this->getUser())->setGuild($guild);
        $permissionsManager->must(['CanEditBlogPost', 'CanWriteBlogPost']);

        $latestBlogPost = $this->getDoctrine()->getRepository('RizzenBlogBundle:Post')->getLatestVisiblePost($guild);

        return $this->render('RizzenBlogBundle:Admin:dashboard.html.twig', [
                'guild' => $guild,
                'permissions' => $permissionsManager->getPermissions(),
                'latestBlogPost' => $latestBlogPost,
                'subdomain' => $subdomain
        ]);
    }

    /**
     * Edit an already existing post
     *
     * @param string $subdomain
     * @param string $slug
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editAction($subdomain, $slug, Request $request)
    {
        $guild = $this->get('get.guild')->getGuild($subdomain);

        $post = $this->getDoctrine()->getRepository('RizzenBlogBundle:Post')->findOneBySlug($slug);

        $permissionsManager = $this->get('permissions.manager')->setUser($this->getUser())->setGuild($guild);
        $permissionsManager->must(['CanWriteBlogPost', 'CanEditBlogPost']);

        if ($post === null) {
            throw $this->createNotFoundException('The blog post was not found');
        }

        $form = $this->createForm(PostType::class, $post, ['guild' => $guild]);

        $form->handleRequest($request);
        if ($form->isValid()) {
            $this->get('blog.poster')->editPost($guild, $post, $this->getUser());
            $this->addFlash('success', 'Post edited successfully!');
            return $this->redirect($this->generateUrl('rizzen_blog_admin_posts', ['subdomain' => $subdomain]));
        }

        return $this->render('RizzenBlogBundle:Admin:editPost.html.twig', [
                'subdomain' => $subdomain,
                'guild' => $guild,
                'post' => $post,
                'permissions' => $permissionsManager->getPermissions(),
                'form' => $form->createView()
            ]);
    }

    /**
     * Write a new blog post
     *
     * @param string $subdomain
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function writeAction($subdomain, Request $request)
    {
        $guild = $this->get('get.guild')->getGuild($subdomain);

        $permissionsManager = $this->get('permissions.manager')->setUser($this->getUser())->setGuild($guild);
        $permissionsManager->must(['CanWriteBlogPost']);

        $post = new Post();
        $form = $this->createForm(PostType::class, $post, ['guild' => $guild]);

        $form->handleRequest($request);
        if ($form->isValid()) {
            $this->get('blog.poster')->newPost($guild, $post, $this->getUser());
            $this->addFlash('success', 'Post created successfully!');
            return $this->redirect($this->generateUrl('rizzen_blog_admin_posts', ['subdomain' => $subdomain]));
        }

        return $this->render('RizzenBlogBundle:Admin:newPost.html.twig', [
                'subdomain' => $subdomain,
                'guild' => $guild,
                'permissions' => $permissionsManager->getPermissions(),
                'form' => $form->createView()
            ]);
    }

    /**
     * List all the posts, including the ones that are invisible
     *
     * @param string $subdomain
     * @param int $page
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function postsAction($subdomain, $page)
    {
        $guild = $this->get('get.guild')->getGuild($subdomain);

        $permissionsManager = $this->get('permissions.manager')->setUser($this->getUser())->setGuild($guild);
        $permissionsManager->must(['CanWriteBlogPost', 'CanEditBlogPost']);

        return $this->render('RizzenBlogBundle:Admin:posts.html.twig', [
                'subdomain' => $subdomain,
                'guild' => $guild,
                'pagination' => $this->get('knp_paginator')->paginate($guild->getBlog()->getPosts(), $page, 8),
                'permissions' => $permissionsManager->getPermissions()
            ]);
    }

    /**
     * List the categories
     *
     * @param string $subdomain
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function categoriesAction($subdomain)
    {
        $guild = $this->get('get.guild')->getGuild($subdomain);

        $permissionsManager = $this->get('permissions.manager')->setUser($this->getUser())->setGuild($guild);
        $permissionsManager->must(['CanWriteBlogPost']);

        return $this->render('RizzenBlogBundle:Admin:categories.html.twig', [
                'subdomain' => $subdomain,
                'guild' => $guild,
                'permissions' => $permissionsManager->getPermissions()
            ]);
    }

    /**
     * Create a new category
     *
     * @param string $subdomain
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function newCategoryAction($subdomain, Request $request)
    {
        $guild = $this->get('get.guild')->getGuild($subdomain);

        $permissionsManager = $this->get('permissions.manager')->setUser($this->getUser())->setGuild($guild);
        $permissionsManager->must(['CanWriteBlogPost']);

        $category = new Category();

        $form = $this->createForm(CategoryType::class, $category);

        $form->handleRequest($request);
        if ($form->isValid()) {
            $this->get('blog.poster')->newCategory($guild, $category);
            $this->addFlash('success', 'Category created successfully!');
            return $this->redirect($this->generateUrl('rizzen_blog_admin_categories', ['subdomain' => $subdomain]));
        }

        return $this->render('RizzenBlogBundle:Admin:newCategory.html.twig', [
                'subdomain' => $subdomain,
                'guild' => $guild,
                'permissions' => $permissionsManager->getPermissions(),
                'form' => $form->createView()
            ]);
    }

    /**
     * Edit an already existing category
     *
     * @param string $subdomain
     * @param string $slug
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editCategoryAction($subdomain, $slug, Request $request)
    {
        $guild = $this->get('get.guild')->getGuild($subdomain);

        $permissionsManager = $this->get('permissions.manager')->setUser($this->getUser())->setGuild($guild);
        $permissionsManager->must(['CanWriteBlogPost']);

        $category = $this->getDoctrine()->getRepository('RizzenBlogBundle:Category')->findOneBySlug($slug);

        /** @var Category $category */
        if ($category === null || $category->getEditable() === false || $category->getBlog()->getGuild() !== $guild) {
            throw $this->createNotFoundException('Category not found or not editable');
        }

        $form = $this->createForm(CategoryType::class, $category);

        $form->handleRequest($request);
        if ($form->isValid()) {
            $this->get('blog.poster')->editCategory($guild, $category);
            $this->addFlash('success', 'Category edited successfully!');
            return $this->redirect($this->generateUrl('rizzen_blog_admin_categories', ['subdomain' => $subdomain]));
        }

        return $this->render('RizzenBlogBundle:Admin:editCategory.html.twig', [
                'subdomain' => $subdomain,
                'guild' => $guild,
                'permissions' => $permissionsManager->getPermissions(),
                'category' => $category,
                'form' => $form->createView()
            ]);
    }
}
