<?php

namespace Rizzen\BlogBundle\Controller;

use Rizzen\BlogBundle\Entity\PostComment;
use Rizzen\BlogBundle\Form\Type\PostCommentType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Rizzen\BlogBundle\Entity\Post;
use Rizzen\BlogBundle\Entity\Category;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class BlogController extends Controller
{
    /**
     * Index of the blog, show all the visible posts of all the categories
     *
     * @param string $subdomain
     * @param int $page
     * @return Response
     */
    public function indexAction($subdomain, $page)
    {
        $guild = $this->get('get.guild')->getGuild($subdomain);
        $permissionsManager = $this->get('permissions.manager')->setUser($this->getUser())->setGuild($guild);

        $postsQuery = $this->getDoctrine()->getRepository('RizzenBlogBundle:Post')->getVisiblePostsQuery($guild->getBlog());
        $pagination = $this->get('knp_paginator')->paginate($postsQuery, $page, 8);

        return $this->render('RizzenBlogBundle::index.html.twig', [
                'subdomain' => $subdomain,
                'guild' => $guild,
                'pagination' => $pagination,
                'permissions' => $permissionsManager->getPermissions()
            ]);
    }

    /**
     * See a blog post
     *
     * @param string $subdomain
     * @param string $slug
     * @return Response
     */
    public function seePostAction($subdomain, $slug)
    {
        $guild = $this->get('get.guild')->getGuild($subdomain);
        $permissionsManager = $this->get('permissions.manager')->setUser($this->getUser())->setGuild($guild);

        /** @var Post $post */
        $post = $this->getDoctrine()->getRepository('RizzenBlogBundle:Post')->findOneBySlug($slug);

        if ($post === null ||
            $post->getBlog()->getGuild() !== $guild ||
            (!$post->isVisible() && !$permissionsManager->can(['CanWriteBlogPost', 'CanEditBlogPost']))) {
            throw $this->createNotFoundException('Post not found');
        }

        $comments = $this->getDoctrine()->getRepository('RizzenBlogBundle:PostComment')->getCommentsOfPost($post);

        return $this->render('RizzenBlogBundle::post.html.twig', [
                'subdomain' => $subdomain,
                'guild' => $guild,
                'post' => $post,
                'form' => $this->createForm(PostCommentType::class, new PostComment())->createView(),
                'comments' => $comments,
                'permissions' => $permissionsManager->getPermissions(),
                'permissionsJSON' => $permissionsManager->getJSONPermissions()
            ]);
    }

    /**
     * See all the blog posts contained in this category
     *
     * @param string $subdomain
     * @param string $slug
     * @param int $page
     * @return Response
     */
    public function categoryAction($subdomain, $slug, $page)
    {
        $guild = $this->get('get.guild')->getGuild($subdomain);

        $permissionsManager = $this->get('permissions.manager')->setUser($this->getUser())->setGuild($guild);

        /** @var Category $category */
        $category = $this->getDoctrine()->getRepository('RizzenBlogBundle:Category')->findOneBySlug($slug);

        if ($category === null || $category->getBlog()->getGuild() !== $guild) {
            throw $this->createNotFoundException('Category not found');
        }

        $postsQuery = $this->getDoctrine()->getRepository('RizzenBlogBundle:Post')->getVisiblePostsInCategoryQuery($category);
        $pagination = $this->get('knp_paginator')->paginate($postsQuery, $page, 8);

        return $this->render('RizzenBlogBundle::category.html.twig', [
                'subdomain' => $subdomain,
                'guild' => $guild,
                'pagination' => $pagination,
                'category' => $category,
                'permissions' => $permissionsManager->getPermissions()
            ]);
    }

    /**
     * Send a comment in an post
     *
     * @param string $subdomain
     * @param string $slug
     * @param Request $request
     * @throws \Exception
     * @return Response
     */
    public function sendCommentAction($subdomain, $slug, Request $request)
    {
        $guild = $this->get('get.guild')->getGuild($subdomain);

        /** @var Post $post */
        $post = $this->getDoctrine()->getManager()->getRepository('RizzenBlogBundle:Post')->findOneBySlug($slug);

        $permissionsManager = $this->get('permissions.manager')->setUser($this->getUser())->setGuild($guild);
        $permissionsManager->isForumMember();

        if ($post === null || $guild !== $post->getBlog()->getGuild()) {
            throw $this->createNotFoundException('Post not found');
        }

        $comment = new PostComment();
        $form = $this->createForm(PostCommentType::class, $comment);

        $form->handleRequest($request);
        if ($form->isValid()) {

            $this->get('comment.poster')
                ->setComment($comment)
                ->setCreator($this->getUser())
                ->setTarget($post)
                ->setTemplate('RizzenBlogBundle::newComment.html.twig')
                ->setWebsocketTopic($post->getSocketTopic())
                ->setReplyEntity('RizzenBlogBundle:PostComment')
                ->post();

            return new Response();
        }

        throw $this->createNotFoundException('Post not found');
    }

    /**
     * Delete a comment in a post
     *
     * @param int $commentId
     * @return Response
     */
    public function deleteCommentAction($commentId)
    {
        $em = $this->getDoctrine()->getManager();
        $comment = $em->getRepository('RizzenBlogBundle:PostComment')->find($commentId);

        if ($comment === null) {
            throw $this->createNotFoundException('Comment not found');
        }

        $guild = $comment->getTarget()->getBlog()->getGuild();

        $permissionsManager = $this->get('permissions.manager')->setUser($this->getUser())->setGuild($guild);
        $permissionsManager->must(['CanWriteBlogPost', 'CanEditBlogPost']);

        $comment->setVisible(false);
        $em->flush();

        return new Response();
    }
}
