<?php

namespace Rizzen\BlogBundle\Entity;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use Rizzen\GuildBundle\Entity\Guild;

/**
 * BlogRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class PostRepository extends EntityRepository
{
    public function getVisiblePostsQuery(Blog $blog)
    {
        $qb = $this->createQueryBuilder('p');
        $qb->where('p.blog = :blog')
            ->setParameter('blog', $blog)
            ->andWhere('p.visible = :visible')
            ->setParameter('visible', true)
            ->orderBy('p.date', 'DESC');

        return $qb->getQuery();
    }

    public function getVisiblePostsInCategoryQuery(Category $category)
    {
        $qb = $this->createQueryBuilder('p');
        $qb->where('p.category = :category')
            ->setParameter('category', $category)
            ->andWhere('p.visible = :visible')
            ->setParameter('visible', true)
            ->orderBy('p.date', 'DESC');

        return $qb->getQuery();
    }

    public function getLatestVisiblePost(Guild $guild)
    {
        $qb = $this->createQueryBuilder('p');
        $qb->where('p.blog = :blog')
            ->setParameter('blog', $guild->getBlog())
            ->andWhere('p.visible = :visible')
            ->setParameter('visible', true)
            ->orderBy('p.date', 'DESC')
            ->leftJoin('p.creator', 'c')
            ->addSelect('c')
            ->leftJoin('p.category', 'category')
            ->addSelect('category')
            ->setMaxResults(1);

        return $qb->getQuery()->setHint(Query::HINT_FORCE_PARTIAL_LOAD, true)->getOneOrNullResult();
    }
}
