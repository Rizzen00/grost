<?php

namespace Rizzen\BlogBundle\Entity;

use Doctrine\ORM\EntityRepository;

class PostCommentRepository extends EntityRepository
{

    public function getCommentsOfPost($post)
    {
        $qb = $this->createQueryBuilder('r');
        $qb->where('r.target = :post')
            ->setParameter('post', $post)
            ->andWhere('r.visible = :visible')
            ->setParameter('visible', true)
            ->andWhere('r.isResponse = :isResponse')
            ->setParameter('isResponse', false);

        return $qb->getQuery()->getResult();
    }

}
