<?php

namespace Rizzen\BlogBundle\Form\Type;

use Doctrine\ORM\EntityRepository;
use Rizzen\GuildBundle\Entity\Guild;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

class PostType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var Guild $guild */
        $guild = $options['guild'];

        $builder
            ->add('title', TextType::class, ['attr' => ['autocomplete' => 'off']])
            ->add('content', TextareaType::class)
            ->add('category', EntityType::class, [
                    'class' => 'RizzenBlogBundle:Category',
                    'choice_label' => 'title',
                    'query_builder' => function (EntityRepository $er) use ($guild) {
                        return $er->createQueryBuilder('c')
                            ->where('c.blog = :blog')
                            ->orderBy('c.title', 'ASC')
                            ->setParameter('blog', $guild->getBlog());
                    },
                    'expanded' => false,
                    'multiple' => false
                ])
            ->add('visible', CheckboxType::class, [
                    'required' => false
            ])
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'Rizzen\BlogBundle\Entity\Post',
            'guild' => null
        ]);
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'grost_blog_new_post';
    }
}
