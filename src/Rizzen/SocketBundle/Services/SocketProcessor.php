<?php

namespace Rizzen\SocketBundle\Services;


/**
 * Service used to send messages to topics using Pusher
 *
 * Class SocketProcessor
 * @author Bryan Haag <misios11@hotmail.fr>
 * @package Rizzen\SocketBundle\Services
 */
class SocketProcessor implements SocketProcessorInterface
{
    private $topics;
    private $subtopic;
    private $template;
    private $additionalInfo;

    private $templater;
    private $pusher;

    /**
     * @param SocketTemplater $templater
     * @param \Pusher $pusher
     */
    public function __construct(SocketTemplater $templater, \Pusher $pusher)
    {
        $this->templater = $templater;
        $this->topics = [];
        $this->pusher = $pusher;
    }

    /**
     * {@inheritdoc}
     */
    public function broadcast()
    {
        $entryData = [
            'subtopic' => $this->subtopic,
            'template' => $this->template,
            'additionalInfo' => $this->additionalInfo
        ];

        $this->pusher->trigger($this->topics, 'message', $entryData );
    }

    /**
     * {@inheritdoc}
     */
    public function addTopic($topic)
    {
        $this->topics[] = $topic;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function useTemplate($template, $templateParam)
    {
        $this->template = $this->templater->generate($template, $templateParam);

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setSubtopic($subtopic)
    {
        $this->subtopic = $subtopic;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setAdditionalInfo($additionalInfo)
    {
        $this->additionalInfo = $additionalInfo;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function clearTopics()
    {
        $this->topics = [];
    }

}
