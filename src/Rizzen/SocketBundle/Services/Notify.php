<?php

namespace Rizzen\SocketBundle\Services;


use Rizzen\UserBundle\Entity\User;

/**
 * Service used to send notifications to clients via websocket
 *
 * Class Notify
 * @package Rizzen\SocketBundle\Services
 */
class Notify implements NotifyInterface
{

    private $users;
    private $type;
    private $icon;
    private $message;
    private $title;
    private $redirectTo;
    private $socketProcessor;

    /**
     * @param SocketProcessor $socketProcessor
     */
    public function __construct(SocketProcessor $socketProcessor)
    {
        $this->socketProcessor = $socketProcessor;
    }

    /**
     * {@inheritdoc}
     */
    public function notify()
    {
        $additionalInfo = [];

        $additionalInfo['title'] = ' ' . $this->title;
        $additionalInfo['icon'] = $this->icon;
        $additionalInfo['type'] = $this->type;

        if ($this->redirectTo !== null) {
            $additionalInfo['url'] = $this->redirectTo;
        }

        foreach ($this->users as $user) {
            $this->socketProcessor->addTopic($user->getSocketTopic());
        }

        $this->socketProcessor
            ->setSubtopic('notify')
            ->useTemplate('raw', ['HTML' => $this->message])
            ->setAdditionalInfo($additionalInfo)
            ->broadcast();
    }

    /**
     * {@inheritdoc}
     */
    public function setContent($title, $message)
    {
        $this->message = $message;
        $this->title = $title;
    }

    /**
     * {@inheritdoc}
     */
    public function setStyle($type, $icon)
    {
        $this->type = $type;
        $this->icon = $icon;
    }

    /**
     * {@inheritdoc}
     */
    public function addUser(User $user)
    {
        $this->users[] = $user;
    }

    /**
     * {@inheritdoc}
     */
    public function clearUsers()
    {
        $this->users = [];
    }

    /**
     * {@inheritdoc}
     */
    public function setRedirectTo($redirectTo)
    {
        $this->redirectTo = $redirectTo;
    }

    /**
     * {@inheritdoc}
     */
    public function clearTargets()
    {
        $this->socketProcessor->clearTopics();
    }

} 
