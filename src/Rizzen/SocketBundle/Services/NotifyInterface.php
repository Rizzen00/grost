<?php

namespace Rizzen\SocketBundle\Services;


use Rizzen\UserBundle\Entity\User;

/**
 * Interface NotifyInterface
 * @package Rizzen\SocketBundle\Services
 */
interface NotifyInterface {

    /**
     * Send a notification via websocket to the users present in the $users array
     */
    public function notify();

    /**
     * Message of the notification
     * @param $title
     * @param $message
     */
    public function setContent($title, $message);

    /**
     * Type of the notification
     *
     * @param $type
     * @param $icon
     * @return
     * @internal param mixed $style
     */
    public function setStyle($type, $icon);

    /**
     * List of user which will receive the notification
     *
     * @param mixed $user
     */
    public function addUser(User $user);

    /**
     * If set, the user will be redirected to this URL if he click the notification
     *
     * @param string|null $redirectTo URL to redirect to
     */
    public function setRedirectTo($redirectTo);

    /**
     * Empty the $users array
     */
    public function clearUsers();

    /**
     * Empty the topics in the socket processor service
     */
    public function clearTargets();

} 
