<?php

namespace Rizzen\SocketBundle\Services;

use Rizzen\CalendarBundle\Entity\Registration;
use Symfony\Bridge\Twig\TwigEngine;

/**
 * Class SocketTemplater
 * @author Bryan Haag <misios11@hotmail.fr>
 * @package Rizzen\SocketBundle\Services
 */
class SocketTemplater implements SocketTemplaterInterface
{

    private $templating;

    public function __construct(TwigEngine $templating)
    {
        $this->templating = $templating;
    }

    /**
     * @param Int $stateFrom
     * @param Registration $registration
     * @param Int $stateTo
     * @return array
     */
    private function processEventStateSwitchArray($stateFrom, Registration $registration, $stateTo)
    {
        // Array that we will convert to JSON and send to every client subscribed to the event
        $usersToSwitch[] = [
            "originalState" => $stateFrom,
            "newState" => $stateTo,
            "registrationId" => $registration->getId(),
            "newList" => $this->templating->render('RizzenCalendarBundle:Event:userList.html.twig', [
                "registration" => $registration,
                "event" => $registration->getEvent(),
                "newState" => $stateTo
            ])
        ];

        return $usersToSwitch;
    }

    public function generate($template, $templateParam)
    {
        if ($template === 'raw') {
            return $templateParam['HTML'];
        } elseif ($template === 'eventStateSwitch') {
            return $this->processEventStateSwitchArray($templateParam['stateFrom'], $templateParam['registration'], $templateParam['stateTo']);
        }
    }

}
