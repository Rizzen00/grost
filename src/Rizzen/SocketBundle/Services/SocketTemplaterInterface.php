<?php

namespace Rizzen\SocketBundle\Services;


interface SocketTemplaterInterface {

    /**
     * Generate the template to use in the websocket message
     *
     * @param string $template
     * @param array|string $templateParam
     * @return array
     */
    public function generate($template, $templateParam);

}
