<?php

namespace Rizzen\SocketBundle\Services;

use Doctrine\ORM\EntityManager;
use Rizzen\GuildBundle\Permissions\PermissionsManager;
use Rizzen\UserBundle\Entity\User;
use Lopi\Bundle\PusherBundle\Authenticator\ChannelAuthenticatorInterface;
use Lopi\Bundle\PusherBundle\Authenticator\ChannelAuthenticatorPresenceInterface;
use Rizzen\ForumBundle\Services\ForumViewable;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Rizzen\ForumBundle\Entity\Thread;
use Rizzen\CalendarBundle\Entity\Event;
use Rizzen\MessengerBundle\Entity\Conversation;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;

/**
 * Class used to authenticate users with Pusher
 *
 * Class TopicAuthenticator
 * @package Rizzen\SocketBundle\Services
 */
class TopicAuthenticator implements ChannelAuthenticatorInterface, ChannelAuthenticatorPresenceInterface
{
    /** @var User $user */
    private $user;
    private $tokenStorage;
    private $em;
    private $forumViewable;
    private $authorizationChecker;
    private $permissionsManager;

    /**
     * @param EntityManager $em
     * @param TokenStorage $tokenStorage
     * @param AuthorizationChecker $authorizationChecker
     * @param ForumViewable $forumViewable
     * @param PermissionsManager $permissionsManager
     */
    public function __construct(EntityManager $em, TokenStorage $tokenStorage, AuthorizationChecker $authorizationChecker, ForumViewable $forumViewable, PermissionsManager $permissionsManager)
    {
        $this->tokenStorage = $tokenStorage;
        $this->user = $tokenStorage->getToken()->getUser();
        $this->em = $em;
        $this->forumViewable = $forumViewable;
        $this->authorizationChecker = $authorizationChecker;
        $this->permissionsManager = $permissionsManager;
    }

    /**
     * @param string $socketId      The socket ID
     * @param string $channelName   The channel name
     *
     * @return bool
     */
    public function authenticate($socketId, $channelName)
    {

        // Make sure the current user is logged in
        if (!$this->authorizationChecker->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            return false;
        }

        /*
         * User Topic: private-user-RANDOM
         * */
        if (strpos($channelName, 'private-user') === 0) {
            if ($this->user->getSocketTopic() === $channelName) {
                return true;
            }
        }

        /*
         * Forum Thread Topic: private-thread-SLUG
         * */
        if (strpos($channelName, 'private-thread') === 0) {

            /** @var Thread $thread */
            $thread = $this->em->getRepository('RizzenForumBundle:Thread')->findOneByPrivateSocketTopic($channelName);

            if ($thread === null) {
                return false;
            }

            $guild = $thread->getSubCategory()->getCategory()->getForum()->getGuild();
            $permissions = $this->permissionsManager->setGuild($guild)->setUser($this->user)->getPermissions();

            if ($this->forumViewable->canSee($thread, $permissions, false)) {
                return true;
            }

        }

        /*
         * Event Topic: private-event-SLUG
         * */
        if (strpos($channelName, 'private-event') === 0) {

            /** @var Event $event */
            $event = $this->em->getRepository('RizzenCalendarBundle:Event')->findOneBySocketTopic($channelName);

            if ($event === null) {
                return false;
            }

            $guild = $event->getCalendar()->getGuild();

            if ($this->permissionsManager->setGuild($guild)->setUser($this->user)->isGuildMember(false)) {
                return true;
            } else {
                $registration = $this->em->getRepository('RizzenCalendarBundle:Registration')->getRegistrationToModify($event, $this->user);
                if ($registration !== null && $this->permissions->getPermissions()->isBannedFromGuild() === false) {
                    return true;
                }
            }

        }

        /*
         * Messenger Topic: private-messenger-SLUG
         * */
        if (strpos($channelName, 'private-messenger') === 0) {

            /** @var Conversation $conversation */
            $conversation = $this->em->getRepository('RizzenMessengerBundle:Conversation')->findOneBySocketTopic($channelName);

            if ($conversation === null) {
                return false;
            }

            $participants = $conversation->getParticipants();

            foreach ($participants as $participant) {
                if ($participant === $this->user) {
                    return true;
                }
            }
        }

        /*
         * Guild Chat Topic: presence-guild-chat-SLUG
         * */
        if (strpos($channelName, 'presence-guild-chat') === 0) {

            $guildSlug = substr($channelName, 20);

            /** @var Event $event */
            $guild = $this->em->getRepository('RizzenGuildBundle:Guild')->findOneBySlug($guildSlug);

            if ($guild === null) {
                return false;
            }

            if ($this->permissionsManager->setGuild($guild)->setUser($this->user)->isGuildMember(false)) {
                return true;
            }

        }

        return false;
    }

    /**
     * Returns an optional array of user info
     *
     * @return array
     */
    public function getUserInfo()
    {
        return [
            'username' => $this->user->getUsername()
        ];
    }

    /**
     * Return the user id when authenticated, used for presence channels
     *
     * @return string
     */
    public function getUserId()
    {
        return $this->user->getId();
    }
}
