<?php

namespace Rizzen\SocketBundle\Services;


/**
 * Interface SocketProcessorInterface
 * @package Rizzen\SocketBundle\Services
 */
interface SocketProcessorInterface {

    /**
     * Prepare and broadcast the JSON data over the websocket connection
     */
    public function broadcast();

    /**
     * Add a new topic to broadcast to
     *
     * @param $topic
     * @return mixed
     */
    public function addTopic($topic);

    /**
     * Generate the template using the SocketTemplater service
     *
     * @param $template
     * @param $templateParam
     * @return mixed
     */
    public function useTemplate($template, $templateParam);

    /**
     * Set the subtopic used in the JS to display the data
     *
     * @param $subtopic
     * @return mixed
     */
    public function setSubtopic($subtopic);

    /**
     * Additional information that will sent with the message to the client
     *
     * @param $additionalInfo
     * @return mixed
     */
    public function setAdditionalInfo($additionalInfo);

    /**
     * Empty the Topics array
     *
     * @return mixed
     */
    public function clearTopics();

} 
