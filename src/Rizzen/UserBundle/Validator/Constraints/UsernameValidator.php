<?php

namespace Rizzen\UserBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class UsernameValidator extends ConstraintValidator
{

    public function validate($value, Constraint $constraint)
    {
        if (!preg_match('/^[a-zA-Z0-9áàâäãåçéèêëíìîïñóòôöõúùûüýÿæœÁÀÂÄÃÅÇÉÈÊËÍÌÎÏÑÓÒÔÖÕÚÙÛÜÝŸÆŒ_-]{3,40}+$/', $value, $matches) || $this->isUsernameReserved($value)) {
            $this->context->addViolation($constraint->message);
        }
    }

    /**
     * Check if the username is reserved
     *
     * @param string $username
     * @return bool
     */
    private function isUsernameReserved($username)
    {
        $reservedNames = [
            'admin',
            'administrator',
            'super-admin',
            'super-administrator',
            'super_admin',
            'super_administrator',
            'super-moderator',
            'super-mod',
            'super_moderator',
            'super_mod',
            'admins',
            'grost',
            'grost-bot',
            'bot',
            'grosts',
            'grost_rizzen',
            'grost-rizzen',
            'grost-admin',
            'grost-administrator',
            'site',
            'site-admin',
            'site-administrator',
            'moderator',
            'mod',
            'moderateur',
            'webmaster',
            'web-master',
            'master',
            'grost-web-master',
            'admin-grost',
            'grost_admin',
            'grost_administrator',
            'grost_bot',
            'admin_grost',
            'grost_creator',
            'site_creator',
            'creator',
            'bot_grost',
            'bot-grost',
            'gro-st',
            'gro_st',
            'grostbot',
            'botgrost',
            'forum',
            'req',
            'request',
            'guild',
            'blog',
            'calendar',
            '_exit'
        ];

        if (in_array(strtolower($username), $reservedNames)) {
            return true;
        }

        return false;
    }
}
