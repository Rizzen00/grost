<?php

namespace Rizzen\UserBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class Username extends Constraint
{
    public $message = "The username contains special characters that aren't allowed.";
}
