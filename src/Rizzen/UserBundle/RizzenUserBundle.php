<?php

namespace Rizzen\UserBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class RizzenUserBundle extends Bundle
{
    public function getParent()
    {
        return 'FOSUserBundle';
    }
}
