<?php

namespace Rizzen\UserBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Rizzen\UserBundle\Entity\User;
use Rizzen\UserBundle\Entity\Email;
use Rizzen\UserBundle\Entity\Messenger;
use Rizzen\UserBundle\Entity\Options;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadUsersData extends AbstractFixture implements OrderedFixtureInterface, FixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;
    private $userManager;
    private $em;

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
        $this->userManager = $this->container->get('fos_user.user_manager');
        $this->em = $this->container->get('doctrine.orm.entity_manager');

        for ($i = 0; $i <= 5; $i++) {
            $this->createUser($i);
        }
    }

    public function createUser($number)
    {

        $username = 'testUser' . $number;
        $email = 'email' . $number . '@gro.st';

        /** @var User $user */
        $user = $this->userManager->createUser();
        $user->setUsername($username);
        $user->setEmail($email);
        $user->setPlainPassword('password');
        $user->setEnabled(true);
        $user->setTimezone('Europe/Paris');

        $this->userManager->updateUser($user, true);

        $options = new Options();
        $emailOptions = new Email();
        $messengerOptions = new Messenger();

        $options->setEmail($emailOptions);
        $options->setMessenger($messengerOptions);

        $this->em->persist($emailOptions);
        $this->em->persist($messengerOptions);
        $this->em->persist($options);

        $user->setOptions($options);

        $this->em->flush();
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 1;
    }
}
