<?php


namespace Rizzen\UserBundle\EventListener;

use Doctrine\ORM\EntityManager;
use FOS\UserBundle\Event\FilterUserResponseEvent;
use Rizzen\UserBundle\Entity\Email;
use Rizzen\UserBundle\Entity\Messenger;
use Rizzen\UserBundle\Entity\Options;
use Rizzen\UserBundle\Entity\User;

class RegistrationCompletedListener
{
    private $em;

    /**
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * Generate the base use options
     *
     * @param FilterUserResponseEvent $event
     */
    public function generateBaseOptions(FilterUserResponseEvent $event)
    {
        $options = new Options();
        $emailOptions = new Email();
        $messengerOptions = new Messenger();

        $options->setEmail($emailOptions);
        $options->setMessenger($messengerOptions);

        $this->em->persist($emailOptions);
        $this->em->persist($messengerOptions);
        $this->em->persist($options);

        /** @var User $user */
        $user = $event->getUser();

        $user->setOptions($options);

        $this->em->flush();
    }


} 
