<?php


namespace Rizzen\UserBundle\EventListener;


use FOS\UserBundle\Event\FormEvent;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Router;

class RegisteredListener
{

    private $router;

    public function __construct(Router $router)
    {
        $this->router = $router;
    }

    /**
     * Check if the user tried to create a guild or join a forum before registering, if he did, redirect him to the right page
     *
     * @param FormEvent $event
     */
    public function onRegistrationSuccess(FormEvent $event)
    {
        $session = $event->getRequest()->getSession();

        if ($session->has('registerThenCreate')) {
            $url = $this->router->generate('rizzen_guild_create');
            $session->remove('registerThenCreate');
        } elseif ($session->has('forumToJoin')) {
            $url = $this->router->generate('rizzen_forum_join', ['subdomain' => $session->get('forumToJoin')]);
            $session->remove('forumToJoin');
        } else {
            $url = $this->router->generate('rizzen_main_homepage');
        }

        $response = new RedirectResponse($url);

        $event->setResponse($response);

    }
} 
