<?php

namespace Rizzen\UserBundle\EventListener;


use FOS\UserBundle\Event\FormEvent;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Router;

class ChangePasswordSuccessListener
{

    private $router;

    public function __construct(Router $router)
    {
        $this->router = $router;
    }

    public function setChangePasswordSuccessResponse(FormEvent $event)
    {
        $url = $this->router->generate('rizzen_options_index');
        $response = new RedirectResponse($url);
        $event->setResponse($response);
    }

}
