<?php


namespace Rizzen\UserBundle\EventListener;


use FOS\UserBundle\Event\FormEvent;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Router;

class PasswordResetListener
{

    private $router;

    public function __construct(Router $router)
    {
        $this->router = $router;
    }

    public function onResettingResetSuccess(FormEvent $event)
    {
        $url = $this->router->generate('rizzen_main_homepage');

        $response = new RedirectResponse($url);

        $event->setResponse($response);

    }
} 
