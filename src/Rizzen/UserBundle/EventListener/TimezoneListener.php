<?php


namespace Rizzen\UserBundle\EventListener;

use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\HttpKernel\HttpKernel;
use Rizzen\UserBundle\Entity\User;

class TimezoneListener
{

    private $storage;
    private $checker;

    function __construct(TokenStorage $storage, AuthorizationChecker $checker)
    {
        $this->storage = $storage;
        $this->checker = $checker;
    }

    /**
     * Set the timezone of the user as the default timezone
     *
     * @param GetResponseEvent $event
     */
    public function setTimezone(GetResponseEvent $event)
    {
        if ($event->getRequestType() !== HttpKernel::MASTER_REQUEST) {
            return;
        }

        if ($this->storage->getToken() && $this->checker->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            /** @var User $user */
            $user = $this->storage->getToken()->getUser();

            date_default_timezone_set($user->getTimezone());
        }

    }

} 
