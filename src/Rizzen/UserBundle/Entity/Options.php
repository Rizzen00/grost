<?php

namespace Rizzen\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Preferences
 *
 * @ORM\Table(name="UserOptions")
 * @ORM\Entity
 */
class Options
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @ORM\OneToOne(targetEntity="Email")
     **/
    protected $email;

    /**
     * @ORM\OneToOne(targetEntity="Messenger")
     **/
    protected $messenger;

    /**
     * Set email
     *
     * @param \Rizzen\UserBundle\Entity\Email $email
     * @return Options
     */
    public function setEmail(\Rizzen\UserBundle\Entity\Email $email = null)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return \Rizzen\UserBundle\Entity\Email 
     */
    public function email()
    {
        return $this->email;
    }

    /**
     * Set messenger
     *
     * @param \Rizzen\UserBundle\Entity\Messenger $messenger
     * @return Options
     */
    public function setMessenger(\Rizzen\UserBundle\Entity\Messenger $messenger = null)
    {
        $this->messenger = $messenger;

        return $this;
    }

    /**
     * Get messenger
     *
     * @return \Rizzen\UserBundle\Entity\Messenger 
     */
    public function messenger()
    {
        return $this->messenger;
    }
}
