<?php

namespace Rizzen\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Messenger
 *
 * @ORM\Table(name="MessengerOptions")
 * @ORM\Entity
 */
class Messenger
{

    public function __construct()
    {
        $this->receiveMessageFromAnybody = true;
        $this->receiveMessageFromFriendsOnly = false;
        $this->receiveMessageFromNobody = false;

        $this->receiveNotificationDisabled = false;
        $this->receiveNotificationForEventInvite = true;
        $this->receiveNotificationForEventReminder = true;
        $this->receiveNotificationForGuildEvent = true;
    }

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var boolean
     *
     * @ORM\Column(name="receiveMessageFromAnybody", type="boolean")
     */
    private $receiveMessageFromAnybody;

    /**
     * @var boolean
     *
     * @ORM\Column(name="receiveMessageFromFriendsOnly", type="boolean")
     */
    private $receiveMessageFromFriendsOnly;

    /**
     * @var boolean
     *
     * @ORM\Column(name="receiveMessageFromNobody", type="boolean")
     */
    private $receiveMessageFromNobody;

    /**
     * @var boolean
     *
     * @ORM\Column(name="receiveNotificationForEventInvite", type="boolean")
     */
    private $receiveNotificationForEventInvite;

    /**
     * @var boolean
     *
     * @ORM\Column(name="receiveNotificationForGuildEvent", type="boolean")
     */
    private $receiveNotificationForGuildEvent;

    /**
     * @var boolean
     *
     * @ORM\Column(name="receiveNotificationForEventReminder", type="boolean")
     */
    private $receiveNotificationForEventReminder;

    /**
     * @var boolean
     *
     * @ORM\Column(name="receiveNotificationDisabled", type="boolean")
     */
    private $receiveNotificationDisabled;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set receiveMessageFromAnybody
     *
     * @param boolean $receiveMessageFromAnybody
     * @return Messenger
     */
    public function setReceiveMessageFromAnybody($receiveMessageFromAnybody)
    {
        $this->receiveMessageFromAnybody = $receiveMessageFromAnybody;

        return $this;
    }

    /**
     * Get receiveMessageFromAnybody
     *
     * @return boolean 
     */
    public function getReceiveMessageFromAnybody()
    {
        return $this->receiveMessageFromAnybody;
    }

    /**
     * Set receiveMessageFromFriendsOnly
     *
     * @param boolean $receiveMessageFromFriendsOnly
     * @return Messenger
     */
    public function setReceiveMessageFromFriendsOnly($receiveMessageFromFriendsOnly)
    {
        $this->receiveMessageFromFriendsOnly = $receiveMessageFromFriendsOnly;

        return $this;
    }

    /**
     * Get receiveMessageFromFriendsOnly
     *
     * @return boolean 
     */
    public function getReceiveMessageFromFriendsOnly()
    {
        return $this->receiveMessageFromFriendsOnly;
    }

    /**
     * Set receiveMessageFromNobody
     *
     * @param boolean $receiveMessageFromNobody
     * @return Messenger
     */
    public function setReceiveMessageFromNobody($receiveMessageFromNobody)
    {
        $this->receiveMessageFromNobody = $receiveMessageFromNobody;

        return $this;
    }

    /**
     * Get receiveMessageFromNobody
     *
     * @return boolean 
     */
    public function getReceiveMessageFromNobody()
    {
        return $this->receiveMessageFromNobody;
    }

    /**
     * Set receiveNotificationForEventInvite
     *
     * @param boolean $receiveNotificationForEventInvite
     * @return Messenger
     */
    public function setReceiveNotificationForEventInvite($receiveNotificationForEventInvite)
    {
        $this->receiveNotificationForEventInvite = $receiveNotificationForEventInvite;

        return $this;
    }

    /**
     * Get receiveNotificationForEventInvite
     *
     * @return boolean 
     */
    public function getReceiveNotificationForEventInvite()
    {
        return $this->receiveNotificationForEventInvite;
    }

    /**
     * Set receiveNotificationForGuildEvent
     *
     * @param boolean $receiveNotificationForGuildEvent
     * @return Messenger
     */
    public function setReceiveNotificationForGuildEvent($receiveNotificationForGuildEvent)
    {
        $this->receiveNotificationForGuildEvent = $receiveNotificationForGuildEvent;

        return $this;
    }

    /**
     * Get receiveNotificationForGuildEvent
     *
     * @return boolean 
     */
    public function getReceiveNotificationForGuildEvent()
    {
        return $this->receiveNotificationForGuildEvent;
    }

    /**
     * Set receiveNotificationForEventReminder
     *
     * @param boolean $receiveNotificationForEventReminder
     * @return Messenger
     */
    public function setReceiveNotificationForEventReminder($receiveNotificationForEventReminder)
    {
        $this->receiveNotificationForEventReminder = $receiveNotificationForEventReminder;

        return $this;
    }

    /**
     * Get receiveNotificationForEventReminder
     *
     * @return boolean 
     */
    public function getReceiveNotificationForEventReminder()
    {
        return $this->receiveNotificationForEventReminder;
    }

    /**
     * Set receiveNotificationDisabled
     *
     * @param boolean $receiveNotificationDisabled
     * @return Messenger
     */
    public function setReceiveNotificationDisabled($receiveNotificationDisabled)
    {
        $this->receiveNotificationDisabled = $receiveNotificationDisabled;

        return $this;
    }

    /**
     * Get receiveNotificationDisabled
     *
     * @return boolean 
     */
    public function getReceiveNotificationDisabled()
    {
        return $this->receiveNotificationDisabled;
    }
}
