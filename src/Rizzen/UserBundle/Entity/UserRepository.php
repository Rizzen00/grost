<?php

namespace Rizzen\UserBundle\Entity;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use Rizzen\GuildBundle\Entity\Guild;


class UserRepository extends EntityRepository
{

    /**
     * Count all the users registered
     *
     * @return int
     */
    public function getUsersCount()
    {
        return $this->createQueryBuilder('u')
            ->select('COUNT(u)')
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * Get the latest registered user
     *
     * @return User
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getLatestUser()
    {
        $qb = $this->createQueryBuilder('u');
        $qb->orderBy("u.id", "DESC")
           ->setMaxResults(1);

        return $qb->getQuery()->setHint(Query::HINT_FORCE_PARTIAL_LOAD, true)->getSingleResult();
    }

    /**
     * Get all the users registered
     * @return User[]
     */
    public function getUsers()
    {
        $qb = $this->createQueryBuilder('u');
        $qb->orderBy("u.id", "DESC");

        return $qb->getQuery()->setHint(Query::HINT_FORCE_PARTIAL_LOAD, true)->getResult();
    }

    /**
     * @param string $username
     * @return User|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getUserForProfile($username)
    {
        $qb = $this->createQueryBuilder('user');
        $qb->where('user.username = :username')
            ->setParameter('username', $username)
            ->leftJoin('user.avatar', 'avatar')
            ->addSelect('avatar')
            ->leftJoin('user.cover', 'cover')
            ->addSelect('cover')
            ->leftJoin('user.friends', 'friends')
            ->addSelect('friends')
            ->leftJoin('friends.avatar', 'friendsAvatar')
            ->addSelect('friendsAvatar')
            ->leftJoin('user.guilds', 'guilds')
            ->addSelect('guilds')
            ->leftJoin('user.forums', 'forums')
            ->addSelect('forums')
            ->leftJoin('forums.guild', 'forumGuild')
            ->addSelect('forumGuild');

        return $qb->getQuery()->setHint(Query::HINT_FORCE_PARTIAL_LOAD, true)->getOneOrNullResult();
    }

}
