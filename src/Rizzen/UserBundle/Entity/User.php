<?php

namespace Rizzen\UserBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="Rizzen\UserBundle\Entity\UserRepository")
 * @ORM\Table(name="GrostUser")
 */
class User extends BaseUser
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="socketTopic", type="string", unique=true, nullable=true)
     */
    protected $socketTopic;

    /**
     * @ORM\OneToOne(targetEntity="Rizzen\ProfileBundle\Entity\Avatar", mappedBy="user")
     **/
    protected $avatar;

    /**
     * @ORM\OneToOne(targetEntity="Rizzen\ProfileBundle\Entity\Cover", mappedBy="user")
     **/
    protected $cover;

    /**
     * @ORM\OneToOne(targetEntity="Rizzen\UserBundle\Entity\Options")
     **/
    protected $options;

    /**
     * @var string
     *
     * @ORM\Column(name="timezone", type="string")
     */
    protected $timezone;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="joinDate", type="datetimetz")
     */
    protected $joinDate;

    /**
     * @var string
     *
     * @ORM\Column(name="bio", type="text", nullable=true)
     *
     * @Assert\Length(
     *      min = "0",
     *      max = "2000"
     * )
     */
    protected $bio;

    /**
     * @var integer
     *
     * @ORM\Column(name="messageCount", type="integer", nullable=false)
     */
    protected $messageCount;

    /**
     * @var integer
     *
     * @ORM\Column(name="threadCount", type="integer", nullable=false)
     */
    protected $threadCount;

    /**
     * @ORM\ManyToMany(targetEntity="Rizzen\UserBundle\Entity\User")
     * @ORM\OrderBy({"onlinePing" = "DESC"})
     */
    protected $friends;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetimetz", nullable=true)
     */
    protected $onlinePing;

    /**
     * @ORM\ManyToMany(targetEntity="Rizzen\GuildBundle\Entity\Guild", mappedBy="members")
     */
    protected $guilds;

    /**
     * @ORM\ManyToMany(targetEntity="Rizzen\ForumBundle\Entity\Forum", mappedBy="members")
     */
    protected $forums;

    /**
     * @ORM\OneToMany(targetEntity="Rizzen\GuildBundle\Entity\RoleAssignment", mappedBy="user")
     */
    protected $rolesAssigned;

    /**
     * @ORM\ManyToMany(targetEntity="Rizzen\MessengerBundle\Entity\Conversation")
     * @ORM\JoinTable(name="users_unreads",
     *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="conversation_id", referencedColumnName="id")}
     *      )
     **/
    protected $unreads;

    /**
     * @ORM\ManyToMany(targetEntity="Rizzen\MessengerBundle\Entity\Conversation", inversedBy="participants")
     * @ORM\OrderBy({"lastMessageDate" = "DESC"})
     * @ORM\JoinTable(name="users_conversations")
     */
    protected $conversations;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set socketTopic
     *
     * @param string $socketTopic
     * @return User
     */
    public function setSocketTopic($socketTopic)
    {
        $this->socketTopic = $socketTopic;

        return $this;
    }

    /**
     * Get socketTopic
     *
     * @return string
     */
    public function getSocketTopic()
    {
        return $this->socketTopic;
    }

    public function setUsername($username)
    {
        $this->username = $username;

        $this->setSocketTopic('private-user-' . uniqid() . mt_rand(100000000, 999999999) . uniqid());

        return $this;
    }

    /**
     * Set bio
     *
     * @param string $bio
     * @return User
     */
    public function setBio($bio)
    {
        $this->bio = $bio;

        return $this;
    }

    /**
     * Get bio
     *
     * @return string
     */
    public function getBio()
    {
        return $this->bio;
    }

    /**
     * Add friends
     *
     * @param \Rizzen\UserBundle\Entity\User $friends
     * @return User
     */
    public function addFriend(\Rizzen\UserBundle\Entity\User $friends)
    {
        $this->friends[] = $friends;

        return $this;
    }

    /**
     * Remove friends
     *
     * @param \Rizzen\UserBundle\Entity\User $friends
     */
    public function removeFriend(\Rizzen\UserBundle\Entity\User $friends)
    {
        $this->friends->removeElement($friends);
    }

    /**
     * Get friends
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFriends()
    {
        return $this->friends;
    }

    /**
     * Set onlinePing
     *
     * @param \DateTime $onlinePing
     * @return User
     */
    public function setOnlinePing($onlinePing)
    {
        $this->onlinePing = $onlinePing;

        return $this;
    }

    /**
     * Get onlinePing
     *
     * @return \DateTime
     */
    public function getOnlinePing()
    {
        return $this->onlinePing;
    }


    /**
     * Add guilds
     *
     * @param \Rizzen\GuildBundle\Entity\Guild $guilds
     * @return User
     */
    public function addGuild(\Rizzen\GuildBundle\Entity\Guild $guilds)
    {
        $this->guilds[] = $guilds;

        return $this;
    }

    /**
     * Remove guilds
     *
     * @param \Rizzen\GuildBundle\Entity\Guild $guilds
     */
    public function removeGuild(\Rizzen\GuildBundle\Entity\Guild $guilds)
    {
        $this->guilds->removeElement($guilds);
    }

    /**
     * Get guilds
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGuilds()
    {
        return $this->guilds;
    }

    /**
     * Add forums
     *
     * @param \Rizzen\ForumBundle\Entity\Forum $forums
     * @return User
     */
    public function addForum(\Rizzen\ForumBundle\Entity\Forum $forums)
    {
        $this->forums[] = $forums;

        return $this;
    }

    /**
     * Remove forums
     *
     * @param \Rizzen\ForumBundle\Entity\Forum $forums
     */
    public function removeForum(\Rizzen\ForumBundle\Entity\Forum $forums)
    {
        $this->forums->removeElement($forums);
    }

    /**
     * Get forums
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getForums()
    {
        return $this->forums;
    }

    /**
     * Set messageCount
     *
     * @param integer $messageCount
     * @return User
     */
    public function setMessageCount($messageCount)
    {
        $this->messageCount = $messageCount;

        return $this;
    }

    /**
     * Get messageCount
     *
     * @return integer
     */
    public function getMessageCount()
    {
        return $this->messageCount;
    }

    /**
     * Set threadCount
     *
     * @param integer $threadCount
     * @return User
     */
    public function setThreadCount($threadCount)
    {
        $this->threadCount = $threadCount;

        return $this;
    }

    /**
     * Get threadCount
     *
     * @return integer
     */
    public function getThreadCount()
    {
        return $this->threadCount;
    }

    /**
     * Add unreads
     *
     * @param \Rizzen\MessengerBundle\Entity\Conversation $unreads
     * @return User
     */
    public function addUnread(\Rizzen\MessengerBundle\Entity\Conversation $unreads)
    {
        $this->unreads[] = $unreads;

        return $this;
    }

    /**
     * Remove unreads
     *
     * @param \Rizzen\MessengerBundle\Entity\Conversation $unreads
     */
    public function removeUnread(\Rizzen\MessengerBundle\Entity\Conversation $unreads)
    {
        $this->unreads->removeElement($unreads);
    }

    /**
     * Get unreads
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUnreads()
    {
        return $this->unreads;
    }

    /**
     * Add conversations
     *
     * @param \Rizzen\MessengerBundle\Entity\Conversation $conversations
     * @return User
     */
    public function addConversation(\Rizzen\MessengerBundle\Entity\Conversation $conversations)
    {
        $this->conversations[] = $conversations;

        return $this;
    }

    /**
     * Remove conversations
     *
     * @param \Rizzen\MessengerBundle\Entity\Conversation $conversations
     */
    public function removeConversation(\Rizzen\MessengerBundle\Entity\Conversation $conversations)
    {
        $this->conversations->removeElement($conversations);
    }

    /**
     * Get conversations
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getConversations()
    {
        return $this->conversations;
    }

    /**
     * Add rolesAssigned
     *
     * @param \Rizzen\GuildBundle\Entity\RoleAssignment $rolesAssigned
     * @return User
     */
    public function addRolesAssigned(\Rizzen\GuildBundle\Entity\RoleAssignment $rolesAssigned)
    {
        $this->rolesAssigned[] = $rolesAssigned;

        return $this;
    }

    /**
     * Remove rolesAssigned
     *
     * @param \Rizzen\GuildBundle\Entity\RoleAssignment $rolesAssigned
     */
    public function removeRolesAssigned(\Rizzen\GuildBundle\Entity\RoleAssignment $rolesAssigned)
    {
        $this->rolesAssigned->removeElement($rolesAssigned);
    }

    /**
     * Get rolesAssigned
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRolesAssigned()
    {
        return $this->rolesAssigned;
    }
    

    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();

        $this->joinDate = new \DateTime('now', new \DateTimeZone('UTC'));
        $this->messageCount = 0;
        $this->threadCount = 0;
        $this->friends = new \Doctrine\Common\Collections\ArrayCollection();
        $this->guilds = new \Doctrine\Common\Collections\ArrayCollection();
        $this->forums = new \Doctrine\Common\Collections\ArrayCollection();
        $this->rolesAssigned = new \Doctrine\Common\Collections\ArrayCollection();
        $this->unreads = new \Doctrine\Common\Collections\ArrayCollection();
        $this->conversations = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set avatar
     *
     * @param \Rizzen\ProfileBundle\Entity\Avatar $avatar
     * @return User
     */
    public function setAvatar(\Rizzen\ProfileBundle\Entity\Avatar $avatar = null)
    {
        $this->avatar = $avatar;

        return $this;
    }

    /**
     * Get avatar
     *
     * @return \Rizzen\ProfileBundle\Entity\Avatar 
     */
    public function getAvatar()
    {
        return $this->avatar;
    }

    /**
     * Set cover
     *
     * @param \Rizzen\ProfileBundle\Entity\Cover $cover
     * @return User
     */
    public function setCover(\Rizzen\ProfileBundle\Entity\Cover $cover = null)
    {
        $this->cover = $cover;

        return $this;
    }

    /**
     * Get cover
     *
     * @return \Rizzen\ProfileBundle\Entity\Cover 
     */
    public function getCover()
    {
        return $this->cover;
    }

    /**
     * Set options
     *
     * @param \Rizzen\UserBundle\Entity\Options $options
     * @return User
     */
    public function setOptions(\Rizzen\UserBundle\Entity\Options $options = null)
    {
        $this->options = $options;

        return $this;
    }

    /**
     * Get options
     *
     * @return \Rizzen\UserBundle\Entity\Options 
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * Set timezone
     *
     * @param string $timezone
     * @return User
     */
    public function setTimezone($timezone)
    {
        $this->timezone = $timezone;

        return $this;
    }

    /**
     * Get timezone
     *
     * @return string 
     */
    public function getTimezone()
    {
        return $this->timezone;
    }

    /**
     * Set joinDate
     *
     * @param string $joinDate
     * @return User
     */
    public function setJoinDate($joinDate)
    {
        $this->joinDate = $joinDate;

        return $this;
    }

    /**
     * Get joinDate
     *
     * @return string
     */
    public function getJoinDate()
    {
        return $this->joinDate;
    }
}
