<?php

namespace Rizzen\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Email
 *
 * @ORM\Table(name="EmailOptions")
 * @ORM\Entity
 */
class Email
{

    public function __construct()
    {
        $this->receiveEmailDisabled = false;
        $this->receiveEmailForEventInvite = true;
        $this->receiveEmailForEventReminder = true;
        $this->receiveEmailForGuildEvent = true;
        $this->receiveEmailForNewConversation = true;
    }

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var boolean
     *
     * @ORM\Column(name="receiveEmailForEventReminder", type="boolean")
     */
    private $receiveEmailForEventReminder;

    /**
     * @var boolean
     *
     * @ORM\Column(name="receiveEmailForEventInvite", type="boolean")
     */
    private $receiveEmailForEventInvite;

    /**
     * @var boolean
     *
     * @ORM\Column(name="receiveEmailForNewConversation", type="boolean")
     */
    private $receiveEmailForNewConversation;

    /**
     * @var boolean
     *
     * @ORM\Column(name="receiveEmailForGuildEvent", type="boolean")
     */
    private $receiveEmailForGuildEvent;

    /**
     * @var boolean
     *
     * @ORM\Column(name="receiveEmailDisabled", type="boolean")
     */
    private $receiveEmailDisabled;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set receiveEmailForEventReminder
     *
     * @param boolean $receiveEmailForEventReminder
     * @return Email
     */
    public function setReceiveEmailForEventReminder($receiveEmailForEventReminder)
    {
        $this->receiveEmailForEventReminder = $receiveEmailForEventReminder;

        return $this;
    }

    /**
     * Get receiveEmailForEventReminder
     *
     * @return boolean 
     */
    public function getReceiveEmailForEventReminder()
    {
        return $this->receiveEmailForEventReminder;
    }

    /**
     * Set $receiveEmailForEventInvite
     *
     * @param boolean $receiveEmailForEventInvite
     * @return Email
     */
    public function setReceiveEmailForEventInvite($receiveEmailForEventInvite)
    {
        $this->receiveEmailForEventInvite = $receiveEmailForEventInvite;

        return $this;
    }

    /**
     * Get receiveEmailForEventInvite
     *
     * @return boolean 
     */
    public function getReceiveEmailForEventInvite()
    {
        return $this->receiveEmailForEventInvite;
    }

    /**
     * Set receiveEmailForNewConversation
     *
     * @param boolean $receiveEmailForNewConversation
     * @return Email
     */
    public function setReceiveEmailForNewConversation($receiveEmailForNewConversation)
    {
        $this->receiveEmailForNewConversation = $receiveEmailForNewConversation;

        return $this;
    }

    /**
     * Get receiveEmailForNewConversation
     *
     * @return boolean 
     */
    public function getReceiveEmailForNewConversation()
    {
        return $this->receiveEmailForNewConversation;
    }

    /**
     * Set receiveEmailForGuildEvent
     *
     * @param boolean $receiveEmailForGuildEvent
     * @return Email
     */
    public function setReceiveEmailForGuildEvent($receiveEmailForGuildEvent)
    {
        $this->receiveEmailForGuildEvent = $receiveEmailForGuildEvent;

        return $this;
    }

    /**
     * Get receiveEmailForGuildEvent
     *
     * @return boolean 
     */
    public function getReceiveEmailForGuildEvent()
    {
        return $this->receiveEmailForGuildEvent;
    }

    /**
     * Set receiveEmailDisabled
     *
     * @param boolean $receiveEmailDisabled
     * @return Email
     */
    public function setReceiveEmailDisabled($receiveEmailDisabled)
    {
        $this->receiveEmailDisabled = $receiveEmailDisabled;

        return $this;
    }

    /**
     * Get receiveEmailDisabled
     *
     * @return boolean 
     */
    public function getReceiveEmailDisabled()
    {
        return $this->receiveEmailDisabled;
    }
}
