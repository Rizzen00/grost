<?php

namespace Rizzen\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Rizzen\ProfileBundle\FriendRequestStates;

/**
 * FriendRequest
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Rizzen\UserBundle\Entity\FriendRequestRepository")
 */
class FriendRequest
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetimetz")
     */
    private $date;

    /**
     * @ORM\ManyToOne(targetEntity="Rizzen\UserBundle\Entity\User")
     * @ORM\JoinColumn(nullable=false)
     */
    private $from;

    /**
     * @ORM\ManyToOne(targetEntity="Rizzen\UserBundle\Entity\User")
     * @ORM\JoinColumn(nullable=false)
     */
    private $to;

    /**
     * @var integer
     *
     * @ORM\Column(name="state", type="integer")
     */
    private $state;

    function __construct()
    {
        $this->date = new \DateTime('now');
        $this->state = FriendRequestStates::REQUEST_PENDING;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return FriendRequest
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set state
     *
     * @param $state
     * @return FriendRequest
     */
    public function setState($state)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Get state
     *
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Set from
     *
     * @param \Rizzen\UserBundle\Entity\User $from
     * @return FriendRequest
     */
    public function setFrom(\Rizzen\UserBundle\Entity\User $from)
    {
        $this->from = $from;

        return $this;
    }

    /**
     * Get from
     *
     * @return \Rizzen\UserBundle\Entity\User
     */
    public function getFrom()
    {
        return $this->from;
    }

    /**
     * Set to
     *
     * @param \Rizzen\UserBundle\Entity\User $to
     * @return FriendRequest
     */
    public function setTo(\Rizzen\UserBundle\Entity\User $to)
    {
        $this->to = $to;

        return $this;
    }

    /**
     * Get to
     *
     * @return \Rizzen\UserBundle\Entity\User
     */
    public function getTo()
    {
        return $this->to;
    }
}
