<?php

namespace Rizzen\UserBundle\Controller;

use Rizzen\UserBundle\Form\Type\EmailType;
use Rizzen\UserBundle\Form\Type\MessengerType;
use Rizzen\UserBundle\Form\Type\TimezoneType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class OptionsController extends Controller
{
    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $timezoneForm = $this->createForm(TimezoneType::class, $this->getUser());

        $timezoneForm->handleRequest($request);
        if ($timezoneForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash('success', 'Your timezone has been successfully modified!');
            $this->redirect($this->generateUrl('rizzen_options_index'));
        }

        return $this->render('RizzenUserBundle:Options:index.html.twig', [
                'timezoneForm' => $timezoneForm->createView(),
                'hasChangePasswordError' => false
            ]);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function messengerAction(Request $request)
    {
        $form = $this->createForm(MessengerType::class, $this->getUser()->getOptions()->messenger());

        $form->handleRequest($request);
        if ($form->isValid()) {
            $this->get('user.options.submit.processor')->submitMessengerOptions($this->getUser()->getOptions()->messenger());
        }

        return $this->render('RizzenUserBundle:Options:messenger.html.twig', [
                'form' => $form->createView(),
            ]);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function emailAction(Request $request)
    {
        $form = $this->createForm(EmailType::class, $this->getUser()->getOptions()->email());

        $form->handleRequest($request);
        if ($form->isValid()) {
            $this->get('user.options.submit.processor')->submitEmailOptions($this->getUser()->getOptions()->email());
        }

        return $this->render('RizzenUserBundle:Options:email.html.twig', [
                'form' => $form->createView(),
            ]);
    }
}
