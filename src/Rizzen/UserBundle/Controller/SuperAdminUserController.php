<?php

namespace Rizzen\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class SuperAdminUserController extends Controller
{
    public function usersAction()
    {
        $doctrine = $this->getDoctrine();

        $userCount = $doctrine->getRepository('RizzenUserBundle:User')->getUsersCount();
        $users = $doctrine->getRepository('RizzenUserBundle:User')->getUsers();

        return $this->render('RizzenUserBundle:SuperAdmin:usersList.html.twig', [
            'userCount' => $userCount,
            'users' => $users
        ]);
    }

    public function userAction($userID)
    {
        $doctrine = $this->getDoctrine();

        $user = $doctrine->getRepository('RizzenUserBundle:User')->find($userID);
        $guildsCreated = $doctrine->getRepository('RizzenGuildBundle:Guild')->getGuildsCreatedByUser($user);

        return $this->render('RizzenUserBundle:SuperAdmin:user.html.twig', [
            'user' => $user,
            'guildsCreated' => $guildsCreated
        ]);
    }
}
