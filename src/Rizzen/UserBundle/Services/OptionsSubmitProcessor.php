<?php


namespace Rizzen\UserBundle\Services;


use Doctrine\ORM\EntityManager;
use Rizzen\UserBundle\Entity\Email;
use Rizzen\UserBundle\Entity\Messenger;
use Symfony\Component\HttpFoundation\Session\Session;

class OptionsSubmitProcessor
{
    private $em;
    private $session;

    function __construct(EntityManager $em, Session $session)
    {
        $this->em = $em;
        $this->session = $session;
    }

    /**
     * Verify that the form is correct and save it if it is
     *
     * @param Messenger $messenger
     * @return void
     */
    public function submitMessengerOptions(Messenger $messenger)
    {
        $this->verifyMessengerOptions($messenger);
        $this->verifyNotificationOptions($messenger);

        $this->em->flush();

        $this->session->getFlashBag()->add('success', 'Your preferences have been successfully updated!');
    }

    /**
     * Check that there is one option selected and no more that one
     *
     * @param Messenger $messenger
     * @return void
     */
    private function verifyMessengerOptions(Messenger $messenger)
    {
        $messengerCheckedOptionsCount = 0;
        if ($messenger->getReceiveMessageFromAnybody()) {
            $messengerCheckedOptionsCount++;
        }

        if ($messenger->getReceiveMessageFromFriendsOnly()) {
            $messengerCheckedOptionsCount++;
        }

        if ($messenger->getReceiveMessageFromNobody()) {
            $messengerCheckedOptionsCount++;
        }

        if ($messengerCheckedOptionsCount !== 1) {
            $messenger
                ->setReceiveMessageFromNobody(true)
                ->setReceiveMessageFromAnybody(false)
                ->setReceiveMessageFromFriendsOnly(false);
        }

    }

    /**
     * Check that the user did not modify the form and checked the disabled state and any other at the same time
     *
     * @param Messenger $messenger
     * @return void
     */
    private function verifyNotificationOptions(Messenger $messenger)
    {
        if ($messenger->getReceiveNotificationDisabled()) {
            $messenger
                ->setReceiveNotificationForEventInvite(false)
                ->setReceiveNotificationForEventReminder(false)
                ->setReceiveNotificationForGuildEvent(false);
        }

        $notificationCheckedOptionsCount = 0;
        if ($messenger->getReceiveNotificationForEventInvite()) {
            $notificationCheckedOptionsCount++;
        }

        if ($messenger->getReceiveNotificationForEventReminder()) {
            $notificationCheckedOptionsCount++;
        }

        if ($messenger->getReceiveNotificationForGuildEvent()) {
            $notificationCheckedOptionsCount++;
        }

        if ($messenger->getReceiveNotificationDisabled()) {
            $notificationCheckedOptionsCount++;
        }

        if ($notificationCheckedOptionsCount === 0) {
            $messenger->setReceiveNotificationDisabled(true);
        }
    }

    /**
     * Verify and save the new email preferences of the user
     *
     * @param Email $email
     * @return void
     */
    public function submitEmailOptions(Email $email)
    {
        if ($email->getReceiveEmailDisabled()) {
            $email
                ->setReceiveEmailForEventInvite(false)
                ->setReceiveEmailForEventReminder(false)
                ->setReceiveEmailForGuildEvent(false)
                ->setReceiveEmailForNewConversation(false);
        }

        if (!$email->getReceiveEmailDisabled() &&
            !$email->getReceiveEmailForEventInvite() &&
            !$email->getReceiveEmailForEventReminder() &&
            !$email->getReceiveEmailForGuildEvent() &&
            !$email->getReceiveEmailForNewConversation())
        {
            $email->setReceiveEmailDisabled(true);
        }

        $this->em->flush();

        $this->session->getFlashBag()->add('success', 'Your preferences have been successfully updated!');
    }
} 
