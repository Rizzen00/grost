<?php

namespace Rizzen\UserBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

class EmailType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('receiveEmailForEventReminder',   CheckboxType::class, ['required' => false])
            ->add('receiveEmailForEventInvite',     CheckboxType::class, ['required' => false])
            ->add('receiveEmailForNewConversation', CheckboxType::class, ['required' => false])
            ->add('receiveEmailForGuildEvent',      CheckboxType::class, ['required' => false])
            ->add('receiveEmailDisabled',           CheckboxType::class, ['required' => false])
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'Rizzen\UserBundle\Entity\Email'
        ]);
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'grost_options_email';
    }
}
