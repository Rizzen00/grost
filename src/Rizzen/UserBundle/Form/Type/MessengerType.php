<?php

namespace Rizzen\UserBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

class MessengerType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('receiveMessageFromAnybody',           CheckboxType::class, ['required' => false])
            ->add('receiveMessageFromFriendsOnly',       CheckboxType::class, ['required' => false])
            ->add('receiveMessageFromNobody',            CheckboxType::class, ['required' => false])
            ->add('receiveNotificationForEventInvite',   CheckboxType::class, ['required' => false])
            ->add('receiveNotificationForGuildEvent',    CheckboxType::class, ['required' => false])
            ->add('receiveNotificationForEventReminder', CheckboxType::class, ['required' => false])
            ->add('receiveNotificationDisabled',         CheckboxType::class, ['required' => false])
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'Rizzen\UserBundle\Entity\Messenger'
        ]);
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'grost_options_messenger';
    }
}
