<?php

namespace Rizzen\UserBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Bert\TimezoneBundle\Form\ReadableTimezoneType;

class TimezoneType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('timezone', ReadableTimezoneType::class, [
                'label'  => 'New timezone:',
            ]);
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'Rizzen\UserBundle\Entity\User'
        ]);
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'grost_user_timezone';
    }
}
