<?php

namespace Rizzen\UserBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Bert\TimezoneBundle\Form\ReadableTimezoneType;

class RegistrationFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('timezone', ReadableTimezoneType::class);
    }

    public function getParent()
    {
        return 'FOS\UserBundle\Form\Type\RegistrationFormType';
    }

    public function getBlockPrefix()
    {
        return 'grost_register';
    }

    public function getName()
    {
        return $this->getBlockPrefix();
    }
}
