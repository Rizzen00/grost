<?php

namespace Rizzen\MessengerBundle\Services\Util;


use Doctrine\ORM\EntityManager;
use Rizzen\MainBundle\Services\Purifier;
use Rizzen\MainBundle\Services\Slugify;
use Rizzen\MessengerBundle\Entity\Conversation;
use Rizzen\ProfileBundle\Services\FriendManager;
use Rizzen\SocketBundle\Services\SocketProcessor;
use Rizzen\MessengerBundle\Entity\Message;
use Rizzen\UserBundle\Entity\User;
use Symfony\Bundle\TwigBundle\TwigEngine;

/**
 * Service used to create, reply and edit conversations
 *
 * Class MessengerProcessor
 * @package Rizzen\MessengerBundle\Services\Util
 */
class MessengerProcessor implements MessengerProcessorInterface {

    private $em;
    private $socketProcessor;
    private $purifier;
    private $slugify;
    private $messengerUtil;
    private $templating;
    private $friendManager;

    private $sentByMessengerBot;
    private $addToUnreads;

    /**
     * @param EntityManager $em
     * @param SocketProcessor $socketProcessor
     * @param Purifier $purifier
     * @param Util $messengerUtil
     * @param TwigEngine $templating
     * @param FriendManager $friendManager
     */
    public function __construct(EntityManager $em, SocketProcessor $socketProcessor, Purifier $purifier, Util $messengerUtil, TwigEngine $templating, FriendManager $friendManager)
    {
        $this->slugify = new Slugify();
        $this->purifier = $purifier;
        $this->socketProcessor = $socketProcessor;
        $this->em = $em;
        $this->messengerUtil = $messengerUtil;
        $this->templating = $templating;

        $this->sentByMessengerBot = false;
        $this->addToUnreads = true;
        $this->friendManager = $friendManager;
    }

    /**
     * {@inheritdoc}
     */
    public function createNewConversation($recipient, Conversation $conversation, User $user = null)
    {
        $this->socketProcessor->clearTopics();

        $result['hasBeenCreated'] = false;

        // The recipient var will contain the user object already if it sent by the bot.
        if (!$this->sentByMessengerBot) {
            $canonicalUsername = $this->slugify->getCanonicalized($conversation->getRecipientUsername());

            /** @var User $recipient */
            $recipient = $this->em->getRepository('RizzenUserBundle:User')->findOneByUsernameCanonical($canonicalUsername);

            if ($recipient === null || $recipient === $user) {

                $result['errorMessage'] = 'The recipient was not found.';
                return $result;
            }

            if ($recipient->getOptions()->messenger()->getReceiveMessageFromFriendsOnly() && !$this->friendManager->areFriends($recipient, $user)) {
                $result['errorMessage'] = 'You need to be a friend of this recipient to start a new conversation with him.';
                return $result;
            }
        }

        if ($recipient->getOptions()->messenger()->getReceiveMessageFromNobody()) {
            $result['errorMessage'] = 'The recipient was found, but he disabled his inbox and won\'t receive any new conversation.';
            return $result;
        }

        $message = new Message;
        $message->setConversation($conversation);
        $message->setBot($this->sentByMessengerBot);

        if ($this->sentByMessengerBot) {
            $conversation->setBot(true);
            $message->setContent($conversation->getFirstMessage()); // keep the unfiltered template when sent by the bot
        } else {
            $message->setCreator($user);
            $message->setContent($this->purifier->purify('default', $conversation->getFirstMessage()));
            $conversation->setCreator($user);
            $conversation->addParticipant($user);
        }

        $conversation->addParticipant($recipient);
        $conversation->setLastMessage($message);
        $conversation->setLastMessageDate(new \DateTime('now'));
        $conversation->addMessage($message);

        if (!$this->sentByMessengerBot) {
            $user->addConversation($conversation);
        }

        $recipient->addConversation($conversation);

        $this->em->persist($conversation);
        $this->em->persist($message);
        $this->em->persist($recipient);
        $this->em->flush();

        $slug = $this->slugify->getSluged($conversation->getTitle().'-'.$conversation->getId());

        $conversation->setSlug($slug);

        $this->em->persist($conversation);
        $this->em->flush();

        $this->messengerUtil->setUnreads($conversation->getParticipants(), $user, $conversation, $this->sentByMessengerBot, true);

        return [
            'hasBeenCreated' => true,
            'slug' => $slug
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function reply(Conversation $conversation, Message $reply, User $user = null)
    {

        if (!$this->sentByMessengerBot) {
            $reply->setCreator($user);
            $reply->setContent($this->purifier->purify('default', $reply->getContent()));
        }

        $reply->setBot($this->sentByMessengerBot);
        $reply->setConversation($conversation);
        $conversation->setLastMessage($reply);
        $conversation->addMessage($reply);

        $this->em->persist($conversation);
        $this->em->persist($reply);
        $this->em->flush();

        if ($this->addToUnreads) {
            $this->messengerUtil->setUnreads($conversation->getParticipants(), $user, $conversation);
        }

        $templateParam['HTML'] = $this->templating->render('RizzenMessengerBundle:Messenger:message.html.twig', [
                'message' => $reply,
        ]);

        $additionalInfo['wrapId'] = '#msg-' . $reply->getId();

        if (!$this->sentByMessengerBot) {
            $additionalInfo['sentBy'] = $user->getUsername();
        } else {
            $additionalInfo['sentBy'] = "Grost's Bot";
        }

        $this->socketProcessor
            ->addTopic($conversation->getSocketTopic())
            ->setSubtopic('conversationResponse')
            ->useTemplate('raw', $templateParam)
            ->setAdditionalInfo($additionalInfo)
            ->broadcast();

        return ['hasBeenSent' => true];
    }

    /**
     * {@inheritdoc}
     */
    public function setSentByMessengerBot($sentByMessengerBot)
    {
        $this->sentByMessengerBot = $sentByMessengerBot;
    }

    /**
     * {@inheritdoc}
     */
    public function setAddToUnreads($addToUnreads)
    {
        $this->addToUnreads = $addToUnreads;
    }
}
