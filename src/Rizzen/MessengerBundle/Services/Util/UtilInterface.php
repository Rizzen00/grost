<?php
/**
 * Created by PhpStorm.
 * User: Bryan
 * Date: 06/11/2014
 * Time: 16:15
 */

namespace Rizzen\MessengerBundle\Services\Util;


use Rizzen\MessengerBundle\Entity\Conversation;
use Rizzen\UserBundle\Entity\User;

interface UtilInterface {

    /**
     * Verify if $user is in the list of participants
     *
     * @param User $user
     * @param \Doctrine\Common\Collections\Collection $participants
     */
    public function isParticipant(User $user, $participants);

    /**
     * Set the conversation as read for $user when he look at a conversation
     *
     * @param \Doctrine\Common\Collections\ArrayCollection $unreads
     * @param Conversation $conversation
     * @param User $user
     */
    public function verifyUnreads($unreads, Conversation $conversation, User $user);

    /**
     * Set all conversations as read
     *
     * @param \Doctrine\Common\Collections\ArrayCollection $unreads
     * @param User $user    Current User
     */
    public function cleanUnreads($unreads, User $user);

    /**
     * Set the conversation as unread for the participants of the conversation if it isn't already, then send a real-time notification
     *
     * @param \Doctrine\Common\Collections\Collection $participants
     * @param User $user                    The current user
     * @param Conversation $conversation    The conversation
     * @param bool $sentByBot               Is the message sent by a bot
     * @param bool $newConversation         Is it a new conversation
     * @return
     */
    public function setUnreads($participants, User $user, Conversation $conversation, $sentByBot, $newConversation);

} 
