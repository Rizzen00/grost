<?php

namespace Rizzen\MessengerBundle\Services\Util;


use Rizzen\MessengerBundle\Entity\Conversation;
use Rizzen\MessengerBundle\Entity\Message;
use Rizzen\UserBundle\Entity\User;

interface MessengerProcessorInterface {

    /**
     * Create a new conversation, handle the creation by a client or via the messenger bot
     *
     * @param string|User $recipient
     * @param Conversation $conversation
     * @param User|null $user
     * @return mixed
     */
    public function createNewConversation($recipient, Conversation $conversation, User $user = null);

    /**
     * Reply to an already created conversation, handle the reply of a client or from the messenger bot
     *
     * @param Conversation $conversation
     * @param Message $reply
     * @param User $user
     * @return mixed
     */
    public function reply(Conversation $conversation, Message $reply, User $user = null);

    /**
     * @param boolean $sentByMessengerBot
     */
    public function setSentByMessengerBot($sentByMessengerBot);

    /**
     * @param boolean $addToUnreads
     */
    public function setAddToUnreads($addToUnreads);

} 
