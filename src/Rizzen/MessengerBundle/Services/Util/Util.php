<?php

namespace Rizzen\MessengerBundle\Services\Util;

use Doctrine\ORM\EntityManager;
use Rizzen\MessengerBundle\Entity\Conversation;
use Rizzen\SocketBundle\Services\SocketProcessor;
use Rizzen\UserBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Routing\Router;

/**
 * Utilities used frequently in the MessengerBundle
 *
 * Class Util
 * @package Rizzen\MessengerBundle\Services\Util
 */
class Util implements UtilInterface
{
    private $em;
    private $socketProcessor;
    private $router;

    /**
     * @param EntityManager $em
     * @param SocketProcessor $socketProcessor
     * @param Router $router
     */
    public function __construct(EntityManager $em, SocketProcessor $socketProcessor, Router $router)
    {
        $this->em = $em;
        $this->socketProcessor = $socketProcessor;
        $this->router = $router;
    }

    /**
     * {@inheritdoc}
     */
    public function isParticipant(User $user, $participants)
    {
        if ($participants->contains($user)) {
            return true;
        }

        return false;
    }

    /**
     * {@inheritdoc}
     */
    public function verifyUnreads($unreads, Conversation $conversation, User $user)
    {

        if ($unreads->contains($conversation)) {
            $user->removeUnread($conversation);
            $this->em->flush();
        }

        return;
    }

    /**
     * {@inheritdoc}
     */
    public function cleanUnreads($unreads, User $user)
    {
        foreach ($unreads as $unread) {
            $user->removeUnread($unread);
        }
        $this->em->flush();
        return;
    }

    /**
     * {@inheritdoc}
     */
    public function setUnreads($participants, User $user = null, Conversation $conversation, $sentByBot = false, $newConversation = false)
    {
        $hasSwitchedSomeone = false;

        /** @var User $participant */
        foreach ($participants as $participant) {

            if (($sentByBot || $participant !== $user) && !$participant->getUnreads()->contains($conversation)) {
                $this->socketProcessor->addTopic($participant->getSocketTopic());
                $participant->addUnread($conversation);
                $hasSwitchedSomeone = true;
            }
        }

        if ($hasSwitchedSomeone) {

            $username = $sentByBot ? "Grost's Bot" : $user->getUsername();

            $this->em->flush();
            $additionalInfo = [
                'sentBy' => $username,
                'conversationSlug' => $conversation->getSlug(),
                'conversationTitle' => $conversation->getTitle(),
                'conversationUrl' => $this->router->generate('rizzen_messenger_conversation', ['slug' => $conversation->getSlug()], true)
            ];

            $subTopic = $newConversation ? 'newUnreadConversation' : 'newUnreadConversationMessage';

            $this->socketProcessor
                ->setAdditionalInfo($additionalInfo)
                ->setSubtopic($subTopic)
                ->useTemplate('raw', null)
                ->broadcast();

        }

        $this->socketProcessor->clearTopics();
    }
}
