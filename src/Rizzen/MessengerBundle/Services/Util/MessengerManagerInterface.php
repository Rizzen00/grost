<?php

namespace Rizzen\MessengerBundle\Services\Util;

use Rizzen\UserBundle\Entity\User;

interface MessengerManagerInterface {

    /**
     * Invite another user to an already created conversation
     *
     * @param string $userToInvite
     * @param User $user
     * @param string $slug
     * @return mixed
     */
    public function invite($userToInvite, User $user, $slug);

    /**
     * Leave a conversation
     *
     * @param string $slug
     * @param User $user
     * @return mixed
     */
    public function leave($slug, User $user);

} 
