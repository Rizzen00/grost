<?php

namespace Rizzen\MessengerBundle\Services\Util;

use Doctrine\ORM\EntityManager;
use Rizzen\MainBundle\Services\Slugify;
use Rizzen\MessengerBundle\Services\Bot\Bot;
use Rizzen\ProfileBundle\Services\FriendManager;
use Rizzen\UserBundle\Entity\User;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class MessengerManager implements MessengerManagerInterface{

    private $em;
    private $messengerUtil;
    private $slugify;
    private $messengerBot;
    private $friendManager;

    /**
     * @param EntityManager $em
     * @param Util $messengerUtil
     * @param Bot $messengerBot
     * @param FriendManager $friendManager
     */
    public function __construct(EntityManager $em, Util $messengerUtil, Bot $messengerBot, FriendManager $friendManager)
    {

        $this->em = $em;
        $this->messengerUtil = $messengerUtil;
        $this->slugify = new Slugify();
        $this->messengerBot = $messengerBot;
        $this->friendManager = $friendManager;
    }

    /**
     * {@inheritdoc}
     */
    public function invite($userToInvite, User $user, $slug)
    {

        $conversation = $this->em->getRepository('RizzenMessengerBundle:Conversation')->findOneBySlug($slug);

        if ($conversation === null || !$this->messengerUtil->isParticipant($user, $conversation->getParticipants())) {
            throw new NotFoundHttpException('The conversation was not found.');
        }

        /** @var User $newParticipant */
        $userToInvite = $this->slugify->getCanonicalized($userToInvite);
        $newParticipant = $this->em->getRepository('RizzenUserBundle:User')->findOneByUsernameCanonical($userToInvite);

        if ($newParticipant === null) {
            return [
                    'invited' => false,
                    'reason' => 'User not found'
                ];
        } elseif ($this->messengerUtil->isParticipant($newParticipant, $conversation->getParticipants()) === true) {
            return [
                'invited' => false,
                'reason' => 'User already in the conversation'
            ];
        } elseif ($newParticipant->getOptions()->messenger()->getReceiveMessageFromNobody()) {
            return [
                'invited' => false,
                'reason' => 'Recipient inbox disabled'
            ];
        } elseif ($newParticipant->getOptions()->messenger()->getReceiveMessageFromNobody()) {
            return [
                'invited' => false,
                'reason' => 'Recipient inbox disabled'
            ];
        } elseif ($newParticipant->getOptions()->messenger()->getReceiveMessageFromFriendsOnly() && !$this->friendManager->areFriends($user, $newParticipant)) {
            return [
                'invited' => false,
                'reason' => 'Recipient not in friends list'
            ];
        }

        $conversation->addParticipant($newParticipant);
        $newParticipant->addConversation($conversation);
        $newParticipant->addUnread($conversation);

        $this->em->persist($conversation);
        $this->em->persist($newParticipant);
        $this->em->flush();

        $this->messengerBot->setTarget($conversation);
        $this->messengerBot->useTemplate('RizzenMessengerBundle:Message:invitedToConversation.html.twig', [
                'invitedBy' => $user,
                'invited' => $newParticipant
            ]);
        $this->messengerBot->postMessage();

        return ['invited' => true];
    }

    /**
     * {@inheritdoc}
     */
    public function leave($slug, User $user)
    {
        $conversation = $this->em->getRepository('RizzenMessengerBundle:Conversation')->findOneBySlug($slug);
        if ($conversation === null || !$this->messengerUtil->isParticipant($user, $conversation->getParticipants())) {
            throw new NotFoundHttpException('The conversation was not found.');
        }

        $conversation->removeParticipant($user);
        $user->removeConversation($conversation);

        $this->em->persist($conversation);
        $this->em->persist($user);
        $this->em->flush();

        return;
    }
}
