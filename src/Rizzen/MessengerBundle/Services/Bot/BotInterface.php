<?php

namespace Rizzen\MessengerBundle\Services\Bot;


use Rizzen\MessengerBundle\Entity\Conversation;
use Rizzen\UserBundle\Entity\User;

interface BotInterface {

    /**
     * Post the conversation
     */
    public function postConversation();

    /**
     * Post the message in the target conversation
     */
    public function postMessage();

    /**
     * Set the recipient to which the bot will send a new conversation
     *
     * @param User $recipient
     */
    public function setRecipient(User $recipient);

    /**
     * Generate the template we'll use in the message
     *
     * @param string $template
     * @param array $templateParam
     */
    public function useTemplate($template, Array $templateParam);

    /**
     * Title of the conversation to create
     *
     * @param string $title
     */
    public function setTitle($title);

    /**
     * Conversation in which we'll send a message in
     *
     * @param $target
     */
    public function setTarget(Conversation $target);

} 
