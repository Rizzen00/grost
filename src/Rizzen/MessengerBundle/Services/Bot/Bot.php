<?php

namespace Rizzen\MessengerBundle\Services\Bot;

use Rizzen\MessengerBundle\Entity\Conversation;
use Rizzen\MessengerBundle\Entity\Message;
use Rizzen\MessengerBundle\Services\Util\MessengerProcessor;
use Rizzen\UserBundle\Entity\User;
use Symfony\Bundle\TwigBundle\TwigEngine;

class Bot implements BotInterface
{
    private $messengerProcessor;
    private $templating;

    private $title;
    private $template;
    private $recipient;
    private $target;

    /**
     * @param MessengerProcessor $messengerProcessor
     * @param TwigEngine $templating
     */
    public function __construct(MessengerProcessor $messengerProcessor, TwigEngine $templating)
    {
        $this->messengerProcessor = $messengerProcessor;
        $this->templating = $templating;
    }

    /**
     * {@inheritdoc}
     */
    public function postConversation()
    {
        $conversation = new Conversation;
        $conversation->setFirstMessage($this->template);
        $conversation->setTitle($this->title);

        $this->messengerProcessor->setSentByMessengerBot(true);
        $this->messengerProcessor->createNewConversation($this->recipient, $conversation);
        return;
    }

    /**
     * {@inheritdoc}
     */
    public function postMessage()
    {
        $message = new Message;

        $message->setContent($this->template);

        $this->messengerProcessor->setSentByMessengerBot(true);
        $this->messengerProcessor->setAddToUnreads(false);
        $this->messengerProcessor->reply($this->target, $message);

    }

    /**
     * {@inheritdoc}
     */
    public function setRecipient(User $recipient)
    {
        $this->recipient = $recipient;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function useTemplate($template, Array $templateParam)
    {
        $this->template = $this->templating->render($template, $templateParam);

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setTarget(Conversation $target)
    {
        $this->target = $target;

        return $this;
    }
}
