<?php

namespace Rizzen\MessengerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Message
 *
 * @ORM\Table(name="PrivateMessage")
 * @ORM\Entity(repositoryClass="Rizzen\MessengerBundle\Entity\MessageRepository")
 */
class Message
{
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->date = new \Datetime('now');
        $this->isBot = false;
    }

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text")
     *
     * @Assert\NotBlank()
     * @Assert\Length(
     *      min = "1",
     *      max = "5500"
     * )
     */
    private $content;

    /**
     * @var string
     *
     * @ORM\Column(name="ipAddress", type="cidr", nullable=true)
     */
    protected $ipAddress;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetimetz")
     */
    private $date;

    /**
     * @var boolean
     *
     * @ORM\Column(name="isBot", type="boolean")
     */
    private $isBot;

    /**
     * @ORM\ManyToOne(targetEntity="Rizzen\UserBundle\Entity\User", cascade={"persist"}, fetch="EAGER")
     * @ORM\JoinColumn(nullable=true)
     */
    private $creator;

    /**
     * @ORM\ManyToOne(targetEntity="Rizzen\MessengerBundle\Entity\Conversation", inversedBy="messages")
     */
    protected $conversation;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set content
     *
     * @param string $content
     * @return Message
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return Message
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set conversation
     *
     * @param \Rizzen\MessengerBundle\Entity\Conversation $conversation
     * @return Message
     */
    public function setConversation(\Rizzen\MessengerBundle\Entity\Conversation $conversation = null)
    {
        $this->conversation = $conversation;

        return $this;
    }

    /**
     * Get conversation
     *
     * @return \Rizzen\MessengerBundle\Entity\Conversation
     */
    public function getConversation()
    {
        return $this->conversation;
    }

    /**
     * Set isBot
     *
     * @param boolean $isBot
     * @return Message
     */
    public function setBot($isBot)
    {
        $this->isBot = $isBot;

        return $this;
    }

    /**
     * Get isBot
     *
     * @return boolean 
     */
    public function isBot()
    {
        return $this->isBot;
    }

    /**
     * Set creator
     *
     * @param \Rizzen\UserBundle\Entity\User $creator
     * @return Message
     */
    public function setCreator(\Rizzen\UserBundle\Entity\User $creator = null)
    {
        $this->creator = $creator;

        return $this;
    }

    /**
     * Get creator
     *
     * @return \Rizzen\UserBundle\Entity\User 
     */
    public function getCreator()
    {
        return $this->creator;
    }

    /**
     * Set ipAddress
     *
     * @param string $ipAddress
     * @return Message
     */
    public function setIpAddress($ipAddress)
    {
        $this->ipAddress = $ipAddress;

        return $this;
    }

    /**
     * Get ipAddress
     *
     * @return string
     */
    public function getIpAddress()
    {
        return $this->ipAddress;
    }
}
