<?php

namespace Rizzen\MessengerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Conversation
 *
 * @ORM\Table(indexes={@ORM\Index(name="conversation_slug_idx", columns={"slug"})})
 * @ORM\Entity(repositoryClass="Rizzen\MessengerBundle\Entity\ConversationRepository")
 */
class Conversation
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Rizzen\UserBundle\Entity\User")
     * @ORM\JoinColumn(nullable=true)
     */
    private $creator;

    /**
     * @ORM\ManyToMany(targetEntity="Rizzen\UserBundle\Entity\User", mappedBy="conversations", cascade={"persist"})
     */
    private $participants;

    /**
     * @Assert\NotBlank()
     * @Assert\Length(
     *      min = "1",
     *      max = "5500"
     * )
     */
    private $firstMessage;

    /**
     * @ORM\OneToMany(targetEntity="Rizzen\MessengerBundle\Entity\Message", mappedBy="conversation", cascade={"remove"})
     */
    private $messages;

    /**
     * @Assert\NotBlank()
     * @Assert\Length(
     *      min = "3",
     *      max = "15"
     * )
     */
    private $recipientUsername;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=100)
     *
     * @Assert\NotBlank()
     * @Assert\Length(
     *      min = "5",
     *      max = "30"
     * )
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="ipAddress", type="cidr", nullable=true)
     */
    protected $ipAddress;

    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=255, nullable=true)
     */
    private $slug;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetimetz")
     */
    private $date;

    /**
     * @ORM\OneToOne(targetEntity="Message", cascade={"persist"}, fetch="EAGER")
     */
    protected $lastMessage;

    /**
    * @var \DateTime
    *
    * @ORM\Column(name="lastMessageDate", type="datetimetz")
    */
    protected $lastMessageDate;

    /**
     * @var boolean
     *
     * @ORM\Column(name="isBot", type="boolean")
     */
    private $isBot;

    /**
     * @var string
     *
     * @ORM\Column(name="socketTopic", type="string", nullable=true)
     */
    private $socketTopic;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return Conversation
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set creator
     *
     * @param \Rizzen\UserBundle\Entity\User $creator
     * @return Conversation
     */
    public function setCreator(\Rizzen\UserBundle\Entity\User $creator)
    {
        $this->creator = $creator;

        return $this;
    }

    /**
     * Get creator
     *
     * @return \Rizzen\UserBundle\Entity\User
     */
    public function getCreator()
    {
        return $this->creator;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Conversation
     */
    public function setTitle($title)
    {
        $this->title = trim($title);

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return Conversation
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        $this->setSocketTopic('private-messenger-' . $slug);

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set firstMessage
     *
     * @param string $firstMessage
     * @return Conversation
     */
    public function setFirstMessage($firstMessage)
    {
        $this->firstMessage = $firstMessage;

        return $this;
    }

    /**
     * Get firstMessage
     *
     * @return string
     */
    public function getFirstMessage()
    {
        return $this->firstMessage;
    }

    /**
     * Set isBot
     *
     * @param boolean $isBot
     * @return Conversation
     */
    public function setBot($isBot)
    {
        $this->isBot = $isBot;

        return $this;
    }

    /**
     * Get isBot
     *
     * @return boolean
     */
    public function isBot()
    {
        return $this->isBot;
    }

    /**
     * Set recipientUsername
     *
     * @param string $recipientUsername
     * @return Conversation
     */
    public function setRecipientUsername($recipientUsername)
    {
        $this->recipientUsername = $recipientUsername;

        return $this;
    }

    /**
     * Get recipientUsername
     *
     * @return string
     */
    public function getRecipientUsername()
    {
        return $this->recipientUsername;
    }

    /**
     * Add participants
     *
     * @param \Rizzen\UserBundle\Entity\User $participants
     * @return Conversation
     */
    public function addParticipant(\Rizzen\UserBundle\Entity\User $participants)
    {
        $this->participants[] = $participants;

        return $this;
    }

    /**
     * Remove participants
     *
     * @param \Rizzen\UserBundle\Entity\User $participants
     */
    public function removeParticipant(\Rizzen\UserBundle\Entity\User $participants)
    {
        $this->participants->removeElement($participants);
    }

    /**
     * Get participants
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getParticipants()
    {
        return $this->participants;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->participants = new \Doctrine\Common\Collections\ArrayCollection();
        $this->date = new \Datetime('now');
        $this->lastMessageDate = new \Datetime('now');
        $this->isBot = false;
        $this->participants = new \Doctrine\Common\Collections\ArrayCollection();
        $this->messages = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set socketTopic
     *
     * @param string $socketTopic
     * @return Conversation
     */
    public function setSocketTopic($socketTopic)
    {
        $this->socketTopic = $socketTopic;

        return $this;
    }

    /**
     * Get socketTopic
     *
     * @return string
     */
    public function getSocketTopic()
    {
        return $this->socketTopic;
    }

    /**
     * Add messages
     *
     * @param \Rizzen\MessengerBundle\Entity\Message $messages
     * @return Conversation
     */
    public function addMessage(\Rizzen\MessengerBundle\Entity\Message $messages)
    {
        $this->messages[] = $messages;

        return $this;
    }

    /**
     * Remove messages
     *
     * @param \Rizzen\MessengerBundle\Entity\Message $messages
     */
    public function removeMessage(\Rizzen\MessengerBundle\Entity\Message $messages)
    {
        $this->messages->removeElement($messages);
    }

    /**
     * Get messages
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMessages()
    {
        return $this->messages;
    }

    /**
     * Set lastMessage
     *
     * @param \Rizzen\MessengerBundle\Entity\Message $lastMessage
     * @return Conversation
     */
    public function setLastMessage(\Rizzen\MessengerBundle\Entity\Message $lastMessage = null)
    {
        $this->lastMessage = $lastMessage;

        return $this;
    }

    /**
     * Get lastMessage
     *
     * @return \Rizzen\MessengerBundle\Entity\Message 
     */
    public function getLastMessage()
    {
        return $this->lastMessage;
    }

    /**
     * Set lastMessageDate
     *
     * @param \DateTime $lastMessageDate
     * @return Conversation
     */
    public function setLastMessageDate($lastMessageDate)
    {
        $this->lastMessageDate = $lastMessageDate;

        return $this;
    }

    /**
     * Get lastMessageDate
     *
     * @return \DateTime 
     */
    public function getLastMessageDate()
    {
        return $this->lastMessageDate;
    }

    /**
     * Set ipAddress
     *
     * @param string $ipAddress
     * @return Conversation
     */
    public function setIpAddress($ipAddress)
    {
        $this->ipAddress = $ipAddress;

        return $this;
    }

    /**
     * Get ipAddress
     *
     * @return string
     */
    public function getIpAddress()
    {
        return $this->ipAddress;
    }
}
