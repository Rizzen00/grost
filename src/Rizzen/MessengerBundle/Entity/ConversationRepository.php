<?php

namespace Rizzen\MessengerBundle\Entity;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;

/**
 * ConversationRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class ConversationRepository extends EntityRepository
{
    public function getConversation($slug)
    {
        $qb =  $this->createQueryBuilder('conversation');
        $qb->where('conversation.slug = :slug')
            ->setParameter('slug', $slug)
            ->leftJoin('conversation.creator', 'conversationCreator')
            ->addSelect('conversationCreator')
            ->leftJoin('conversation.participants', 'participants')
            ->addSelect('participants')
            ->leftJoin('conversation.messages', 'messages')
            ->addSelect('messages')
            ->orderBy('messages.date', 'ASC')
            ->leftJoin('messages.creator', 'creator')
            ->addSelect('creator')
            ->leftJoin('creator.avatar', 'avatar')
            ->addSelect('avatar');

        return $qb->getQuery()->setHint(Query::HINT_FORCE_PARTIAL_LOAD, true)->getSingleResult();
    }
}
