<?php

namespace Rizzen\MessengerBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Rizzen\MessengerBundle\Entity\Conversation;
use Rizzen\MessengerBundle\Entity\Message;
use Rizzen\MessengerBundle\Form\Type\ConversationType;
use Rizzen\MessengerBundle\Form\Type\MessageType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

class MessengerController extends Controller
{
    /**
     * View the inbox
     *
     * @param int $page
     * @return Response
     */
    public function inboxAction($page)
    {
        return $this->render('RizzenMessengerBundle:Messenger:inbox.html.twig', [
            'pagination' => $this->get('knp_paginator')->paginate($this->getUser()->getConversations(), $page, 10)
        ]);
    }

    /**
     * View an already created conversation
     *
     * @param string $slug
     * @param int $page
     * @return Response
     * @throws \Exception
     */
    public function conversationAction($slug, $page)
    {
        $em = $this->getDoctrine()->getManager();

        /** @var Conversation $conversation */
        $conversation = $em->getRepository('RizzenMessengerBundle:Conversation')->getConversation($slug);
        if ($conversation === null) {
            throw $this->createNotFoundException('The conversation was not found.');
        }

        $util = $this->container->get('messenger.util');

        if (!$util->isParticipant($this->getUser(), $conversation->getParticipants())) {
            throw $this->createNotFoundException('The conversation was not found.');
        }

        $util->verifyUnreads($this->getUser()->getUnreads(), $conversation, $this->getUser());

        $form = $this->createForm(MessageType::class, new Message);

        return $this->render('RizzenMessengerBundle:Messenger:conversation.html.twig', [
            'conversation' => $conversation,
            'messages' => $this->get('knp_paginator')->paginate($conversation->getMessages(), $page, 8),
            'form' => $form->createView(),
        ]);
    }

    /**
     * Reply to an already created conversation
     *
     * @param string$slug
     * @param Request $request
     * @return Response
     * @throws \Exception
     */
    public function replyAction($slug, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $conversation = $em->getRepository('RizzenMessengerBundle:Conversation')->findOneBySlug($slug);

        $util = $this->container->get('messenger.util');
        if (!$util->isParticipant($this->getUser(), $conversation->getParticipants())) {
            throw $this->createNotFoundException('The conversation was not found.');
        }

        $message = new Message;
        $form = $this->createForm(MessageType::class, $message);

        $response = ['hasBeenSent' => false];

        $form->handleRequest($request);
        if ($form->isValid()) {
            $message->setIpAddress($request->getClientIp());
            $response = $this->get('messenger.processor')->reply($conversation, $message, $this->getUser());
        }

        return new JsonResponse($response);
    }

    /**
     * Invite someone to an already created conversation
     *
     * @param string $slug
     * @param Request $request
     * @return JsonResponse
     */
    public function inviteAction($slug, Request $request)
    {
        if ($this->isCsrfTokenValid('inviteUserInput_intention', $request->request->get('_csrf_token'))) {
            $result = $this->get('messenger.manager')->invite($request->request->get('username'), $this->getUser(), $slug);
            return new JsonResponse($result);
        }

        return new JsonResponse([
                'invited' => false,
                'reason' => 'CSRF Token invalid'
            ]);

    }

    /**
     * Create a new conversation
     *
     * @param string|null $recipient
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function writeAction($recipient = null, Request $request)
    {
        $conversation = new Conversation;
        if ($recipient !== null) {
            $conversation->setRecipientUsername($recipient);
        }

        $form = $this->createForm(ConversationType::class, $conversation);

        $form->handleRequest($request);
        if ($form->isValid()) {
            $conversation->setIpAddress($request->getClientIp());
            $result = $this->get('messenger.processor')->createNewConversation($recipient, $conversation, $this->getUser());

            // If the recipient was not found or the client tried to send a message to himself
            if (!$result['hasBeenCreated']) {
                $this->addFlash('error', $result['errorMessage']);
                return $this->render('RizzenMessengerBundle:Messenger:write.html.twig', ['form' => $form->createView()]);
            }
            return $this->redirect($this->generateUrl('rizzen_messenger_conversation', ['slug' => $result['slug']]));
        }

        return $this->render('RizzenMessengerBundle:Messenger:write.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * Set all the client conversation to the read state
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function clearUnreadsAction()
    {
        $this->container->get('messenger.util')->cleanUnreads($this->getUser()->getUnreads(), $this->getUser());
        return $this->redirect($this->generateUrl('rizzen_messenger_inbox'));
    }

    /**
     * Leave a conversation
     *
     * @param string $slug
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function leaveAction($slug)
    {
        $this->get('messenger.manager')->leave($slug, $this->getUser());
        $this->addFlash('success', 'You left the conversation!');
        return $this->redirect($this->generateUrl('rizzen_messenger_inbox'));
    }

    /**
     * When the user is on the page where a message has been sent
     *
     * @param string $slug
     * @return Response
     */
    public function unreadCallbackAction($slug)
    {
        $util = $this->container->get('messenger.util');

        /** @var Conversation $conversation */
        $conversation = $this->getDoctrine()->getManager()->getRepository('RizzenMessengerBundle:Conversation')->findOneBySlug($slug);
        if ($conversation !== null && $util->isParticipant($this->getUser(), $conversation->getParticipants())) {
            $this->container->get('messenger.util')->verifyUnreads($this->getUser()->getUnreads(), $conversation, $this->getUser());
        }

        return new Response();
    }
}
