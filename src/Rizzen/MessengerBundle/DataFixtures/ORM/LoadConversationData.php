<?php

namespace Rizzen\MessengerBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Rizzen\UserBundle\Entity\User;
use Doctrine\Common\Persistence\ObjectManager;
use Rizzen\MessengerBundle\Entity\Conversation;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadConversationData extends AbstractFixture implements OrderedFixtureInterface, FixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
        $userManager = $this->container->get('fos_user.user_manager');

        /** @var User $creator */
        $creator = $userManager->findUserBy(['username' => 'testUser0']);

        $conversation = new Conversation();

        $conversation->setTitle('TestConversation');
        $conversation->setFirstMessage('This is a test conversation');
        $conversation->setRecipientUsername('testUser1');

        $messengerProcessor =  $this->container->get('messenger.processor');

        $messengerProcessor->setSentByMessengerBot(false);
        $messengerProcessor->createNewConversation(null, $conversation, $creator);
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 8;
    }
}
