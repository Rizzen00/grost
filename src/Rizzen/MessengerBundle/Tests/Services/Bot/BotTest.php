<?php

namespace Rizzen\MessengerBundle\Tests\Services\Bot;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Rizzen\MessengerBundle\Services\Bot\Bot;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Request;

class BotTest extends WebTestCase
{

    /** @var Bot $messengerBot */
    private $messengerBot;

    /** @var EntityManager $em */
    private $em;

    public function setUp()
    {
        static::$kernel = static::createKernel();
        static::$kernel->boot();
        $this->em = static::$kernel->getContainer()->get('doctrine.orm.entity_manager');
        $this->messengerBot = static::$kernel->getContainer()->get('messenger.bot');
    }

    public function testPostConversation()
    {
        $recipient1 = $this->em->getRepository('RizzenUserBundle:User')->find(1);
        $recipient2 = $this->em->getRepository('RizzenUserBundle:User')->find(2);

        $this->messengerBot->setTitle('testing the bot');
        $this->messengerBot->setRecipient($recipient1);
        $this->messengerBot->useTemplate('RizzenMessengerBundle:Message:invitedToConversation.html.twig', [
                'invitedBy' => $recipient1,
                'invited' => $recipient2
            ]);

        $this->messengerBot->postConversation();
    }

    public function testPostMessage()
    {
        $conversation = $this->em->getRepository('RizzenMessengerBundle:Conversation')->find(1);
        $recipient1 = $this->em->getRepository('RizzenUserBundle:User')->find(1);
        $recipient2 = $this->em->getRepository('RizzenUserBundle:User')->find(2);

        $this->messengerBot->setTarget($conversation);
        $this->messengerBot->useTemplate('RizzenMessengerBundle:Message:invitedToConversation.html.twig', [
                'invitedBy' => $recipient1,
                'invited' => $recipient2
            ]);
        $this->messengerBot->postMessage();
    }
}
