<?php

namespace Rizzen\MessengerBundle\Tests\Services\Util;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Rizzen\MessengerBundle\Services\Util\MessengerManager;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Request;

class MessengerManagerTest extends WebTestCase
{
    private $userManager;

    /** @var MessengerManager $messengerManager */
    private $messengerManager;

    /** @var EntityManager $em */
    private $em;

    public function setUp()
    {
        static::$kernel = static::createKernel();
        static::$kernel->boot();
        $this->userManager = static::$kernel->getContainer()->get('fos_user.user_manager');
        $this->em = static::$kernel->getContainer()->get('doctrine.orm.entity_manager');
        $this->messengerManager = static::$kernel->getContainer()->get('messenger.manager');
    }

    public function testInvite()
    {
        $conversation = $this->em->getRepository('RizzenMessengerBundle:Conversation')->findOneByTitle('TestConversation');
        $user = $this->userManager->findUserBy(['username' => 'testUser0']);

        $result = $this->messengerManager->invite('testUser2', $user, $conversation->getSlug());

        $this->assertTrue($result['invited']);
    }

    public function testInviteParticipant()
    {
        $conversation = $this->em->getRepository('RizzenMessengerBundle:Conversation')->findOneByTitle('TestConversation');

        $user = $this->userManager->findUserBy(['username' => 'testUser0']);

        $result = $this->messengerManager->invite('testUser2', $user, $conversation->getSlug());

        $this->assertFalse($result['invited']);
    }

    public function testInviteUserNotFound()
    {
        $conversation = $this->em->getRepository('RizzenMessengerBundle:Conversation')->findOneByTitle('TestConversation');

        $user = $this->userManager->findUserBy(['username' => 'testUser0']);

        $result = $this->messengerManager->invite('123412304', $user, $conversation->getSlug());

        $this->assertFalse($result['invited']);
    }

    public function testLeave()
    {
        $conversation = $this->em->getRepository('RizzenMessengerBundle:Conversation')->findOneByTitle('TestConversation');
        $user = $this->userManager->findUserBy(['username' => 'testUser2']);

        $this->messengerManager->leave($conversation->getSlug(), $user);
    }

    public function testLeaveConversationNotFound()
    {
        $user = $this->userManager->findUserBy(['username' => 'testUser2']);

        $this->setExpectedException('Symfony\Component\HttpKernel\Exception\NotFoundHttpException');
        $this->messengerManager->leave('2398471238904', $user);
    }

    public function testLeaveNotInConversation()
    {
        $conversation = $this->em->getRepository('RizzenMessengerBundle:Conversation')->findOneByTitle('TestConversation');
        $user = $this->userManager->findUserBy(['username' => 'testUser2']);

        $this->setExpectedException('Symfony\Component\HttpKernel\Exception\NotFoundHttpException');
        $this->messengerManager->leave($conversation->getSlug(), $user);
    }
}
