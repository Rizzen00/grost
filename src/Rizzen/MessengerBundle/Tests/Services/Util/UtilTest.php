<?php

namespace Rizzen\MessengerBundle\Tests\Services\Util;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Doctrine\ORM\EntityManager;
use Rizzen\MessengerBundle\Services\Util\Util;

class UtilTest extends WebTestCase
{
    private $userManager;

    /** @var Util $util */
    private $util;

    /** @var EntityManager $em */
    private $em;

    public function setUp()
    {
        static::$kernel = static::createKernel();
        static::$kernel->boot();
        $this->userManager = static::$kernel->getContainer()->get('fos_user.user_manager');
        $this->util = static::$kernel->getContainer()->get('messenger.util');
        $this->em = static::$kernel->getContainer()->get('doctrine.orm.entity_manager');
    }

    public function testIsParticipant()
    {
        $conversationCreator = $this->em->getRepository('RizzenUserBundle:User')->find(1);
        $conversation = $this->em->getRepository('RizzenMessengerBundle:Conversation')->find(1);

        $result = $this->util->isParticipant($conversationCreator, $conversation->getParticipants());

        $this->assertTrue($result);
    }

    public function testIsNotParticipant()
    {
        $userManager = $this->userManager;

        $recipient = $userManager->findUserBy(['username' => 'testUser5']);

        $conversation = $this->em->getRepository('RizzenMessengerBundle:Conversation')->find(1);

        $result = $this->util->isParticipant($recipient, $conversation->getParticipants());

        $this->assertFalse($result);
    }
}
