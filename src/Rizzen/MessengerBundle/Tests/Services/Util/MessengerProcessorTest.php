<?php

namespace Rizzen\MessengerBundle\Tests\Services\Util;

use Rizzen\MessengerBundle\Entity\Conversation;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Rizzen\MessengerBundle\Services\Util\MessengerProcessor;
use Doctrine\ORM\EntityManager;

class MessengerProcessorTest extends WebTestCase
{
    private $userManager;

    /** @var MessengerProcessor $messengerProcessor */
    private $messengerProcessor;

    /** @var EntityManager $em */
    private $em;

    public function setUp()
    {
        static::$kernel = static::createKernel();
        static::$kernel->boot();
        $this->userManager = static::$kernel->getContainer()->get('fos_user.user_manager');
        $this->em = static::$kernel->getContainer()->get('doctrine.orm.entity_manager');
        $this->messengerProcessor = static::$kernel->getContainer()->get('messenger.processor');
    }

    public function testCreateNewConversation()
    {
        $creator = $this->userManager->findUserBy(['username' => 'testUser1']);

        $conversation = new Conversation();
        $conversation->setTitle('testCreateNewConversation');
        $conversation->setFirstMessage('This is just a test');
        $conversation->setRecipientUsername('testUser2');

        $result = $this->messengerProcessor->createNewConversation(null, $conversation, $creator);
        $this->assertTrue($result['hasBeenCreated']);
    }
}
