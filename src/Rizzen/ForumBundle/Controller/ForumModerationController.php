<?php

namespace Rizzen\ForumBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Type;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;


class ForumModerationController extends Controller
{
    /**
     * Delete a message in a thread
     *
     * @param string $subdomain
     * @param int $messageId
     * @return JsonResponse
     */
    public function deleteMessageAction($subdomain, $messageId)
    {
        $guild = $this->get('get.guild')->getGuild($subdomain);

        $permissionsManager = $this->get('permissions.manager')->setGuild($guild)->setUser($this->getUser());
        $permissionsManager->must(['CanDeleteForumPost']);

        $result = $this->get('forum.moderator')->deleteMessage($messageId, $guild->getForum());

        return new JsonResponse($result);
    }

    /**
     * Delete a thread
     *
     * @param string $subdomain
     * @param string $threadSlug
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteThreadAction($subdomain, $threadSlug)
    {
        $guild = $this->get('get.guild')->getGuild($subdomain);
        $permissionsManager = $this->get('permissions.manager')->setGuild($guild)->setUser($this->getUser());
        $permissionsManager->must(['CanEditForumThread']);

        $thread = $this->getDoctrine()->getRepository('RizzenForumBundle:Thread')->findOneBySlug($threadSlug);

        if ($thread === null || !$this->get('forum.viewable')->canSee($thread, $permissionsManager->getPermissions())) {
            throw $this->createNotFoundException('Thread not found');
        }

        $this->get('forum.moderator')->deleteThread($thread);

        $this->addFlash('success', 'Thread successfully deleted!');
        return $this->redirectToRoute('rizzen_forum_see', ['subdomain' => $subdomain]);

    }

    /**
     * Edit a message
     *
     * @param string $subdomain
     * @param Request $request
     * @return JsonResponse
     */
    public function editMessageAction($subdomain, Request $request)
    {
        $guild = $this->get('get.guild')->getGuild($subdomain);

        $result = [
            'hasBeenEdited' => false,
            'reason' => 'The message must be at least 1 character long and a maximum of 20000'
        ];

        $data = [];
        $editForm = $this->createFormBuilder($data)
            ->add('content', TextareaType::class, [
                    'constraints' => [
                        new NotBlank(),
                        new Length(['min' => 1, 'max' => 20000]),
                    ],
            ]
            )
            ->add('target', HiddenType::class, [
                    'constraints' => [
                        new NotBlank(),
                        new Type(['type' => 'numeric'])
                    ],
            ]
            )
            ->add('lock', CheckboxType::class, ['required' => false])
            ->getForm();

        $editForm->handleRequest($request);
        if ($editForm->isValid()) {
            $result = $this->get('forum.moderator')->editMessage($guild, $editForm->getData(), $this->getUser());
        }

        return new JsonResponse($result);
    }

    /**
     * Move the thread to another subcategory
     *
     * @param string $subdomain
     * @param string $threadSlug
     * @param string $subcatSlug
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function moveThreadAction($subdomain, $threadSlug, $subcatSlug)
    {
        $guild = $this->get('get.guild')->getGuild($subdomain);

        $permissionsManager = $this->get('permissions.manager')->setGuild($guild)->setUser($this->getUser());
        $permissionsManager->must(['CanEditForumThread']);

        $thread = $this->getDoctrine()->getRepository('RizzenForumBundle:Thread')->findOneBySlug($threadSlug);
        $target = $this->getDoctrine()->getRepository('RizzenForumBundle:SubCategory')->findOneBySlug($subcatSlug);

        $this->get('forum.thread.mover')->moveThreadTo($guild, $thread, $target);

        $this->addFlash('success', 'The thread has been successfully moved!');
        return $this->redirectToRoute('rizzen_forum_thread', ['subdomain' => $subdomain, 'slug' => $target->getSlug(), 'threadSlug' => $threadSlug]);

    }

    /**
     * Lock or unlock the thread
     *
     * @param string $subdomain
     * @param string $threadSlug
     * @return JsonResponse
     */
    public function toggleThreadLockAction($subdomain, $threadSlug)
    {
        $guild = $this->get('get.guild')->getGuild($subdomain);

        $permissionsManager = $this->get('permissions.manager')->setGuild($guild)->setUser($this->getUser());
        $permissionsManager->must(['CanEditForumThread']);

        $thread = $this->getDoctrine()->getRepository('RizzenForumBundle:Thread')->findOneBySlug($threadSlug);

        $this->get('forum.moderator')->toggleThreadLock($thread, $guild);

        return new JsonResponse();

    }

    /**
     * Pin or unpin a thread
     *
     * @param string $subdomain
     * @param string $threadSlug
     * @return JsonResponse
     */
    public function toggleThreadPinAction($subdomain, $threadSlug)
    {
        $guild = $this->get('get.guild')->getGuild($subdomain);

        $permissionsManager = $this->get('permissions.manager')->setGuild($guild)->setUser($this->getUser());
        $permissionsManager->must(['CanPinThread']);

        $thread = $this->getDoctrine()->getRepository('RizzenForumBundle:Thread')->findOneBySlug($threadSlug);

        $result = $this->get('forum.moderator')->toggleThreadPin($thread, $guild);

        return new JsonResponse($result);
    }
}
