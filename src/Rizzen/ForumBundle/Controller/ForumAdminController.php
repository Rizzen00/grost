<?php

namespace Rizzen\ForumBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Rizzen\ForumBundle\Form\Type\CategoryType;
use Rizzen\ForumBundle\Form\Type\SubCategoryType;
use Rizzen\ForumBundle\Entity\Category;
use Rizzen\ForumBundle\Entity\SubCategory;


class ForumAdminController extends Controller
{

    /**
     * Dashboard of the forum administration
     *
     * @param string $subdomain
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function adminIndexAction($subdomain)
    {
        $guild = $this->get('get.guild')->getGuild($subdomain);

        $permissionsManager = $this->get('permissions.manager')->setGuild($guild)->setUser($this->getUser());
        $permissionsManager->must(['CanEditForum']);

        return $this->render('RizzenForumBundle:Admin:index.html.twig', [
            'guild' => $guild,
            'subdomain' => $subdomain,
            'permissions' => $permissionsManager->getPermissions()
        ]);
    }

    /**
     * List the categories
     *
     * @param string $subdomain
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function categoriesAction($subdomain)
    {
        $guild = $this->get('get.guild')->getGuild($subdomain);

        $permissionsManager = $this->get('permissions.manager')->setGuild($guild)->setUser($this->getUser());
        $permissionsManager->must(['CanEditForum']);

        return $this->render('RizzenForumBundle:Admin:categories.html.twig', [
            'guild' => $guild,
            'subdomain' => $subdomain,
            'permissions' => $permissionsManager->getPermissions()
        ]);
    }

    /**
     * Page used to edit an already existing category
     *
     * @param string $subdomain
     * @param string $slug
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function categoryEditAction($subdomain, $slug, Request $request)
    {
        $guild = $this->get('get.guild')->getGuild($subdomain);

        $permissionsManager = $this->get('permissions.manager')->setGuild($guild)->setUser($this->getUser());
        $permissionsManager->must(['CanEditForum']);

        $category = $this->getDoctrine()->getRepository('RizzenForumBundle:Category')->findOneBySlug($slug);

        if ($category === null || $category->getForum()->getGuild() !== $guild) {
            throw $this->createNotFoundException('Category not found');
        }

        $form = $this->createForm(CategoryType::class, $category);
        $form->handleRequest($request);
        if ($form->isValid()) {
            $this->get('forum.category.manager')->editCategory($category);
            return $this->redirect($this->generateUrl('rizzen_forum_admin_categories', ['subdomain' => $subdomain]));
        }

        return $this->render('RizzenForumBundle:Admin:editCategory.html.twig', [
                'guild' => $guild,
                'subdomain' => $subdomain,
                'category' => $category,
                'form' => $form->createView(),
                'permissions' => $permissionsManager->getPermissions()
        ]);
    }

    /**
     * Page used to edit an already existing sub-category
     *
     * @param string $subdomain
     * @param string $slug
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function subcategoryEditAction($subdomain, $slug, Request $request)
    {
        $guild = $this->get('get.guild')->getGuild($subdomain);
        $subCategory = $this->getDoctrine()->getRepository('RizzenForumBundle:SubCategory')->findOneBySlug($slug);

        $permissionsManager = $this->get('permissions.manager')->setGuild($guild)->setUser($this->getUser());
        $permissionsManager->must(['CanEditForum']);

        if ($subCategory === null || $subCategory->getCategory()->getForum()->getGuild() !== $guild) {
            throw $this->createNotFoundException('Sub-Category not found');
        }

        $form = $this->createForm(SubCategoryType::class, $subCategory);
        $form->handleRequest($request);
        if ($form->isValid()) {
            $this->get('forum.category.manager')->editSubCategory($subCategory);
            return $this->redirect($this->generateUrl('rizzen_forum_admin_categories', ['subdomain' => $subdomain]));
        }

        return $this->render('RizzenForumBundle:Admin:editSubCategory.html.twig', [
                'guild' => $guild,
                'subdomain' => $subdomain,
                'subCategory' => $subCategory,
                'form' => $form->createView(),
                'permissions' => $permissionsManager->getPermissions()
        ]);
    }

    /**
     * Add a new category to the forum
     *
     * @param string $subdomain
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function createCategoryAction($subdomain, Request $request)
    {
        $guild = $this->get('get.guild')->getGuild($subdomain);

        $permissionsManager = $this->get('permissions.manager')->setGuild($guild)->setUser($this->getUser());
        $permissionsManager->must(['CanEditForum']);

        $category = new Category();

        // If there is no categories created in this forum yet, set the position of this one to '1', else set it as the last one in the list
        sizeof($guild->getForum()->getCategories()) !== 0
            ? $category->setPosition($guild->getForum()->getCategories()->last()->getPosition() + 1)
            : $category->setPosition(1);

        $form = $this->createForm(CategoryType::class, $category);

        $form->handleRequest($request);
        if ($form->isValid()) {
            $this->get('forum.category.manager')->createCategory($category, $guild);
            return $this->redirect($this->generateUrl('rizzen_forum_admin_categories', ['subdomain' => $subdomain]));
        }

        return $this->render('RizzenForumBundle:Admin:createCategory.html.twig', [
            'guild' => $guild,
            'subdomain' => $subdomain,
            'form' => $form->createView(),
            'permissions' => $permissionsManager->getPermissions()
        ]);
    }

    /**
     * Add a new sub-category to a category
     *
     * @param string $subdomain
     * @param string $slug
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function createSubCategoryAction($subdomain, $slug, Request $request)
    {
        $guild = $this->get('get.guild')->getGuild($subdomain);
        $category = $this->getDoctrine()->getRepository('RizzenForumBundle:Category')->findOneBySlug($slug);

        $permissionsManager = $this->get('permissions.manager')->setGuild($guild)->setUser($this->getUser());
        $permissionsManager->must(['CanEditForum']);

        if ($category === null) {
            throw $this->createNotFoundException('Category not found');
        }

        $subCategory = new SubCategory();

        sizeof($category->getSubCategories()) !== 0
            ? $subCategory->setPosition($category->getSubCategories()->last()->getPosition() + 1)
            : $subCategory->setPosition(1);

        $form = $this->createForm(SubCategoryType::class, $subCategory);

        $form->handleRequest($request);
        if ($form->isValid()) {
            $this->get('forum.category.manager')->createSubCategory($subCategory, $category, $guild);
            return $this->redirect($this->generateUrl('rizzen_forum_admin_categories', ['subdomain' => $subdomain]));
        }

        return $this->render('RizzenForumBundle:Admin:createSubCategory.html.twig', [
            'guild' => $guild,
            'subdomain' => $subdomain,
            'category' => $category,
            'form' => $form->createView(),
            'permissions' => $permissionsManager->getPermissions()
        ]);
    }

    /**
     * @param string $subdomain
     * @param string $slug
     * @param string $action
     * @param int $roleId
     * @return JsonResponse
     */
    public function updateCategoryAccessAction($subdomain, $slug, $action, $roleId)
    {
        $guild = $this->get('get.guild')->getGuild($subdomain);
        $category = $this->getDoctrine()->getRepository('RizzenForumBundle:Category')->findOneBySlug($slug);

        $permissionsManager = $this->get('permissions.manager')->setGuild($guild)->setUser($this->getUser());
        $permissionsManager->must(['CanEditForum']);

        if ($category === null) {
            throw $this->createNotFoundException('Category not found');
        }

        return new JsonResponse($this->get('forum.category.manager')->updateCategoryAccess($action, $category, $roleId));
    }

    /**
     * @param string $subdomain
     * @param string $slug
     * @param string $action
     * @return JsonResponse
     */
    public function categorySetGuestAction($subdomain, $slug, $action)
    {
        $guild = $this->get('get.guild')->getGuild($subdomain);
        $category = $this->getDoctrine()->getRepository('RizzenForumBundle:Category')->findOneBySlug($slug);

        $permissionsManager = $this->get('permissions.manager')->setGuild($guild)->setUser($this->getUser());
        $permissionsManager->must(['CanEditForum']);

        if ($category === null) {
            throw $this->createNotFoundException('Category not found');
        }

        return new JsonResponse($this->get('forum.category.manager')->setGuestAccess($action, $category));
    }
}
