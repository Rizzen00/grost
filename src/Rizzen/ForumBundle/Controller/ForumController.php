<?php

namespace Rizzen\ForumBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Rizzen\ForumBundle\Entity\Thread;
use Rizzen\ForumBundle\Entity\Message;
use Rizzen\ForumBundle\Form\Type\ThreadType;
use Rizzen\ForumBundle\Form\Type\MessageType;
use Rizzen\ForumBundle\Entity\SubCategory;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

class ForumController extends Controller
{
    /**
     * The forum index
     *
     * @param string $subdomain     Guild subdomain
     * @return Response
     */
    public function indexAction($subdomain)
    {
        $guild = $this->get('get.guild')->getGuild($subdomain);

        $permissionsManager = $this->get('permissions.manager')->setGuild($guild)->setUser($this->getUser());

        $categories = $this->getDoctrine()->getRepository('RizzenForumBundle:Category')->getCategories($guild->getForum());

        $categories = $this->get('forum.viewable')->getAccessibleCategories($categories, $permissionsManager->getPermissions());

        return $this->render('RizzenForumBundle:Forum:index.html.twig', [
            'guild' => $guild,
            'subdomain' => $subdomain,
            'categories' => $categories,
            'permissions' => $permissionsManager->getPermissions()
        ]);
    }

    /**
     * Join the forum of a guild
     *
     * @param string $subdomain     Guild subdomain
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function joinAction($subdomain, Request $request)
    {
        $this->denyAccessUnlessGranted('fos_user_security_login');

        $guild = $this->get('get.guild')->getGuild($subdomain);

        $permissionsManager = $this->get('permissions.manager')->setGuild($guild)->setUser($this->getUser());

        if ($permissionsManager->isForumMember(false)) {
            throw $this->createNotFoundException('Page not found, you might already be registered to this forum.');
        }

        $this->get('forum.manager')->joinForum($guild, $this->getUser());

        if ($request->headers->get('referer') === null) {
            return $this->redirectToRoute('rizzen_forum_see', ['subdomain' => $subdomain]);
        } else {
            return $this->redirect($request->headers->get('referer'), 301);
        }

    }

    /**
     * @param string $subdomain     Guild subdomain
     * @param string $slug          SubCategory slug
     * @param int $page             Current page
     * @return Response
     */
    public function subCategoryAction($subdomain, $slug, $page)
    {
        $subCategory = $this->get('forum.manager')->getSubCategory($slug);
        $guild = $this->get('get.guild')->getGuild($subdomain);

        $permissionsManager = $this->get('permissions.manager')->setGuild($guild)->setUser($this->getUser());

        $this->get('forum.viewable')->canSee($subCategory, $permissionsManager->getPermissions(), true);
        $threads = $this->getDoctrine()->getRepository('RizzenForumBundle:Thread')->getAllVisibleThreadsInSubCategory($subCategory);
        $pagination = $this->get('knp_paginator')->paginate($threads, $page, 10);

        return $this->render('RizzenForumBundle:Forum:subcat.html.twig', [
            'subcat' => $subCategory,
            'subdomain' => $subdomain,
            'guild' => $guild,
            'threads' => $pagination,
            'permissions' => $permissionsManager->getPermissions()
        ]);
    }

    /**
     * Figure out what the last page is, and forward to it
     *
     * @param string $subdomain Guild subdomain
     * @param string $slug SubCategory Slug
     * @param string $threadSlug Thread Slug
     * @param Request $request
     * @return Response
     */
    public function threadLatestMessageAction($subdomain, $slug, $threadSlug, Request $request)
    {
        $thread = $this->get('forum.manager')->getThreadOrNotFound($threadSlug, $slug, $subdomain);
        $messageCount = $this->getDoctrine()->getRepository('RizzenForumBundle:Message')->getMessageCount($thread);

        $lastPage = $messageCount < 1
            ? 1
            : ceil($messageCount / 8);

        $request->request->add(['seeLatestMessage' => true]);

        return $this->forward('RizzenForumBundle:Forum:seeThread', [
                'subdomain'  => $subdomain,
                'slug' => $slug,
                'threadSlug' => $threadSlug,
                'page' => $lastPage
            ]
        );
    }

    /**
     * Search for threads the forum in all the the categories the user can access
     * This search is limited in the number of results in can output.
     *
     * @param string $subdomain
     * @param string $query
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function searchAction($subdomain, $query)
    {
        $guild = $this->get('get.guild')->getGuild($subdomain);

        $searchManipulator = $this->get('thread.search.manipulator');

        $query = $searchManipulator->cleanSearchQuery($query);

        if ($query === false) {
            $this->addFlash('error', 'The search query is too long or too short. The query must contain at least 3 characters and a maximum of 40.');
            return $this->redirectToRoute('rizzen_forum_see', ['subdomain' => $subdomain]);
        }

        $permissionsManager = $this->get('permissions.manager')->setGuild($guild)->setUser($this->getUser());

        $threads = $this->getDoctrine()->getRepository('RizzenForumBundle:Thread')->searchThread($query, $guild->getForum(), $this->getUser());
        $threads = $searchManipulator->cleanSearchResults($threads, $this->getUser(), $guild);

        $threadVisits = $this->get('forum.thread.visits')->getThreadVisits($threads, $guild);

        return $this->render('RizzenForumBundle:Forum:search.html.twig', [
            'subdomain' => $subdomain,
            'threadVisits' => $threadVisits,
            'query' => urldecode($query),
            'guild' => $guild,
            'threads' => $threads,
            'permissions' => $permissionsManager->getPermissions()
        ]);
    }

    /**
     * Search for threads in a sub category
     *
     * @param string $subdomain
     * @param string $slug
     * @param string $query
     * @param int $page
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function searchSubCatAction($subdomain, $slug, $query, $page)
    {
        $guild = $this->get('get.guild')->getGuild($subdomain);
        $subcat = $this->get('forum.manager')->getSubCategory($slug);

        $permissionsManager = $this->get('permissions.manager')->setGuild($guild)->setUser($this->getUser());
        $this->get('forum.viewable')->canSee($subcat, $permissionsManager->getPermissions(), true);

        $searchManipulator = $this->get('thread.search.manipulator');

        $query = $searchManipulator->cleanSearchQuery($query);

        if ($query === false) {
            $this->addFlash('error', 'The search query is too long or too short. The query must contain at least 3 characters and a maximum of 40.');
            return $this->redirectToRoute('rizzen_forum_see', ['subdomain' => $subdomain]);
        }

        $threads = $this->getDoctrine()->getRepository('RizzenForumBundle:Thread')->searchThreadInSubCategory($query, $subcat);

        $pagination = $this->get('knp_paginator')->paginate($threads, $page, 10);
        $threadVisits = $this->get('forum.thread.visits')->getThreadVisits($pagination, $guild, true);

        return $this->render('RizzenForumBundle:Forum:searchSubcat.html.twig', [
                'subdomain' => $subdomain,
                'guild' => $guild,
                'threadVisits' => $threadVisits,
                'subcat' => $subcat,
                'query' => urldecode($query),
                'threads' => $pagination,
                'permissions' => $permissionsManager->getPermissions()
            ]
        );
    }

    /**
     * The thread page
     *
     * @param string $subdomain     Guild subdomain
     * @param string $slug          SubCategory Slug
     * @param string $threadSlug    Thread Slug
     * @param int $page             Current page
     * @return Response
     */
    public function seeThreadAction($subdomain, $slug, $threadSlug, $page)
    {
        $thread = $this->get('forum.manager')->getThreadOrNotFound($threadSlug, $slug, $subdomain);
        $threadMessages = $this->getDoctrine()->getRepository('RizzenForumBundle:Message')->getThreadContent($thread);

        $guild = $thread->getSubCategory()->getCategory()->getForum()->getGuild();
        $permissionsManager = $this->get('permissions.manager')->setGuild($guild)->setUser($this->getUser());
        $this->get('forum.viewable')->canSee($thread, $permissionsManager->getPermissions(), true);

        $categories = $this->get('forum.viewable')->getCategoriesForThreadMove($thread, $permissionsManager->getPermissions());

        $form = null;
        $editForm = null;

        $pagination = $this->get('knp_paginator')->paginate($threadMessages, $page, 8);
        $pagination->setUsedRoute('rizzen_forum_thread');

        if ($permissionsManager->isForumMember(false)) {
            $message = new Message();
            $form = $this->createForm(MessageType::class, $message)->createView();

            $editForm = $this->createFormBuilder([])
                ->add('content', TextareaType::class)
                ->add('target', HiddenType::class)
                ->add('lock', CheckboxType::class, ['required' => false])
                ->getForm()->createView();
        }

        return $this->render('RizzenForumBundle:Forum:thread.html.twig', [
            'thread' => $thread,
            'pagination' => $pagination,
            'subdomain' => $subdomain,
            'guild' => $guild,
            'form' => $form,
            'categories' => $categories,
            'editForm' => $editForm,
            'permissions' => $permissionsManager->getPermissions(),
            'permissionsJSON' => $permissionsManager->getJSONPermissions()
        ]);
    }

    /**
     * When someone is accessing a thread from the recently viewed list, only use the slug in case the thread was moved
     *
     * @param string $subdomain
     * @param string $threadSlug
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function seeThreadFromHistoryAction($subdomain, $threadSlug)
    {
        $guild = $this->get('get.guild')->getGuild($subdomain, true);

        /** @var Thread $thread */
        $thread = $this->getDoctrine()->getRepository('RizzenForumBundle:Thread')->findOneBySlug($threadSlug);

        if ($thread === null || $thread->getSubCategory()->getCategory()->getForum()->getGuild() !== $guild) {
            return $this->createNotFoundException('The thread was not found');
        }

        return $this->redirectToRoute('rizzen_forum_thread', [
                'subdomain' => $guild->getSlug(),
                'slug' => $thread->getSubCategory()->getSlug(),
                'threadSlug' => $thread->getSlug()
            ]);
    }

    /**
     * Reply to a thread
     *
     * @param string $subdomain     Guild subdomain
     * @param string $slug          SubCategory Slug
     * @param string $threadSlug    Thread slug
     * @param Request $request
     * @return Response
     */
    public function replyAction($subdomain, $slug, $threadSlug, Request $request)
    {
        $thread = $this->get('forum.manager')->getThreadOrNotFound($threadSlug, $slug, $subdomain);
        $guild = $this->get('get.guild')->getGuild($subdomain, true);

        $permissionsManager = $this->get('permissions.manager')->setGuild($guild)->setUser($this->getUser());
        $permissionsManager->isForumMember();

        $this->get('forum.viewable')->canSee($thread, $permissionsManager->getPermissions(), true);

        $message = new Message();
        $form = $this->createForm(MessageType::class, $message);

        $form->handleRequest($request);
        if ($form->isValid() && !$thread->isLocked() && !$thread->getSubCategory()->isLocked() && !$thread->getSubCategory()->getCategory()->isLocked()) {
            $data = $this->get('forum.poster')->postMessage($thread, $message, $this->getUser(), $request->getClientIp(), $subdomain);
            $request->attributes->set('threadUpdated', ['thread' => $thread, 'user' => $this->getUser()]);
            $request->attributes->set('newThreadMessage', $data);
        } else {
            return new JsonResponse(['hasBeenSent' => false]);
        }

        return new Response();
    }

    /**
     * Start a new thread
     *
     * @param string $subdomain    Subdomain of the guild
     * @param string $slug         Slug of the SubCategory
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function newThreadAction($subdomain, $slug, Request $request)
    {
        $guild = $this->get('get.guild')->getGuild($subdomain, true);
        $permissionsManager = $this->get('permissions.manager')->setGuild($guild)->setUser($this->getUser());
        $permissionsManager->isForumMember();

        /** @var SubCategory $subcat */
        $subcat = $this->get('forum.manager')->getSubCategory($slug);

        $this->get('forum.viewable')->canSee($subcat, $permissionsManager->getPermissions());

        if ($subcat->isLocked() || $subcat->getCategory()->isLocked()) {
            $this->addFlash('error', 'Sorry, this category or subcategory is currently locked.');
        }

        $thread = new Thread();
        $form = $this->createForm(ThreadType::class, $thread);

        $form->handleRequest($request);
        if ($form->isValid() && !$subcat->isLocked() && !$subcat->getCategory()->isLocked()) {
            $threadSlug = $this->get('forum.poster')->newThread($subcat, $thread, $this->getUser(), $request->getClientIp());
            $this->addFlash('success', 'Thread successfully created!');

            $request->attributes->set('threadUpdated', ['thread' => $thread, 'user' => $this->getUser()]);
            return $this->redirect($this->generateUrl('rizzen_forum_thread', ['subdomain' => $subdomain, 'slug' => $slug, 'threadSlug' => $threadSlug]));
        }

        return $this->render('RizzenForumBundle:Forum:newThread.html.twig', [
            'subCategory' => $subcat,
            'guild' => $guild,
            'subdomain' => $subdomain,
            'form' => $form->createView(),
            'permissions' => $permissionsManager->getPermissions()
        ]);
    }
}
