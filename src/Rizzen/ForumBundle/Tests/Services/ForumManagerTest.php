<?php

namespace Rizzen\ForumBundle\Tests\Services;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Rizzen\ForumBundle\Services\ForumManager;

class ForumManagerTest extends WebTestCase
{
    private $userManager;
    private $em;
    /** @var ForumManager $permissionsOfUser */
    private $forumManager;

    public function setUp()
    {
        static::$kernel = static::createKernel();
        static::$kernel->boot();
        $this->forumManager = static::$kernel->getContainer()->get('forum.manager');
        $this->userManager = static::$kernel->getContainer()->get('fos_user.user_manager');
        $this->em = static::$kernel->getContainer()->get('doctrine.orm.entity_manager');
    }

    public function testAddUser()
    {

        $userManager = $this->userManager;
        $user = $userManager->findUserBy(['username' => 'testUser1']);

        $guild = $this->em->getRepository('RizzenGuildBundle:Guild')->findOneByTitle('TestGuild');

        $result = $this->forumManager->joinForum($guild, $user);
        $this->assertTrue($result);

        $result = $this->forumManager->joinForum($guild, $user);
        $this->assertFalse($result);
    }
}
