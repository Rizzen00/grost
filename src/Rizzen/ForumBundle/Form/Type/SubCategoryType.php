<?php

namespace Rizzen\ForumBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

class SubCategoryType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, ['attr' => ['autocomplete' => 'off']])
            ->add('position', IntegerType::class, ['attr' => ['autocomplete' => 'off']])
            ->add('locked', CheckboxType::class, [
                    'required' => false,
            ])
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'Rizzen\ForumBundle\Entity\SubCategory'
        ]);
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'rizzen_forumbundle_subcategory';
    }
}
