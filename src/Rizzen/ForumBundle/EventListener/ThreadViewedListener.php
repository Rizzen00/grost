<?php

namespace Rizzen\ForumBundle\EventListener;


use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpKernel\Event\PostResponseEvent;
use Symfony\Component\HttpFoundation\Request;
use Rizzen\ForumBundle\Entity\Thread;

class ThreadViewedListener
{
    private $em;

    function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * Increment the view counter of a thread
     *
     * @param PostResponseEvent $event
     */
    public function onKernelTerminate(PostResponseEvent $event)
    {

        /** @var Request $request */
        $request = $event->getRequest();

        $route = $request->get('_route');

        if ($request->isXmlHttpRequest() || ($route !== 'rizzen_forum_thread' && $route !== 'rizzen_forum_thread_latest') || $event->getResponse()->getStatusCode() !== 200 ) {
            return;
        }

        $routeParams = $request->attributes->get('_route_params');

        /** @var Thread $thread */
        $thread = $this->em->getRepository('RizzenForumBundle:Thread')->findOneBySlug($routeParams['threadSlug']);
        $referer = $request->headers->get('referer');

        // Check if the user has recently seen the thread
        if ($thread === null || ($referer !== null && strpos($referer, $thread->getSlug())) || $request->cookies->getBoolean('thread-viewed-' . $thread->getSlug()) === true) {
            return;
        }

        $thread->increaseViewCount();
        $this->em->flush();
    }

}
