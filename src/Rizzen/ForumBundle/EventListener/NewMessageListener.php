<?php

namespace Rizzen\ForumBundle\EventListener;


use Rizzen\SocketBundle\Services\SocketProcessor;
use Symfony\Component\HttpKernel\Event\PostResponseEvent;
use Symfony\Component\HttpFoundation\Request;

class NewMessageListener
{

    private $processor;

    function __construct(SocketProcessor $processor)
    {
        $this->processor = $processor;
    }

    /**
     * Send the reply to the thread with the web sockets
     *
     * @param PostResponseEvent $event
     */
    public function onKernelTerminate(PostResponseEvent $event)
    {

        /** @var Request $request */
        $request = $event->getRequest();

        $route = $request->get('_route');
        if ($route === 'rizzen_forum_reply') {

            $param = $request->attributes->get('newThreadMessage');

            $this->processor
                ->addTopic($param['topic'])
                ->setSubtopic('forumResponse')
                ->useTemplate('raw', $param['template'])
                ->setAdditionalInfo($param['additionalInfo'])
                ->broadcast();

        }

    }
}
