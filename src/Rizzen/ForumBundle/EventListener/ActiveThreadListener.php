<?php

namespace Rizzen\ForumBundle\EventListener;


use Predis\Client;
use Symfony\Component\HttpKernel\Event\PostResponseEvent;
use Symfony\Component\HttpFoundation\Request;
use Rizzen\ForumBundle\Entity\Thread;
use Rizzen\UserBundle\Entity\User;

class ActiveThreadListener
{
    private $redis;

    function __construct(Client $redis)
    {
        $this->redis = $redis;
    }

    /**
     * Add the updated thread to the list of active thread of the guild
     *
     * @param PostResponseEvent $event
     */
    public function onKernelTerminate(PostResponseEvent $event)
    {

        /** @var Request $request */
        $request = $event->getRequest();

        $route = $request->get('_route');

        if ($route === 'rizzen_forum_reply' || ($route === 'rizzen_forum_newthread' && $request->isMethod('POST'))) {

            $requestAttributes = $request->attributes->get('threadUpdated');

            /** @var Thread $thread */
            $thread = $requestAttributes['thread'];

            /** @var User $user */
            $user = $requestAttributes['user'];

            if ($thread->getSubCategory()->getCategory()->getAccessRestricted()) {
                return;
            }

            $currentDate = new \DateTime('now', new \DateTimeZone('UTC'));

            $data = [
                'threadTitle'   => $thread->getTitle(),
                'threadSlug'    => $thread->getSlug(),
                'subcatTitle'   => $thread->getSubCategory()->getTitle(),
                'categoryTitle' => $thread->getSubCategory()->getCategory()->getTitle(),
                'subcatSlug'    => $thread->getSubCategory()->getSlug(),
                'updateDate'    => $currentDate->getTimestamp(),
                'updatedBy'     => $user->getUsername(),
                'categoryId'    => $thread->getSubCategory()->getCategory()->getId()
            ];

            $key = 'activeThreads-g' . $thread->getSubCategory()->getCategory()->getForum()->getGuild()->getId();
            $oldData = $this->redis->lrange($key, 0, -1);

            /*
             * Check if the thread is already in the list
             * */
            $threadAlreadyInList = false;
            $threadToDelete = null;
            foreach ($oldData as $threadData) {
                $decodedData = json_decode($threadData, true);

                if ($decodedData['threadSlug'] === $data['threadSlug']) {

                    $threadAlreadyInList = true;
                    $threadToDelete = $threadData;

                    break;
                }
            }

            $data = json_encode($data);
            $pipe = $this->redis->pipeline();

            /*
             * If the current thread is in the list, remove it
             * */
            if ($threadAlreadyInList) {
                $pipe->lrem($key, 0, $threadToDelete);
            }

            $pipe->lpush($key, $data);
            $pipe->ltrim($key, 0, 16);

            $pipe->execute();

        }

    }
}
