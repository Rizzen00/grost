<?php


namespace Rizzen\ForumBundle\Services;


use Doctrine\ORM\EntityManager;
use Predis\Client;
use Rizzen\ForumBundle\Entity\Forum;
use Rizzen\ForumBundle\Entity\Thread;
use Rizzen\GuildBundle\Entity\Guild;
use Rizzen\GuildBundle\Permissions\PermissionsManager;
use Rizzen\MainBundle\Services\Purifier;
use Rizzen\MainBundle\Services\TextManipulator;
use Rizzen\SocketBundle\Services\SocketProcessor;
use Rizzen\UserBundle\Entity\User;

class ForumModerator implements ForumModeratorInterface {

    protected $em;
    protected $purifier;
    protected $textManipulator;
    protected $permissionsManager;
    protected $redis;
    protected $socketProcessor;

    /**
     * @param EntityManager $em
     * @param Purifier $purifier
     * @param TextManipulator $textManipulator
     * @param PermissionsManager $permissionsManager
     * @param Client $redis
     * @param SocketProcessor $socketProcessor
     */
    public function __construct(EntityManager $em, Purifier $purifier, TextManipulator $textManipulator, PermissionsManager $permissionsManager, Client $redis, SocketProcessor $socketProcessor)
    {
        $this->em = $em;
        $this->purifier = $purifier;
        $this->textManipulator = $textManipulator;
        $this->permissionsManager = $permissionsManager;
        $this->redis = $redis;
        $this->socketProcessor = $socketProcessor;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteMessage($messageId, Forum $forum)
    {
        $message = $this->em->getRepository('RizzenForumBundle:Message')->find($messageId);

        if ($message === null || $message->getThread()->getSubCategory()->getCategory()->getForum() !== $forum || $message->isOriginalPost()) {
            return [
                'hasBeenDeleted' => false,
                'reason' => 'This message was not posted in the current forum or is not deletable.'
            ];
        }

        $thread = $message->getThread();
        $messageCount = $this->em->getRepository('RizzenForumBundle:Message')->getMessageCount($message->getThread());
        $thread->setMessagesCount($messageCount - 1);

        $message->setVisible(false);
        $this->em->flush();

        return ['hasBeenDeleted' => true];
    }

    /**
     * {@inheritdoc}
     */
    public function editMessage(Guild $guild, array $formData, User $user)
    {
        $message = $this->em->getRepository('RizzenForumBundle:Message')->find($formData['target']);
        $this->permissionsManager->setUser($user)->setGuild($guild);

        if ($message === null ||
            $message->getThread()->getSubCategory()->getCategory()->getForum() !== $guild->getForum() ||
            !$this->permissionsManager->isForumMember(false)) {
            return [
                'hasBeenEdited' => false,
                'reason' => 'An error occurred while editing this message.'
            ];
        }

        // If the user does not have the permission to edit messages in general, check if he is the creator of the message
        if (!$this->permissionsManager->can(['CanEditForumPost', 'CanEditForumThread'])) {

            if ($message->getCreator() !== $user) {
                return [
                    'hasBeenEdited' => false,
                    'reason' => 'Your permissions does not let you edit the messages of other members.'
                ];
            }

            if ($message->isLocked() === true ||
                $message->getThread()->isLocked() ||
                $message->getThread()->getSubCategory()->isLocked() ||
                $message->getThread()->getSubCategory()->getCategory()->isLocked()
            ) {
                return [
                    'hasBeenEdited' => false,
                    'reason' => 'This message or thread is locked and can only be edited by moderators.'
                ];
            }

            if ($message->isLocked() !== $formData['lock']) {
                return [
                    'hasBeenEdited' => false,
                    'reason' => 'Your permissions does not let you lock/unlock messages.'
                ];
            }

        }

        $content = $this->purifier->purify('forumPost', $formData['content']);
        $content = $this->textManipulator->addNoFollow($content);

        $message->setContent($content);
        $message->setLocked($formData['lock']);
        $this->em->flush();

        $thread = $message->getThread();

        $wsTopic = $thread->getSubCategory()->getCategory()->getAccessRestricted() ? $thread->getPrivateSocketTopic() : $thread->getPublicSocketTopic();

        $additionalInfo = ['msgId' => $message->getId(), 'editedBy' => $user->getUsername()];

        $template = ['HTML' => $message->getContent()];

        $this->socketProcessor
            ->addTopic($wsTopic)
            ->setSubtopic('forumMessageEdited')
            ->useTemplate('raw', $template)
            ->setAdditionalInfo($additionalInfo)
            ->broadcast();

        return ['hasBeenEdited' => true];
    }

    /**
     * {@inheritdoc}
     */
    public function deleteThread(Thread $thread)
    {
        $thread->setVisible(false);
        $this->em->flush();

        $key = 'activeThreads-g' . $thread->getSubCategory()->getCategory()->getForum()->getGuild()->getId();

        $oldData = $this->redis->lrange($key, 0, -1);

        foreach ($oldData as $threadData) {
            $decodedData = json_decode($threadData, true);
            if ($decodedData['threadSlug'] === $thread->getSlug()) {
                $this->redis->lrem($key, 0, $threadData);
                break;
            }
        }

        $subcategory = $thread->getSubCategory();

        $topOfCurrent = $this->em->getRepository('RizzenForumBundle:Thread')->getThreadOnTopOfSubCategory($thread->getSubCategory());
        $subcategory->setLatestThreadActivity($topOfCurrent);

        $this->em->flush();

    }

    /**
     * {@inheritdoc}
     */
    public function toggleThreadLock(Thread $thread = null, Guild $guild)
    {
        if ($thread === null ||
            $thread->getSubCategory()->getCategory()->getForum() !== $guild->getForum()) {
            return [
                'hasBeenToggled' => false,
                'reason' => 'The thread was not found'
            ];
        }

        // Toggle the thread lock
        $thread->setLocked(!$thread->isLocked());

        $this->em->flush();

        $wsTopic = $thread->getSubCategory()->getCategory()->getAccessRestricted() ? $thread->getPrivateSocketTopic() : $thread->getPublicSocketTopic();

        $this->socketProcessor
            ->addTopic($wsTopic)
            ->setSubtopic('forumThreadLock')
            ->useTemplate('raw', ['HTML' => ''])
            ->setAdditionalInfo(['isLocked' => $thread->isLocked()])
            ->broadcast();

        return ['hasBeenToggled' => true];
    }

    /**
     * {@inheritdoc}
     */
    public function toggleThreadPin(Thread $thread = null, Guild $guild)
    {
        if ($thread === null ||
            $thread->getSubCategory()->getCategory()->getForum() !== $guild->getForum()) {
            return [
                'hasBeenToggled' => false,
                'reason' => 'The thread was not found'
            ];
        }

        $thread->setPinned(!$thread->isPinned());

        $this->em->flush();

        return [
            'hasBeenToggled' => true,
            'isPinned' => $thread->isPinned()
        ];
    }
}
