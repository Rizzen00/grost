<?php


namespace Rizzen\ForumBundle\Services;


use Rizzen\ForumBundle\Entity\Forum;
use Rizzen\ForumBundle\Entity\Thread;
use Rizzen\GuildBundle\Entity\Guild;
use Rizzen\UserBundle\Entity\User;

interface ForumModeratorInterface {

    /**
     * Delete a message from a thread
     *
     * @param int $messageId        Message to delete
     * @param Forum $forum          Forum where the message will be deleted
     * @return mixed
     */
    public function deleteMessage($messageId, Forum $forum);

    /**
     * Edit a message
     *
     * @param Guild $guild          Guild containing the forum where the message will be edited
     * @param array $formData       Form data received
     * @param User $user            User that is editing the message
     * @return mixed
     */
    public function editMessage(Guild $guild, Array $formData, User $user);

    /**
     * Delete a forum thread
     *
     * @param Thread $thread        Thread to delete
     * @return void
     */
    public function deleteThread(Thread $thread);

    /**
     * Lock or unlock a thread
     *
     * @param Thread $thread
     * @param Guild $guild
     * @return array
     */
    public function toggleThreadLock(Thread $thread, Guild $guild);

    /**
     * Pin or unpin a thread
     *
     * @param Thread $thread
     * @param Guild $guild
     * @return array
     */
    public function toggleThreadPin(Thread $thread, Guild $guild);

} 
