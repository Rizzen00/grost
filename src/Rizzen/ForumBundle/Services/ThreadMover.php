<?php

namespace Rizzen\ForumBundle\Services;


use Doctrine\ORM\EntityManager;
use Predis\Client;
use Rizzen\ForumBundle\Entity\SubCategory;
use Rizzen\ForumBundle\Entity\Thread;
use Rizzen\GuildBundle\Entity\Guild;
use Rizzen\SocketBundle\Services\SocketProcessor;
use Symfony\Component\Routing\Router;

class ThreadMover
{

    private $em;
    private $redis;
    private $socketProcessor;
    private $router;

    function __construct(EntityManager $em, Client $redis, SocketProcessor $socketProcessor, Router $router)
    {
        $this->em = $em;
        $this->redis = $redis;
        $this->socketProcessor = $socketProcessor;
        $this->router = $router;
    }

    /**
     * Move a thread to another sub-category
     *
     * @param Guild $guild              Guild where the thread is contained
     * @param Thread $thread            Thread to move
     * @param SubCategory $target       SubCategory where the thread will be moved
     */
    public function moveThreadTo(Guild $guild, Thread $thread, SubCategory $target)
    {
        $currentSubCategory = $thread->getSubCategory();

        if ($currentSubCategory === $target ||
            $thread === null ||
            $target === null ||
            $thread->getSubCategory()->getCategory()->getForum()->getGuild() !== $guild ||
            $target->getCategory()->getForum()->getGuild() !== $guild) {

            return;
        }

        $currentSubCategory->removeThread($thread);

        $thread->setSubCategory($target);
        $target->addThread($thread);

        $this->em->flush();

        $topOfCurrent = $this->em->getRepository('RizzenForumBundle:Thread')->getThreadOnTopOfSubCategory($currentSubCategory);
        $topOfTarget = $this->em->getRepository('RizzenForumBundle:Thread')->getThreadOnTopOfSubCategory($target);

        $currentSubCategory->setLatestThreadActivity($topOfCurrent);
        $target->setLatestThreadActivity($topOfTarget);

        $this->em->flush();

        $this->lockOriginalThread($thread, $target, $currentSubCategory);
        $this->updateActiveThreadsList($topOfCurrent, $topOfTarget, $target);
    }

    /**
     * Update the list of active threads in the guild dashboard
     *
     * @param Thread $topOfCurrent
     * @param Thread $topOfTarget
     * @param SubCategory $target
     * @throws \Exception
     * @throws null
     */
    private function updateActiveThreadsList(Thread $topOfCurrent = null, Thread $topOfTarget = null, SubCategory $target)
    {
        $redisKey = 'activeThreads-g' . $target->getCategory()->getForum()->getGuild()->getId();
        $oldData = $this->redis->lrange($redisKey, 0, -1);

        $currentDate = new \DateTime('now', new \DateTimeZone('UTC'));
        $isInList = false;

        foreach ($oldData as $key => $data) {
            $data = json_decode($data, true);

            if ($topOfCurrent !== null && $data['threadSlug'] === $topOfCurrent->getSlug()) {
                unset($oldData[$key]);

                $data = [
                    'threadTitle'   => $topOfCurrent->getTitle(),
                    'threadSlug'    => $topOfCurrent->getSlug(),
                    'subcatTitle'   => $topOfCurrent->getSubCategory()->getTitle(),
                    'categoryTitle' => $topOfCurrent->getSubCategory()->getCategory()->getTitle(),
                    'subcatSlug'    => $topOfCurrent->getSubCategory()->getSlug(),
                    'updateDate'    => $currentDate->getTimestamp(),
                    'updatedBy'     => $topOfCurrent->getCreator()->getUsername(),
                    'categoryId'    => $topOfCurrent->getSubCategory()->getCategory()->getId()
                ];

                $oldData[$key] = json_encode($data);
                $isInList = true;

            } elseif ($topOfTarget !== null && $data['threadSlug'] === $topOfTarget->getSlug()) {
                unset($oldData[$key]);

                $data = [
                    'threadTitle'   => $topOfTarget->getTitle(),
                    'threadSlug'    => $topOfTarget->getSlug(),
                    'subcatTitle'   => $topOfTarget->getSubCategory()->getTitle(),
                    'categoryTitle' => $topOfTarget->getSubCategory()->getCategory()->getTitle(),
                    'subcatSlug'    => $topOfTarget->getSubCategory()->getSlug(),
                    'updateDate'    => $currentDate->getTimestamp(),
                    'updatedBy'     => $topOfTarget->getCreator()->getUsername(),
                    'categoryId'    => $topOfTarget->getSubCategory()->getCategory()->getId()
                ];

                $oldData[$key] = json_encode($data);
                $isInList = true;
            }
        }


        if ($isInList) {
            $pipe = $this->redis->pipeline();

            $pipe->del($redisKey);

            foreach ($oldData as $data) {
                $pipe->lpush($redisKey, $data);
            }

            $pipe->execute();
        }
    }

    /**
     * Lock the old thread page in case someone was typing a message in it.
     *
     * @param Thread $thread
     * @param SubCategory $target
     * @param SubCategory $currentSubCategory
     */
    private function lockOriginalThread(Thread $thread, SubCategory $target, SubCategory $currentSubCategory)
    {

        $additionalInfo = [
            'newPath' => $this->router->generate('rizzen_forum_thread', [
                    'subdomain' => $target->getCategory()->getForum()->getGuild()->getSlug(),
                    'slug' => $target->getSlug(),
                    'threadSlug' => $thread->getSlug()
                ])
        ];

        $topic = $currentSubCategory->getCategory()->getAccessRestricted() ? $thread->getPrivateSocketTopic() : $thread->getPublicSocketTopic();
        $this->socketProcessor
            ->addTopic($topic)
            ->setAdditionalInfo($additionalInfo)
            ->useTemplate('raw', null)
            ->setSubtopic('threadMoved')
            ->broadcast();
    }
}
