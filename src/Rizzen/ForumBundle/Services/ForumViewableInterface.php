<?php

namespace Rizzen\ForumBundle\Services;


use Rizzen\ForumBundle\Entity\Category;
use Rizzen\GuildBundle\Entity\Guild;
use Rizzen\GuildBundle\Entity\Role;
use Rizzen\ForumBundle\Entity\SubCategory;
use Rizzen\ForumBundle\Entity\Thread;

interface ForumViewableInterface
{

    /**
     * Prepare the list of categories
     *
     * @param Category[] $categories    Categories of the current forum
     * @param Role $permissions         Permissions of the current user
     * @return mixed
     */
    public function getAccessibleCategories($categories, Role $permissions);

    /**
     * Check if the current user can see the thread or subcategory he's trying to access
     *
     * @param SubCategory|Thread        $subCategoryOrThread
     * @param Role $permissions         Permissions of the current user
     * @param bool $throw               If true will throw an exception if the user cannot access this page
     * @return
     */
    public function canSee($subCategoryOrThread, Role $permissions, $throw = true);

    /**
     * Remove all the threads the user isn't supposed to see from the guild dashboard
     *
     * @param array $threads            Array of thread fetched from redis
     * @param Guild $guild              The current guild
     * @param Role $permissions         The permissions of the current user
     * @return array
     */
    public function removeThreadsNotVisibleForDashboard(array $threads, Guild $guild, Role $permissions);

    /**
     * Remove all the threads the user isn't supposed to see from the search results
     *
     * @param array $threads            Array of thread fetched from redis
     * @param Guild $guild              The current guild
     * @param Role $permissions         The permissions of the current user
     * @return array
     */
    public function removeThreadsNotVisibleForSearch(array $threads, Guild $guild, Role $permissions);


    /**
     * Get the categories to display them in a select
     *
     * @param Thread $thread            Current thread
     * @param Role $permissions         Permissions of the current user
     * @return void
     */
    public function getCategoriesForThreadMove(Thread $thread, Role $permissions);

} 
