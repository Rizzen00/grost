<?php

namespace Rizzen\ForumBundle\Services;


use Rizzen\ForumBundle\Entity\Category;
use Rizzen\ForumBundle\Entity\SubCategory;
use Rizzen\ForumBundle\Entity\Thread;
use Rizzen\GuildBundle\Entity\Guild;
use Rizzen\GuildBundle\Entity\Role;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class ForumViewable implements ForumViewableInterface
{
    /**
     * {@inheritdoc}
     */
    public function getAccessibleCategories($categories, Role $permissions)
    {
        /** @var Category $category */
        foreach ($categories as $key => $category) {

            // Check if the current user has at least one role that is in the access list of the category
            if ($category->getAccessRestricted() && sizeof(array_intersect($category->getAccessList(), $permissions->getRolesAttributed())) === 0) {
                unset($categories[$key]);
            } elseif ($category->getAccessibleByGuests() === false && $permissions->isForumMember() === false) {
                unset($categories[$key]);
            }
        }

        return $categories;
    }

    /**
     * {@inheritdoc}
     */
    public function canSee($subCategoryOrThread, Role $permissions, $throw = true)
    {
        if ($subCategoryOrThread instanceof SubCategory) {
            /** @var SubCategory $subCategoryOrThread */
            $category = $subCategoryOrThread->getCategory();
        } else {
            /** @var Thread $subCategoryOrThread */
            $category = $subCategoryOrThread->getSubCategory()->getCategory();
        }

        if ($category->getAccessRestricted()) {
            // Check if the current user has at least one role that is in the access list of the category
            if (sizeof(array_intersect($category->getAccessList(), $permissions->getRolesAttributed())) === 0) {
                if ($throw) {
                    throw new AccessDeniedException('None of the roles granted to you gives you the ability to see this page.');
                } else {
                    return false;
                }
            }
        } elseif ($category->getAccessibleByGuests() === false && $permissions->isForumMember() === false) {
            if ($throw) {
                throw new AccessDeniedException('This page can only be accessed by forum members.');
            } else {
                return false;
            }
        }

        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function removeThreadsNotVisibleForDashboard(array $threads, Guild $guild, Role $permissions)
    {
        $threads = $this->removeThreadsNotVisible($threads, $guild, $permissions);
        return array_slice($threads, 0, 6);
    }

    /**
     * {@inheritdoc}
     */
    public function removeThreadsNotVisibleForSearch(array $threads, Guild $guild, Role $permissions)
    {
        return $this->removeThreadsNotVisible($threads, $guild, $permissions, false);
    }

    /**
     * {@inheritdoc}
     */
    public function getCategoriesForThreadMove(Thread $thread, Role $permissions)
    {
        if (!$permissions->getCanEditForumThread()) {
            return null;
        }

        /** @var Category[] $categories */
        $categories = $thread->getSubCategory()->getCategory()->getForum()->getCategories();

        /** @var Category $category */
        foreach ($categories as $category) {

            // Check if the current user has at least one role that is in the access list of the category
            if ($category->getAccessRestricted() && sizeof(array_intersect($category->getAccessList(), $permissions->getRolesAttributed())) === 0) {
                $categories->removeElement($category);
            }
        }

        return $categories;
    }

    /**
     * @param array $threads
     * @param Guild $guild
     * @param Role $permissions
     * @param bool $removeFromDashboard
     * @return array
     */
    private function removeThreadsNotVisible(array $threads, Guild $guild, Role $permissions, $removeFromDashboard = true)
    {
        /** @var Category $category[] */
        $categories = $guild->getForum()->getCategories();

        /** @var array $rolesOfUser */
        $rolesOfUser = $permissions->getRolesAttributed();

        /** @var Category $category */
        foreach ($threads as $key => $thread) {

            if (($removeFromDashboard === true && $this->isNotVisibleFromArray($categories, $thread, $rolesOfUser, $permissions) ||
                ($removeFromDashboard === false && $this->isNotVisibleFromObject($thread, $rolesOfUser, $permissions)))) {
                unset($threads[$key]);
            }

        }

        return $threads;
    }

    /**
     * @param Category[] $categories
     * @param array $thread
     * @param array $rolesOfUser
     * @param Role $permissions
     * @return bool
     */
    private function isNotVisibleFromArray($categories, array $thread, $rolesOfUser, Role $permissions)
    {
        foreach ($categories as $category) {
            if ($category->getId() === $thread['categoryId']) {

                if ($category->getAccessRestricted() && sizeof(array_intersect($category->getAccessList(), $rolesOfUser)) === 0 ||
                    $category->getAccessibleByGuests() === false && $permissions->isForumMember() === false) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * @param Thread $thread
     * @param array $rolesOfUser
     * @param Role $permissions
     * @return bool
     */
    private function isNotVisibleFromObject(Thread $thread, $rolesOfUser, Role $permissions)
    {
        $category = $thread->getSubCategory()->getCategory();
        if ($category->getAccessRestricted()) {

            if (sizeof(array_intersect($category->getAccessList(), $rolesOfUser)) === 0 ||
                $category->getAccessibleByGuests() === false &&
                $permissions->isForumMember() === false) {
                return true;
            }
        }

        return false;
    }
}
