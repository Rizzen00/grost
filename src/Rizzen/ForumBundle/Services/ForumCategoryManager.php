<?php

namespace Rizzen\ForumBundle\Services;

use Doctrine\ORM\EntityManager;
use Rizzen\ForumBundle\Entity\Category;
use Rizzen\ForumBundle\Entity\SubCategory;
use Rizzen\GuildBundle\Entity\Guild;
use Rizzen\MainBundle\Services\Slugify;
use Symfony\Component\HttpFoundation\Session\Session;

class ForumCategoryManager implements ForumCategoryManagerInterface {

    protected $em;
    protected $slugify;
    private $session;

    /**
     * @param EntityManager $em
     * @param Session $session
     */
    public function __construct(EntityManager $em, Session $session)
    {
        $this->em = $em;
        $this->slugify = new Slugify();
        $this->session = $session;
    }

    /**
     * {@inheritdoc}
     */
    public function createCategory(Category $category, Guild $guild)
    {

        $forum = $guild->getForum();

        if ($forum->getCategories()->count() >= 8) {
            throw new \Exception('You have already reached the category limit of 8.');
        }

        if ($category->getPosition() > 10000) {
            $category->setPosition(10000);
        } elseif ($category->getPosition() < 0) {
            $category->setPosition(1);
        }

        $category->setForum($forum);
        $forum->addCategory($category);

        $this->em->persist($category);
        $this->em->persist($guild);
        $this->em->flush();

        $slug = $this->slugify->getSluged($category->getTitle()) . '-' . $category->getId();

        $category->setSlug($slug);
        $this->em->flush();

        $this->session->getFlashBag()->add('success', 'The category has been successfully created!');

        return $slug;
    }

    /**
     * {@inheritdoc}
     */
    public function editCategory(Category $category)
    {
        $this->em->flush();
        $this->session->getFlashBag()->add('success', 'The category has been edited successfully!');
    }

    /**
     * {@inheritdoc}
     */
    public function createSubCategory(SubCategory $subCategory, Category $category, Guild $guild)
    {

        if ($category->getSubCategories()->count() >= 8) {
            throw new \Exception('You have already reached the sub-category limit of 8.');
        }

        if ($subCategory->getPosition() > 10000) {
            $subCategory->setPosition(10000);
        } elseif ($subCategory->getPosition() < 0) {
            $subCategory->setPosition(1);
        }

        $subCategory->setCategory($category);
        $category->addSubCategory($subCategory);

        $this->em->persist($subCategory);
        $this->em->persist($category);
        $this->em->persist($guild);
        $this->em->flush();

        $subCategory->setSlug($this->slugify->getSluged($subCategory->getTitle()).'-'.$subCategory->getId());
        $this->em->flush();

        $this->session->getFlashBag()->add('success', 'The Sub-category has been successfully created!');
    }

    /**
     * {@inheritdoc}
     */
    public function editSubCategory(SubCategory $subCategory)
    {
        $this->em->persist($subCategory);
        $this->em->flush();

        $this->session->getFlashBag()->add('success', 'The Sub-category has been edited successfully!');
    }

    /**
     * {@inheritdoc}
     */
    public function updateCategoryAccess($action, Category $category, $roleId)
    {
        $role = $this->em->getRepository('RizzenGuildBundle:Role')->find($roleId);

        if ($role === null || $role->getGuild() !== $category->getForum()->getGuild()) {
            return [
                'hasBeenUpdated' => false,
                'reason' => 'Role not found'
            ];
        }

        if ($category->getAccessibleBy()->contains($role)) {
            if ($action === 'add') {
                return [
                    'hasBeenUpdated' => false,
                    'reason' => 'This role is already in the access list.'
                ];
            } else {
                $category->removeAccessibleBy($role);
                $this->em->flush();

                return ['hasBeenUpdated' => true];
            }
        }

        $category->addAccessibleBy($role);
        $this->em->flush();

        return ['hasBeenUpdated' => true];
    }

    /**
     * {@inheritdoc}
     */
    public function setGuestAccess($action, Category $category)
    {
        if (sizeof($category->getAccessList()) === 0) {

            $action === 'accessible'
                ? $category->setAccessibleByGuests(true)
                : $category->setAccessibleByGuests(false);

            $this->em->flush();
            return [
                'hasBeenUpdated' => true,
                'isAccessible' => $category->getAccessibleByGuests()
            ];
        }

        return ['hasBeenUpdated' => false];
    }
}
