<?php

namespace Rizzen\ForumBundle\Services;


use Rizzen\GuildBundle\Entity\Guild;
use Rizzen\UserBundle\Entity\User;

interface ForumManagerInterface {

    /**
     * Get a subcategory from a slug
     *
     * @param String $slug
     * @return mixed
     */
    public function getSubCategory($slug);

    /**
     * Get a thread from a slug
     *
     * @param String $slug
     * @param $subcatSlug
     * @param $subdomain
     * @return mixed
     */
    public function getThreadOrNotFound($slug, $subcatSlug, $subdomain);

    /**
     * Join a forum
     *
     * @param Guild $guild
     * @param User $user
     * @return bool
     */
    public function joinForum(Guild $guild, User $user);

}
