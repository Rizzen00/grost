<?php


namespace Rizzen\ForumBundle\Services;


use Rizzen\GuildBundle\Entity\Guild;
use Rizzen\GuildBundle\Permissions\PermissionsManager;
use Rizzen\UserBundle\Entity\User;
use Rizzen\ForumBundle\Entity\Thread;

class ThreadSearchManipulator
{
    private $forumViewable;
    private $permissionsManager;

    /**
     * @param ForumViewable $forumViewable
     * @param PermissionsManager $permissionsManager
     */
    function __construct(ForumViewable $forumViewable, PermissionsManager $permissionsManager)
    {
        $this->forumViewable = $forumViewable;
        $this->permissionsManager = $permissionsManager;
    }

    /**
     * Remove the ranks from the results, and remove the threads the user isn't able to see
     *
     * @param array $results        The search results
     * @param User $currentUser     The current user
     * @param Guild $guild          The guild where the forum is located
     * @return array
     */
    public function cleanSearchResults($results, User $currentUser = null, Guild $guild)
    {
        $this->permissionsManager->setUser($currentUser)->setGuild($guild);

        $threads = $this->removeSearchRanks($results);
        return $this->forumViewable->removeThreadsNotVisibleForSearch($threads, $guild, $this->permissionsManager->getPermissions());

    }

    /**
     * Decode the URL and check if the length is right
     *
     * @param string $query     The search query
     * @return bool|string
     */
    public function cleanSearchQuery($query)
    {
        $query = trim(urldecode($query));
        $queryLength = strlen($query);

        if ($queryLength < 3 || $queryLength > 40) {
            return false;
        }

        return $query;
    }

    /**
     * @param array $results
     * @return Thread[]
     */
    public function removeSearchRanks($results)
    {
        $threads = [];
        foreach ($results as $result) {
            $threads[] = $result[0];
        }

        return $threads;
    }
}
