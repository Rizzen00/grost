<?php

namespace Rizzen\ForumBundle\Services;

use Rizzen\ForumBundle\Entity\Category;
use Rizzen\ForumBundle\Entity\SubCategory;
use Rizzen\GuildBundle\Entity\Guild;

interface ForumCategoryManagerInterface {

    /**
     * Handle the category creation
     *
     * @param Category $category
     * @param Guild $guild
     * @return mixed
     */
    public function createCategory(Category $category, Guild $guild);

    /**
     * @param Category $category
     * @return mixed
     */
    public function editCategory(Category $category);

    /**
     * @param SubCategory $subCategory
     * @param Category $category        The category where the Sub-Category will be attached
     * @param Guild $guild
     * @return mixed
     */
    public function createSubCategory(SubCategory $subCategory, Category $category, Guild $guild);

    /**
     * @param SubCategory $subCategory
     * @return mixed
     */
    public function editSubCategory(SubCategory $subCategory);

    /**
     * Add or remove a role from the access list
     *
     * @param string $action            Can be "add" or "remove"
     * @param Category $category        Category to update
     * @param int $roleId               Role to add or remove from the list
     * @return array
     */
    public function updateCategoryAccess($action, Category $category, $roleId);

    /**
     * @param string $action            What the access type will be, can be "accessible" or "inaccessible"
     * @param Category $category        Category to modify
     * @return array
     */
    public function setGuestAccess($action, Category $category);
}
