<?php

namespace Rizzen\ForumBundle\Services;


use Rizzen\ForumBundle\Entity\Message;
use Rizzen\ForumBundle\Entity\SubCategory;
use Rizzen\ForumBundle\Entity\Thread;
use Rizzen\UserBundle\Entity\User;

interface ForumPosterInterface
{

    /**
     * Post a new message in a thread
     *
     * @param Thread $thread        Forum Thread
     * @param Message $message      Thread message
     * @param User $user
     * @param string $userIp        IP of the message creator
     * @param string                $subdomain
     * @return array
     */
    public function postMessage(Thread $thread, Message $message, User $user, $userIp, $subdomain);

    /**
     * Crate a new thread
     *
     * @param SubCategory $subcat   Forum SubCategory
     * @param Thread $thread        Forum Thread
     * @param User $user
     * @param string $userIp        IP of the thread creator
     * @return string $slug         Slug of the thread created
     */
    public function newThread(SubCategory $subcat, Thread $thread, User $user, $userIp);

} 
