<?php


namespace Rizzen\ForumBundle\Services;


use Doctrine\ORM\EntityManager;
use Rizzen\ForumBundle\Entity\Message;
use Rizzen\ForumBundle\Entity\SubCategory;
use Rizzen\ForumBundle\Entity\Thread;
use Rizzen\MainBundle\Services\Purifier;
use Rizzen\MainBundle\Services\Slugify;
use Rizzen\MainBundle\Services\TextManipulator;
use Rizzen\SocketBundle\Services\SocketProcessor;
use Rizzen\UserBundle\Entity\User;
use Symfony\Bundle\TwigBundle\TwigEngine;

/**
 * Handle the creation of threads and the posting of messages inside threads
 *
 * Class ForumPoster
 * @package Rizzen\ForumBundle\Services
 */
class ForumPoster implements ForumPosterInterface
{
    protected $em;
    protected $slugify;
    protected $purifier;
    protected $textManipulator;
    protected $socketProcessor;
    protected $templating;

    /**
     * @param EntityManager $em
     * @param Purifier $purifier
     * @param TextManipulator $textManipulator
     * @param SocketProcessor $socketProcessor
     * @param TwigEngine $templating
     */
    public function __construct(EntityManager $em, Purifier $purifier, TextManipulator $textManipulator, SocketProcessor $socketProcessor, TwigEngine $templating)
    {
        $this->em = $em;
        $this->slugify = new Slugify();
        $this->purifier = $purifier;
        $this->textManipulator = $textManipulator;
        $this->socketProcessor = $socketProcessor;
        $this->templating = $templating;
    }

    /**
     * {@inheritdoc}
     */
    public function postMessage(Thread $thread, Message $message, User $user, $userIp, $subdomain)
    {

        $user->setMessageCount($user->getMessageCount() + 1);

        $purifiedMessage = $this->purifier->purify('forumPost', $message->getContent());
        $purifiedMessage = $this->textManipulator->addNoFollow($purifiedMessage);

        $thread->addMessage($message);

        $message
            ->setCreator($user)
            ->setCreatorIp($userIp)
            ->setContent($purifiedMessage)
            ->setThread($thread);

        $thread->getSubCategory()->setLatestThreadActivity($thread);

        $messageCount = $this->em->getRepository('RizzenForumBundle:Message')->getMessageCount($thread);
        $thread->setMessagesCount($messageCount + 1);

        $this->em->flush();

        $additionalInfo = ['wrapId' => '#msg-'.$message->getId(), 'sentBy' => $user->getUsername(), 'messageId' => $message->getId()];
        $topic = $thread->getSubCategory()->getCategory()->getAccessRestricted() ? $thread->getPrivateSocketTopic() : $thread->getPublicSocketTopic();

        $templateParam['HTML'] = $this->templating->render('RizzenForumBundle:Forum:message.html.twig', [
                'message' => $message,
                'subdomain' => $subdomain
        ]);

        $data = [
            'additionalInfo' => $additionalInfo,
            'topic' => $topic,
            'template' => $templateParam
        ];

        return $data;
    }

    /**
     * {@inheritdoc}
     */
    public function newThread(SubCategory $subcat, Thread $thread, User $user, $userIp)
    {
        $purifiedMessage = $this->purifier->purify('forumPost', $thread->getFirstMessage());
        $purifiedMessage = $this->textManipulator->addNoFollow($purifiedMessage);

        $message = new Message();

        $message->setContent($purifiedMessage);
        $message->setCreator($user);
        $message->setOriginalPost(true);
        $message->setCreatorIp($userIp);
        $message->setThread($thread);

        $thread->setCreator($user);
        $thread->setMessagesCount(1);
        $thread->setSubCategory($subcat);
        $thread->addMessage($message);

        $subcat->addThread($thread);

        $user->setMessageCount($user->getMessageCount() + 1);
        $user->setThreadCount($user->getThreadCount() + 1);

        $subcat->setLatestThreadActivity($thread);

        $this->em->persist($thread);
        $this->em->persist($message);
        $this->em->flush();

        $slug = $this->slugify->getSluged($thread->getTitle() . '-' . $thread->getId());
        $thread->setSlug($slug);

        $this->em->flush();

        return $slug;
    }

}
