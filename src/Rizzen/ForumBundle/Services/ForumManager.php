<?php

namespace Rizzen\ForumBundle\Services;

use Doctrine\ORM\EntityManager;
use Predis\Client;
use Rizzen\ForumBundle\Entity\Thread;
use Rizzen\GuildBundle\Entity\Guild;
use Rizzen\GuildBundle\Permissions\PermissionsManager;
use Rizzen\UserBundle\Entity\User;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Everything needed to manage the forum
 *
 * Class ForumManager
 * @author Bryan Haag <misios11@hotmail.fr>
 * @package Rizzen\ForumBundle\Services
 */
class ForumManager implements ForumManagerInterface
{
    protected $em;
    protected $permissionsManager;
    protected $session;
    protected $redis;

    /**
     * @param EntityManager $em
     * @param PermissionsManager $permissionsManager
     * @param Session $session
     * @param Client $redis
     */
    public function __construct(EntityManager $em, PermissionsManager $permissionsManager, Session $session, Client $redis)
    {
        $this->em = $em;
        $this->permissionsManager = $permissionsManager;
        $this->session = $session;
        $this->redis = $redis;
    }

    /**
     * {@inheritdoc}
     */
    public function getSubCategory($slug)
    {
        $subcat = $this->em->getRepository('RizzenForumBundle:SubCategory')->getSubCat($slug);
        if ($subcat === null) {
            throw new NotFoundHttpException('The sub category was not found');
        }

        return $subcat;
    }

    /**
     * {@inheritdoc}
     */
    public function getThreadOrNotFound($slug, $subcatSlug, $subdomain)
    {
        /** @var Thread|null $thread */
        $thread = $this->em->getRepository('RizzenForumBundle:Thread')->getThread($slug);

        if ($thread === null ||
            $thread->getSubCategory()->getSlug() !== $subcatSlug ||
            $subdomain !== $thread->getSubCategory()->getCategory()->getForum()->getGuild()->getSlug()) {
            throw new NotFoundHttpException('The thread was not found');
        }

        return $thread;
    }

    /**
     * {@inheritdoc}
     */
    public function joinForum(Guild $guild, User $user)
    {
        $forum = $guild->getForum();
        if (!$forum->isOpen()) {
            $this->session->getFlashBag()->add('error', 'The registrations in this forum are currently closed.');
            return false;
        }

        $this->permissionsManager->setUser($user)->setGuild($guild);

        if ($this->permissionsManager->getPermissions()->isBannedFromForum()) {
            $this->session->getFlashBag()->add('error', 'You are currently banned from this forum.');
            return false;
        } elseif ($this->permissionsManager->isForumMember(false)) {
            $this->session->getFlashBag()->add('error', 'You are already a member of this forum.');
            return false;
        }

        $forum->addMember($user);
        $user->addForum($forum);

        $this->em->persist($user);
        $this->em->persist($forum);
        $this->em->flush();

        $this->session->getFlashBag()->add('success', 'You have successfully joined the forum of this guild!');

        $this->permissionsManager->removePermissionsFromCache();
        $this->permissionsManager->clearLocalPermissions();

        return true;
    }
}
