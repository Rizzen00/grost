<?php

namespace Rizzen\ForumBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Category
 *
 * @ORM\Table(indexes={@ORM\Index(name="forum_category_idx", columns={"slug"})})
 * @ORM\Entity(repositoryClass="Rizzen\ForumBundle\Entity\CategoryRepository")
 */
class Category
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     *
     * @Assert\NotBlank()
     * @Assert\Length(
     *      min = "5",
     *      max = "40"
     * )
     */
    protected $title;

    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=255, nullable=true)
     */
    protected $slug;

    /**
     * @var integer
     *
     * @ORM\Column(name="position", type="integer")
     *
     * @Assert\NotBlank()
     * @Assert\Type(type="numeric")
     */
    protected $position;

    /**
     * @var boolean
     *
     * @ORM\Column(name="locked", type="boolean")
     */
    protected $locked;

    /**
     * @ORM\ManyToOne(targetEntity="Forum", inversedBy="categories")
     */
    protected $forum;

    /**
     * @ORM\ManyToMany(targetEntity="Rizzen\GuildBundle\Entity\Role")
     * @ORM\JoinTable(name="category_roles",
     *      joinColumns={@ORM\JoinColumn(name="category_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="role_id", referencedColumnName="id", unique=false)}
     *      )
     **/
    protected $accessibleBy;

    /**
     * @var boolean
     *
     * @ORM\Column(name="accessList", type="array")
     */
    protected $accessList;

    /**
     * @var boolean
     *
     * @ORM\Column(name="accessRestricted", type="boolean")
     */
    protected $accessRestricted;

    /**
     * @var boolean
     *
     * @ORM\Column(name="accessibleByGuests", type="boolean")
     */
    protected $accessibleByGuests;


    /**
     * @ORM\OneToMany(targetEntity="SubCategory", mappedBy="category", cascade={"remove"})
     * @ORM\OrderBy({"position" = "ASC"})
     */
    private $subCategories;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Category
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set position
     *
     * @param integer $position
     * @return Category
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return integer
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set locked
     *
     * @param integer $locked
     * @return Category
     */
    public function setLocked($locked)
    {
        $this->locked = $locked;

        return $this;
    }

    /**
     * Get locked
     *
     * @return integer
     */
    public function isLocked()
    {
        return $this->locked;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->accessibleByGuests = true;
        $this->accessRestricted = false;
        $this->accessList = [];
        $this->locked = false;
        $this->subCategories = new \Doctrine\Common\Collections\ArrayCollection();
        $this->accessibleBy = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set forum
     *
     * @param \Rizzen\ForumBundle\Entity\Forum $forum
     * @return Category
     */
    public function setForum(\Rizzen\ForumBundle\Entity\Forum $forum = null)
    {
        $this->forum = $forum;

        return $this;
    }

    /**
     * Get forum
     *
     * @return \Rizzen\ForumBundle\Entity\Forum
     */
    public function getForum()
    {
        return $this->forum;
    }

    /**
     * Add subCategories
     *
     * @param \Rizzen\ForumBundle\Entity\SubCategory $subCategories
     * @return Category
     */
    public function addSubCategory(\Rizzen\ForumBundle\Entity\SubCategory $subCategories)
    {
        $this->subCategories[] = $subCategories;

        return $this;
    }

    /**
     * Remove subCategories
     *
     * @param \Rizzen\ForumBundle\Entity\SubCategory $subCategories
     */
    public function removeSubCategory(\Rizzen\ForumBundle\Entity\SubCategory $subCategories)
    {
        $this->subCategories->removeElement($subCategories);
    }

    /**
     * Get subCategories
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSubCategories()
    {
        return $this->subCategories;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return Category
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set accessibleByGuests
     *
     * @param boolean $accessibleByGuests
     * @return Category
     */
    public function setAccessibleByGuests($accessibleByGuests)
    {
        $this->accessibleByGuests = $accessibleByGuests;

        return $this;
    }

    /**
     * Get accessibleByGuests
     *
     * @return boolean 
     */
    public function getAccessibleByGuests()
    {
        return $this->accessibleByGuests;
    }

    /**
     * Add accessibleBy
     *
     * @param \Rizzen\GuildBundle\Entity\Role $accessibleBy
     * @param bool $noUpdate    If true, the updateAccessList method wont be fired once the Role has been added
     * @return Category
     */
    public function addAccessibleBy(\Rizzen\GuildBundle\Entity\Role $accessibleBy, $noUpdate = false)
    {
        $this->accessibleBy[] = $accessibleBy;

        if (!$noUpdate) {
            $this->updateAccessList();
        }

        return $this;
    }

    /**
     * Remove accessibleBy
     *
     * @param \Rizzen\GuildBundle\Entity\Role $accessibleBy
     */
    public function removeAccessibleBy(\Rizzen\GuildBundle\Entity\Role $accessibleBy)
    {
        $this->accessibleBy->removeElement($accessibleBy);

        $this->updateAccessList();
    }

    /**
     * Get accessibleBy
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAccessibleBy()
    {
        return $this->accessibleBy;
    }

    /**
     * Set accessList
     *
     * @param array $accessList
     * @return Category
     */
    public function setAccessList($accessList)
    {
        $this->accessList = $accessList;

        return $this;
    }

    /**
     * Get accessList
     *
     * @return array 
     */
    public function getAccessList()
    {
        return $this->accessList;
    }

    /**
     * Set accessRestricted
     *
     * @param boolean $accessRestricted
     * @return Category
     */
    public function setAccessRestricted($accessRestricted)
    {
        $this->accessRestricted = $accessRestricted;

        return $this;
    }

    /**
     * Get accessRestricted
     *
     * @return boolean 
     */
    public function getAccessRestricted()
    {
        return $this->accessRestricted;
    }

    /**
     * Update the access list array
     */
    public function updateAccessList()
    {
        $accessList = [];
        $loopCount = 0;
        foreach ($this->accessibleBy as $accessibleByRole) {
            $accessList[] = $accessibleByRole->getId();
            $loopCount++;
        }

        if ($loopCount === 0) {
            $this->setAccessRestricted(false);
            $this->setAccessibleByGuests(false);
        } else {
            $this->setAccessRestricted(true);
        }

        $this->setAccessList($accessList);
    }
}
