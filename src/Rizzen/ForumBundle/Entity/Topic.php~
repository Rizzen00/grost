<?php

namespace Rizzen\ForumBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Topic
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Rizzen\ForumBundle\Entity\TopicRepository")
 */
class Topic
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=255, nullable=true)
     */
    private $slug;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;

    /**
     * @var boolean
     *
     * @ORM\Column(name="visible", type="boolean")
     */
    private $visible;

    /**
     * @ORM\ManyToOne(targetEntity="Rizzen\UserBundle\Entity\User")
     * @ORM\JoinColumn(nullable=false)
     */
    private $creator;

    /**
     * @ORM\ManyToOne(targetEntity="SubCategory", inversedBy="topics")
     */
    protected $subCategory;

    /**
     * @var integer
     *
     * @ORM\Column(name="messagesCount", type="integer")
     */
    private $messagesCount;

    /**
     * @ORM\OneToOne(targetEntity="Message", cascade={"persist"})
     */
    private $lastMessage;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="lastMessageDate", type="datetime")
     */
    private $lastMessageDate;

    /**
     * @ORM\OneToMany(targetEntity="Message", mappedBy="topic", cascade={"remove"})
     */
    private $messages;

    private $firstMessage;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return Topic
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set visible
     *
     * @param boolean $visible
     * @return Topic
     */
    public function setVisible($visible)
    {
        $this->visible = $visible;

        return $this;
    }

    /**
     * Get visible
     *
     * @return boolean
     */
    public function getVisible()
    {
        return $this->visible;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->date = new \DateTime('now');
        $this->visible = true;
        $this->messages = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set creator
     *
     * @param \Rizzen\UserBundle\Entity\User $creator
     * @return Topic
     */
    public function setCreator(\Rizzen\UserBundle\Entity\User $creator)
    {
        $this->creator = $creator;

        return $this;
    }

    /**
     * Get creator
     *
     * @return \Rizzen\UserBundle\Entity\User
     */
    public function getCreator()
    {
        return $this->creator;
    }

    /**
     * Set subCategory
     *
     * @param \Rizzen\ForumBundle\Entity\SubCategory $subCategory
     * @return Topic
     */
    public function setSubCategory(\Rizzen\ForumBundle\Entity\SubCategory $subCategory = null)
    {
        $this->subCategory = $subCategory;

        return $this;
    }

    /**
     * Get subCategory
     *
     * @return \Rizzen\ForumBundle\Entity\SubCategory
     */
    public function getSubCategory()
    {
        return $this->subCategory;
    }

    /**
     * Add messages
     *
     * @param \Rizzen\ForumBundle\Entity\Message $messages
     * @return Topic
     */
    public function addMessage(\Rizzen\ForumBundle\Entity\Message $messages)
    {
        $this->messages[] = $messages;

        return $this;
    }

    /**
     * Remove messages
     *
     * @param \Rizzen\ForumBundle\Entity\Message $messages
     */
    public function removeMessage(\Rizzen\ForumBundle\Entity\Message $messages)
    {
        $this->messages->removeElement($messages);
    }

    /**
     * Get messages
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMessages()
    {
        return $this->messages;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Topic
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return Topic
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    public function setFirstMessage($firstMessage)
    {
        $this->firstMessage = $firstMessage;

        return $this;
    }

    public function getFirstMessage()
    {
        return $this->firstMessage;
    }

    /**
     * Set messagesCount
     *
     * @param integer $messagesCount
     * @return Topic
     */
    public function setMessagesCount($messagesCount)
    {
        $this->messagesCount = $messagesCount;

        return $this;
    }

    /**
     * Get messagesCount
     *
     * @return integer
     */
    public function getMessagesCount()
    {
        return $this->messagesCount;
    }

    /**
     * Set lastMessage
     *
     * @param \Rizzen\ForumBundle\Entity\Message $lastMessage
     * @return Topic
     */
    public function setLastMessage(\Rizzen\ForumBundle\Entity\Message $lastMessage = null)
    {
        $this->lastMessage = $lastMessage;

        return $this;
    }

    /**
     * Get lastMessage
     *
     * @return \Rizzen\ForumBundle\Entity\Message
     */
    public function getLastMessage()
    {
        return $this->lastMessage;
    }
}
