<?php

namespace Rizzen\ForumBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * SubCategoryRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class SubCategoryRepository extends EntityRepository
{

    public function getSubCat($slug)
    {
        $qb = $this->createQueryBuilder('subcat');
        $qb->where('subcat.slug = :slug')
            ->setParameter('slug', $slug)
            ->orderBy('subcat.position', 'ASC')
            ->leftJoin('subcat.category', 'category')
            ->addSelect('category');

        return $qb->getQuery()->getOneOrNullResult();
    }

}
