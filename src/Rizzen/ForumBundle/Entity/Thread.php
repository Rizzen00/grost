<?php

namespace Rizzen\ForumBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Thread
 *
 * @ORM\Table(indexes={@ORM\Index(name="forum_thread_idx", columns={"slug"})})
 * @ORM\Entity(repositoryClass="Rizzen\ForumBundle\Entity\ThreadRepository")
 */
class Thread
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     *
     * @Assert\NotBlank()
     * @Assert\Length(
     *      min = "5",
     *      max = "40"
     * )
     */
    protected $title;

    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=255, nullable=true)
     */
    protected $slug;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetimetz")
     */
    protected $date;

    /**
     * @var string
     *
     * @ORM\Column(name="search_fts", type="tsvector", options={"customSchemaOptions": {"searchFields":{"title"}}}, nullable=true)
     */
    protected $searchFts;

    /**
     * @var boolean
     *
     * @ORM\Column(name="visible", type="boolean")
     */
    protected $visible;

    /**
     * @var boolean
     *
     * @ORM\Column(name="locked", type="boolean")
     */
    protected $locked;

    /**
     * @var boolean
     *
     * @ORM\Column(name="pinned", type="boolean")
     */
    protected $pinned;

    /**
     * @var string
     *
     * @ORM\Column(name="publicSocketTopic", type="string", length=255, nullable=true)
     */
    protected $publicSocketTopic;

    /**
     * @var string
     *
     * @ORM\Column(name="privateSocketTopic", type="string", length=255, nullable=true)
     */
    protected $privateSocketTopic;

    /**
     * @ORM\ManyToOne(targetEntity="Rizzen\UserBundle\Entity\User")
     * @ORM\JoinColumn(nullable=false)
     */
    protected $creator;

    /**
     * @ORM\ManyToOne(targetEntity="SubCategory", inversedBy="threads")
     */
    protected $subCategory;

    /**
     * @var integer
     *
     * @ORM\Column(name="messagesCount", type="integer")
     */
    protected $messagesCount;

    /**
     * @ORM\OneToOne(targetEntity="Message", cascade={"persist"})
     */
    protected $lastMessage;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="lastMessageDate", type="datetimetz")
     */
    protected $lastMessageDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="views", type="integer")
     */
    protected $views;

    /**
     * @ORM\OneToMany(targetEntity="Message", mappedBy="thread", cascade={"remove"})
     * @ORM\OrderBy({"date" = "ASC"})
     */
    protected $messages;

    /**
     * @Assert\NotBlank()
     * @Assert\Length(
     *      min = "1",
     *      max = "21000"
     * )
     * */
    protected $firstMessage;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return Thread
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set visible
     *
     * @param boolean $visible
     * @return Thread
     */
    public function setVisible($visible)
    {
        $this->visible = $visible;

        return $this;
    }

    /**
     * Get visible
     *
     * @return boolean
     */
    public function getVisible()
    {
        return $this->visible;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->date = new \DateTime('now');
        $this->messagesCount = 0;
        $this->views = 0;
        $this->locked = false;
        $this->visible = true;
        $this->pinned = false;
        $this->messages = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set creator
     *
     * @param \Rizzen\UserBundle\Entity\User $creator
     * @return Thread
     */
    public function setCreator(\Rizzen\UserBundle\Entity\User $creator)
    {
        $this->creator = $creator;

        return $this;
    }

    /**
     * Get creator
     *
     * @return \Rizzen\UserBundle\Entity\User
     */
    public function getCreator()
    {
        return $this->creator;
    }

    /**
     * Set subCategory
     *
     * @param \Rizzen\ForumBundle\Entity\SubCategory $subCategory
     * @return Thread
     */
    public function setSubCategory(\Rizzen\ForumBundle\Entity\SubCategory $subCategory = null)
    {
        $this->subCategory = $subCategory;

        return $this;
    }

    /**
     * Get subCategory
     *
     * @return \Rizzen\ForumBundle\Entity\SubCategory
     */
    public function getSubCategory()
    {
        return $this->subCategory;
    }

    /**
     * Add messages
     *
     * @param \Rizzen\ForumBundle\Entity\Message $messages
     * @return Thread
     */
    public function addMessage(\Rizzen\ForumBundle\Entity\Message $messages)
    {
        $this->messages[] = $messages;

        $this->setLastMessage($messages)
             ->setLastMessageDate(new \DateTime('now'));

        return $this;
    }

    /**
     * Remove messages
     *
     * @param \Rizzen\ForumBundle\Entity\Message $messages
     */
    public function removeMessage(\Rizzen\ForumBundle\Entity\Message $messages)
    {
        $this->messages->removeElement($messages);
    }

    /**
     * Get messages
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMessages()
    {
        return $this->messages;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Thread
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set slug, it adds the socket topics too
     *
     * @param string $slug
     * @return Thread
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
        $this->setPrivateSocketTopic('private-thread-' . $slug);
        $this->setPublicSocketTopic('public-thread-' . $slug);

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    public function setFirstMessage($firstMessage)
    {
        $this->firstMessage = $firstMessage;

        return $this;
    }

    public function getFirstMessage()
    {
        return $this->firstMessage;
    }

    /**
     * Set messagesCount
     *
     * @param integer $messagesCount
     * @return Thread
     */
    public function setMessagesCount($messagesCount)
    {
        $this->messagesCount = $messagesCount;

        return $this;
    }

    /**
     * Get messagesCount
     *
     * @return integer
     */
    public function getMessagesCount()
    {
        return $this->messagesCount;
    }

    /**
     * Set lastMessage
     *
     * @param \Rizzen\ForumBundle\Entity\Message $lastMessage
     * @return Thread
     */
    public function setLastMessage(\Rizzen\ForumBundle\Entity\Message $lastMessage = null)
    {
        $this->lastMessage = $lastMessage;

        return $this;
    }

    /**
     * Get lastMessage
     *
     * @return \Rizzen\ForumBundle\Entity\Message
     */
    public function getLastMessage()
    {
        return $this->lastMessage;
    }

    /**
     * Set lastMessageDate
     *
     * @param \DateTime $lastMessageDate
     * @return Thread
     */
    public function setLastMessageDate($lastMessageDate)
    {
        $this->lastMessageDate = $lastMessageDate;

        return $this;
    }

    /**
     * Get lastMessageDate
     *
     * @return \DateTime
     */
    public function getLastMessageDate()
    {
        return $this->lastMessageDate;
    }

    /**
     * Set publicSocketTopic
     *
     * @param string $publicSocketTopic
     * @return Thread
     */
    public function setPublicSocketTopic($publicSocketTopic)
    {
        $this->publicSocketTopic = $publicSocketTopic;

        return $this;
    }

    /**
     * Get publicSocketTopic
     *
     * @return string 
     */
    public function getPublicSocketTopic()
    {
        return $this->publicSocketTopic;
    }

    /**
     * Set privateSocketTopic
     *
     * @param string $privateSocketTopic
     * @return Thread
     */
    public function setPrivateSocketTopic($privateSocketTopic)
    {
        $this->privateSocketTopic = $privateSocketTopic;

        return $this;
    }

    /**
     * Get privateSocketTopic
     *
     * @return string 
     */
    public function getPrivateSocketTopic()
    {
        return $this->privateSocketTopic;
    }

    /**
     * Set locked
     *
     * @param boolean $locked
     *
     * @return Thread
     */
    public function setLocked($locked)
    {
        $this->locked = $locked;

        return $this;
    }

    /**
     * Is locked
     *
     * @return boolean
     */
    public function isLocked()
    {
        return $this->locked;
    }

    /**
     * Set searchFts
     *
     * @param string $searchFts
     *
     * @return Thread
     */
    public function setSearchFts($searchFts)
    {
        $this->searchFts = $searchFts;

        return $this;
    }

    /**
     * Get searchFts
     *
     * @return string
     */
    public function getSearchFts()
    {
        return $this->searchFts;
    }

    /**
     * Set pinned
     *
     * @param boolean $pinned
     *
     * @return Thread
     */
    public function setPinned($pinned)
    {
        $this->pinned = $pinned;

        return $this;
    }

    /**
     * Get pinned
     *
     * @return boolean
     */
    public function isPinned()
    {
        return $this->pinned;
    }

    /**
     * Set views
     *
     * @param integer $views
     *
     * @return Thread
     */
    public function setViews($views)
    {
        $this->views = $views;

        return $this;
    }

    /**
     * Get views
     *
     * @return integer
     */
    public function getViews()
    {
        return $this->views;
    }

    /**
     * Increase the view count by 1
     *
     * @return void
     */
    public function increaseViewCount()
    {
        $this->views++;
    }
}
