<?php

namespace Rizzen\ForumBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * SubCategory
 *
 * @ORM\Table(indexes={@ORM\Index(name="forum_subcategory_idx", columns={"slug"})})
 * @ORM\Entity(repositoryClass="Rizzen\ForumBundle\Entity\SubCategoryRepository")
 */
class SubCategory
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     *
     * @Assert\NotBlank()
     * @Assert\Length(
     *      min = "5",
     *      max = "40"
     * )
     */
    protected $title;

    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=255, nullable=true)
     */
    protected $slug;

    /**
     * @var integer
     *
     * @ORM\Column(name="position", type="integer")
     *
     * @Assert\NotBlank()
     * @Assert\Type(type="numeric")
     */
    protected $position;

    /**
     * @var boolean
     *
     * @ORM\Column(name="locked", type="boolean")
     */
    protected $locked;

    /**
     * @ORM\OneToMany(targetEntity="Thread", mappedBy="subCategory", cascade={"remove"})
     * @ORM\OrderBy({"lastMessageDate" = "DESC"})
     */
    protected $threads;

    /**
     * @ORM\OneToOne(targetEntity="Thread")
     */
    protected $latestThreadActivity;

    /**
     * @ORM\ManyToOne(targetEntity="Category", inversedBy="subCategories")
     */
    protected $category;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return SubCategory
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set position
     *
     * @param integer $position
     * @return SubCategory
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return integer
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set locked
     *
     * @param bool $locked
     * @return SubCategory
     */
    public function setLocked($locked)
    {
        $this->locked = $locked;

        return $this;
    }

    /**
     * Get locked
     *
     * @return integer
     */
    public function isLocked()
    {
        return $this->locked;
    }

    /**
     * Add threads
     *
     * @param \Rizzen\ForumBundle\Entity\Thread $threads
     * @return SubCategory
     */
    public function addThread(\Rizzen\ForumBundle\Entity\Thread $threads)
    {
        $this->threads[] = $threads;

        return $this;
    }

    /**
     * Remove threads
     *
     * @param \Rizzen\ForumBundle\Entity\Thread $threads
     */
    public function removeThread(\Rizzen\ForumBundle\Entity\Thread $threads)
    {
        $this->threads->removeElement($threads);
    }

    /**
     * Get threads
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getThreads()
    {
        return $this->threads;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->threads = new \Doctrine\Common\Collections\ArrayCollection();
        $this->locked = false;
    }

    /**
     * Set category
     *
     * @param \Rizzen\ForumBundle\Entity\Category $category
     * @return Category
     */
    public function setCategory(\Rizzen\ForumBundle\Entity\Category $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return \Rizzen\ForumBundle\Entity\Category
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return SubCategory
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set latestThreadActivity
     *
     * @param \Rizzen\ForumBundle\Entity\Thread $latestThreadActivity
     * @return SubCategory
     */
    public function setLatestThreadActivity(\Rizzen\ForumBundle\Entity\Thread $latestThreadActivity = null)
    {
        $this->latestThreadActivity = $latestThreadActivity;

        return $this;
    }

    /**
     * Get latestThreadActivity
     *
     * @return \Rizzen\ForumBundle\Entity\Thread
     */
    public function getLatestThreadActivity()
    {
        return $this->latestThreadActivity;
    }
}
