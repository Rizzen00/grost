<?php

namespace Rizzen\ForumBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Message
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Rizzen\ForumBundle\Entity\MessageRepository")
 */
class Message
{
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->date = new \DateTime('now');
        $this->originalPost = false;
        $this->visible = true;
        $this->isLocked = false;
    }

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text")
     *
     * @Assert\NotBlank()
     * @Assert\Length(
     *      min = "1",
     *      max = "11000"
     * )
     */
    protected $content;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetimetz")
     */
    protected $date;

    /**
     * @var string
     *
     * @ORM\Column(name="creatorIp", type="cidr")
     */
    protected $creatorIp;

    /**
     * @ORM\ManyToOne(targetEntity="Rizzen\UserBundle\Entity\User")
     * @ORM\JoinColumn(nullable=false)
     */
    protected $creator;

    /**
     * @var boolean
     *
     * @ORM\Column(name="visible", type="boolean")
     */
    protected $visible;

    /**
     * @ORM\ManyToOne(targetEntity="Thread", inversedBy="messages")
     */
    protected $thread;

    /**
     * @var boolean
     *
     * @ORM\Column(name="isLocked", type="boolean", nullable=true)
     */
    protected $isLocked;

    /**
     * @var boolean
     *
     * @ORM\Column(name="originalPost", type="boolean")
     */
    protected $originalPost;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set content
     *
     * @param string $content
     * @return Message
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return Message
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set creatorIp
     *
     * @param string $creatorIp
     * @return Message
     */
    public function setCreatorIp($creatorIp)
    {
        $this->creatorIp = $creatorIp;

        return $this;
    }

    /**
     * Get creatorIp
     *
     * @return string
     */
    public function getCreatorIp()
    {
        return $this->creatorIp;
    }

    /**
     * Set visible
     *
     * @param boolean $visible
     * @return Message
     */
    public function setVisible($visible)
    {
        $this->visible = $visible;

        return $this;
    }

    /**
     * Get visible
     *
     * @return boolean
     */
    public function isVisible()
    {
        return $this->visible;
    }

    /**
     * Set creator
     *
     * @param \Rizzen\UserBundle\Entity\User $creator
     * @return Message
     */
    public function setCreator(\Rizzen\UserBundle\Entity\User $creator)
    {
        $this->creator = $creator;

        return $this;
    }

    /**
     * Get creator
     *
     * @return \Rizzen\UserBundle\Entity\User
     */
    public function getCreator()
    {
        return $this->creator;
    }

    /**
     * Set thread
     *
     * @param \Rizzen\ForumBundle\Entity\Thread $thread
     * @return Message
     */
    public function setThread(\Rizzen\ForumBundle\Entity\Thread $thread = null)
    {
        $this->thread = $thread;

        return $this;
    }

    /**
     * Get thread
     *
     * @return \Rizzen\ForumBundle\Entity\Thread
     */
    public function getThread()
    {
        return $this->thread;
    }

    /**
     * Set isLocked
     *
     * @param boolean $isLocked
     * @return Message
     */
    public function setLocked($isLocked)
    {
        $this->isLocked = $isLocked;

        return $this;
    }

    /**
     * Get isLocked
     *
     * @return boolean 
     */
    public function isLocked()
    {
        return $this->isLocked;
    }

    /**
     * Set originalPost
     *
     * @param boolean $originalPost
     *
     * @return Message
     */
    public function setOriginalPost($originalPost)
    {
        $this->originalPost = $originalPost;

        return $this;
    }

    /**
     * Get originalPost
     *
     * @return boolean
     */
    public function isOriginalPost()
    {
        return $this->originalPost;
    }
}
