<?php

namespace Rizzen\ForumBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Forum
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Rizzen\ForumBundle\Entity\ForumRepository")
 */
class Forum
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\OneToOne(targetEntity="Rizzen\GuildBundle\Entity\Guild", inversedBy="forum")
     */
    protected $guild;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    protected $title;

    /**
     * @var boolean
     *
     * @ORM\Column(name="open", type="boolean")
     */
    protected $open;

    /**
     * @ORM\OneToMany(targetEntity="Category", mappedBy="forum", cascade={"remove"})
     * @ORM\OrderBy({"position" = "ASC"})
     */
    protected $categories;

    /**
     * @ORM\ManyToMany(targetEntity="Rizzen\UserBundle\Entity\User", inversedBy="forums")
     */
    protected $members;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Forum
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }


    /**
     * Set guild
     *
     * @param \Rizzen\GuildBundle\Entity\Guild $guild
     * @return Forum
     */
    public function setGuild(\Rizzen\GuildBundle\Entity\Guild $guild = null)
    {
        $this->guild = $guild;

        return $this;
    }

    /**
     * Get guild
     *
     * @return \Rizzen\GuildBundle\Entity\Guild
     */
    public function getGuild()
    {
        return $this->guild;
    }

    /**
     * Add categories
     *
     * @param \Rizzen\ForumBundle\Entity\Category $categories
     * @return Forum
     */
    public function addCategory(\Rizzen\ForumBundle\Entity\Category $categories)
    {
        $this->categories[] = $categories;

        return $this;
    }

    /**
     * Remove categories
     *
     * @param \Rizzen\ForumBundle\Entity\Category $categories
     */
    public function removeCategory(\Rizzen\ForumBundle\Entity\Category $categories)
    {
        $this->categories->removeElement($categories);
    }

    /**
     * Get categories
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCategories()
    {
        return $this->categories;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->open = true;
        $this->categories = new \Doctrine\Common\Collections\ArrayCollection();
        $this->members = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add members
     *
     * @param \Rizzen\UserBundle\Entity\User $members
     * @return Forum
     */
    public function addMember(\Rizzen\UserBundle\Entity\User $members)
    {
        $this->members[] = $members;

        return $this;
    }

    /**
     * Remove members
     *
     * @param \Rizzen\UserBundle\Entity\User $members
     */
    public function removeMember(\Rizzen\UserBundle\Entity\User $members)
    {
        $this->members->removeElement($members);
    }

    /**
     * Get members
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMembers()
    {
        return $this->members;
    }

    /**
     * Set open
     *
     * @param boolean $open
     * @return Forum
     */
    public function setOpen($open)
    {
        $this->open = $open;

        return $this;
    }

    /**
     * Is open
     *
     * @return boolean
     */
    public function isOpen()
    {
        return $this->open;
    }
}
