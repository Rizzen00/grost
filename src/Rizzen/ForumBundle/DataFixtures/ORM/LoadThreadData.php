<?php
/**
 * Created by PhpStorm.
 * User: bryan
 * Date: 18/07/15
 * Time: 18:50
 */

namespace Rizzen\ForumBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Rizzen\ForumBundle\Entity\Thread;
use Rizzen\UserBundle\Entity\User;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadThreadData extends AbstractFixture implements OrderedFixtureInterface, FixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
        $userManager = $this->container->get('fos_user.user_manager');

        /** @var User $user */
        $user = $userManager->findUserBy(['username' => 'testUser0']);

        $subcat = $manager->getRepository('RizzenForumBundle:SubCategory')->findOneByTitle('TestSubCategory');

        $thread = new Thread();
        $thread->setTitle('TestThread');
        $thread->setFirstMessage('This is a test message');

        $this->container->get('forum.poster')->newThread($subcat, $thread, $user, '192.168.1.1');
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 5;
    }
}
