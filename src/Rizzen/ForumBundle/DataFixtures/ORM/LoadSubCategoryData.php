<?php
/**
 * Created by PhpStorm.
 * User: bryan
 * Date: 18/07/15
 * Time: 18:50
 */

namespace Rizzen\ForumBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Rizzen\ForumBundle\Entity\SubCategory;
use Rizzen\GuildBundle\Entity\Guild;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;


class LoadSubCategoryData extends AbstractFixture implements OrderedFixtureInterface, FixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {

        /** @var Guild $guild */
        $guild = $manager->getRepository('RizzenGuildBundle:Guild')->findOneByTitle('TestGuild');
        $category = $manager->getRepository('RizzenForumBundle:Category')->findOneByTitle('TestCategory');

        $subCategory = new SubCategory();
        $subCategory->setTitle('TestSubCategory');
        $subCategory->setPosition(1);

        $this->container->get('forum.category.manager')->createSubCategory($subCategory, $category, $guild);

    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 4;
    }
}
