<?php

namespace Rizzen\CalendarBundle\Command;

use Rizzen\CalendarBundle\Constants\RegistrationStates;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Formatter\OutputFormatterStyle;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Rizzen\CalendarBundle\Entity\Event;
use Rizzen\CalendarBundle\Entity\Registration;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManager;

class ReminderCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('grost:event:remind')
            ->setDescription('Send an email to every participant of an event that is starting in less than 1hour')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $formatter = $this->getHelperSet()->get('formatter');

        $style = new OutputFormatterStyle('white', 'blue', ['bold']);
        $output->getFormatter()->setStyle('title', $style);

        $message = ['Grost - Event Reminder', 'Checking if any event is starting soon...'];
        $formattedBlock = $formatter->formatBlock($message, 'title', true);
        $output->writeln($formattedBlock);

        $output->writeln('');

        /** @var EntityManager $em */
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');

        $currentDate = new \DateTime('now');
        $output->writeln('<info>Current PHP time: ' . $currentDate->format('Y-m-d H:i:s') . '</info>');
        $output->writeln('<info>Current TimeZone ' . date_default_timezone_get() . '</info>');

        $startDate = new \DateTime('now');
        $startDate->setTimezone(new \DateTimeZone('UTC'));

        $endDate = new \DateTime('now');
        $endDate->setTimezone(new \DateTimeZone('UTC'));
        $endDate->add(new \DateInterval('PT1H'));

        $output->writeln('<info>Start date:</info>');
        $output->writeln('<info>' . $startDate->format('Y-m-d H:i:s') . '</info>');
        $output->writeln('<info>End date:</info>');
        $output->writeln('<info>' . $endDate->format('Y-m-d H:i:s') . '</info>');

        /** @var Event[] $events */
        $events = $em->getRepository('RizzenCalendarBundle:Event')->getEventsToRemind($startDate, $endDate);

        $eventCount = count($events);

        if ($eventCount < 1) {
            $output->writeln('<info>Nothing is starting in the next hour</info>');
            return;
        } else {
            $output->writeln('<info>' . $eventCount . ' Event(s) found!</info>');
        }

        $progress = $this->getHelperSet()->get('progress');
        $progress->start($output, $eventCount);

        $output->writeln('');
        $output->writeln('<info>Sending the reminders...</info>');
        $output->writeln('');

        $remindersSentCount = 0;

        foreach ($events as $event) {

            /** @var Registration $registration */
            foreach ($event->getRegistrations() as $registration) {

                if (!$registration->isReminded() && $registration->getState() === RegistrationStates::ACCEPTED) {
                    $this->remindUser($registration);
                    $registration->setReminded(true);

                    $remindersSentCount++;
                }

            }

            $progress->advance();

        }

        $progress->finish();

        $output->writeln('');
        $output->writeln('<info>Saving the reminded registrations in the database...</info>');
        $output->writeln('');

        $em->flush();

        $style = new OutputFormatterStyle('white', 'green', ['bold']);
        $output->getFormatter()->setStyle('done', $style);

        $message = ['Event Reminder - Done', '[' . $remindersSentCount . '] Reminder(s) have been sent.'];
        $formattedBlock = $formatter->formatBlock($message, 'done', true);
        $output->writeln($formattedBlock);

    }

    private function remindUser(Registration $registration)
    {
        $user = $registration->getUser();

        if (!$user->getOptions()->email()->getReceiveEmailForEventReminder()) {
            return;
        }

        $templating = $this->getContainer()->get('templating');

        $template = $templating->render('RizzenCalendarBundle:Email:reminder.html.twig', [
            'guild' => $registration->getEvent()->getCalendar()->getGuild(),
            'event' => $registration->getEvent()
        ]);

        $message = \Swift_Message::newInstance()
            ->setSubject('Grost - An event is starting soon!')
            ->setFrom('noreply@gro.st', 'Grost')
            ->setTo($user->getEmailCanonical())
            ->setBody($template, 'text/html');

        $this->getContainer()->get('mailer')->send($message);

    }
}
