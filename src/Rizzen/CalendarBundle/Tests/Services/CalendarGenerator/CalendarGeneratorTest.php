<?php


namespace Rizzen\CalendarBundle\Tests\Services\CalendarGenerator;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class CalendarGeneratorTest extends WebTestCase {

    private $em;
    private $calendarGenerator;
    private $currentDate;

    function __construct()
    {
        static::$kernel = static::createKernel();
        static::$kernel->boot();
        $this->em = static::$kernel->getContainer()->get('doctrine.orm.entity_manager');
        $this->calendarGenerator = static::$kernel->getContainer()->get('rizzen.calendar.generator');
        $this->currentDate = new \DateTime('now');
    }

    public function testDateVerification()
    {
        $yearInputNormal = 2017;
        $yearInputWrong1 = 2050;
        $yearInputWrong2 = 2010;

        $resultYear1 = $this->calendarGenerator->verifyYear($yearInputNormal, $this->currentDate);
        $resultYear2 = $this->calendarGenerator->verifyYear($yearInputWrong1, $this->currentDate);
        $resultYear3 = $this->calendarGenerator->verifyYear($yearInputWrong2, $this->currentDate);

        $this->assertEquals($resultYear1, 2017);
        $this->assertEquals($resultYear2, $this->currentDate->format('Y'));
        $this->assertEquals($resultYear3, $this->currentDate->format('Y'));

        $monthInputNormal = 11;
        $monthInputWrong1 = 14;
        $monthInputWrong2 = -1;

        $resultMonth1 = $this->calendarGenerator->verifyMonth($monthInputNormal, $this->currentDate);
        $resultMonth2 = $this->calendarGenerator->verifyMonth($monthInputWrong1, $this->currentDate);
        $resultMonth3 = $this->calendarGenerator->verifyMonth($monthInputWrong2, $this->currentDate);

        $this->assertEquals($resultMonth1, 11);
        $this->assertEquals($resultMonth2, $this->currentDate->format('m'));
        $this->assertEquals($resultMonth3, $this->currentDate->format('m'));

    }

    public function testBlanks()
    {
        $resultBlank1 = $this->calendarGenerator->processBlanks('Mon');
        $resultBlank2 = $this->calendarGenerator->processBlanks('Tue');
        $resultBlank3 = $this->calendarGenerator->processBlanks('Wed');
        $resultBlank4 = $this->calendarGenerator->processBlanks('Thu');
        $resultBlank5 = $this->calendarGenerator->processBlanks('Fri');
        $resultBlank6 = $this->calendarGenerator->processBlanks('Sat');
        $resultBlank7 = $this->calendarGenerator->processBlanks('Sun');

        $this->assertEquals($resultBlank1, 0);
        $this->assertEquals($resultBlank2, 1);
        $this->assertEquals($resultBlank3, 2);
        $this->assertEquals($resultBlank4, 3);
        $this->assertEquals($resultBlank5, 4);
        $this->assertEquals($resultBlank6, 5);
        $this->assertEquals($resultBlank7, 6);

    }

    public function testMonthProcess()
    {
        $nextAndPrev = $this->calendarGenerator->processMonths(12, 2016);

        $this->assertEquals($nextAndPrev['previousMonth'], 11);
        $this->assertEquals($nextAndPrev['previousMonthString'], 'November');

        $this->assertEquals($nextAndPrev['nextMonth'], 1);
        $this->assertEquals($nextAndPrev['nextMonthString'], 'January');

        $this->assertEquals($nextAndPrev['nextYear'], 2017);
        $this->assertEquals($nextAndPrev['previousYear'], 2016);

        $nextAndPrev = $this->calendarGenerator->processMonths(1, 2016);

        $this->assertEquals($nextAndPrev['previousYear'], 2015);

        $this->assertEquals($nextAndPrev['previousMonth'], 12);
        $this->assertEquals($nextAndPrev['previousMonthString'], 'December');
    }

    public function testGetCalendar()
    {
        $guild = $this->em->getRepository('RizzenGuildBundle:Guild')->find(1);

        $calendar = $this->calendarGenerator->getCalendar(2016, 11, $guild);

        $this->assertEquals($calendar['currentMonth'], 11);
        $this->assertEquals($calendar['currentYear'], 2016);
        $this->assertEquals($calendar['currentDay'], null);
        $this->assertEquals($calendar['currentMonthString'], 'November');
    }

}
