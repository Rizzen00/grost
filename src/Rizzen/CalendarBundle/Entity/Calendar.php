<?php

namespace Rizzen\CalendarBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Calendar
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Rizzen\CalendarBundle\Entity\CalendarRepository")
 */
class Calendar
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="Event", mappedBy="calendar", cascade={"persist"})
     */
    protected $events;

    /**
     * @ORM\OneToOne(targetEntity="Rizzen\GuildBundle\Entity\Guild", inversedBy="calendar")
     */
    private $guild;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add events
     *
     * @param Event $events
     * @return Calendar
     */
    public function addEvent(Event $events)
    {
        $this->events[] = $events;

        return $this;
    }

    /**
     * Remove events
     *
     * @param Event $events
     */
    public function removeEvent(Event $events)
    {
        $this->events->removeElement($events);
    }

    /**
     * Get events
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEvents()
    {
        return $this->events;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->events = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Set guild
     *
     * @param \Rizzen\GuildBundle\Entity\Guild $guild
     * @return Calendar
     */
    public function setGuild(\Rizzen\GuildBundle\Entity\Guild $guild = null)
    {
        $this->guild = $guild;

        return $this;
    }

    /**
     * Get guild
     *
     * @return \Rizzen\GuildBundle\Entity\Guild 
     */
    public function getGuild()
    {
        return $this->guild;
    }
}
