<?php

namespace Rizzen\CalendarBundle\Entity;

use Doctrine\ORM\EntityRepository;

class EventCommentRepository extends EntityRepository
{

    public function getCommentsOfEvent($event)
    {
        $qb = $this->createQueryBuilder('r');
        $qb->where('r.target = :event')
            ->setParameter('event', $event)
            ->andWhere('r.visible = :visible')
            ->setParameter('visible', true)
            ->andWhere('r.isResponse = :isResponse')
            ->setParameter('isResponse', false);

        return $qb->getQuery()->getResult();
    }

} 
