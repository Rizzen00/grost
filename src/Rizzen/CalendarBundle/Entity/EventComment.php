<?php

namespace Rizzen\CalendarBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * EventComment
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="EventCommentRepository")
 */
class EventComment
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetimetz")
     */
    private $date;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text")
     */
    private $content;

    /**
     * @var boolean
     *
     * @ORM\Column(name="visible", type="boolean")
     */
    private $visible;

    /**
     * @ORM\ManyToOne(targetEntity="Event")
     * @ORM\JoinColumn(nullable=false)
     */
    private $target;

    /**
     * @ORM\ManyToOne(targetEntity="Rizzen\UserBundle\Entity\User")
     * @ORM\JoinColumn(nullable=false)
     */
    private $sender;

    /**
     * @var boolean
     *
     * @ORM\Column(name="isResponse", type="boolean")
     */
    private $isResponse;

    /**
     * @ORM\OneToMany(targetEntity="EventComment", mappedBy="parent")
     **/
    private $responses;

    /**
     * @ORM\ManyToOne(targetEntity="EventComment", inversedBy="responses")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id")
     */
    private $parent;

    private $responseTarget;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return EventComment
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set content
     *
     * @param string $content
     * @return EventComment
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    public function setResponseTarget($responseTarget)
    {
        $this->responseTarget = $responseTarget;

        return $this;
    }

    public function getResponseTarget()
    {
        return $this->responseTarget;
    }

    /**
     * Set visible
     *
     * @param boolean $visible
     * @return EventComment
     */
    public function setVisible($visible)
    {
        $this->visible = $visible;

        return $this;
    }

    /**
     * Get visible
     *
     * @return boolean
     */
    public function getVisible()
    {
        return $this->visible;
    }

    /**
     * Set isResponse
     *
     * @param boolean $isResponse
     * @return EventComment
     */
    public function setResponse($isResponse)
    {
        $this->isResponse = $isResponse;

        return $this;
    }

    /**
     * Get isResponse
     *
     * @return boolean
     */
    public function isResponse()
    {
        return $this->isResponse;
    }

    /**
     * Set target
     *
     * @param Event $target
     * @return EventComment
     */
    public function setTarget(Event $target)
    {
        $this->target = $target;

        return $this;
    }

    /**
     * Get target
     *
     * @return Event
     */
    public function getTarget()
    {
        return $this->target;
    }

    /**
     * Set sender
     *
     * @param \Rizzen\UserBundle\Entity\User $sender
     * @return EventComment
     */
    public function setSender(\Rizzen\UserBundle\Entity\User $sender)
    {
        $this->sender = $sender;

        return $this;
    }

    /**
     * Get sender
     *
     * @return \Rizzen\UserBundle\Entity\User
     */
    public function getSender()
    {
        return $this->sender;
    }

    /**
     * Add responses
     *
     * @param EventComment $responses
     * @return EventComment
     */
    public function addResponse(EventComment $responses)
    {
        $this->responses[] = $responses;

        return $this;
    }

    /**
     * Remove responses
     *
     * @param EventComment $responses
     */
    public function removeResponse(EventComment $responses)
    {
        $this->responses->removeElement($responses);
    }

    /**
     * Get responses
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getResponses()
    {
        return $this->responses;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->visible = true;
        $this->date = new \DateTime('now');
        $this->responses = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Set parent
     *
     * @param EventComment $parent
     * @return EventComment
     */
    public function setParent(EventComment $parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return EventComment
     */
    public function getParent()
    {
        return $this->parent;
    }
}
