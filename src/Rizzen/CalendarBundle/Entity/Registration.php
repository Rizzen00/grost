<?php

namespace Rizzen\CalendarBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Registration
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="RegistrationRepository")
 */
class Registration
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="ipAddress", type="cidr", nullable=true)
     */
    private $ipAddress;

    /**
     * @ORM\ManyToOne(targetEntity="Event", inversedBy="registrations")
     */
    protected $event;

    /**
     * @var string
     *
     * @ORM\Column(name="month", type="integer")
     */
    protected $month;

    /**
     * @var string
     *
     * @ORM\Column(name="dateId", type="integer")
     */
    protected $dateId;

    /**
     * @ORM\ManyToOne(targetEntity="Rizzen\UserBundle\Entity\User")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetimetz")
     */
    private $date;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="eventDate", type="datetimetz")
     */
    private $eventDate;

    /**
     * @var boolean
     *
     * @ORM\Column(name="invited", type="boolean")
     */
    private $invited;

    /**
     * @var boolean
     *
     * @ORM\Column(name="reminded", type="boolean")
     */
    private $reminded;

    /**
     * @var boolean
     *
     * @ORM\Column(name="registrationCancelled", type="boolean")
     */
    private $registrationCancelled;

    /**
     * @var boolean
     * @var boolean
     *
     * @ORM\Column(name="modified", type="boolean")
     */
    private $modified;

    /**
     * @var integer
     *
     * @ORM\Column(name="state", type="integer")
     */
    private $state;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="lastModificationDate", type="datetime", nullable=true)
     */
    private $lastModificationDate;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return Registration
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set state
     *
     * @param integer $state
     * @return Registration
     */
    public function setState($state)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Get state
     *
     * @return integer
     */
    public function getState()
    {
        return $this->state;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->date = new \Datetime('now');
        $this->state = 1;
        $this->modified = false;
        $this->invited = false;
        $this->registrationCancelled = false;
        $this->users = new \Doctrine\Common\Collections\ArrayCollection();
        $this->reminded = false;
    }

    /**
     * Set user
     *
     * @param \Rizzen\UserBundle\Entity\User $user
     * @return Registration
     */
    public function setUser(\Rizzen\UserBundle\Entity\User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Rizzen\UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set event
     *
     * @param Event $event
     * @return Registration
     */
    public function setEvent(Event $event = null)
    {
        $this->event = $event;
        $this->setEventDate($event->getDate());

        return $this;
    }

    /**
     * Get event
     *
     * @return Event
     */
    public function getEvent()
    {
        return $this->event;
    }

    /**
     * Set lastModificationDate
     *
     * @param \DateTime $lastModificationDate
     * @return Registration
     */
    public function setLastModificationDate($lastModificationDate)
    {
        $this->lastModificationDate = $lastModificationDate;
    
        return $this;
    }

    /**
     * Get lastModificationDate
     *
     * @return \DateTime 
     */
    public function getLastModificationDate()
    {
        return $this->lastModificationDate;
    }

    /**
     * Set ipAddress
     *
     * @param string $ipAddress
     * @return Registration
     */
    public function setIpAddress($ipAddress)
    {
        $this->ipAddress = $ipAddress;

        return $this;
    }

    /**
     * Get ipAddress
     *
     * @return string 
     */
    public function getIpAddress()
    {
        return $this->ipAddress;
    }

    /**
     * Set month
     *
     * @param integer $month
     * @return Registration
     */
    public function setMonth($month)
    {
        $this->month = $month;

        return $this;
    }

    /**
     * Get month
     *
     * @return integer 
     */
    public function getMonth()
    {
        return $this->month;
    }

    /**
     * Set dateId
     *
     * @param integer $dateId
     * @return Registration
     */
    public function setDateId($dateId)
    {
        $this->dateId = $dateId;

        return $this;
    }

    /**
     * Get dateId
     *
     * @return integer 
     */
    public function getDateId()
    {
        return $this->dateId;
    }

    /**
     * Set invited
     *
     * @param boolean $invited
     * @return Registration
     */
    public function setInvited($invited)
    {
        $this->invited = $invited;

        return $this;
    }

    /**
     * Get invited
     *
     * @return boolean 
     */
    public function isInvited()
    {
        return $this->invited;
    }

    /**
     * Set modified
     *
     * @param boolean $modified
     * @return Registration
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return boolean 
     */
    public function isModified()
    {
        return $this->modified;
    }

    /**
     * Set registrationCancelled
     *
     * @param boolean $registrationCancelled
     * @return Registration
     */
    public function setRegistrationCancelled($registrationCancelled)
    {
        $this->registrationCancelled = $registrationCancelled;

        return $this;
    }

    /**
     * Get registrationCancelled
     *
     * @return boolean 
     */
    public function getRegistrationCancelled()
    {
        return $this->registrationCancelled;
    }

    /**
     * Set eventDate
     *
     * @param \DateTime $eventDate
     * @return Registration
     */
    public function setEventDate($eventDate)
    {
        $this->eventDate = $eventDate;

        return $this;
    }

    /**
     * Get eventDate
     *
     * @return \DateTime 
     */
    public function getEventDate()
    {
        return $this->eventDate;
    }

    /**
     * Set reminded
     *
     * @param boolean $reminded
     *
     * @return Registration
     */
    public function setReminded($reminded)
    {
        $this->reminded = $reminded;

        return $this;
    }

    /**
     * Get reminded
     *
     * @return boolean
     */
    public function isReminded()
    {
        return $this->reminded;
    }
}
