#Events Documentation

----------

###Registrations states

Registrations can have different states, some states are persisted in the database, some are only used internally to perform actions on a registration.

####State list:
	

 - **State 1: Pending** *[Persisted]*
	 -  State **1** is the **"Pending"** state, used when an user register manually to an event, or when a registration reset occur.
	 - The event manager can decide to accept or refuse the registration when in this state.
 - **State 2: Accepted** *[Persisted]*
	 - State **2** is the **"Accepted"** state, used when a registration or an invitation is accepted.
	 - Event managers can modify this registration to **Refused**, if refused by an Event Manager, the registration won't be modifiable by the user registered.
	 - If **State 2** is the result of an accepted invitation, the user invited will be able to modify his registration as long as an Event Manager has not refused the registration.
 - **State 3: Refused** *[Persisted]*
	 - State **3** is the **"Refused"** state, used when a registration is refused or cancelled.
	 - If the registration was cancelled, only the user that cancelled it can reset his registration and go back to the pending state (state 1).
	 - If the registration was refused by an event manager, it can be modified by them at anytime.
 - **State 4: Invited** *[Persisted]*
	 - State **4** is the **"Invited"** state, used when an user is invited.
	 - When invited, only the user that is invited can decide if he accept or refuse the invitation.
	 - As long as no Event Manager refuse the registration once the user accepted the invitation, the invited user can modify his registration to refused or accepted as many time as he want.
	 - The registration is only editable by Event Managers if the user accept the invitation.
 - **State 5: Cancelling**
	 - State **5** is used internally to cancel an event, only an invited registration (state 4) cannot be cancelled.
 - **State 6: Reset**
	 - State **6** is used internally when a registration is getting reset, only a refused (state 3) registration can be reset. 
