<?php

namespace Rizzen\CalendarBundle\Services;

use Doctrine\ORM\EntityManager;
use Rizzen\GuildBundle\Entity\Guild;
use Rizzen\CalendarBundle\Entity\Registration;
use Rizzen\GuildBundle\Permissions\PermissionsManager;
use Rizzen\UserBundle\Entity\User;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Rizzen\CalendarBundle\Constants\RegistrationStates;

class EventViewHelper implements EventViewHelperInterface
{
    protected $em;
    protected $permissionsManager;
    protected $session;

    /**
     * @param EntityManager $em
     * @param PermissionsManager $permissionsManager
     * @param Session $session
     */
    public function __construct(EntityManager $em, PermissionsManager $permissionsManager, Session $session)
    {
        $this->em = $em;
        $this->permissionsManager = $permissionsManager;
        $this->session = $session;
    }

    /**
     * {@inheritdoc}
     */
    public function getEventViewVariables(Guild $guild, User $user, $eventSlug)
    {
        $event = $this->em->getRepository('RizzenCalendarBundle:Event')->getEventOfSelectedDay($guild->getCalendar(), $eventSlug);

        $this->permissionsManager->setUser($user)->setGuild($guild);

        if ($event === null) {
            throw new NotFoundHttpException('The requested even was not found');
        }

        $eventInformation['permissions'] = $this->permissionsManager->getPermissions();
        $eventInformation['permissionsJSON'] = $this->permissionsManager->getJSONPermissions();
        $eventInformation['registrations'] = $event->getRegistrations();
        $eventInformation['event'] = $event;
        $eventInformation['pendingCount'] = $eventInformation['acceptedCount'] = $eventInformation['refusedCount'] = $eventInformation['invitedCount'] = 0;
        $eventInformation['registeredCount'] = count($eventInformation['registrations']);
        $eventInformation['currentUserState'] = null;
        $eventInformation['isCurrentUserInEvent'] = false;
        $eventInformation['isCurrentUserInvited'] = false;
        $eventInformation['currentUserCancelled'] = false;
        $eventInformation['modified'] = false;
        $eventInformation['guild'] = $guild;

        /** @var Registration $registeredUser */
        foreach ($eventInformation['registrations'] as $registeredUser) {

            switch ($registeredUser->getState()) {
                case RegistrationStates::PENDING:
                    $eventInformation['pendingCount']++;
                    break;
                case RegistrationStates::ACCEPTED:
                    $eventInformation['acceptedCount']++;
                    break;
                case RegistrationStates::REFUSED:
                    $eventInformation['refusedCount']++;
                    break;
                case RegistrationStates::INVITED:
                    $eventInformation['invitedCount']++;
                    break;
            }

            if ($registeredUser->getUser() === $user) {
                $eventInformation['isCurrentUserInEvent'] = true;
                $eventInformation['currentUserState'] = $registeredUser->getState();
                $eventInformation['currentUserCancelled'] = $registeredUser->getRegistrationCancelled();

                if ($registeredUser->isInvited() === true) {
                    $eventInformation['isCurrentUserInvited'] = true;
                }

                if ($registeredUser->isModified()) {
                    $eventInformation['modified'] = true;
                }
            }

        }

        if (!$this->permissionsManager->isGuildMember(false) && $eventInformation['isCurrentUserInEvent'] === false ||
            $this->permissionsManager->getPermissions()->isBannedFromGuild() === true) {
            throw new NotFoundHttpException('The requested even was not found');
        }

        $eventInformation['currentDate'] = new \DateTime('now');

        return $eventInformation;
    }
}

