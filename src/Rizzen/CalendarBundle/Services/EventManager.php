<?php

namespace Rizzen\CalendarBundle\Services;

use Doctrine\ORM\EntityManager;
use Rizzen\CalendarBundle\Entity\Event;
use Rizzen\CalendarBundle\Entity\Registration;
use Rizzen\GuildBundle\Entity\Guild;
use Rizzen\GuildBundle\Permissions\PermissionsManager;
use Rizzen\MessengerBundle\Services\Bot\Bot;
use Rizzen\SocketBundle\Services\SocketProcessor;
use Rizzen\UserBundle\Entity\User;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Rizzen\CalendarBundle\Constants\RegistrationStates;

class EventManager implements EventManagerInterface
{

    protected $em;
    protected $socketProcessor;
    protected $permissionsManager;
    protected $messengerBot;

    /**
     * @param EntityManager $em
     * @param SocketProcessor $socketProcessor
     * @param PermissionsManager $permissionsManager
     * @param Bot $messengerBot
     */
    public function __construct(EntityManager $em, SocketProcessor $socketProcessor, PermissionsManager $permissionsManager, Bot $messengerBot)
    {
        $this->em = $em;
        $this->socketProcessor = $socketProcessor;
        $this->permissionsManager = $permissionsManager;
        $this->messengerBot = $messengerBot;
    }

    /**
     * {@inheritdoc}
     */
    public function updateUserState(Registration $registration, Guild $guild, User $currentUser, $state, $newRegistration = false, $isInvite = false)
    {
        $this->canUpdateRegistrationState($registration, $guild, $currentUser, $state, $newRegistration);

        // We need those to show the right controls on the front-end, set them to false so we don't need to check if they're set in javascript
        $additionalInfo['registrationCancelled'] = false;
        $additionalInfo['registrationReset'] = false;
        $additionalInfo['isInvite'] = $isInvite;

        $eventSwitchWSInfo['stateFrom'] = $registration->getState();
        if ($state === $eventSwitchWSInfo['stateFrom'] && $state !== RegistrationStates::INTERNAL_CANCELLING && $newRegistration === false) {
            return;
        }

        if ($state === RegistrationStates::INTERNAL_CANCELLING) {
            $state = RegistrationStates::REFUSED;
            $registration->setRegistrationCancelled(true);
            $additionalInfo['registrationCancelled'] = true;

        } elseif ($state === RegistrationStates::INTERNAL_RESET) {
            $additionalInfo['registrationReset'] = true;
            $state = RegistrationStates::PENDING;
            $registration->setRegistrationCancelled(false);

        }

        // For the front-end, the user that own this registration we're modifying
        $additionalInfo['modified'] = $registration->isModified();
        $additionalInfo['userModified'] = $registration->getUser()->getUsername();

        if (!$newRegistration) {

            $additionalInfo['modified'] = true;
            $registration->setState($state);
            $registration->setModified(true);

            $registration->setLastModificationDate(new \DateTime("now"));
            $this->em->flush();
        }

        $eventSwitchWSInfo['registration'] = $registration;
        $eventSwitchWSInfo['stateTo'] = $state;

        $this->socketProcessor->addTopic($registration->getEvent()->getSocketTopic());

        if ($newRegistration) {
            $this->socketProcessor
                ->setSubtopic('registrationNewUser')
                ->setAdditionalInfo($additionalInfo)
                ->useTemplate('eventStateSwitch', $eventSwitchWSInfo);
        } else {
            $this->socketProcessor
                ->setSubtopic('registrationStateSwitch')
                ->setAdditionalInfo($additionalInfo)
                ->useTemplate('eventStateSwitch', $eventSwitchWSInfo);
        }
        $this->socketProcessor->broadcast();
    }

    /**
     * Check if the current user can update the state of the registration
     *
     * @param Registration $registration    Registration to modify
     * @param Guild $guild                  Guild of the event
     * @param User $currentUser             Current user
     * @param int $state                    New state
     * @param bool $newRegistration         Is it a new registration
     * @throws \Exception
     */
    private function canUpdateRegistrationState(Registration $registration, Guild $guild, User $currentUser, $state, $newRegistration)
    {
        if ($registration === null || $registration->getEvent()->getCalendar()->getGuild() !== $guild) {
            throw new NotFoundHttpException('Registration not found');
        }

        $canProceed = false;
        $this->permissionsManager->setGuild($guild)->setUser($currentUser);

        if ($this->permissionsManager->can(['CanManageEvent', 'CanCreateEvent'])) {

            // The user has been invited, and is accepting or refusing the event
            if ($registration->isModified() === false && $registration->isInvited()) {

                if ($registration->getUser() === $currentUser) {
                    if ($state === RegistrationStates::ACCEPTED || $state === RegistrationStates::REFUSED) {
                        $canProceed = true;
                    }
                } elseif ($registration->getState() === RegistrationStates::ACCEPTED || $newRegistration) {
                    $canProceed = true;
                }

            } elseif ($registration->isInvited() === false  || $registration->isModified()) {
                $canProceed = true;
            }
        } elseif ($registration->isModified() === false &&
                  $registration->isInvited() &&
                  $registration->getUser() === $currentUser &&
                  ($state === RegistrationStates::ACCEPTED || $state === RegistrationStates::REFUSED)) {
            $canProceed = true;
        } elseif ($newRegistration) {
            $canProceed = true;
        }

        if ($state === RegistrationStates::INTERNAL_CANCELLING || $state === RegistrationStates::INTERNAL_RESET) {
            $canProceed = true;
        }

        // Registration canceled, the used isn't trying to reset it so don't let him change it.
        if ($registration->getRegistrationCancelled() === true && $state !== RegistrationStates::INTERNAL_RESET) {
            $canProceed = false;
        }

        if ($canProceed === false) {
            throw new \Exception('You cannot modify this registration');
        }
    }

    /**
     * {@inheritdoc}
     */
    public function inviteResult(Registration $registration, $choice, User $currentUser)
    {
        if ($registration === null || $registration->isModified() || $registration->getUser() !== $currentUser || $registration->isInvited() === false) {
            throw new \Exception('Unable to modify the registration state');
        }

        $state = $choice === 'accept'
            ? RegistrationStates::ACCEPTED
            : RegistrationStates::INTERNAL_CANCELLING;

        $this->updateUserState($registration, $registration->getEvent()->getCalendar()->getGuild(), $currentUser, $state, false, true);
    }

    /**
     * {@inheritdoc}
     */
    public function inviteUser($username, $dateId, $eventSlug, Guild $guild, User $currentUser)
    {
        $usernameLength = strlen($username);
        if ($usernameLength < 3 || $usernameLength > 15) {
            return [
                'hasBeenInvited' => false,
                'reason' => 'The username must contain at least 3 characters and a maximum of 15'
            ];
        }

        /** @var User $user */
        $user = $this->em->getRepository('RizzenUserBundle:User')->findOneByUsername($username);
        if ($user === null) {
            return [
                'hasBeenInvited' => false,
                'reason' => 'User not found'
            ];
        }

        $this->permissionsManager->setGuild($guild)->setUser($currentUser);

        if (!$this->permissionsManager->can(['CanManageEvent'])) {
            return [
                'hasBeenInvited' => false,
                'reason' => 'Insufficient permissions, unable to perform the action'
            ];
        }

        /** @var Event $event */
        $event = $this->em->getRepository('RizzenCalendarBundle:Event')->findOneBySlug($eventSlug);
        if ($event === null) {
            return [
                'hasBeenInvited' => false,
                'reason' => 'Event not found'
            ];
        }

        /** @var Registration $registration */
        foreach ($event->getRegistrations() as $registration) {
            if ($registration->getUser() === $user) {
                return [
                    'hasBeenInvited' => false,
                    'reason' => 'The user is already registered or invited to this event'
                ];
            }
        }

        $registration = new Registration();

        $registration->setEvent($event);
        $registration->setUser($user);
        $registration->setInvited(true);
        $registration->setMonth($event->getMonth());
        $registration->setDateId($dateId);
        $registration->setState(RegistrationStates::INVITED);
        $event->addRegistration($registration);

        $this->em->persist($registration);
        $this->em->flush();

        $this->updateUserState($registration, $guild, $currentUser, RegistrationStates::INVITED, true, true);

        if ($user->getOptions()->messenger()->getReceiveNotificationForEventInvite()) {
            $this->messengerBot
                ->setRecipient($user)
                ->setTitle($currentUser->getUsername() . ' has invited you to an event')
                ->useTemplate('RizzenCalendarBundle:Message:eventInvite.html.twig', [
                    'event' => $event,
                    'invitedBy' => $currentUser->getUsername(),
                    'dateId' => $dateId,
                    'subdomain' => $event->getCalendar()->getGuild()->getSlug()
                ])
                ->postConversation();
        }

        return [
            'hasBeenInvited' => true,
            'reason' => 'The user has been successfully invited!'
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function register(Guild $guild, User $user, Request $request, $dateId, $eventSlug)
    {
        $calendarEvent = $this->em->getRepository('RizzenCalendarBundle:Event')->getEventOfSelectedDay($guild->getCalendar(), $eventSlug);

        if ($calendarEvent->isOpen() === false || $calendarEvent === null) {
            throw new \Exception("This event is closed or does not exit");
        }

        // Check that the user is not already registered to this event
        /** @var Registration $registeredUser */
        foreach ($calendarEvent->getRegistrations() as $registeredUser) {
            if ($registeredUser->getUser() === $user) {
                throw new \Exception("You're already in this event");
            }
        }

        // check if the user has access to the guild that created this event, will throw an exception if not
        $this->permissionsManager->setGuild($guild)->setUser($user);
        $this->permissionsManager->isGuildMember();

        $registration = new Registration();

        $registration->setEvent($calendarEvent);
        $registration->setIpAddress($request->getClientIp());
        $registration->setUser($user);
        $registration->setMonth($calendarEvent->getMonth());
        $registration->setDateId($dateId);
        $calendarEvent->addRegistration($registration);

        $this->em->persist($registration);
        $this->em->flush();

        $this->updateUserState($registration, $guild, $user, RegistrationStates::PENDING, true);
    }

    /**
     * {@inheritdoc}
     */
    public function cancelRegistration(Registration $registration, User $user)
    {
        if ($registration->getUser() !== $user) {
            throw new \Exception('Unable to cancel the registration of another user');
        }

        if ($registration->getState() === RegistrationStates::INVITED) {
            throw new \Exception('Only the accepted, pending or refused registrations can be cancelled');
        }

        $guild = $registration->getEvent()->getCalendar()->getGuild();

        $this->updateUserState($registration, $guild, $user, RegistrationStates::INTERNAL_CANCELLING);
    }

    /**
     * {@inheritdoc}
     */
    public function resetRegistration(Registration $registration, User $user)
    {
        if ($registration->getUser() !== $user) {
            throw new \Exception('Unable to reset the registration of another user');
        }

        if ($registration->getRegistrationCancelled() === false) {
            throw new \Exception('Unable to reset a registration that is not cancelled');
        }

        $guild = $registration->getEvent()->getCalendar()->getGuild();

        $this->updateUserState($registration, $guild, $user, RegistrationStates::INTERNAL_RESET);
    }

    /**
     * {@inheritdoc}
     */
    public function toggleAccess(Guild $guild, $dateId, $eventSlug)
    {
        $event = $this->em->getRepository('RizzenCalendarBundle:Event')->getEventOfSelectedDay($guild->getCalendar(), $eventSlug);
        if ($event === null) {
            throw new NotFoundHttpException('Event not found');
        }

        // Toggle the access
        $event->setOpen(!$event->getOpen());
        $this->em->flush();

        $this->socketProcessor
            ->addTopic($event->getSocketTopic())
            ->setSubtopic('eventRegistrationAccessToggle')
            ->setAdditionalInfo(['isOpened' => $event->getOpen()])
            ->useTemplate('raw', null)
            ->broadcast();
    }
} 
