<?php

namespace Rizzen\CalendarBundle\Services;


use Doctrine\ORM\EntityManager;
use Rizzen\CalendarBundle\Entity\Event;
use Rizzen\GuildBundle\Entity\Guild;
use Rizzen\MainBundle\Services\Purifier;
use Rizzen\MainBundle\Services\Slugify;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class EventPoster implements EventPosterInterface
{
    protected $em;
    protected $slugify;
    protected $purifier;

    /**
     * @param EntityManager $em
     * @param Purifier $purifier
     */
    public function __construct(EntityManager $em, Purifier $purifier)
    {
        $this->em = $em;
        $this->slugify = new Slugify();
        $this->purifier = $purifier;
    }

    /**
     * {@inheritdoc}
     */
    public function processNewEventPost(Guild $guild, Event $event, $dateId)
    {
        $day = substr($dateId, 0, 2);
        $month = substr($dateId, 2, 2);
        $year = substr($dateId, 4, 4);
        $currentYear = date('Y');

        if ($day > 31 || $month > 12 || $year < ($currentYear - 1) || $year > ($currentYear + 1)) {
            throw new NotFoundHttpException('Page not found: Date invalid');
        }

        $event->setMonth($month);
        $event->setYear($year);

        if ($event->getDescription() !== null) {
            $event->setDescription($this->purifier->purify('default', $event->getDescription()));
        }

        $event->setCalendar($guild->getCalendar());

        $dateOfEvent = new \DateTime();
        $dateOfEvent->setTimestamp(mktime($event->getHour(), $event->getMinute(), 0, $month, $day, $year));
        $dateOfEvent->setTimezone(new \DateTimeZone('UTC'));

        $event->setDate($dateOfEvent);

        $event->setDateid($dateId);
        $this->em->persist($event);
        $this->em->flush();

        $eventId = $event->getId();
        $slug = $this->slugify->getSluged($event->getTitle().'-'.$eventId);
        $event->setSlug($slug);
        $this->em->persist($event);
        $this->em->flush();

        return $slug;
    }

    /**
     * {@inheritdoc}
     */
    public function processEditEventPost(Event $event, $dateId)
    {
        $day = substr($dateId, 0,2);
        $month = substr($dateId, 2,2);
        $year = substr($dateId, 4,4);

        $date = mktime($event->getHour(), $event->getMinute(), 0, $month, $day, $year);

        $dateOfEvent = new \DateTime();
        $dateOfEvent->setTimestamp($date);
        $dateOfEvent->setTimezone(new \DateTimeZone('UTC'));

        $event->setDate($dateOfEvent);
        $event->setDescription($this->purifier->purify('default', $event->getDescription()));

        $this->em->persist($event);
        $this->em->flush();
    }
} 
