<?php

namespace Rizzen\CalendarBundle\Services;


use Rizzen\GuildBundle\Entity\Guild;
use Rizzen\UserBundle\Entity\User;

interface EventViewHelperInterface
{

    /**
     * Return all the data necessary to make the view and throw exceptions if the user cannot access the event
     *
     * @param Guild $guild              Guild where the event is located
     * @param User $user                Current user
     * @param string $eventSlug         Slug of the event
     * @return Array                    Array containing all the data necessary to make the view
     */
    public function getEventViewVariables(Guild $guild, User $user, $eventSlug);
} 
