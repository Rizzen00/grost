<?php

namespace Rizzen\CalendarBundle\Services\CalendarGenerator;


use Rizzen\GuildBundle\Entity\Guild;
use Rizzen\CalendarBundle\Entity\Calendar;
use Rizzen\UserBundle\Entity\User;

interface CalendarGeneratorInterface {

    /**
     * Return all the param needed to generate the calendar in the view
     *
     * @param int $year
     * @param int $month
     * @param Guild $guild
     * @return array
     */
    public function getCalendar($year, $month, Guild $guild);

    /**
     * Verify that the year is correct
     *
     * @param Calendar $calendar
     * @param \DateTime $startOfMonth
     * @param \DateTime $endOfMonth
     * @return array
     */
    public function getEvents(Calendar $calendar, \DateTime $startOfMonth, \DateTime $endOfMonth);

    /**
     * Get the registrations of the current month, used if the user is accessing his own calendar
     *
     * @param User $user
     * @param \DateTime $startOfMonth
     * @param \DateTime $endOfMonth
     * @return mixed
     */
    public function getOwnRegistrations(User $user, \DateTime $startOfMonth, \DateTime $endOfMonth);

    /**
     * Verify that the year is correct
     *
     * @param int $year
     * @param \DateTime $currentDate
     * @return mixed
     */
    public function verifyYear($year, $currentDate);

    /**
     * Verify that the year is correct
     * @param int $month
     * @param \DateTime $currentDate
     * @return mixed
     */
    public function verifyMonth($month, $currentDate);

    /**
     * Process the blanks at the start of the month
     *
     * @param string $dayOfWeek
     * @return mixed
     */
    public function processBlanks($dayOfWeek);

    /**
     * Process the months to put in the pagination
     *
     * @param int $month
     * @param int $year
     * @return mixed
     */
    public function processMonths($month, $year);

} 
