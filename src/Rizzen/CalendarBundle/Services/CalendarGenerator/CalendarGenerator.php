<?php

namespace Rizzen\CalendarBundle\Services\CalendarGenerator;

use Doctrine\ORM\EntityManagerInterface;
use Rizzen\CalendarBundle\Entity\Calendar;
use Rizzen\GuildBundle\Entity\Guild;
use Rizzen\UserBundle\Entity\User;

class CalendarGenerator implements CalendarGeneratorInterface
{

    private $em;

    /**
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * {@inheritdoc}
     */
    public function getCalendar($year, $month, Guild $guild = null, User $user = null)
    {
        $currentDate = new \Datetime('now');
        $year = $this->verifyYear($year, $currentDate);
        $month = $this->verifyMonth($month, $currentDate);

        $startDate = new \Datetime('01-' . $month . '-' . $year);
        $month = $startDate->format('m');
        $year = $startDate->format('Y');

        if ($month == $currentDate->format('m') && $year == $currentDate->format('Y')) {
            $currentDay = $currentDate->format('d');
        } else {
            $currentDay = null;
        }

        $daysInMonth = cal_days_in_month(0, $month, $year);
        $calendar['blanks'] = $this->processBlanks($startDate->format('D'));

        for ($i = 1; $i <= $daysInMonth; $i++) { // put all the days in an array
            $calendar['days'][] = $i;
        }

        $nextAndPreviousMonth = $this->processMonths($month, $year);

        $calendar['currentMonth'] = $month;
        $calendar['currentYear'] = $year;
        $calendar['currentDay'] = $currentDay;
        $calendar['currentMonthString'] = $startDate->format('F');
        $calendar['nextAndPreviousMonth'] = $nextAndPreviousMonth;

        $endOfMonth = new \Datetime($daysInMonth . '-' . $month . '-' . $year);

        if ($guild !== null) {
            $calendar['events'] = $this->getEvents($guild->getCalendar(), $startDate, $endOfMonth);
        } else {
            $calendar['registrations'] = $this->getOwnRegistrations($user, $startDate, $endOfMonth);
        }

        return $calendar;
    }

    /**
     * {@inheritdoc}
     */
    public function getEvents(Calendar $calendar, \DateTime $startOfMonth, \DateTime $endOfMonth)
    {
        $endOfMonth->add(new \DateInterval('PT86399S')); // add 24 hours minus one second
        $events = $this->em->getRepository('RizzenCalendarBundle:Event')->getEventsOfTheMonth($calendar, $startOfMonth, $endOfMonth);

        return $events;
    }

    /**
     * {@inheritdoc}
     */
    public function getOwnRegistrations(User $user, \DateTime $startOfMonth, \DateTime $endOfMonth)
    {
        $endOfMonth->add(new \DateInterval('PT86399S')); // add 24 hours minus one second
        return $this->em->getRepository('RizzenCalendarBundle:Registration')->getUserRegistrationsOfTheMonth($user, $startOfMonth, $endOfMonth);
    }

    /**
     * {@inheritdoc}
     */
    public function verifyYear($year, $currentDate)
    {
        $currentYear = date('Y');

        if (!isset($year) || $year === null || strlen($year) !== 4 || $year < ($currentYear - 1) || $year > ($currentYear + 1)) {
            $year = $currentDate->format('Y');
        }
        return $year;
    }

    /**
     * {@inheritdoc}
     */
    public function verifyMonth($month, $currentDate)
    {
        if (!isset($month) || $month === null || strlen($month) !== 2 || $month > 12 || $month < 01 ) {
            $month = $currentDate->format('m');
        }
        return $month;
    }

    /**
     * {@inheritdoc}
     */
    public function processBlanks($dayOfWeek)
    {
        $blanks = 0;
        switch ($dayOfWeek) {
            case 'Tue': $blanks = 1; break;
            case 'Wed': $blanks = 2; break;
            case 'Thu': $blanks = 3; break;
            case 'Fri': $blanks = 4; break;
            case 'Sat': $blanks = 5; break;
            case 'Sun': $blanks = 6; break;
        }
        return $blanks;
    }

    /**
     * {@inheritdoc}
     */
    public function processMonths($month, $year)
    {
        if ($month == 01) {
            $previousMonth = '12';
            $prevYear = $year - 1;
        } else {
            $previousMonth = $month - 1;
            $prevYear = $year;
            if ($previousMonth <= 9) {
                $previousMonth = '0' . $previousMonth;
            }
        }

        if ($month == 12) {
            $nextMonth = '01';
            $nextYear = $year + 1;
        } else {
            $nextMonth = $month + 1;
            $nextYear = $year;
            if ($nextMonth <= 9)
            {
                $nextMonth = '0' . $nextMonth;
            }
        }

        switch ($month) {
            case '01': $previousMonthString = 'December'; $nextMonthString = 'February'; break;
            case '02': $previousMonthString = 'January'; $nextMonthString = 'March'; break;
            case '03': $previousMonthString = 'February'; $nextMonthString = 'April'; break;
            case '04': $previousMonthString = 'March'; $nextMonthString = 'May';break;
            case '05': $previousMonthString = 'April'; $nextMonthString = 'June'; break;
            case '06': $previousMonthString = 'May'; $nextMonthString = 'July'; break;
            case '07': $previousMonthString = 'June'; $nextMonthString = 'August'; break;
            case '08': $previousMonthString = 'July'; $nextMonthString = 'September'; break;
            case '09': $previousMonthString = 'August'; $nextMonthString = 'October'; break;
            case '10': $previousMonthString = 'September'; $nextMonthString = 'November'; break;
            case '11': $previousMonthString = 'October'; $nextMonthString = 'December'; break;
            case '12': $previousMonthString = 'November'; $nextMonthString = 'January'; break;
        }

        return [
            'previousMonth' => $previousMonth,
            'nextMonth' => $nextMonth,
            'previousMonthString' => $previousMonthString,
            'nextMonthString' => $nextMonthString,
            'previousYear' => $prevYear,
            'nextYear' => $nextYear
        ];
    }
}
