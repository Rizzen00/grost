<?php

namespace Rizzen\CalendarBundle\Services;

use Rizzen\CalendarBundle\Entity\Registration;
use Rizzen\GuildBundle\Entity\Guild;
use Rizzen\UserBundle\Entity\User;
use Symfony\Component\HttpFoundation\Request;

interface EventManagerInterface {

    /**
     * Update the state of a registration and send a message over to every client subscribed to the websocket topic
     * This method use the event state system, which is described in detain in the events documentation of this bundle
     *
     * @param Registration $registration    Registration to update
     * @param Guild $guild                  Guild where the event is located
     * @param User $currentUser             The current user
     * @param int $state                    State that the registration will have
     * @param bool $newRegistration         Is this for a new registration
     * @param bool $inviteToggle            Is it for an invitation to an event
     * @return void
     */
    public function updateUserState(Registration $registration, Guild $guild, User $currentUser, $state, $newRegistration, $inviteToggle);

    /**
     * @param Guild $guild                  Guild where the event is located
     * @param User $user                    User that is registering
     * @param Request $request              Request
     * @param int $dateId                   DateId
     * @param string $eventSlug             Slug of the event
     * @return mixed
     */
    public function register(Guild $guild, User $user, Request $request, $dateId, $eventSlug);

    /**
     * Cancelling the registration of the current user
     *
     * @param Registration $registration    Registration to cancel
     * @param User $user                    The user that is cancelling it
     * @return void
     */
    public function cancelRegistration(Registration $registration, User $user);

    /**
     * Reset the registration of the current user, only if he cancelled his registration
     *
     * @param Registration $registration    Registration to reset
     * @param User $user                    The user that is trying to reset it
     * @return void
     */
    public function resetRegistration(Registration $registration, User $user);

    /**
     * @param Guild $guild                  Guild where the event is located
     * @param int $dateId                   DateId
     * @param string $eventSlug             Slug of the event
     * @return mixed
     */
    public function toggleAccess(Guild $guild, $dateId, $eventSlug);

} 
