<?php

namespace Rizzen\CalendarBundle\Services;


use Rizzen\CalendarBundle\Entity\Event;
use Rizzen\GuildBundle\Entity\Guild;

interface EventPosterInterface
{

    /**
     * Process and save the event form data to make it usable in the site
     *
     * @param Guild $guild
     * @param Event $event
     * @param Int $dateId
     * @return String
     */
    public function processNewEventPost(Guild $guild, Event $event, $dateId);

    /**
     * Edit an already created event
     *
     * @param Event $event
     * @param Int $dateId
     */
    public function processEditEventPost(Event $event, $dateId);

} 
