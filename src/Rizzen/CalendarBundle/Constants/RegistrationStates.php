<?php

namespace Rizzen\CalendarBundle\Constants;


/**
 * Class RegistrationStates
 * @package Rizzen\CalendarBundle\Constants
 */
class RegistrationStates
{
    const PENDING = 1;

    const ACCEPTED = 2;

    const REFUSED = 3;

    const INVITED = 4;

    const INTERNAL_CANCELLING = 5;

    const INTERNAL_RESET = 6;
}
