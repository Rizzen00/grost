<?php

namespace Rizzen\CalendarBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class CalendarController extends Controller
{
    /**
     * @param string $subdomain
     * @param int|null $year
     * @param int|null $month
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function seeAction($subdomain, $year = null, $month = null)
    {
        $guild = $this->get('get.guild')->getGuild($subdomain);

        $permissionsManager = $this->get('permissions.manager')->setUser($this->getUser())->setGuild($guild);
        $permissionsManager->isGuildMember();

        $calendar = $this->get('rizzen.calendar.generator')->getCalendar($year, $month, $guild);

        return $this->render('RizzenCalendarBundle:Calendar:calendar.html.twig', [
            'calendar' => $calendar,
            'guild' => $guild,
            'subdomain' => $subdomain,
            'permissions' => $permissionsManager->getPermissions()
        ]);
    }
}
