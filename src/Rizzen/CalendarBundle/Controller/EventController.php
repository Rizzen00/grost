<?php

namespace Rizzen\CalendarBundle\Controller;

use Rizzen\CalendarBundle\Constants\RegistrationStates;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Rizzen\CalendarBundle\Entity\Event;
use Rizzen\CalendarBundle\Entity\EventComment;
use Rizzen\CalendarBundle\Form\Type\EventType;
use Rizzen\CalendarBundle\Form\Type\EventCommentType;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class EventController extends Controller
{
    /**
     * View an event and everyone registered in it
     *
     * @param string $subdomain
     * @param int $dateId
     * @param string $eventSlug
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function seeAction($subdomain, $dateId, $eventSlug)
    {
        $guild = $this->get('get.guild')->getGuild($subdomain);
        $em = $this->getDoctrine()->getManager();

        $eventInformation = $this->get('event.view.helper')->getEventViewVariables($guild, $this->getUser(), $eventSlug);

        $permissionsManager = $this->get('permissions.manager')->setPermissions($eventInformation['permissions']);

        $inviteForm = $permissionsManager->can(['CanManageEvent', 'CanCreateEvent'])
            ? $this->getEventInviteForm()->createView()
            : null;

        $comments = $em->getRepository('RizzenCalendarBundle:EventComment')->getCommentsOfEvent($eventInformation['event']);

        return $this->render('RizzenCalendarBundle:Event:see.html.twig', [
                'event' => $eventInformation['event'],
                'registrationStates' => new RegistrationStates(),
                'comments' => $comments,
                'dateId' => $dateId,
                'eventInformation' => $eventInformation,
                'form' => $this->createForm(EventCommentType::class, new EventComment())->createView(),
                'permissions' => $eventInformation['permissions'],
                'inviteForm' => $inviteForm,
                'subdomain' => $subdomain
        ]);
    }

    /**
     * Process the registration of an user to an event
     *
     * @param string $subdomain
     * @param int $dateId
     * @param string $eventSlug
     * @param Request $request
     * @throws \Exception
     * @return Response
     */
    public function registerAction($subdomain, $dateId, $eventSlug, Request $request)
    {
        $guild = $this->get('get.guild')->getGuild($subdomain);
        $this->get('calendar.event.manager')->register($guild, $this->getUser(), $request, $dateId, $eventSlug);

        return new Response();
    }

    /**
     * Invite a specified user to the event
     *
     * @param $subdomain
     * @param $dateId
     * @param $eventSlug
     * @param Request $request
     * @return JsonResponse
     */
    public function inviteAction($subdomain, $dateId, $eventSlug, Request $request)
    {
        $guild = $this->get('get.guild')->getGuild($subdomain);

        $form = $this->getEventInviteForm();

        $result = ['hasBeenInvited' => false];

        $form->handleRequest($request);
        if ($form->isValid()) {
            $data = $form->getData();
            $result = $this->get('calendar.event.manager')->inviteUser($data['username'], $dateId, $eventSlug, $guild, $this->getUser());
        }

        return new JsonResponse($result);
    }

    /**
     * Answer an event invitation
     *
     * @param $subdomain
     * @param $dateId
     * @param $eventId
     * @param $choice
     * @return Response
     * @throws \Exception
     */
    public function inviteResponseAction($subdomain, $dateId, $eventId, $choice)
    {
        $event = $this->getDoctrine()->getRepository('RizzenCalendarBundle:Event')->find($eventId);
        $registration = $this->getDoctrine()->getRepository('RizzenCalendarBundle:Registration')->getRegistrationToModify($event, $this->getUser());

        $this->get('calendar.event.manager')->inviteResult($registration, $choice, $this->getUser());

        return new Response();
    }

    /**
     * List all the events in the calendar for a certain day in a specified guild
     *
     * @param string $subdomain
     * @param int $dateId
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listAction($subdomain, $dateId)
    {
        $guild = $this->get('get.guild')->getGuild($subdomain);

        $permissionsManager = $this->get('permissions.manager')->setGuild($guild)->setUser($this->getUser());
        $permissionsManager->isGuildMember();

        $startDate = new \DateTime();
        $endDate = new \DateTime();

        $timestamp = mktime(0, 0, 0, substr($dateId, 2,2), substr($dateId, 0,2), substr($dateId, 4,4));
        $startDate->setTimestamp($timestamp);

        $endDate->setTimestamp($timestamp);
        $endDate->add(new \DateInterval('PT86399S')); // add 24 hours minus one second

        $events = $this->getDoctrine()->getManager()->getRepository('RizzenCalendarBundle:Event')->getEventsForList($guild->getCalendar(), $startDate, $endDate);

        $dateOfList = new \DateTime();
        $dateOfList->setTimestamp($timestamp);

        return $this->render('RizzenCalendarBundle:Event:list.html.twig', [
                'events' => $events,
                'guild' => $guild,
                'dateOfList' => $dateOfList,
                'dateId' => $dateId,
                'permissions' => $permissionsManager->getPermissions(),
                'subdomain' => $subdomain
        ]);
    }

    /**
     * Create a new event for a guild in the calendar
     *
     * @param string $subdomain
     * @param int $dateId
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function createAction($subdomain, $dateId, Request $request)
    {
        $guild = $this->get('get.guild')->getGuild($subdomain);

        $permissionsManager = $this->get('permissions.manager')->setGuild($guild)->setUser($this->getUser());
        $permissionsManager->must(['CanCreateEvent']);

        $dateOfList = new \DateTime();
        $dateOfList->setTimestamp(mktime(0, 0, 0, substr($dateId, 2, 2), substr($dateId, 0, 2), substr($dateId, 4, 4)));

        $event = new Event();
        $form = $this->createForm(EventType::class, $event);

        $form->handleRequest($request);
        if ($form->isValid()) {
            $slug = $this->get('calendar.event.poster')->processNewEventPost($guild, $event, $dateId);
            return $this->redirect($this->generateUrl('rizzen_event_see', ['subdomain' => $subdomain, 'dateId' => $dateId, 'eventSlug' => $slug]));
        }

        return $this->render('RizzenCalendarBundle:Event:create.html.twig', [
                'form' => $form->createView(),
                'guild' => $guild,
                'dateId' => $dateId,
                'dateOfList' => $dateOfList,
                'permissions' => $permissionsManager->getPermissions(),
                'subdomain' => $subdomain
        ]);
    }

    /**
     * Editing an already created event
     *
     * @param string $subdomain
     * @param int $dateId
     * @param string $eventSlug
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editAction($subdomain, $dateId, $eventSlug, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $guild = $this->get('get.guild')->getGuild($subdomain);

        $permissionsManager = $this->get('permissions.manager')->setGuild($guild)->setUser($this->getUser());
        $permissionsManager->must(['CanCreateEvent']);

        $event = $em->getRepository('RizzenCalendarBundle:Event')->getEventForEdit($guild->getCalendar(), $eventSlug);
        if ($event === null) {
            throw $this->createNotFoundException('Event not found');
        }

        $form = $this->createForm(EventType::class, $event);

        $form->handleRequest($request);
        if ($form->isValid()) {
            $this->get('calendar.event.poster')->processEditEventPost($event, $dateId);
            $this->addFlash('success', 'Event successfully edited!');
            return $this->redirect($this->generateUrl('rizzen_event_see', [
                    'subdomain' => $subdomain,
                    'dateId' => $dateId,
                    'eventSlug' => $eventSlug
                ]
            ));
        }

        return $this->render('RizzenCalendarBundle:Event:edit.html.twig', [
                'event' => $event,
                'guild' => $guild,
                'form' => $form->createView(),
                'dateId' => $dateId,
                'permissions' => $permissionsManager->getPermissions(),
                'subdomain' => $subdomain
        ]);
    }

    /**
     * Used to switch the registration state of an existent registration
     *
     * @param string $subdomain
     * @param int $userId
     * @param int $eventId
     * @param int $state
     * @return Response
     */
    public function setStateAction($subdomain, $userId, $eventId, $state)
    {
        $guild = $this->get('get.guild')->getGuild($subdomain);

        $user = $this->getDoctrine()->getRepository('RizzenUserBundle:User')->find($userId);
        $event = $this->getDoctrine()->getRepository('RizzenCalendarBundle:Event')->find($eventId);

        if ($user === null || $event === null) {
            throw $this->createNotFoundException('Event or User not found');
        }

        $registration = $this->getDoctrine()->getRepository('RizzenCalendarBundle:Registration')->getRegistrationToModify($event, $user);

        $this->get('calendar.event.manager')->updateUserState($registration, $guild, $this->getUser(), (int) $state);

        return new Response();
    }

    /**
     * Cancel an event registration
     *
     * @param string $subdomain
     * @param int $dateId
     * @param int $eventId
     * @return Response
     * @throws \Exception
     */
    public function cancelRegistrationAction($subdomain, $dateId, $eventId)
    {
        $event = $this->getDoctrine()->getRepository('RizzenCalendarBundle:Event')->find($eventId);
        if ($event === null) {
            throw $this->createNotFoundException('Event not found');
        }

        $registration = $this->getDoctrine()->getRepository('RizzenCalendarBundle:Registration')->getRegistrationToModify($event, $this->getUser());

        $this->get('calendar.event.manager')->cancelRegistration($registration, $this->getUser());

        return new Response();

    }

    /**
     * Reset a registration (go back to the pending state)
     *
     * @param string $subdomain
     * @param int $eventId
     * @return Response
     * @throws \Exception
     */
    public function resetRegistrationAction($subdomain, $eventId)
    {
        $event = $this->getDoctrine()->getRepository('RizzenCalendarBundle:Event')->find($eventId);
        if ($event === null) {
            throw $this->createNotFoundException('Event not found');
        }

        $registration = $this->getDoctrine()->getRepository('RizzenCalendarBundle:Registration')->getRegistrationToModify($event, $this->getUser());

        $this->get('calendar.event.manager')->resetRegistration($registration, $this->getUser());

        return new Response();
    }

    /**
     * Allow or disallow users to register to the event
     *
     * @param string $subdomain
     * @param int $dateId
     * @param string $eventSlug
     * @return Response
     */
    public function toggleRegistrationsAction($subdomain, $dateId, $eventSlug)
    {
        $guild = $this->get('get.guild')->getGuild($subdomain);

        $permissionsManager = $this->get('permissions.manager')->setGuild($guild)->setUser($this->getUser());
        $permissionsManager->must(['CanCreateEvent', 'CanManageEvent']);

        $this->get('calendar.event.manager')->toggleAccess($guild, $dateId, $eventSlug);

        return new Response();
    }

    /**
     * Send a comment in an event
     *
     * @param string $subdomain
     * @param int $dateId
     * @param string $eventSlug
     * @param Request $request
     * @throws \Exception
     * @return Response
     */
    public function sendCommentAction($subdomain, $dateId, $eventSlug, Request $request)
    {
        $guild = $this->get('get.guild')->getGuild($subdomain);
        $event = $this->getDoctrine()->getManager()->getRepository('RizzenCalendarBundle:Event')->getEventOfSelectedDay($guild->getCalendar(), $eventSlug);
        $registration = $this->getDoctrine()->getManager()->getRepository('RizzenCalendarBundle:Registration')->getRegistrationToModify($event, $this->getUser());

        $permissionsManager = $this->get('permissions.manager')->setGuild($guild)->setUser($this->getUser());

        if ($event === null || $registration === null || $permissionsManager->getPermissions()->isBannedFromGuild()) {
            throw $this->createNotFoundException('Event not found');
        }

        $comment = new EventComment;
        $form = $this->createForm(EventCommentType::class, $comment);

        $form->handleRequest($request);
        if ($form->isValid()) {

            $this->get('comment.poster')
                ->setComment($comment)
                ->setCreator($this->getUser())
                ->setTarget($event)
                ->setTemplate('RizzenCalendarBundle:Event:newComment.html.twig')
                ->setWebsocketTopic($event->getSocketTopic())
                ->setReplyEntity('RizzenCalendarBundle:EventComment')
                ->post();

            return new Response();
        }

        throw $this->createNotFoundException('Event not found');
    }

    /**
     *
     * @param int $commentId
     * @return Response
     */
    public function deleteCommentAction($commentId)
    {
        $em = $this->getDoctrine()->getManager();
        $comment = $em->getRepository('RizzenCalendarBundle:EventComment')->find($commentId);

        if ($comment === null) {
            throw $this->createNotFoundException('Comment not found');
        }

        $guild = $comment->getTarget()->getCalendar()->getGuild();

        $permissionsManager = $this->get('permissions.manager')->setGuild($guild)->setUser($this->getUser());
        $permissionsManager->must(['CanCreateEvent', 'CanManageEvent']);

        $comment->setVisible(false);
        $em->flush();

        return new Response();
    }

    /**
     * Get the form used to invite an user to the event
     *
     * @return \Symfony\Component\Form\Form
     */
    private function getEventInviteForm()
    {
        return $this->createFormBuilder()
            ->add('username', TextType::class, [ 'constraints' => [ new NotBlank() ] ])
            ->getForm();
    }

}
