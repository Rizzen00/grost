<?php

namespace Rizzen\CalendarBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class OwnCalendarController extends Controller
{
    /**
     * @param int|null $year
     * @param int|null $month
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function seeAction($year = null, $month = null)
    {
        $calendar = $this->get('rizzen.calendar.generator')->getCalendar($year, $month, null, $this->getUser());

        return $this->render('RizzenCalendarBundle:Own:ownCalendar.html.twig', [
                'calendar' => $calendar
        ]);
    }

    /**
     * List all the events where the current user is registered or invited to for the day selected
     *
     * @param int $dateId
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listAction($dateId)
    {
        $startDate = new \DateTime();
        $endDate = new \DateTime();

        $timestamp = mktime(0, 0, 0, substr($dateId, 2,2), substr($dateId, 0,2), substr($dateId, 4,4));
        $startDate->setTimestamp($timestamp);

        $endDate->setTimestamp($timestamp);
        $endDate->add(new \DateInterval('PT86399S')); // add 24 hours minus one second

        $registrations = $this->getDoctrine()->getManager()->getRepository('RizzenCalendarBundle:Registration')->getUserRegistrationsForList($this->getUser(), $startDate, $endDate);

        $dateOfList = new \DateTime();
        $dateOfList->setTimestamp(mktime(0, 0, 0, substr($dateId, 2, 2), substr($dateId, 0, 2), substr($dateId, 4, 4)));

        return $this->render('RizzenCalendarBundle:Own:ownList.html.twig', [
                'registrations' => $registrations,
                'dateOfList' => $dateOfList,
                'dateId' => $dateId,
                'currentDate' => new \DateTime('now'),
        ]);
    }
}
