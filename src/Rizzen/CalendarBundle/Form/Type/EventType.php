<?php

namespace Rizzen\CalendarBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

class EventType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, ['required'  => true, 'attr' => ['autocomplete' => 'off']])
            ->add('hour', ChoiceType::class, [
                  'choices' => ['23' => '23', '22' => '22', '21' => '21', '20' => '20', '19' => '19', '18' => '18', '17' => '17', '16' => '16', '15' => '15', '14' => '14', '13' => '13', '12' => '12', '11' => '11', '10' => '10', '09' => '09', '08' => '08', '07' => '07', '06' => '06', '05' => '05', '04' => '04', '03' => '03', '02' => '02', '01' => '01', '00' => '00'],
                  'required' => true,
                  'expanded' => false,
                  'multiple' => false
            ])
            ->add('minute', ChoiceType::class, [
                  'choices' => ['00' => '00', '05' => '05', '10' => '10', '15' => '15', '20' => '20', '25' => '25', '30' => '30', '35' => '35', '40' => '40', '45' => '45', '50' => '50', '55' => '55'],
                  'required' => true,
                  'expanded' => false,
                  'multiple' => false
            ])
            ->add('description', TextareaType::class, ['required'  => true, 'attr' => ['autocomplete' => 'off']])
            ->add('open', CheckboxType::class, [
                'required' => false,
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'Rizzen\CalendarBundle\Entity\Event'
        ]);
    }

    public function getBlockPrefix()
    {
        return 'grost_event';
    }
}
