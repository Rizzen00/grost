var template =
    "<div class='popover tour'>" +
        "<div class='arrow'></div>" +
        "<h3 class='popover-title'></h3>" +
        "<div class='popover-content'></div>" +
        "<div class='popover-navigation'>" +
            "<button class='btn btn-sm btn-primary' data-role='prev'>« Prev</button>" +
            "<span data-role='separator'> </span>" +
            "<button class='btn btn-sm btn-primary' data-role='next'>Next »</button>" +
            "<button class='btn btn-sm btn-primary pull-right' onclick='disableGuildTour()' data-role='end'>End tour</button>" +
        "</div>" +
    "</div>";

if (currentTour === 'dashboard') {
    var tour = new Tour({
        orphan: true,
        storage: false,
        backdrop: true,
        template: template,
        steps: [
            {
                title: "Welcome",
                content: "Your guild has been successfully created. If you're not familiar with Grost, please follow this quick little tour, we will guide you through the features available, and teach you how to use them!<br> <small class='small-lighter'>To skip the tour, click \"End Tour\".</small>",
                backdrop: true
            },
            {
                element: "#quickAccess",
                title: "Quick access",
                content: "From here, you can access your forum, blog and event calendar, the event calendar is only accessible by you and your guild members.",
                placement: 'left'
            },
            {
                element: "#guildEvents",
                title: "Your upcoming guild events",
                content: "Here will be displayed the upcoming events in your guild, only visible to you and your guild members, it will show when the event will start, and the status of your registration in the event.",
                placement: 'right'
            },
            {
                element: "#guildAdmin",
                title: "The Administration",
                content: "This is where you can add members to your guild, manage then, create the categories for your forum and much more. Please, click next, we will go through the options available in the administration and tell you what all of them does.",
                placement: 'bottom',
                backdrop: false
            },
            {
                path: 'https://' + window.location.host + '/admin/'
            }
        ]});
} else {
    var tour = new Tour({
        orphan: true,
        storage: false,
        backdrop: true,
        onEnd: function () {
            disableGuildTour();
        },
        template: template,
        steps: [
            {
                title: "Welcome to your administration",
                content: "Managing your guild has been made as easy as possible, let's go through what you can do."
            },
            {
                element: "#adminRoles",
                title: "Roles",
                content: "This is where you can find the default roles and create new ones. The roles let you grant permissions to some of your users to do certain tasks, like editing messages in the forum.",
                placement: 'right'
            },
            {
                element: "#adminUsers",
                title: "Your users",
                content: "There, you will see a list containing all the user that registered to your forum and the list of all your guild members. When clicking on a member, you will see his/her guild/forum profile. Their profile page let you add them to your guild (they need to join your forum first), give them roles or ban them from your guild and/or forum.",
                placement: 'right'
            },
            {
                element: "#adminBanned",
                title: "Banned users",
                content: "That one is easy, it contains all the users you banned from your guild or forum.",
                placement: 'right'
            },
            {
                element: "#adminForumCategories",
                title: "Forum Categories",
                content: "This is where you can create the categories for your forum. Since you've just created your guild, your forum contains no category, so we recommend you create some!",
                placement: 'right'
            },
            {
                element: "#adminBlogCategories",
                title: "Blog Categories",
                content: "In here, you will see the categories of your blog, your blog already contains a default category, but you can of course create new ones.",
                placement: 'right'
            },
            {
                element: "#adminBlogPosts",
                title: "Blog Posts",
                content: "This is where your blog posts are located, you can create new blog posts from this page.",
                placement: 'right'
            },
            {
                element: "#adminCSS",
                title: "Custom style",
                content: "If you want to further personalize your guild, you can do so there. You can use your own CSS to modify the styling of your guild.",
                placement: 'right'
            },
            {
                element: "#adminLogo",
                title: "Guild Logo",
                content: "There you can upload your own logo, it will replace the name of your guild in the navbar.",
                placement: 'right'
            },
            {
                element: "#adminName",
                title: "Guild Name",
                content: "Finally, you can rename your guild there if you want to.",
                placement: 'right'
            },
            {
                title: "That's it!",
                content: 'Thank you for taking this little tour, we hope you and your guild will like what we are doing with Grost, if you have a question or an idea, you can contact us using the forum (link in the footer), by email or using the messaging system ("Contact" in the footer)!',
                placement: 'right',
                backdrop: false
            }
        ]});
}


// Initialize the tour
tour.init();

// Start the tour
tour.start();
