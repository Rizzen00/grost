/*global $, guildChatChanel */
/*
* General scripts
* */

$("#nav-base").headroom({
    offset : 51
});

// Make the footer stay at the bottom
$(document).ready(function() {
    var docHeight = $(window).height();
    var footer = $('.footer');
    var footerHeight = $(footer).height();
    var footerTop = $(footer).position().top + footerHeight;

    if (footerTop < docHeight) {
        $(footer).css('margin-top', 10 + (docHeight - footerTop) + 'px').addClass('footer-shown');
    } else {
        $(footer).addClass('footer-shown');
    }
});

function updateCharCount(charCountId, editorId) {
    "use strict";

    var charCount = $(editorId).val().length - 1;

    if (charCount === -1) {
        $(charCountId).text(0);
    } else {
        $(charCountId).text(charCount);
    }
}

function ajaxRequestResponse(url, id, parentId, otherId)
{
    $(id).removeClass('btn-primary').addClass('disabled').html('<i class="fa fa-spinner fa-spin"></i> Processing...');
    $(otherId).addClass('disabled');
    $.ajax({
        type: "GET",
        url: url,
        success: function(data){
            $(parentId).animate({height: 0, opacity: 0}, 'fast', function() {
                $(this).remove();
            });
            setTimeout(function(){
                var reqWrap = $('.req-wrap');
                if ($(reqWrap).children().length === 0) {
                    $(reqWrap).append("<p class='d-none text-center' id='noPost'><strong>You have no more requests!</strong></p>");
                    $("#noPost").slideAnimate(500);
                }
            }, 500);
        },
        error: function() {
            $(id).addClass('btn-danger').removeClass('disabled').html('<i class="fa fa-close"></i> An error occurred.');
            $(otherId).removeClass('disabled');
        }
    });
    return false;
}

function sendFriendRequest(url) {

    var btn = $('#sendFriendRequest');

    $(btn).addClass('disabled').html('<i class="fa fa-spinner fa-spin"></i> Sending the request...');

    $.ajax({
        type: "GET",
        url: url,
        success: function(){
            $(btn).html('<i class="fa fa-check"></i> Friend request sent!').removeClass('btn-primary').addClass('btn-success');

            setTimeout(function(){
                $(btn).slideAnimateOut(500);

            }, 2000);
        },
        error: function() {
            $(btn).html('<i class="fa fa-warning"></i> An error occured!').removeClass('btn-primary').addClass('btn-danger');
        }
    });

}

function ajaxDeleteComment(commentId)
{
    var btnId = '#button-del-com-' + commentId,
        containerId = '#comment-container-' + commentId;

    $(btnId).removeClass('btn-danger').prop('disabled', true).prop('disabled', true);

    $.ajax({
        type: "GET",
        url: $(btnId).attr('href'),
        success: function(data){
            $(btnId).addClass('btn-success').html('<span class="glyphicon glyphicon-ok"></span> Message deleted!');

            setTimeout(function () {
                $('[data-toggle="popover"]').popover('hide');
                $(containerId).animate({ height: 'toggle', opacity: 'toggle' }, 'slow', function() {
                    $(this).remove();
                    var comments = $('#comments');
                    if ($(comments).children().length == 0) {
                        $(comments).append("<p id='noCommentText' style='display:none'><i>There is no comment left.</i></p>");
                        $("#noCommentText").toggleAnimate('slow');
                    }
                });
            }, 1500);

        },
        error: function() {
            $(btnId).addClass('btn-danger').prop('disabled', false).html('An error occurred.');
        }
    });
    return false;
}

// Because Ajax modals are not emptied when closed, the content needs to be removed, it will not do it if .local-modal class in on the modal
$(".modal").not(".local-modal").on("hidden.bs.modal", function (e) { $(e.target).removeData("bs.modal").find(".modal-content").empty(); });

function setResponseTarget(targetId, targetName) {
    $(".comment-target-input").val(targetId);
    $(".response-form-alert-name").html(targetName);
    $('html, body').animate({
        scrollTop: $(".comment-form").offset().top
    }, 1000);
    $(".response-form-alert").fadeIn(150);
}

function resetResponseTarget() {
    $(".comment-target-input").val('');
    $(".response-form-alert").fadeOut(150);
}

$(".forum-reply-form").on("submit", function() {
    var submitButton = $('#submitButton');
    var forumReplyForm = $(".forum-reply-form");
    var url = $(forumReplyForm).attr('action');

    $(submitButton).removeClass('btn-primary').addClass('disabled').html('<span class="fa fa-send"></span> Sending...');

    $.ajax({
        url: url,
        type: 'POST',
        data: $(forumReplyForm).serialize(),

        success: function (data) {
            if (data.hasBeenSent === false) {
                $('#forumReplyError').slideAnimate(200);
                $("#submitButton").addClass('btn-danger disabled').removeClass('btn-primary').html('<span class="fa fa-warning"></span> Error!');
                setTimeout(function () {
                    $("#submitButton").removeClass('btn-danger disabled').addClass('btn-primary').html('<span class="fa fa-send"></span> Send');
                }, 2000);
                return;
            }

            $(submitButton).addClass('btn-success').html('<span class="fa fa-check"></span> Sent!');
            $('#rizzen_forumbundle_message_content').val('');
            setTimeout(function () {
                $(submitButton).removeClass('btn-success disabled').addClass('btn-primary').html('<span class="fa fa-send"></span> Send');
            }, 2000);
        },
        error: function() {
            $(submitButton).addClass('btn-danger').prop('disabled', false).html('An error occurred.');
            setTimeout(function () {
                $(submitButton).removeClass('btn-danger disabled').addClass('btn-primary').html('<span class="fa fa-send"></span> Send');
            }, 5000);
        }
    });
    return false;
});

function forumQuote(messageId) {
    var messageToQuote = '<blockquote>' + $(messageId).find('.forum-message').html() + '<em>' + $(messageId).find('.forum-username').html() + '</em></blockquote>';
    CKEDITOR.instances.rizzen_forumbundle_message_content.insertHtml(messageToQuote);
    $.scrollTo('.reply-form', 1000);
}

function forumEdit (messageId, lockStatus) {
    var messageToEdit = $('#msg-f-' + messageId).html();
    CKEDITOR.instances.form_content.setData(messageToEdit);
    $('#form_target').val(messageId);
    $('#form_lock').bootstrapToggle(lockStatus);

    var resumeEditButton = $('#resumeEditingButton');

    if ($(resumeEditButton).hasClass('resumeEditBtn')) {
        $(resumeEditButton).addClass('resumeEditBtn-visible').removeClass('resumeEditBtn disabled');
    }
}

function setForumViewedCookie(threadSlug) {

    if (Cookies.get('thread-viewed-' + threadSlug)) {
        return;
    }

    var currentDatePlus3Min = new Date();
    currentDatePlus3Min.setMinutes(currentDatePlus3Min.getMinutes() + 3);
    Cookies.set('thread-viewed-' +  threadSlug, true, { path: '/', expires: currentDatePlus3Min });
}

function forumEditSubmit(url) {
    var toggleState = '';
    $('#forumEditButton').addClass('disabled').html('<i class="fa fa-spinner fa-spin"></i> Processing...');
    $('.btn-panel-title').addClass('disabled');
    $('#editFormInfo').slideAnimateOut(200);
    $.ajax({
        type: 'POST',
        url: url,
        data: $('#editForm').serialize(),
        success: function (data) {
            if (data.hasBeenEdited === true) {
                if ($('#editPostModal').is(':visible')) {
                    $('#forumEditButton').addClass('btn-success').removeClass('btn-primary').html('<i class="fa fa-check"></i> Message edited!');
                    $('#editFormSuccess').slideAnimate(200);
                }

                toggleState = $('#form_lock').prop('checked') ? 'on' : 'off';

                var formTarget = $('#form_target');

                $('#editBtn' + $(formTarget).val()).attr("onclick", "forumEdit(" + $(formTarget).val() + ", '" + toggleState + "')");

                $('#form_content, #form_target').val('');
                $('#resumeEditingButton').removeClass('resumeEditBtn-visible').addClass('resumeEditBtn disabled');

            } else {
                $('#forumEditButton').addClass('btn-danger').removeClass('btn-primary').html('<i class="fa fa-warning"></i> Error!');
                if ($('#editPostModal').is(':visible')) {
                    $('#editModalErrorContent').text(data.reason);
                    $('#editFormError').slideAnimate(200);

                    setTimeout(function () {
                        $('#forumEditButton').addClass('btn-primary').removeClass('btn-danger disabled').html('<i class="fa fa-check"></i> Edit');
                        $('#editFormError').slideAnimateOut(200);
                        $('#editFormInfo').slideAnimate(200);
                    }, 3500);
                }
            }
            $('.btn-panel-title').removeClass('disabled');
        }
    });
}

$('#editPostModal').on('hidden.bs.modal', function () {
    $('#forumEditButton').addClass('btn-primary').removeClass('btn-success disabled').html('<i class="fa fa-check"></i> Edit');
    $('#editFormSuccess, #editFormError').slideAnimateOut(0);
    $('#editFormInfo').slideAnimate(0);
});


$('#deletePostModal').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget);

    $('#deleteMessageButton').data({ url: button.data('url'), messageid: button.data('messageid') });

}).on('hidden.bs.modal', function (event) {
    $('#deleteMessageButton').addClass('btn-danger').removeClass('btn-success disabled').html('<i class="fa fa-check"></i> Delete');
});

function deleteForumMessage() {
    var deleteButton = $('#deleteMessageButton');
    var deleteData = $(deleteButton).data();

    $('.btn-panel-title').addClass('disabled');
    $('#msg-' + deleteData.messageid).find('.panel-heading').addClass('deletedColor').find('.forum-messages-controls').fadeOut('fast');
    $(deleteButton).addClass('disabled').html('<i class="fa fa-spinner fa-spin"></i> Deleting...');

    $.ajax({
        type: 'GET',
        url: deleteData.url,
        success: function () {
            $('#msg-' + deleteData.messageid).find('.panel-body').toggleAnimate(400, false);

            if ($('#deletePostModal').is(':visible')) {
                $(deleteButton).addClass('btn-success').removeClass('btn-danger').html('<i class="fa fa-check"></i> Deleted!');
            }

            $('.btn-panel-title').removeClass('disabled');

        }
    });
}

function joinEvent(url) {
    $('#joinEventButton').html('<i class="fa fa-spin fa-spinner"></i> Joining the event...').addClass('disabled');
    $.ajax({
         type: "GET",
         url: url,
         success: function(data){
             $('#joinEventRow').slideAnimateOut(200);
         },
         error: function() {
             $('#joinEventButton').html('An error occurred');
         }
    });
}

function modifyStateOfUserInEvent(userId, eventId, newState, registrationId, clickedButtonId) {

    var url = '/calendar/event/modify-state/'+ userId +'/'+ eventId +'/'+ newState,
        oldButtonIcon = $(clickedButtonId).html(),
        btnGroupId = registrationId + ' > .chat-text-container > .chat-from-username-fl > .btn-group';

    $(clickedButtonId).html('<i class="fa fa-spinner fa-spin"></i>').addClass('disabled');
    $(btnGroupId).children().attr("disabled", "disabled");

    $.ajax({
        type: "GET",
        url: url,
        success: function(data){

        },
        error: function() {
            $(clickedButtonId).html(oldButtonIcon);
            $(btnGroupId).children().removeAttr("disabled").html('<i class="fa fa-spinner fa-spin"></i>').removeClass('disabled');
        }
    });
}

function toggleEventAccess() {
    "use strict";
    var regBtn = $('#toggleRegistrations');

    regBtn.addClass('disabled');
    if (regBtn.data('action') === 'close') {
        regBtn.html('<i class="fa fa-spinner fa-spin"></i> Closing the registrations...');
    } else {
        regBtn.html('<i class="fa fa-spinner fa-spin"></i> Opening the registrations...');
    }

    $.ajax({
        type: 'POST',
        url: regBtn.data('url'),
        error: function () {
            regBtn.html('<i class="fa fa-close"></i> An error occured').addClass('btn-danger').removeClass('btn-info');
        }
    });
}

$('#inviteModal').on('show.bs.modal', function () {
    $('[data-toggle="tooltip"]').tooltip('hide');
});

// Conversation specific scripts

$('#conversationReplyForm').on("submit", function() {
    "use strict";
    $("#formSubmitButton").addClass('disabled').html('<i class="fa fa-spin fa-spinner"></i> Replying...');

    var messengerReplyError = $('#messengerReplyError');

    if ($(messengerReplyError).is(':visible')) {
        $(messengerReplyError).slideAnimateOut(150);
    }

    $.ajax({
        url: $('#conversationReplyForm').attr('action'),
        type: 'post',
        data: $(".reply").serialize(),

        success: function (data) {
            if (data.hasBeenSent) {
                $('#grost_messenger_message_content').val('');
                $("#formSubmitButton").removeClass('disabled btn-success').addClass('btn-primary').html('<i class="fa fa-send"></i> Reply');
            } else {
                $(messengerReplyError).slideAnimate(200);
                $("#formSubmitButton").removeClass('disabled btn-success').addClass('btn-primary').html('<i class="fa fa-send"></i> Reply');
            }

        }
    });
    return false;
});

$('#eventInviteUserForm').on("submit", function() {
    "use strict";

    var formUsername = $('#form_username');
    var username = $(formUsername).val();
    var inviteForm = $("#eventInviteUserForm");
    var serializedForm = $(inviteForm).serialize();
    var inviteButton = $("#sendInviteButton");
    var messengerReplyError = $('#messengerReplyError');

    $(inviteButton).addClass('disabled');
    $(formUsername).attr('disabled', 'disabled');

    if ($(messengerReplyError).is(':visible')) {
        $(messengerReplyError).slideAnimateOut(150);
    }

    if (username.length < 3) {
        $(formUsername).val('Username too short (3 char. min.)');
        $('#inviteFormErrorIcon').fadeIn('fast');
        $('#inviteFormGroup').addClass('has-error has-feedback');

        setTimeout(function () {
            $(inviteButton).removeClass('disabled').html('<i class="fa fa-envelope"></i> Send an invitation');
            $(formUsername).removeClass('disabled').val(username);

            $('#inviteFormErrorIcon').fadeOut('fast').promise().done(function () {
                $('#inviteFormGroup').removeClass('has-error has-feedback');
            });
        }, 2000);
        return false;
    }

    if (username.length > 15) {

        $(formUsername).val('Username too long (15 char. max.)');
        $('#inviteFormErrorIcon').fadeIn('fast');
        $('#inviteFormGroup').addClass('has-error has-feedback');

        setTimeout(function () {
            $(inviteButton).removeClass('disabled').html('<i class="fa fa-envelope"></i> Send an invitation');
            $(formUsername).removeClass('disabled').val(username);

            $('#inviteFormErrorIcon').fadeOut('fast').promise().done(function () {
                $('#inviteFormGroup').removeClass('has-error has-feedback');
            });

        }, 2000);
        return false;
    }

    $(inviteButton).html('<i class="fa fa-spin fa-spinner"></i> Sending the invite...');

    $.ajax({
        url: $(inviteForm).attr('action'),
        type: 'post',
        data: serializedForm,

        success: function (data) {
            if (data.hasBeenInvited === true) {

                $(formUsername).val(username + ' has been invited!').parent().addClass('has-success');
                $(inviteButton).removeClass('btn-primary').addClass('btn-success').html('<i class="fa fa-check"></i> Invitation sent!');

                setTimeout(function () {

                    $(inviteButton).removeClass('disabled btn-success').addClass('btn-primary');
                    $(formUsername).removeAttr('disabled').val('').parent().removeClass('has-success');

                }, 1500);

            } else {

                $('#inviteFormGroup').addClass('has-error has-feedback');
                $('#inviteFormErrorIcon').fadeIn('fast');

                $(formUsername).val(data.reason).parent().parent().addClass('has-error');

                $(inviteButton).removeClass('btn-primary').addClass('btn-danger');
                setTimeout(function () {

                    $('#inviteFormErrorIcon').fadeOut('fast').promise().done(function () {
                        $('#inviteFormGroup').removeClass('has-error has-feedback');
                    });

                    $(inviteButton).removeClass('disabled btn-danger').addClass('btn-primary');
                    $(formUsername).removeAttr('disabled').val(username).parent().parent().removeClass('has-error');
                }, 1500);
            }

            $(inviteButton).addClass('btn-primary').html('<i class="fa fa-envelope"></i> Send an invitation');

        }
    });
    return false;
});

function inviteSet(stateSelected) {
    "use strict";

    var url;

    $('#inviteSetAccepted, #inviteSetRefused').addClass('disabled');

    if (stateSelected === 2) {
        url = $('#inviteSetAccepted').html('<i class="fa fa-spin fa-spinner"></i> Accepting the invitation...').data('url');
    } else {
        url = $('#inviteSetRefused').html('<i class="fa fa-spin fa-spinner"></i> Refusing the invitation...').data('url');
    }

    $.ajax({
        url: url,
        type: 'get',

        success: function (data) {
            if (stateSelected === 2) {
                $('#inviteSetRefused').removeClass('disabled');
                $('#inviteSetAccepted').html('<i class="fa fa-check"></i> Invitation accepted!');

                setTimeout(function () {
                    $('#inviteSetAccepted').html('<i class="fa fa-check"></i> Accept the invitation');
                }, 1500);

            } else {
                $('#inviteSetAccepted').removeClass('disabled');
                $('#inviteSetRefused').html('<i class="fa fa-check"></i> Invitation refused!');

                setTimeout(function () {
                    $('#inviteSetRefused').html('<i class="fa fa-close"></i> Refuse the invitation');
                }, 1500);

            }

        }
    });
}

function cancelRegistration() {
    var cancelButton = $('#cancelRegistrationButton');

    $(cancelButton).addClass('disabled').html('<i class="fa fa-spin fa-spinner"></i> Canceling the registration...');

    $.ajax({
        url: $(cancelButton).data('url'),
        type: 'get',

        success: function (data) {
            setTimeout(function () {
                $(cancelButton).removeClass('disabled').html('<i class="fa fa-close"></i> Cancel your registration');
            }, 1000);
        }
    });

}

function resetRegistration() {
    var resetButton = $('#resetRegistrationButton');

    $(resetButton).addClass('disabled').html('<i class="fa fa-spin fa-spinner"></i> Resetting the registration');

    $.ajax({
        url: $(resetButton).data('url'),
        type: 'get',

        success: function (data) {
            setTimeout(function () {
                $(resetButton).removeClass('disabled').html('<i class="fa fa-refresh"></i> Reset your registration');
            }, 1000);
        }
    });
}

function conversationSendUserInvite(url) {
    "use strict";

    var values = {};
    var inviteUserInput = $('.inviteUserInput');
    var inviteButton = $('#sendInviteButton');
    var inviteFormGroup = $('#inviteFormGroup');
    var inviteFormErrorIcon = $('#inviteFormErrorIcon');

    values.username = $(inviteUserInput).val().replace(/ /g,'');
    values._csrf_token = $('.inviteUserCSRF').val();

    if (values.username.length < 3) {
        $(inviteButton).attr('disabled', 'disabled');
        $(inviteUserInput).attr('disabled', 'disabled').val('Username too short (3 char. min.)');
        setTimeout(function () {
            $(inviteButton).removeAttr('disabled');
            $(inviteUserInput).removeAttr('disabled').val(values.username);
        }, 2000);
        return;
    }

    if (values.username.length > 15) {
        $(inviteButton).attr('disabled', 'disabled');
        $(inviteUserInput).attr('disabled', 'disabled').val('Username too long (15 char. max.)');
        setTimeout(function () {
            $(inviteButton).removeAttr('disabled');
            $(inviteUserInput).removeAttr('disabled').val(values.username);
        }, 2000);
        return;
    }

    $(inviteButton).attr('disabled', 'disabled');
    $(inviteUserInput).attr('disabled', 'disabled').val('Inviting ' + values.username + '...');

    $.ajax({
        type: 'POST',
        url: url,
        data: values,
        success: function(data) {
            if (data.invited === true) {
                $(inviteUserInput).val('Invitation sent!').parent().addClass('has-success');
                $(inviteButton).removeClass('btn-info').addClass('btn-success');
                setTimeout(function () {
                    $(inviteButton).removeAttr('disabled').removeClass('btn-success').addClass('btn-info');
                    $(inviteUserInput).removeAttr('disabled').val('').parent().removeClass('has-success');
                }, 1500);
            } else {
                $(inviteFormGroup).addClass('has-error has-feedback');
                $(inviteFormErrorIcon).fadeIn('fast');

                $(inviteUserInput).val(data.reason).parent().parent().addClass('has-error');

                $(inviteButton).removeClass('btn-info').addClass('btn-danger');
                setTimeout(function () {

                    $(inviteFormErrorIcon).fadeOut('fast').promise().done(function () {
                        $(inviteFormGroup).removeClass('has-error has-feedback');
                    });

                    $(inviteButton).removeAttr('disabled').removeClass('btn-danger').addClass('btn-info');
                    $(inviteUserInput).removeAttr('disabled').val(values.username).parent().parent().removeClass('has-error');
                }, 1500);
            }

        }
    });
}

function leaveConversation() {
    window.location.href = $('#leaveConversationButton').attr('href');
}

function roleCreationSetPermissions(onOrOff) {
    $('.list-group').find('[type="checkbox"]').bootstrapToggle(onOrOff);
}

function userManagementAssignRoleSelect(btnId) {

    $(btnId).parent().parent().find('button').removeClass('active btn-primary').addClass('btn-info');
    $(btnId).addClass('active btn-primary').removeClass('btn-info');

    var roleData = $(btnId).data();

    $('#userNewRoleSelected').html('<strong>' + roleData.title + '</strong>');
    $('#roleBtnSubmit').data({ role: roleData.role, user: roleData.user, title: roleData.title });

}

function selectRoleAccess(btnId, categoryId) {
    var btn = $(btnId),
        addBtnRow = $('#addRole' + categoryId),
        removeBtnRow = $('#removeRole' + categoryId),
        submitButton = $('#accessBtnSubmit' + categoryId);

    $(addBtnRow).find('button').addClass('btn-success').removeClass('active btn-primary');
    $(removeBtnRow).find('button').addClass('btn-danger').removeClass('active btn-primary');

    if (btn.hasClass('btn-role-add')) {
        submitButton.addClass('btn-success').removeClass('btn-primary disabled').html('<i class="fa fa-check"></i> Add the role').data('selectedbtn', btnId);
    } else {
        submitButton.addClass('btn-success').removeClass('btn-primary disabled').html('<i class="fa fa-close"></i> Remove the role').data('selectedbtn', btnId);
    }

    btn.addClass('active btn-primary').removeClass('btn-danger btn-success');
}

function roleAccessSubmit(btnId, categoryId) {
    var btn = $(btnId);
    var selectedBtn = $($(btnId).data('selectedbtn'));
    var url;
    var btnWrap;
    var noRolesWell = $('#noRolesWell' + categoryId);
    var removeRole = $('#removeRole' + categoryId);
    var accessRestricted = $('#accessRestricted'+ categoryId);

    btn.closest('.modal-content').find('.modal-body').find('button').addClass('disabled');

    if (selectedBtn.hasClass('btn-role-add')) {
        btn.addClass('disabled').html('<i class="fa fa-cog fa-spin"></i> Adding the role...');
        url = selectedBtn.data('addurl');
    } else {
        btn.addClass('disabled').html('<i class="fa fa-cog fa-spin"></i> Removing the role...');
        url = selectedBtn.data('removeurl');
    }

    $.ajax({
        type: 'GET',
        url: url,
        success: function(data) {

            if (data.hasBeenUpdated) {
                selectedBtn.parent().animate({width:'toggle', margin: 'toggle', padding: 'toggle', opacity: 'toggle'}, 200);

                setTimeout(function(){
                    if (selectedBtn.hasClass('btn-role-add')) {
                        if ($(noRolesWell).css('display') !== 'none') {
                            $(noRolesWell).slideAnimateOut(100);
                            $(accessRestricted).fadeIn();
                        }
                        selectedBtn.addClass('btn-role-remove').removeClass('btn-role-add');
                        btnWrap = selectedBtn.parent().detach();

                        selectedBtn.removeClass('btn-primary active').addClass('btn-danger');
                        $(removeRole).append(btnWrap);
                    } else {

                        selectedBtn.addClass('btn-role-add').removeClass('btn-role-remove');
                        btnWrap = selectedBtn.parent().detach();

                        selectedBtn.removeClass('btn-primary active').addClass('btn-success');
                        $('#addRole' + categoryId).append(btnWrap);

                        if ($(removeRole).find('button').size() === 0) {
                            $(noRolesWell).slideAnimate(200);
                            $(accessRestricted).fadeOut();
                        }
                    }
                }, 330);

                setTimeout(function(){
                    btnWrap.animate({width:'toggle', margin: 'inherit', padding: 'toggle', opacity: 'toggle'}, 200);
                }, 330);

                btn.html('<i class="fa fa-info-circle"></i> Select a role to add or remove')
                   .removeClass('btn-success btn-danger')
                   .addClass('btn-primary')
                   .closest('.modal-content')
                   .find('.modal-body')
                   .find('button')
                   .not('#btnSetGuests' + categoryId)
                   .removeClass('disabled');

                setTimeout(function(){
                    if ($(removeRole).find('button').size() === 0) {
                        $('#btnSetGuests' + categoryId).removeClass('disabled').html('<i class="fa fa-eye"></i> Make the category accessible and visible to guests').data('accessible', 'accessible');
                    } else {
                        $('#btnSetGuests' + categoryId).addClass('disabled').html('<i class="fa fa-eye"></i> Make the category accessible and visible to guests').data('accessible', 'accessible');
                    }
                }, 350);


            } else {
                btn.addClass('disabled').html('<i class="fa fa-cog fa-warning"></i> An error occured!');
            }

        }
    });

}

function guestsAccess(categoryId) {
    var btn = $('#btnSetGuests' + categoryId),
        btnData = btn.data(),
        url;

    btn.html('<i class="fa fa-cog fa-spin"></i> Toggling the accessibility and visibility of guests...')
        .addClass('disabled')
        .parent()
        .parent('.modal-body')
        .find('button')
        .addClass('disabled');


    if (btnData.accessible === 'accessible') {
        url = btnData.accessibleurl;
    } else {
        url = btnData.inaccessibleurl;
    }

    $.ajax({
        type: 'GET',
        url: url,
        success: function(data) {

            if (data.hasBeenUpdated) {
                if (data.isAccessible) {

                    btn.addClass('btn-success')
                        .removeClass('btn-primary btn-danger')
                        .html('<i class="fa fa-check"></i> Guests are now able to see and access this category!');

                    $('#guestsLabel' + categoryId).fadeOut();

                    setTimeout(function(){
                        btn.html('<i class="fa fa-eye-slash"></i> Make the category inaccessible and invisible to guests').data('accessible', 'inaccessible');

                        btn.removeClass('disabled btn-success')
                           .addClass('btn-primary')
                           .parent()
                           .parent('.modal-body')
                           .find('button')
                           .removeClass('disabled');
                    }, 2000);

                } else {
                    btn.addClass('btn-success')
                        .removeClass('btn-primary btn-danger')
                        .html('<i class="fa fa-check"></i> Guests are now unable to see and access this category!');

                    $('#guestsLabel' + categoryId).fadeIn();

                    setTimeout(function(){
                        btn.html('<i class="fa fa-eye"></i> Make the category accessible and visible to guests').data('accessible', 'accessible');

                        btn.removeClass('disabled btn-success')
                            .addClass('btn-danger')
                            .parent()
                            .parent('.modal-body')
                            .find('button')
                            .removeClass('disabled');
                    }, 2000);

                }
            } else {
                btn.addClass('btn-danger').removeClass('btn-success btn-primary').html('<i class="fa fa-warning"></i> An error occured, please refresh the page.');
            }
        }
    });


}

function userManagementAssignRoleSubmit() {

    var roleButtonSubmit = $('#roleBtnSubmit');
    var roleNoneSelected = $('#roleNoneSelected');
    var roleData = $(roleButtonSubmit).data();
    var roleModalAlert = $('#roleModalAlert');

    if (roleData.role === "" || roleData.title === "" || roleData.user === "") {
        $(roleNoneSelected).slideAnimate();
        return;
    }

    if ($(roleNoneSelected).is(":visible")) {
        $(roleNoneSelected).slideAnimateOut();
    }

    $(roleModalAlert).children().slideAnimateOut();
    $(roleButtonSubmit).addClass('disabled').html('<i class="fa fa-cog fa-spin"></i> Assigning the role, please wait...');

    $.ajax({
        type: 'GET',
        url: '/admin/users/' + roleData.user + '/roles/assign/' + roleData.role,
        success: function(data) {

            $(roleModalAlert).html(
            '<div style="display: none;" class="alert ' + data.alert + ' alert-dismissible" role="alert">' +
                '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' +
                data.message +
            '</div>'
            ).children().slideAnimate();

            if (data.hasBeenAssigned === true) {
                $('#roleBtn' + roleData.role).addClass('disabled btn-info').removeClass('active btn-primary');
                $('#removeRoleBtnConfirm').removeClass('hasRemovedRole' + roleData.role + '');
                userAddRoleToTable(roleData);
                updateUserPermissionList(JSON.parse(data.permissions));
            }

            $(roleButtonSubmit).removeClass('disabled').html('<i class="fa fa-check"></i> Confirm and assign the role').data({ role: '', user: '', title: '' });

            $('#userNewRoleSelected').html('<em>No role is currently selected.</em>');
        }
    });

}

function userAddRoleToTable(roleData) {

    var noRolesText = $('#noRolesText');
    var roleTableTbody = $('#roleTableTbody');

    if ($(noRolesText).is(":visible")) {
        $(noRolesText).slideAnimateOut(200);
    }

    var newRowCount = $(roleTableTbody).children().size() + 1,
    newTableRow = '<tr id="roleRow' + roleData.role + '">' +
                      '<th>' + newRowCount + '</th>' +
                      '<td>' + roleData.title + '</td>' +
                      '<td>Now</td>' +
                      '<td class="btn-xs-td"><button type="button" data-role="" data-title="" data-toggle="modal" data-target="#removeRoleModal" class="btn btn-xs btn-danger"><i class="fa fa-close"></i> Remove role</button></td>' +
                  '</tr>';


    $(roleTableTbody).append(newTableRow).children().last().find('button').data({ role: roleData.role, title: roleData.title });

}

function removeRoleProceed() {

    var removeRoleConfirm = $('#removeRoleBtnConfirm');
    var roleData = $(removeRoleConfirm).data();
    var roleButton = $('#roleBtn' + roleData.role);
    var roleTableTbody = $('#roleTableTbody');

    $(removeRoleConfirm).addClass('disabled').html('<i class="fa fa-cog fa-spin"></i> Removing the role, please wait...');

    $.ajax({
        type: 'GET',
        url: '/admin/users/' + roleData.user + '/roles/remove/' + roleData.role,
        success: function(data) {

            $('#removeRoleAlert').html(
                '<div style="display: none;" class="alert ' + data.alert + ' alert-dismissible" role="alert">' +
                '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' +
                data.message +
                '</div>'
            ).children().slideAnimate();

            if (data.hasBeenRemoved === true) {
                $(removeRoleConfirm).addClass('disabled hasRemovedRole' + roleData.role + '');

                if ($(roleButton).parent().css('display') !== 'none') {
                    $(roleButton).removeClass('disabled');
                } else {
                    $(roleButton).parent().show();
                }

                $('#roleRow' + roleData.role).remove();

                if ($(roleTableTbody).children().size() === 0) {
                    $('#noRolesText').slideAnimate();
                }

                $(roleTableTbody).children().each(function(index) {
                    $(this).find('th').html(index + 1);
                });
                updateUserPermissionList(JSON.parse(data.permissions));
            }


            $(removeRoleConfirm).html('<i class="fa fa-check"></i> Proceed and remove the role').data('role', '');
        }
    });
}

$('#removeRoleModal').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget),
        roleId = button.data('role'),
        roleTitle = button.data('title'),
        modal = $(this);

    modal.find('.modal-title').text('Removing the role: ' + roleTitle)
    modal.find('#removeRoleName').text(roleTitle);
    modal.find('#removeRoleBtnConfirm').data('role', roleId);
}).on('hidden.bs.modal', function () {
    $(this).find('.alert').alert('close');
    $('#removeRoleBtnConfirm').removeClass('disabled');
});

function updateUserPermissionList(permissions) {

    $.each(permissions, function(key) {

        var keyId = $('#' + key);

        if (permissions[key] === true) {
            if ($(keyId).hasClass('roles-list-cant')) {
                $(keyId).addClass('roles-list-can').removeClass('roles-list-cant').find('i').removeClass('fa-close').addClass('fa-check');
            }
        } else if (permissions[key] === false) {
            if ($(keyId).hasClass('roles-list-can')) {
                $(keyId).addClass('roles-list-cant').removeClass('roles-list-can').find('i').removeClass('fa-check').addClass('fa-close');
            }
        }

    });

}

function addUserToGuild() {

    var addToGuildButton = $('#addToGuildButton');
    var url = $(addToGuildButton).html('<i class="fa fa-spin fa-spinner"></i> Adding the user, please wait...').data('url');

    $.ajax({
        type: 'GET',
        url: url,
        success: function (data) {
            if (data.hasBeenAdded === true) {
                $('#inGuildIndicator').html('Guild Member <i class="fa fa-check"></i>').addClass('btn-success').removeClass('btn-danger');
                $('#addToGuild').addClass('disabled').removeClass('btn-primary');
                $('#banFromGuild').removeClass('disabled').addClass('btn-danger');
                $(addToGuildButton).html('<i class="fa fa-check"></i> Add the user to the guild').addClass('disabled');
                $('#userAddedAlert').slideAnimate(200);
            } else {
                $(addToGuildButton).addClass('disabled btn-danger').removeClass('btn-primary').html('<i class="fa fa-cog fa-warning"></i> Unable to add the user to the guild');
            }
        }
    });
}

$('#addToGuildModal').on('hidden.bs.modal', function () {
    $('#userAddedAlert').hide();
});

function userBanForum() {
    var banForumConfirmButton = $('#banForumConfirmButton');
    var guildBannedIndicator = $('#guildBannedIndicator');
    var url = $(banForumConfirmButton).addClass('disabled').html('<i class="fa fa-cog fa-spin"></i> Banning the user, please wait...').data('url');

    $.ajax({
        type: 'GET',
        url: url,
        success: function(data) {
            if (data.hasBeenBanned === true) {
                $(banForumConfirmButton).removeClass('btn-primary').addClass('btn-success').html('<i class="fa fa-check"></i> The user has been successfully banned!');
                $('#inGuildIndicator').html('Guild Member <i class="fa fa-close"></i>').addClass('btn-danger').removeClass('btn-success');
                $('#inForumIndicator').html('<i class="fa fa-close"></i> Forum Member').addClass('btn-danger').removeClass('btn-success');
                $('#banFromForum').addClass('disabled').removeClass('btn-danger');
                $('#addToGuild').addClass('disabled').removeClass('btn-primary');
                $('#btnContainer').find('button').addClass('disabled').removeClass('btn-primary btn-danger');

                $('#forumBannedIndicator, #noRolesText').slideAnimate(400);
                if ($(guildBannedIndicator).css('display') === 'none') {
                    $(guildBannedIndicator).slideAnimate(400);
                    $('#manageBanButton').slideAnimate(400);
                }

                $('#roleTableTbody').slideAnimateOut(300);

                $('#unbanGuild').removeClass('disabled btn-success').addClass('btn-primary').html('<i class="fa fa-warning"></i> Unban from guild');
                $('#unbanForum').removeClass('disabled btn-success').addClass('btn-primary').html('<i class="fa fa-warning"></i> Unban from forum');
                $('#unbanBoth').removeClass('disabled btn-success').addClass('btn-primary').html('<i class="fa fa-warning"></i> Unban from both');

            } else {
                $('#banGuildConfirmButton').removeClass('btn-primary').addClass('btn-danger').html('<i class="fa fa-close"></i> Unable to ban the user');
            }

        }
    });

}

function userBanGuild() {

    var banGuildConfigButton = $('#banGuildConfirmButton');
    var url = $(banGuildConfigButton).addClass('disabled').html('<i class="fa fa-cog fa-spin"></i> Banning the user, please wait...').data('url');

    $.ajax({
        type: 'GET',
        url: url,
        success: function(data) {

            if (data.hasBeenBanned === true) {
                $(banGuildConfigButton).removeClass('btn-primary').addClass('btn-success').html('<i class="fa fa-check"></i> The user has been successfully banned!');
                $('#inGuildIndicator').html('Guild Member <i class="fa fa-close"></i>').addClass('btn-danger').removeClass('btn-success');
                $('#guildBannedIndicator, #manageBanButton').slideAnimate(200);
                $('#banFromGuild').addClass('disabled').removeClass('btn-danger');
                $('#unbanGuild').removeClass('disabled btn-success').addClass('btn-primary').html('<i class="fa fa-warning"></i> Unban from guild');

            } else {
                $(banGuildConfigButton).removeClass('btn-primary').addClass('btn-danger').html('<i class="fa fa-close"></i> Unable to ban the user');
            }

        }
    });

}

function adminUnbanUser(buttonId) {
    var button = $(buttonId);
    var forumBannedIndicator = $('#forumBannedIndicator');
    var guildBannedIndicator = $('#guildBannedIndicator');
    var banGuildConfirmButton = $('#banGuildConfirmButton');
    var manageBanButton = $('#manageBanButton');

    button.html('<i class="fa fa-cog fa-spin"></i> Unbanning...');
    $('#unbanButtonWrap').find('button').addClass('disabled');

    $.ajax({
        type: 'GET',
        url: button.data('url'),
        success: function(data) {

            if (data.hasBeenUnbanned === true) {

                if (button.attr('id') === 'unbanBoth') {

                    button.addClass('btn-success').removeClass('btn-primary').html('<i class="fa fa-check"></i> Unbanned!');
                    $('#unbanGuild, #unbanForum').removeClass('btn-primary');

                    $('#forumBannedIndicator, #guildBannedIndicator, #manageBanButton').slideAnimateOut(400, true);

                } else if (button.attr('id') === 'unbanForum') {

                    button.addClass('btn-success').removeClass('btn-primary').html('<i class="fa fa-check"></i> Unbanned!');
                    $('#unbanGuild').removeClass('disabled');
                    $('#unbanBoth').removeClass('btn-primary');


                    $(forumBannedIndicator).slideAnimateOut(400, true);
                    if ($(guildBannedIndicator).css('display') === 'none' && $(manageBanButton).css('display') !== 'none') {
                        $('#manageBanButton').slideAnimateOut(400, true);
                    }


                } else if (button.attr('id') === 'unbanGuild') {

                    button.addClass('btn-success').removeClass('btn-primary').html('<i class="fa fa-check"></i> Unbanned!');

                    if ($(forumBannedIndicator).is(":visible")) {
                        $('#unbanForum').removeClass('disabled');
                    }
                    $('#addToGuildButton').removeClass('disabled');
                    $(banGuildConfirmButton).removeClass('disabled btn-success').addClass('btn-primary').html('<i class="fa fa-check"></i> Proceed and ban the user');

                    if ($('#inForumIndicator').hasClass('btn-success')) {
                        $('#addToGuild').removeClass('disabled').addClass('btn-primary');
                    }

                    $(guildBannedIndicator).slideAnimateOut(400, true);
                    if ($(forumBannedIndicator).css('display') === 'none' && $(manageBanButton).css('display') !== 'none') {
                        $('#manageBanButton').slideAnimateOut(400, true);
                    }
                }

            } else {
                $(banGuildConfirmButton).removeClass('btn-primary').addClass('btn-danger').html('<i class="fa fa-close"></i> Unable to ban the user');
            }

        }
    });

}

function deleteUserScreenshot() {

    var url,
        currentElement = screenshotGallery.currItem.el;

    currentElement = jQuery(currentElement);

    screenshotGallery.close();
    url = '/user/' + currentUser + '/screenshot/delete/' + currentElement.attr('id').replace('screenshot', '');

    $('#galleryThumbnailsContainer').isotope( 'remove', currentElement ).isotope('layout');

    $.ajax({
        type: "GET",
        url: url
    });

    if ($('#galleryThumbnailsContainer > figure').length <= 16) {
        $('#seeMoreScreenshotsBtn div button').slideAnimateOut(200);
    }

    updateUserUploadCount('add');
}

function updateProfileScreenshotList(response) {
    updateUserUploadCount('remove');

    if ($('#seeMoreScreenshotsBtn div button').not(':visible')) {
        var id = $(response).attr('id');
        $(response).imagesLoaded(function () {
            $('#galleryThumbnailsContainer').isotope( 'insert', $(response) );
            $('#' + id).slideAnimate(400);
        });
    }
}

function updateUserUploadCount(action) {
    var newCount;
    var screenshotCounter = $('#screenshotUploadCount');
    var screenshotsEmpty = $('#screenshotsEmpty');
    var noUploadsLeft = $('#noUploadsLeft');
    var seeAllScreenshotsButton = $('#seeAllScreenshotsBtn');

    if (action === 'add') {
        newCount = parseInt(screenshotCounter.html()) + 1;
        screenshotCounter.html(newCount);
    } else {
        newCount = parseInt(screenshotCounter.html()) - 1;
        screenshotCounter.html(newCount);
    }

    if (newCount === 50) {
        $(screenshotsEmpty).slideAnimate(400);
    } else if ($(screenshotsEmpty).is(':visible')) {
        $(screenshotsEmpty).slideAnimateOut(200);
    }

    if (newCount > 34 && $(seeAllScreenshotsButton).is(':visible')) {
        $(seeAllScreenshotsButton).slideAnimateOut(200);
    }

    if (newCount === 0) {
        $(noUploadsLeft).slideAnimate(200);
    } else if ($(noUploadsLeft).is(':visible')) {
        $(noUploadsLeft).slideAnimateOut(200);
    }

}

function updateProfileAvatar(response) {

    var avatarContainer = $('#avatarContainer');

    $(response).appendTo(avatarContainer).imagesLoaded(function () {
        $(avatarContainer).find('img').not('.uploaded').fadeOut(200).promise().done(function () {
            $(avatarContainer).find('.uploaded').fadeIn(500).removeClass('uploaded');
        });

    });

}

function loadMoreProfileScreenshots() {
    var loadMoreButton = $('#seeMoreScreenshotsBtn div button');
    var originalButton = loadMoreButton.html();
    var thumbnailsContainer = $('#galleryThumbnailsContainer');

    loadMoreButton.addClass('disabled').html('<i class="fa fa-spinner fa-spin"></i> Loading more screenshots, please wait...');

    $.ajax({
        type: "GET",
        url: '/user/' + currentProfile + '/screenshots/load/' + $('#galleryThumbnailsContainer > figure').length,
        dataType: 'html',
        success: function (data) {
            $(data).imagesLoaded(function () {
                $(data).appendTo(thumbnailsContainer);

                $('.justLoaded').each(function () {
                    $(this).removeClass('justLoaded');
                    $(thumbnailsContainer).isotope( 'insert', $(this) );
                });
            }).done(function () {
                loadMoreButton.removeClass('disabled').html(originalButton);
                if ($('#galleryThumbnailsContainer > figure').length >= imgCount) {
                    loadMoreButton.slideAnimateOut(200);
                }
            });
        }
    });
}

function detectTimezone() {
    var tz = jstz.determine(),
        elem,
        elemByAttr,
        timezoneSplit = tz.name().split('/')[1];

    elem = $('#grost_user_timezone_timezone > option:contains("' + timezoneSplit + '")');
    elemByAttr = $("[value='" + tz.name() + "']");

    if (elem[0] !== undefined) {
        $(elem[0]).prop('selected', true).trigger("chosen:updated");
    } else if (elemByAttr !== undefined) {
        $(elemByAttr[0]).prop('selected', true).trigger("chosen:updated");
    } else {
        $('#timezoneNotFound').slideAnimate(200);
        return;
    }

    $('#timezoneFound').slideAnimate(200);
}

function detectTimezoneRegister() {
    var tz = jstz.determine(),
        elem,
        elemByAttr,
        timezoneSplit = tz.name().split('/')[1];

    elem = $('#fos_user_registration_form_timezone > option:contains("' + timezoneSplit + '")');
    elemByAttr = $("[value='" + tz.name() + "']");

    if (elem[0] !== undefined) {
        $(elem[0]).prop('selected', true).trigger("chosen:updated");
    } else if (elemByAttr !== undefined) {
        $(elemByAttr[0]).prop('selected', true).trigger("chosen:updated");
    }
}

function checkRegisterPasswordMatch() {
    var password = $("#fos_user_registration_form_plainPassword_first").val(),
        confirmPassword = $("#fos_user_registration_form_plainPassword_second").val();

    if (password !== confirmPassword && confirmPassword !== '') {
        $(".password-match").fadeIn(100);
        return false;
    } else {
        $(".password-match").fadeOut(100);
        return true;
    }

}

$(".grost-register").on("submit", function() {

    if (checkRegisterPasswordMatch()) {
        return;
    }

    $('#pass-match').slideAnimate(150);

    return false;
});

function updateProfileCover(response) {

    var coverContainer = $('#coverContainer');

    $(response).appendTo(coverContainer).imagesLoaded(function () {
        $(coverContainer).find('img').not('.uploaded').fadeOut(200).promise().done(function () {
            $(coverContainer).find('.uploaded').fadeIn(500).removeClass('uploaded');
        });

    });

}

function checkReqPasswordMatch() {
    var password = $("#fos_user_resetting_form_plainPassword_first").val(),
        confirmPassword = $("#fos_user_resetting_form_plainPassword_second").val();

    if (password !== confirmPassword && confirmPassword !== '') {
        $(".password-match").fadeIn(100);
        return false;
    } else {
        $(".password-match").fadeOut(100);
        return true;
    }

}

$(".reset-pw").on("submit", function() {

    if (checkReqPasswordMatch()) {
        return;
    }

    $('#pass-match').slideAnimate(150);

    return false;
});

function moveThread() {

    var target = $('#moveToSelect :selected').attr('value'),
        url = '/admin/forum/move/thread/' + currentThread + '/to/' + target;

    window.location.replace(url);

    $('#moveConfirm').addClass('disabled').html('<i class="fa fa-spinner fa-spin"></i> Confirmed, moving the thread...');
}

function backToTop() {
    $('html, body').animate({
        scrollTop: 0
    }, 800);
}

function slugify(text)
{
    var defaultDiacriticsRemovalMap = [
        {'base':'A', 'letters':/[\u0041\u24B6\uFF21\u00C0\u00C1\u00C2\u1EA6\u1EA4\u1EAA\u1EA8\u00C3\u0100\u0102\u1EB0\u1EAE\u1EB4\u1EB2\u0226\u01E0\u00C4\u01DE\u1EA2\u00C5\u01FA\u01CD\u0200\u0202\u1EA0\u1EAC\u1EB6\u1E00\u0104\u023A\u2C6F]/g},
        {'base':'AA','letters':/[\uA732]/g},
        {'base':'AE','letters':/[\u00C6\u01FC\u01E2]/g},
        {'base':'AO','letters':/[\uA734]/g},
        {'base':'AU','letters':/[\uA736]/g},
        {'base':'AV','letters':/[\uA738\uA73A]/g},
        {'base':'AY','letters':/[\uA73C]/g},
        {'base':'B', 'letters':/[\u0042\u24B7\uFF22\u1E02\u1E04\u1E06\u0243\u0182\u0181]/g},
        {'base':'C', 'letters':/[\u0043\u24B8\uFF23\u0106\u0108\u010A\u010C\u00C7\u1E08\u0187\u023B\uA73E]/g},
        {'base':'D', 'letters':/[\u0044\u24B9\uFF24\u1E0A\u010E\u1E0C\u1E10\u1E12\u1E0E\u0110\u018B\u018A\u0189\uA779]/g},
        {'base':'DZ','letters':/[\u01F1\u01C4]/g},
        {'base':'Dz','letters':/[\u01F2\u01C5]/g},
        {'base':'E', 'letters':/[\u0045\u24BA\uFF25\u00C8\u00C9\u00CA\u1EC0\u1EBE\u1EC4\u1EC2\u1EBC\u0112\u1E14\u1E16\u0114\u0116\u00CB\u1EBA\u011A\u0204\u0206\u1EB8\u1EC6\u0228\u1E1C\u0118\u1E18\u1E1A\u0190\u018E]/g},
        {'base':'F', 'letters':/[\u0046\u24BB\uFF26\u1E1E\u0191\uA77B]/g},
        {'base':'G', 'letters':/[\u0047\u24BC\uFF27\u01F4\u011C\u1E20\u011E\u0120\u01E6\u0122\u01E4\u0193\uA7A0\uA77D\uA77E]/g},
        {'base':'H', 'letters':/[\u0048\u24BD\uFF28\u0124\u1E22\u1E26\u021E\u1E24\u1E28\u1E2A\u0126\u2C67\u2C75\uA78D]/g},
        {'base':'I', 'letters':/[\u0049\u24BE\uFF29\u00CC\u00CD\u00CE\u0128\u012A\u012C\u0130\u00CF\u1E2E\u1EC8\u01CF\u0208\u020A\u1ECA\u012E\u1E2C\u0197]/g},
        {'base':'J', 'letters':/[\u004A\u24BF\uFF2A\u0134\u0248]/g},
        {'base':'K', 'letters':/[\u004B\u24C0\uFF2B\u1E30\u01E8\u1E32\u0136\u1E34\u0198\u2C69\uA740\uA742\uA744\uA7A2]/g},
        {'base':'L', 'letters':/[\u004C\u24C1\uFF2C\u013F\u0139\u013D\u1E36\u1E38\u013B\u1E3C\u1E3A\u0141\u023D\u2C62\u2C60\uA748\uA746\uA780]/g},
        {'base':'LJ','letters':/[\u01C7]/g},
        {'base':'Lj','letters':/[\u01C8]/g},
        {'base':'M', 'letters':/[\u004D\u24C2\uFF2D\u1E3E\u1E40\u1E42\u2C6E\u019C]/g},
        {'base':'N', 'letters':/[\u004E\u24C3\uFF2E\u01F8\u0143\u00D1\u1E44\u0147\u1E46\u0145\u1E4A\u1E48\u0220\u019D\uA790\uA7A4]/g},
        {'base':'NJ','letters':/[\u01CA]/g},
        {'base':'Nj','letters':/[\u01CB]/g},
        {'base':'O', 'letters':/[\u004F\u24C4\uFF2F\u00D2\u00D3\u00D4\u1ED2\u1ED0\u1ED6\u1ED4\u00D5\u1E4C\u022C\u1E4E\u014C\u1E50\u1E52\u014E\u022E\u0230\u00D6\u022A\u1ECE\u0150\u01D1\u020C\u020E\u01A0\u1EDC\u1EDA\u1EE0\u1EDE\u1EE2\u1ECC\u1ED8\u01EA\u01EC\u00D8\u01FE\u0186\u019F\uA74A\uA74C]/g},
        {'base':'OI','letters':/[\u01A2]/g},
        {'base':'OO','letters':/[\uA74E]/g},
        {'base':'OU','letters':/[\u0222]/g},
        {'base':'P', 'letters':/[\u0050\u24C5\uFF30\u1E54\u1E56\u01A4\u2C63\uA750\uA752\uA754]/g},
        {'base':'Q', 'letters':/[\u0051\u24C6\uFF31\uA756\uA758\u024A]/g},
        {'base':'R', 'letters':/[\u0052\u24C7\uFF32\u0154\u1E58\u0158\u0210\u0212\u1E5A\u1E5C\u0156\u1E5E\u024C\u2C64\uA75A\uA7A6\uA782]/g},
        {'base':'S', 'letters':/[\u0053\u24C8\uFF33\u1E9E\u015A\u1E64\u015C\u1E60\u0160\u1E66\u1E62\u1E68\u0218\u015E\u2C7E\uA7A8\uA784]/g},
        {'base':'T', 'letters':/[\u0054\u24C9\uFF34\u1E6A\u0164\u1E6C\u021A\u0162\u1E70\u1E6E\u0166\u01AC\u01AE\u023E\uA786]/g},
        {'base':'TZ','letters':/[\uA728]/g},
        {'base':'U', 'letters':/[\u0055\u24CA\uFF35\u00D9\u00DA\u00DB\u0168\u1E78\u016A\u1E7A\u016C\u00DC\u01DB\u01D7\u01D5\u01D9\u1EE6\u016E\u0170\u01D3\u0214\u0216\u01AF\u1EEA\u1EE8\u1EEE\u1EEC\u1EF0\u1EE4\u1E72\u0172\u1E76\u1E74\u0244]/g},
        {'base':'V', 'letters':/[\u0056\u24CB\uFF36\u1E7C\u1E7E\u01B2\uA75E\u0245]/g},
        {'base':'VY','letters':/[\uA760]/g},
        {'base':'W', 'letters':/[\u0057\u24CC\uFF37\u1E80\u1E82\u0174\u1E86\u1E84\u1E88\u2C72]/g},
        {'base':'X', 'letters':/[\u0058\u24CD\uFF38\u1E8A\u1E8C]/g},
        {'base':'Y', 'letters':/[\u0059\u24CE\uFF39\u1EF2\u00DD\u0176\u1EF8\u0232\u1E8E\u0178\u1EF6\u1EF4\u01B3\u024E\u1EFE]/g},
        {'base':'Z', 'letters':/[\u005A\u24CF\uFF3A\u0179\u1E90\u017B\u017D\u1E92\u1E94\u01B5\u0224\u2C7F\u2C6B\uA762]/g},
        {'base':'a', 'letters':/[\u0061\u24D0\uFF41\u1E9A\u00E0\u00E1\u00E2\u1EA7\u1EA5\u1EAB\u1EA9\u00E3\u0101\u0103\u1EB1\u1EAF\u1EB5\u1EB3\u0227\u01E1\u00E4\u01DF\u1EA3\u00E5\u01FB\u01CE\u0201\u0203\u1EA1\u1EAD\u1EB7\u1E01\u0105\u2C65\u0250]/g},
        {'base':'aa','letters':/[\uA733]/g},
        {'base':'ae','letters':/[\u00E6\u01FD\u01E3]/g},
        {'base':'ao','letters':/[\uA735]/g},
        {'base':'au','letters':/[\uA737]/g},
        {'base':'av','letters':/[\uA739\uA73B]/g},
        {'base':'ay','letters':/[\uA73D]/g},
        {'base':'b', 'letters':/[\u0062\u24D1\uFF42\u1E03\u1E05\u1E07\u0180\u0183\u0253]/g},
        {'base':'c', 'letters':/[\u0063\u24D2\uFF43\u0107\u0109\u010B\u010D\u00E7\u1E09\u0188\u023C\uA73F\u2184]/g},
        {'base':'d', 'letters':/[\u0064\u24D3\uFF44\u1E0B\u010F\u1E0D\u1E11\u1E13\u1E0F\u0111\u018C\u0256\u0257\uA77A]/g},
        {'base':'dz','letters':/[\u01F3\u01C6]/g},
        {'base':'e', 'letters':/[\u0065\u24D4\uFF45\u00E8\u00E9\u00EA\u1EC1\u1EBF\u1EC5\u1EC3\u1EBD\u0113\u1E15\u1E17\u0115\u0117\u00EB\u1EBB\u011B\u0205\u0207\u1EB9\u1EC7\u0229\u1E1D\u0119\u1E19\u1E1B\u0247\u025B\u01DD]/g},
        {'base':'f', 'letters':/[\u0066\u24D5\uFF46\u1E1F\u0192\uA77C]/g},
        {'base':'g', 'letters':/[\u0067\u24D6\uFF47\u01F5\u011D\u1E21\u011F\u0121\u01E7\u0123\u01E5\u0260\uA7A1\u1D79\uA77F]/g},
        {'base':'h', 'letters':/[\u0068\u24D7\uFF48\u0125\u1E23\u1E27\u021F\u1E25\u1E29\u1E2B\u1E96\u0127\u2C68\u2C76\u0265]/g},
        {'base':'hv','letters':/[\u0195]/g},
        {'base':'i', 'letters':/[\u0069\u24D8\uFF49\u00EC\u00ED\u00EE\u0129\u012B\u012D\u00EF\u1E2F\u1EC9\u01D0\u0209\u020B\u1ECB\u012F\u1E2D\u0268\u0131]/g},
        {'base':'j', 'letters':/[\u006A\u24D9\uFF4A\u0135\u01F0\u0249]/g},
        {'base':'k', 'letters':/[\u006B\u24DA\uFF4B\u1E31\u01E9\u1E33\u0137\u1E35\u0199\u2C6A\uA741\uA743\uA745\uA7A3]/g},
        {'base':'l', 'letters':/[\u006C\u24DB\uFF4C\u0140\u013A\u013E\u1E37\u1E39\u013C\u1E3D\u1E3B\u017F\u0142\u019A\u026B\u2C61\uA749\uA781\uA747]/g},
        {'base':'lj','letters':/[\u01C9]/g},
        {'base':'m', 'letters':/[\u006D\u24DC\uFF4D\u1E3F\u1E41\u1E43\u0271\u026F]/g},
        {'base':'n', 'letters':/[\u006E\u24DD\uFF4E\u01F9\u0144\u00F1\u1E45\u0148\u1E47\u0146\u1E4B\u1E49\u019E\u0272\u0149\uA791\uA7A5]/g},
        {'base':'nj','letters':/[\u01CC]/g},
        {'base':'o', 'letters':/[\u006F\u24DE\uFF4F\u00F2\u00F3\u00F4\u1ED3\u1ED1\u1ED7\u1ED5\u00F5\u1E4D\u022D\u1E4F\u014D\u1E51\u1E53\u014F\u022F\u0231\u00F6\u022B\u1ECF\u0151\u01D2\u020D\u020F\u01A1\u1EDD\u1EDB\u1EE1\u1EDF\u1EE3\u1ECD\u1ED9\u01EB\u01ED\u00F8\u01FF\u0254\uA74B\uA74D\u0275]/g},
        {'base':'oi','letters':/[\u01A3]/g},
        {'base':'ou','letters':/[\u0223]/g},
        {'base':'oo','letters':/[\uA74F]/g},
        {'base':'p','letters':/[\u0070\u24DF\uFF50\u1E55\u1E57\u01A5\u1D7D\uA751\uA753\uA755]/g},
        {'base':'q','letters':/[\u0071\u24E0\uFF51\u024B\uA757\uA759]/g},
        {'base':'r','letters':/[\u0072\u24E1\uFF52\u0155\u1E59\u0159\u0211\u0213\u1E5B\u1E5D\u0157\u1E5F\u024D\u027D\uA75B\uA7A7\uA783]/g},
        {'base':'s','letters':/[\u0073\u24E2\uFF53\u00DF\u015B\u1E65\u015D\u1E61\u0161\u1E67\u1E63\u1E69\u0219\u015F\u023F\uA7A9\uA785\u1E9B]/g},
        {'base':'t','letters':/[\u0074\u24E3\uFF54\u1E6B\u1E97\u0165\u1E6D\u021B\u0163\u1E71\u1E6F\u0167\u01AD\u0288\u2C66\uA787]/g},
        {'base':'tz','letters':/[\uA729]/g},
        {'base':'u','letters':/[\u0075\u24E4\uFF55\u00F9\u00FA\u00FB\u0169\u1E79\u016B\u1E7B\u016D\u00FC\u01DC\u01D8\u01D6\u01DA\u1EE7\u016F\u0171\u01D4\u0215\u0217\u01B0\u1EEB\u1EE9\u1EEF\u1EED\u1EF1\u1EE5\u1E73\u0173\u1E77\u1E75\u0289]/g},
        {'base':'v','letters':/[\u0076\u24E5\uFF56\u1E7D\u1E7F\u028B\uA75F\u028C]/g},
        {'base':'vy','letters':/[\uA761]/g},
        {'base':'w','letters':/[\u0077\u24E6\uFF57\u1E81\u1E83\u0175\u1E87\u1E85\u1E98\u1E89\u2C73]/g},
        {'base':'x','letters':/[\u0078\u24E7\uFF58\u1E8B\u1E8D]/g},
        {'base':'y','letters':/[\u0079\u24E8\uFF59\u1EF3\u00FD\u0177\u1EF9\u0233\u1E8F\u00FF\u1EF7\u1E99\u1EF5\u01B4\u024F\u1EFF]/g},
        {'base':'z','letters':/[\u007A\u24E9\uFF5A\u017A\u1E91\u017C\u017E\u1E93\u1E95\u01B6\u0225\u0240\u2C6C\uA763]/g}
    ];

    for (var i = 0; i < defaultDiacriticsRemovalMap.length; i++) {
        text = text.replace(defaultDiacriticsRemovalMap[i].letters, defaultDiacriticsRemovalMap[i].base);
    }

    return text.toString().toLowerCase()
        .replace(/\s+/g, '-')
        .replace(/[^\w\-]+/g, '')
        .replace(/\-\-+/g, '-')
        .replace(/^-+/, '')
        .replace(/-+$/, '');
}

$("#createGuildForm").on("submit", function() {

    var form = $("#createGuildForm"),
        submitBtn = $('#submitBtn'),
        formErrorAlert = $('#formInvalid'),
        nameTakenAlert = $('#nameTaken');

    $(submitBtn).html('<i class="fa fa-spinner fa-spin"></i> Submited, please wait...').addClass('disabled');

    $.ajax({
        url: $(form).attr('action'),
        type: 'POST',
        data: $(form).serialize(),

        success: function (data) {
            if (data.created) {
                Cookies.set('guildtour-' +  data.slug, true, { path: '/', domain: window.location.host, expires: 7 });
                window.location.replace('http://' + data.slug + '.' + window.location.host);
            } else if (data.alreadyTaken) {
                $(nameTakenAlert).slideAnimate(200);
                $(submitBtn).html('<i class="fa fa-check"></i> Submit & create the guild').removeClass('disabled');

                if ($(formErrorAlert).is(':visible')) {
                    $(formErrorAlert).slideAnimateOut(200);
                }

            }
        },
        error: function() {
            $(submitBtn).html('<i class="fa fa-check"></i> Submit & create the guild').removeClass('disabled');
            $(formErrorAlert).slideAnimate(200);
        }
    });
    return false;
});

function disableGuildTour() {

    var host = window.location.host.replace(guildSubdomain + ".", "");

    Cookies.remove('guildtour-' +  guildSubdomain, { path: '/', domain: host });
}

$("#aboutMeUpdate").on("submit", function() {

    var form = $("#aboutMeUpdate"),
        newAbout = $('#grost_about_me_update_bio').val(),
        submitBtn = $('#bioSubmitBtn');

    $(submitBtn).html('<i class="fa fa-spinner fa-spin"></i> Submitted, please wait...').addClass('disabled');

    $.ajax({
        url: $(form).attr('action'),
        type: 'POST',
        data: $(form).serialize(),

        success: function () {
            $('#aboutMeContainer').html(newAbout);
            $(submitBtn).html('<i class="fa fa-check"></i> Updated!');
        },
        error: function() {
            $(submitBtn).html('<i class="fa fa-check"></i> An error occurred');
            $('#aboutMeError').slideAnimate(200);
        }
    });


    setTimeout(function(){
        $(submitBtn).html('<i class="fa fa-check"></i> Update').removeClass('disabled');
    }, 2000);

    return false;
});


$("#chatForm").on("submit", function() {

    var form = $("#chatForm"),
        chatInputWrap = $("#chatInput"),
        chatInput = $("#form_message"),
        chatMsg = $(chatInput).val();

    if ($(chatInput).hasClass('disabled')) {
        return false;
    }

    $(chatInput).addClass('disabled');

    $.ajax({
        url: $(form).attr('action'),
        type: 'POST',
        data: $(form).serialize(),

        success: function () {
            $(chatInputWrap).addClass('has-success has-feedback');
            $(chatInput).removeClass('disabled');
            $(chatInput).val('');
        },
        error: function() {
            $(chatInputWrap).addClass('has-error has-feedback');
            $(chatInput).attr('placeholder', 'An error occurred, please refresh the page.');
            $(chatInput).val('');

            setTimeout(function(){
                $(chatInput).val(chatMsg);
            }, 3000);
            return false;
        }
    });

    setTimeout(function(){
        $(chatInputWrap).removeClass('has-success has-feedback');
        $(chatInput).removeClass('disabled');
    }, 1500);

    return false;
});

function getDomain() {
  return location.hostname.split('.').slice(-2).join('.');
}

function initGChatMembers(member) {
    var newGChatMember = '<li id="gchat-' + member.id + '"><strong title="' + member.info.username + '"><a target="_blank" class="chat-user-list" href="https://' + getDomain() + '/user/' + member.info.username + '">' + member.info.username + '</a></strong></li>',
        usrLoadingText = $('#usrLoading');

    $(newGChatMember).hide().appendTo('.chat-users-list').fadeIn(150);

    $(usrLoadingText).fadeOut(150).promise().done(function () {
        $(usrLoadingText).remove();
    });

    $(".chat-usr-container").nanoScroller();
}

function addGChatMember(member) {

    if($("#gchat-" + member.id).length === 0) {
        var newGChatMember = '<li id="gchat-' + member.id + '"><strong title="' + member.info.username + '"><a target="_blank" class="chat-user-list" href="https://' + getDomain() + '/user/' + member.info.username + '">' + member.info.username + '</a></strong></li>';
        $(newGChatMember).hide().appendTo('.chat-users-list').fadeIn(100);
        notifyNewGChatUser(member);
    }

}

function removeGChatMember(member) {
    var memberId = "#gchat-" + member.id;

    $(memberId).slideAnimateOut(100).done(function () {
        $(memberId).remove();
    });

}

function notifyNewGChatUser(member) {
    var panelTitle = $('#GChatPanelTitle'),
        notificationTemplate = '<span id="gChatNotif" class="pull-right"><i class="fa fa-user-plus"></i> <strong>[' + member.info.username + '<span class="newGUNotif"></span>]</strong> <span class="gChatHasHave">has</span> joined the chat!</span>',
        newUsernameSpan = $('.newGUNotif');

    if ($(newUsernameSpan).length) {
        $(newUsernameSpan).append(' & ' + member.info.username);
        $('.gChatHasHave').text('have');
        return;
    }

    $(notificationTemplate).hide().appendTo(panelTitle).fadeIn(100).promise().done(function () {
        setTimeout(function(){
            $('#gChatNotif').fadeOut(150).promise().done(function () {
                $('#gChatNotif').remove();
            });
        }, 2500);
    });

}

function guildLogoUploaded(data) {

    var togglesWrap = $('#toggleLogo'),
        enableButton = $('#logoDisable'),
        disableButton = $('#logoEnable');

    if (data.logoUploaded) {
        $('#uploadSuccess').slideAnimate(200);

        if ($(togglesWrap).not(':visible')) {
            $(togglesWrap).slideAnimate(100);
        }

        if (data.logoEnabled) {
            $(disableButton).slideAnimate(100);
            $(enableButton).slideAnimateOut(100);
        } else {
            $(disableButton).slideAnimateOut(100);
            $(enableButton).slideAnimate(100);
        }
    }
}

function toggleThreadLock(url) {
    var modToolsCog = $('#modToolsCog'),
        toggleLockBtn = $('#toggleLockBtn');

    $(modToolsCog).addClass('fa-spin');
    $(toggleLockBtn).addClass('a-disabled');

    $.ajax({
        type: "GET",
        url: url,
        success: function(data){
            $(modToolsCog).removeClass('fa-spin');
            $(toggleLockBtn).removeClass('a-disabled');
        },
        error: function() {
            $(modToolsCog).removeClass('fa-spin');
            $(toggleLockBtn).removeClass('a-disabled');
        }
    });
}

function toggleThreadPin(url) {
    $.ajax({
        type: "GET",
        url: url,
        success: function(data) {

            var pinAlertText = data.isPinned ? "pinned" : "unpinned";
            var pinButtonText = data.isPinned ? "Unpin" : "Pin";

            $('#toggleThreadPin').text(pinButtonText + ' this thread');

            $.growl({
                icon: 'fa fa-check',
                title: ' <strong>Success!</strong><br>',
                message: 'The thread has been ' + pinAlertText
            }, {
                animate: {
                    enter: 'animated bounceInDown',
                    exit: 'animated bounceOutRight'
                },
                type: 'success',
                delay: 5000,
                offset: {
                    y: 60,
                    x: 10
                }
            });
        },
        error: function() {
            console.error('Unable to toggle the thread pin');
        }
    });
}

function isForumSearchValid() {
    var searchInput = $('#forumSearchInput'),
        searchButton = $('#forumSearchButton'),
        searchFormGroup = $('#searchFormGroup'),
        searchFormErrIcon = $('#searchFormErrorIcon'),
        rawSearchQuery = $(searchInput).val().trim(),
        isSearchValid = true;


    if (rawSearchQuery.length < 3) {
        $(searchInput).addClass('disabled').val('The query is too short! (3 char. min.)');
        isSearchValid = false;
    } else if (rawSearchQuery.length > 40) {
        $(searchInput).addClass('disabled').val('The query is too long! (40 char. max.)');
        isSearchValid = false;
    }

    if (isSearchValid === false) {
        $(searchButton).addClass('disabled btn-danger').removeClass('btn-primary');
        $(searchFormErrIcon).fadeIn('fast');
        $(searchFormGroup).addClass('has-error has-feedback');

        setTimeout(function(){
            $(searchButton).addClass('btn-primary').removeClass('disabled btn-danger');
            $(searchInput).removeClass('disabled').val(rawSearchQuery);
            $(searchFormErrIcon).fadeOut('fast').promise().done(function () {
                $(searchFormGroup).removeClass('has-error has-feedback');
            });
        }, 2000);
    }

    return isSearchValid;

}

function executeForumSearch() {

    var searchInput = $('#forumSearchInput'),
        rawSearchQuery = $(searchInput).val().trim(),
        encodedSearchQuery = encodeURI(rawSearchQuery.replace(/ +(?= )/g,''));

    if (!isForumSearchValid()) {
        return false;
    }

    $(searchInput).addClass('disabled');
    $('#forumSearchButton').addClass('disabled').html('<i class="fa fa-spinner fa-spin"></i><span class="hidden-md hidden-lg"> Search</span>');

    if (typeof subcatSlug !== 'undefined') {
        window.location.href = '/forum/search/' + subcatSlug + '/' + encodedSearchQuery;
    } else {
        window.location.href = '/forum/search/' + encodedSearchQuery;
    }

}

$("#forumSearchForm").on("submit", function() {
    executeForumSearch();
    return false;
});
