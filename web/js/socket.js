/*global $ */
/*
 * EVENT PAGE SCRIPTS [START]
 *
 * Scripts handling all the inbound messages coming from the websocket server and modify the DOM accordingly.
 * */

// Triggered when the state of a registration switch
var registrationStateSwitch = function (data) {
    "use strict";
    var registrationId = '#reg-' + data.template[0].registrationId,
        newTotalInCurrentState = parseInt($('#state-' + data.template[0].originalState + '-total').html()) - 1,
        startIdOldState = '#state-' + data.template[0].originalState,
        startIdNewState = '#state-' + data.template[0].newState;

    $(registrationId).parent().toggleAnimate(400, false);

    $(startIdOldState + '-total').html(newTotalInCurrentState);

    if (newTotalInCurrentState > 1) {
        $(startIdOldState + '-user').html('Users');
    } else {
        $(startIdOldState + '-user').html('User');
    }

    $(":animated").promise().done(function () {
        $(registrationId).remove();


        var newTotalInNewState = parseInt($(startIdNewState + '-total').html()) + 1;

        $(startIdNewState + '-total').html(newTotalInNewState);
        if (newTotalInNewState > 1) {
            $(startIdNewState + '-user').html('Users');
        } else {
            $(startIdNewState + '-user').html('User');
        }

        $(startIdNewState).append($(data.template[0].newList).hide());
        $('.appended-event-user').toggleAnimate(400, false);

        if (permissions.can_manage_event) {
            $(startIdNewState + ' .event-user-wrap .btn-group').children().removeAttr("disabled");
        }

        if (data.additionalInfo.userModified !== null && currentUser === data.additionalInfo.userModified && data.additionalInfo.modified === true) {

            var inviteButtonRow = $('#inviteBtnRow');

            if ($(inviteButtonRow).is(':visible')) {
                $(inviteButtonRow).slideAnimateOut(200);
            }
        }

        if (data.additionalInfo.userModified !== null && currentUser === data.additionalInfo.userModified) {

            var cancelRegistration = $('#cancelRegistration');
            var resetRegistration = $('#resetRegistration');

            if (data.additionalInfo.registrationCancelled === false && parseInt(data.template[0].newState, 10) === 2 && data.additionalInfo.modified === true) {

                if (!$(cancelRegistration).is(':visible')) {
                    $(cancelRegistration).slideAnimate(200);
                }
            }

            if (data.additionalInfo.registrationCancelled === false && parseInt(data.template[0].newState, 10) === 1) {

                if ($(resetRegistration).is(':visible')) {
                    $(resetRegistration).slideAnimateOut(200);
                }

                if (data.additionalInfo.modified) {
                    $(cancelRegistration).slideAnimate(200);
                }

            }

            if (data.additionalInfo.registrationCancelled === true && parseInt(data.template[0].newState, 10) === 3 && data.additionalInfo.modified === true) {

                if ($(cancelRegistration).is(':visible')) {
                    $(cancelRegistration).slideAnimateOut(200);
                }

                if (!$(resetRegistration).is(':visible')) {
                    $(resetRegistration).slideAnimate(200);
                }
            }

            if (data.additionalInfo.registrationCancelled === false && (parseInt(data.template[0].newState, 10) === 3 || parseInt(data.template[0].newState, 10) === 2)) {

                if (!$(cancelRegistration).is(':visible')) {
                    $(cancelRegistration).slideAnimate(200);
                }
            }

            if (data.additionalInfo.registrationReset === true) {

                if (!$(resetRegistration).is(':visible')) {
                    $(resetRegistration).slideAnimateOut(200);
                }
            }
        }


        $(registrationId).parent().removeClass('appended-event-user');
    });
},

// Triggered when a user registered to an event
registrationNewUser = function (data) {
    var registrationId = '#reg-' + data.template[0].registrationId,
        newTotalInNewState = parseInt($('#state-' + data.template[0].newState + '-total').html()) + 1,
        startIdNewState = '#state-' + data.template[0].newState;

    $(startIdNewState + '-total').html(newTotalInNewState);
    if (newTotalInNewState > 1) {
        $(startIdNewState + '-user').html('Users');
    } else {
        $(startIdNewState + '-user').html('User');
    }

    $(startIdNewState).append($(data.template[0].newList).hide());
    $('.appended-event-user').toggleAnimate(400, false);

    if (permissions.can_manage_event) {
        $(startIdNewState + ' .event-user-wrap .btn-group').children().removeAttr("disabled");
    }

    if (data.additionalInfo.userModified !== null && currentUser === data.additionalInfo.userModified && data.additionalInfo.isInvite === true) {

        var inviteButtonRow = $('#inviteBtnRow');
        var joinEventRow = $('#joinEventRow');

        if (!$(inviteButtonRow).is(':visible')) {
            $(inviteButtonRow).slideAnimate(200);
        }
        if ($(joinEventRow).is(':visible')) {
            $(joinEventRow).slideAnimateOut(200);
        }
    } else if (data.additionalInfo.userModified !== null && currentUser === data.additionalInfo.userModified && data.additionalInfo.isInvite === false) {

        var cancelRegistration = $('#cancelRegistration');

        if (!$(cancelRegistration).is(':visible')) {
            $(cancelRegistration).slideAnimate(200);
        }
    }

    $(registrationId).parent().removeClass('appended-event-user');
},

eventRegistrationAccessToggle = function (data) {
    "use strict";

    if (data.additionalInfo.isOpened) {
        $('#joinEventButton').addClass('btn-primary').removeClass('disabled').html('<i class="fa fa-plus-square"></i> Join this event and wait for approval');
        $('#toggleRegistrations')
            .html('<i class="fa fa-close"></i> Close the registrations')
            .removeClass('disabled btn-success')
            .addClass('btn-danger')
            .data('action', 'close');
    } else {
        $('#joinEventButton').addClass('disabled').removeClass('btn-primary').html('<i class="fa fa-plus-square"></i> The registrations have been closed');
        $('#toggleRegistrations')
            .html('<i class="fa fa-check"></i> Open the registrations')
            .removeClass('disabled btn-danger')
            .addClass('btn-success')
            .data('action', 'open');
    }
},
/* EVENT PAGE SCRIPTS [END] */


/*
 * COMMENT INBOUND SCRIPTS [START]
 *
 * These scripts are universal and used on all the pages that have comments on them
 * */
// Triggered when a new comment is posted
newMessage = function (data) {
    var newComment = $(data.template);

    $("#comments").append(newComment);

    if (moderationButtonsEnabled) {
        $(newComment).find('.remove-comment').show();
        $('[data-toggle="popover"]').popover({html: 'true'});
    }

    var noCommentText = $("#noCommentText");
    if ($(noCommentText).length === 1) {
        $(noCommentText).toggleAnimate('slow');
        setTimeout(function () {
            $(noCommentText).remove();
            newComment.toggleAnimate('slow');
        }, 300);
    } else {
        newComment.toggleAnimate('slow');
    }

},

// Triggered when a response to a comment is posted
newMessageResponse = function (data) {
    var newComment = $(data.template),
        parentId = '#' + data.additionalInfo + "-comment";

    $("#comments").find(parentId).append(newComment);

    if (moderationButtonsEnabled) {
        $(newComment).find('.remove-comment').show();
        $('[data-toggle="popover"]').popover({html: 'true'});
    }

    newComment.toggleAnimate('slow');
},
/* COMMENT INBOUND SCRIPTS [END] */

/*
 * FORUM SCRIPTS [START]
 * */
// Triggered when a message is posted in a thread
forumResponse = function (data) {
    $('#forum-new-messages').append(data.template);
    $(data.additionalInfo.wrapId).slideAnimate(300, false);

    var forumNewMessagesTitle = $('#forum-new-messages-title');
    var newMessageCounter = $('#newMessageCounter');

    if (!$(forumNewMessagesTitle).is(":visible")) {
        $(forumNewMessagesTitle).slideAnimate(200);
    } else if ($(newMessageCounter).length === 0) {
        $(forumNewMessagesTitle).find('h3').html('<span id="newMessageCounter">2</span> New messages:');
    } else {
        $(newMessageCounter).text(parseInt($(newMessageCounter).html()) + 1);
    }

    if (permissions.is_forum_member) {
        $('#quoteBtnWrap' + data.additionalInfo.messageId).slideAnimate(100);
    }

    if (permissions.can_edit_forum_post || data.additionalInfo.sentBy === currentUser) {
        $('#editBtnWrap' + data.additionalInfo.messageId).slideAnimate(100);
    }

    if (permissions.can_delete_forum_post) {
        $('#deleteBtnWrap' + data.additionalInfo.messageId).slideAnimate(100);
    }

    if (typeof currentUser === 'undefined' || currentUser !== data.additionalInfo.sentBy) { // If it isn't the current user or a visitor, play a notification sound
        $('#forumNotification')[0].play();

        $.growl({
            icon: "fa fa-info-circle",
            message: " A new message has been posted in this thread by <strong>" + data.additionalInfo.sentBy + "</strong>"
        }, {
            placement: {
                from: "bottom",
                align: "right"
            }
        });
    }

},

forumMessageEdited = function (data) {

    var contentId = '#msg-f-' + data.additionalInfo.msgId,
        panelId = '#panel-' + data.additionalInfo.msgId;

    if ($(contentId).length) {

        $(contentId).html(data.template);

        $('#chatNotification')[0].play();

        if (!$(panelId).hasClass('panel-primary')) {
            $(panelId).addClass('panel-primary').removeClass('panel-info panel-success');
            var currentHtml = $(panelId + ' .panel-heading h3').html();
            $(panelId + ' .panel-heading h3').html('<strong>[Edited by ' + data.additionalInfo.editedBy + ']</strong> ' + currentHtml);
        }

        $.growl({
            icon: "fa fa-info-circle",
            message: " <strong>" + data.additionalInfo.editedBy + "</strong> Edited a post on this page"
        }, {
            placement: {
                from: "bottom",
                align: "right"
            }
        });
    }

},

threadMoved = function (data) {
    var threadNewLink = $('#moveToThreadLink'),
        threadTitle = threadNewLink.html();

    threadNewLink.html('<a href="' + data.additionalInfo.newPath + '">' + threadTitle + '</a>');

    $('#threadMovedAlert').slideAnimate('200');

    $('#negativeNotification')[0].play();

    if (typeof CKEDITOR !== 'undefined') {
        CKEDITOR.instances['rizzen_forumbundle_message_content'].setReadOnly(true);
        CKEDITOR.instances['form_content'].setReadOnly(true);
    }

    $('#forumEditButton').addClass('disabled');
    $('#submitButton').addClass('disabled');
    $('.btn-panel-title').addClass('disabled');
    $('#deleteMessageButton').addClass('disabled');

},

forumThreadLock = function (data) {

    var threadIsLocked = $('#threadLockedAlert'),
        submitBtn = $('#submitButton'),
        lockBtn = $('#toggleLockBtn');

    if (data.additionalInfo.isLocked) {
        $('#negativeNotification')[0].play();
        $(threadIsLocked).slideAnimate(200);
        if (typeof CKEDITOR !== 'undefined') {
            CKEDITOR.instances['rizzen_forumbundle_message_content'].setReadOnly(true);
        }
        $(submitBtn).addClass('disabled');
        $(lockBtn).text('Unlock this thread');
    } else {
        $('#positiveNotification')[0].play();
        $(threadIsLocked).slideAnimateOut(200);
        if (typeof CKEDITOR !== 'undefined') {
            CKEDITOR.instances['rizzen_forumbundle_message_content'].setReadOnly(false);
        }
        $(submitBtn).removeClass('disabled');
        $(lockBtn).text('Lock this thread');
    }

    if (permissions.can_edit_forum_thread === false && permissions.can_edit_forum_post === false) {
        if (data.additionalInfo.isLocked) {
            $('.btn-panel-title').addClass('disabled');
        } else {
            $('.btn-panel-title').removeClass('disabled');
        }
    } else if (data.additionalInfo.isLocked === false) {
        $('.btn-panel-title').removeClass('disabled');
    }

},
/* FORUM SCRIPTS [END] */

/*
* MESSENGER SCRIPTS [START]
* */
// Triggered when a new message is posted in a conversation

newUnreadConversationMessage = function (data) {
    "use strict";

    if (typeof conversationSlug !== 'undefined' && conversationSlug === data.additionalInfo.conversationSlug) {
        return;
    }

    var labels = $('.new-message-label'),
        labelsCurrentVal = parseInt(labels.first().text(), 10);

    labels.text(labelsCurrentVal + 1);

    if (labelsCurrentVal === 0) {
        labels.fadeIn(150);
    }

    if (data.additionalInfo.sentBy !== currentUser) {
        $('#chatNotification')[0].play();

        $.growl({
            icon: 'fa fa-envelope',
            title: ' <strong>New message!</strong><br>',
            message:
            'New unread message in the conversation: <strong>"' + data.additionalInfo.conversationTitle + '"</strong>' +
            '<br/>' +
            '<small>Click the notification to see the conversation</small>',
            url: data.additionalInfo.conversationUrl
        }, {
            animate: {
                enter: 'animated bounceInDown',
                exit: 'animated bounceOutRight'
            },
            type: 'success',
            url_target: '',
            delay: 10000,
            offset: {
                y: 60,
                x: 10
            }
        });
    }

},

newUnreadConversation = function (data) {
    "use strict";

    var labels = $('.new-message-label'),
        labelsCurrentVal = parseInt(labels.first().text(), 10);

    labels.text(labelsCurrentVal + 1);

    if (labelsCurrentVal === 0) {
        labels.fadeIn(150);
    }

    if (data.additionalInfo.sentBy !== currentUser) {
        $('#chatNotification')[0].play();

        $.growl({
            icon: 'fa fa-envelope',
            title: ' <strong>New conversation!</strong><br>',
            message:
            '<strong>' + data.additionalInfo.sentBy + '</strong> has created a new conversation: <strong>"' + data.additionalInfo.conversationTitle + '"</strong>' +
            '<br/>' +
            '<small>Click the notification to see the conversation</small>',
            url: data.additionalInfo.conversationUrl
        }, {
            animate: {
                enter: 'animated bounceInDown',
                exit: 'animated bounceOutRight'
            },
            type: 'success',
            url_target: '',
            delay: 10000,
            offset: {
                y: 60,
                x: 10
            }
        });
    }

},

conversationResponse = function (data) {

    $('#conversation-new-messages').append(data.template);
    $(data.additionalInfo.wrapId).slideAnimate(300, false);

    var conversationNewMessagesTitle = $('#conversation-new-messages-title');
    var newMessageCounter = $('#newMessageCounter');

    if (!$(conversationNewMessagesTitle).is(":visible")) {
        $(conversationNewMessagesTitle).slideAnimate(200);
    } else if ($(newMessageCounter).length === 0) {
        $(conversationNewMessagesTitle).find('h3').html('<span id="newMessageCounter">2</span> New messages:');
    } else {
        $(newMessageCounter).text(parseInt($(newMessageCounter).html()) + 1);
    }

    $('.msg-count-wrap').html('<span class="msg-count">' + (parseInt($('.msg-count').html()) + 1) + '</span> Messages');

    if (currentUser !== data.additionalInfo.sentBy) { // If it isn't the current user, play a notification sound and tell the server that the user saw the message
        $('#forumNotification')[0].play();
        $.get(
            '/messenger/conversation/' + conversationSlug + '/unread',
            '',
            'text'
        );
    }
},
/* MESSENGER SCRIPTS [END] */

/*
* GUILD CHAT SCRIPTS [START]
* */

guildChatMessage = function (data) {

    var msgDate = moment.unix(data.additionalInfo.timestamp).tz(timezone).format('D MMMM YYYY'),
        msgTime = moment.unix(data.additionalInfo.timestamp).tz(timezone).format('H:mm:ss'),
        chatMessage = '<li class="newGChatMsg"><small title="' + msgDate + '" class="small-lighter chat-timestamp">[' + msgTime + '] </small> <strong>' + data.additionalInfo.sentBy + ':</strong> <span class="guild-chat-msg">' + data.additionalInfo.message + '</span></li>';

    $(chatMessage).hide().appendTo('.chat-msg-list').linkify({linkClass: 'chat-link'}).show();

    $(".msg-container").nanoScroller().nanoScroller({ scroll: 'bottom' });
},

/* GUILD CHAT SCRIPTS [END] */

/*
* NOTIFICATION SCRIPTS [START]
* */
notify = function (data) {
    $.growl({
        icon: data.additionalInfo.icon,
        title: '<strong>' + data.additionalInfo.title + '</strong><br>',
        message: data.template,
        url: data.additionalInfo.url
    }, {
        animate: {
            enter: 'animated bounceInDown',
            exit: 'animated bounceOutRight'
        },
        type: data.additionalInfo.type,
        url_target: '',
        offset: {
            y: 60,
            x: 10
        }
    });
};
/* NOTIFICATION SCRIPTS [END] */

