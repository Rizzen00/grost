/*jslint browser: true*/
/*global $ ,moment */
(function () {
    "use strict";
    moment.locale('en', {
        longDateFormat : {
            LT: "HH:mm",
            LTS: "HH:mm:ss",
            L: "MM/DD/YYYY",
            l: "M/D/YYYY",
            LL: "MMMM Do YYYY",
            ll: "MMM D YYYY",
            LLL: "MMMM Do YYYY LT",
            lll: "MMM D YYYY LT",
            LLLL: "dddd, MMMM Do YYYY LT",
            llll: "ddd, MMM D YYYY LT"
        },
        relativeTime : {
            future: "Starts in %s",
            past:   "Started %s ago",
            s:  "seconds",
            m:  "a minute",
            mm: "%d minutes",
            h:  "an hour",
            hh: "%d hours",
            d:  "a day",
            dd: "%d days",
            M:  "a month",
            MM: "%d months",
            y:  "a year",
            yy: "%d years"
        },
        calendar : {
            lastDay : '[Yesterday at] LT',
            sameDay : '[Today at] LT',
            nextDay : '[Tomorrow at] LT',
            lastWeek : '[last] dddd [at] LT',
            nextWeek : 'dddd [at] LT',
            sameElse : 'L [at] LT'
        }
    });

    function updateClocks() {
        $('#clockDMY').html(moment().tz(timezone).format('D MMMM YYYY'));
        $('#clockTime').html(moment().tz(timezone).format('H:mm:ss'));

        $('.startInWrap .startInDashboard').each(function () {
            $(this).html(moment.unix($(this).data('timestamp')).tz(timezone).fromNow());
        });
    }

    updateClocks();
    $('.upcomingEventTime').each(function () {
        $(this).html(moment.unix($(this).data('timestamp')).tz(timezone).calendar());
    });

    setInterval(updateClocks, 1000);
}());