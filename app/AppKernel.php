<?php

use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Config\Loader\LoaderInterface;

class AppKernel extends Kernel
{
    public function registerBundles()
    {
        $bundles = array(
            new Symfony\Bundle\FrameworkBundle\FrameworkBundle(),
            new Symfony\Bundle\SecurityBundle\SecurityBundle(),
            new Symfony\Bundle\TwigBundle\TwigBundle(),
            new Symfony\Bundle\MonologBundle\MonologBundle(),
            new Symfony\Bundle\SwiftmailerBundle\SwiftmailerBundle(),
            new Symfony\Bundle\AsseticBundle\AsseticBundle(),
            new Doctrine\Bundle\DoctrineBundle\DoctrineBundle(),
            new Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle(),

            // Installed bundles
            new FOS\UserBundle\FOSUserBundle(),
            new Knp\Bundle\TimeBundle\KnpTimeBundle(),
            new Knp\Bundle\PaginatorBundle\KnpPaginatorBundle(),
            new Liip\ImagineBundle\LiipImagineBundle(),
            new Snc\RedisBundle\SncRedisBundle(),
            new JMS\SerializerBundle\JMSSerializerBundle(),
            new Lopi\Bundle\PusherBundle\LopiPusherBundle(),
            new Bert\TimezoneBundle\BertTimezoneBundle(),
            new FLE\Bundle\PostgresqlTypeBundle\FLEPostgresqlTypeBundle(),
            new Doctrine\Bundle\MigrationsBundle\DoctrineMigrationsBundle(),
            new Intaro\PostgresSearchBundle\IntaroPostgresSearchBundle(),
            new Doctrine\Bundle\FixturesBundle\DoctrineFixturesBundle(),

            // My bundles
            new Rizzen\UserBundle\RizzenUserBundle(),
            new Rizzen\MainBundle\RizzenMainBundle(),
            new Rizzen\SocketBundle\RizzenSocketBundle(),
            new Rizzen\ProfileBundle\RizzenProfileBundle(),
            new Rizzen\GuildBundle\RizzenGuildBundle(),
            new Rizzen\ForumBundle\RizzenForumBundle(),
            new Rizzen\BlogBundle\RizzenBlogBundle(),
            new Rizzen\CalendarBundle\RizzenCalendarBundle(),
            new Rizzen\MessengerBundle\RizzenMessengerBundle(),
        );

        if (in_array($this->getEnvironment(), array('dev', 'test'))) {
            $bundles[] = new Symfony\Bundle\DebugBundle\DebugBundle();
            $bundles[] = new Symfony\Bundle\WebProfilerBundle\WebProfilerBundle();
            $bundles[] = new Sensio\Bundle\DistributionBundle\SensioDistributionBundle();
            $bundles[] = new Sensio\Bundle\GeneratorBundle\SensioGeneratorBundle();
        }

        return $bundles;
    }

    public function registerContainerConfiguration(LoaderInterface $loader)
    {
        $loader->load(__DIR__.'/config/config_'.$this->getEnvironment().'.yml');
    }

}
