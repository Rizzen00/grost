Grost - Guild Hosting Service
===================

## What is Grost? ##

Grost is a hosted CMS made for online gaming communities.

Here is what users get when they create a guild:

 - Real-time forum system
 - Guild blog
 - Real-time event management system
 - Real-time guild chat
 - Customizable permission system
 - Custom subdomain

The users have total control over their guilds, and Grost gives them powerful administration tools to manage their members and the guild itself.

A single account can be used to create and join multiple guilds.

And here are some of the features that aren't tied to the guilds:

 - Real-time and multi-users messaging system
 - Friend list
 - Customizable profiles
 - Personal event calendar
 - A dashboard showing the recently viewed threads and guilds visited, and the upcoming events where the user is currently registered


Requirements
-------------

Grost is built in PHP on top of the Symfony2 framework.

####Databases:
 - Redis
 - PostgreSQL

####External service:

 - [Pusher](https://pusher.com/)

####Misc

 - ImageMagick
 - sassphp

----------

Config
-------------
    [Parameters.yml]
    domain: [grost.dev]
    static_url: ['https://static.grost.dev']
    pusher_app_id: [Your Pusher app id]
    pusher_key: [Your Pusher key]
    pusher_secret: [Your Pusher secret]
    
You will need to configure Redis in `config_prod.yml` and `config_dev.yml` too.


----------

Grost uses subdomains for the guilds, you need to make sure to use a wildcard so everything goes to the site.

    
